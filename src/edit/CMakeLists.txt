cmake_minimum_required(VERSION 3.8)

project(edit LANGUAGES CXX)

if(${CROSS_WIN})
  enable_language("RC")
  set(ICON_RESOURCE ${CMAKE_CURRENT_SOURCE_DIR}/simodoedit.rc)
endif()

# configure_file(${CMAKE_CURRENT_SOURCE_DIR}/src/MainWindow.cpp.in
#                ${CMAKE_CURRENT_SOURCE_DIR}/src/MainWindow.cpp)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(
  QT NAMES Qt6 Qt5 REQUIRED
  COMPONENTS Widgets
             Charts
             DataVisualization
             3DCore
             3DRender
             3DInput
             3DLogic
             3DExtras
             3DAnimation
             Svg)
find_package(
  Qt${QT_VERSION_MAJOR} REQUIRED
  COMPONENTS Widgets
             Charts
             DataVisualization
             3DCore
             3DRender
             3DInput
             3DLogic
             3DExtras
             3DAnimation
             Svg)

# if(NOT (${CROSS_WIN}))
#   set(TERM_WIDGET_CPP src/TermWidget.cpp)
#   set(TERM_WIDGET_H src/TermWidget.h)
# endif()

add_executable(
  ${PROJECT_NAME}
  ${ICON_RESOURCE}
  res/simgui.qrc
  src/BufferedListWidget.cpp
  src/BufferedListWidget.h
  src/ChartBuffer.cpp
  src/ChartBuffer.h
  src/ChartView.cpp
  src/ChartView.h
  src/cockpit/Cockpit3D.cpp
  src/cockpit/Cockpit3D.h
  src/cockpit/CockpitAviagorizont.h
  src/cockpit/CockpitBar.cpp
  src/cockpit/CockpitBar.h
  # src/cockpit/CockpitCBar.cpp
  # src/cockpit/CockpitCBar.h
  src/cockpit/CockpitHBar.h
  src/cockpit/CockpitVBar.h
  src/cockpit/CockpitVariometer.h
  src/cockpit/CockpitView.cpp
  src/cockpit/CockpitView.h
  src/codeeditor/CodeEditor.cpp
  src/codeeditor/CodeEditor.h
  src/codeeditor/CodeEditorColorTheme.h
  src/codeeditor/ErrorInfo.h
  src/codeeditor/Highlighter.cpp
  src/codeeditor/Highlighter.h
  src/codeeditor/LineNumberArea.cpp
  src/codeeditor/LineNumberArea.h
  src/codeeditor/RuleData.cpp
  src/codeeditor/RuleData.h
  src/codeeditor/SpellChecker.cpp
  src/codeeditor/SpellChecker.h
  src/codeeditor/TextBlockData.cpp
  src/codeeditor/TextBlockData.h
  src/CommonConfig.cpp
  src/CommonConfig.h
  src/FindReplace.cpp
  src/FindReplace.h
  src/FindReplace.ui
  src/GrammarTable.cpp
  src/GrammarTable.h
  src/Graph3D.cpp
  src/Graph3D.h
  src/Graph3DBuffer.cpp
  src/Graph3DBuffer.h
  src/ListReporter.cpp
  src/ListReporter.h
  src/ListReporterBuffer.cpp
  src/ListReporterBuffer.h
  src/main.cpp
  src/MainWindow.cpp
  src/MainWindow.h
  src/MdiChild.cpp
  src/MdiChild.h
  src/ScriptC_NS_Chart.cpp
  src/ScriptC_NS_Chart.h
  src/ScriptC_NS_Cockpit.cpp
  src/ScriptC_NS_Cockpit.h
  src/ScriptC_NS_Q3DScatter.cpp
  src/ScriptC_NS_Q3DScatter.h
  src/SimodoRuler.cpp
  src/SimodoRuler.h
  # ${TERM_WIDGET_CPP}
  # ${TERM_WIDGET_H}
  src/WorkDirectory.cpp
  src/WorkDirectory.h
  src/WorkDirectoryGetName.cpp
  src/WorkDirectoryGetName.h
  src/WorkDirectoryGetName.ui
  src/WorkObject.cpp
  src/WorkObject.h
  ts/simgui_ru_RU.ts)

target_include_directories(${PROJECT_NAME}
                           PUBLIC ${CMAKE_SOURCE_DIR}/src/thirdparts/hunspell)

target_link_libraries(${PROJECT_NAME} Qt${QT_VERSION_MAJOR}::Widgets)
target_link_libraries(${PROJECT_NAME} Qt${QT_VERSION_MAJOR}::Charts)
target_link_libraries(
  ${PROJECT_NAME}
  Qt${QT_VERSION_MAJOR}::DataVisualization
  Qt${QT_VERSION_MAJOR}::3DCore
  Qt${QT_VERSION_MAJOR}::3DRender
  Qt${QT_VERSION_MAJOR}::3DInput
  Qt${QT_VERSION_MAJOR}::3DLogic
  Qt${QT_VERSION_MAJOR}::3DExtras
  Qt${QT_VERSION_MAJOR}::3DAnimation
  Qt${QT_VERSION_MAJOR}::Svg)
target_link_libraries(${PROJECT_NAME} SIMODOdsl)
target_link_libraries(${PROJECT_NAME} hunspell)
# if(NOT (${CROSS_WIN}))
#   target_link_libraries(${PROJECT_NAME} "qtermwidget5")
# endif()
# target_link_libraries(${PROJECT_NAME} "stdc++fs")

set(PROJECT_OUTPUT_NAME "simodoedit")
set_target_properties(
  ${PROJECT_NAME}
  PROPERTIES MACOSX_BUNDLE_GUI_IDENTIFIER my.example.com
             MACOSX_BUNDLE_BUNDLE_VERSION ${SIMODO_VERSION}
             MACOSX_BUNDLE_SHORT_VERSION_STRING
             ${SIMODO_VERSION_MAJOR}.${SIMODO_VERSION_MINOR}
             MACOSX_BUNDLE TRUE
             WIN32_EXECUTABLE TRUE
             RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin
             OUTPUT_NAME ${PROJECT_OUTPUT_NAME})

set(RELEASE_STRIP $<IF:$<CONFIG:Release>,${CMAKE_STRIP},>)
if(NOT (${RELEASE_STRIP}) STREQUAL "" AND (${CROSS_WIN}))
  set(PON_EXT .exe)
endif()
set(RELEASE_STRIP_ARGS
    $<IF:$<CONFIG:Release>,"${CMAKE_SOURCE_DIR}/bin/${PROJECT_OUTPUT_NAME}${PON_EXT}",>
)
add_custom_command(
  TARGET ${PROJECT_NAME}
  POST_BUILD
  COMMAND ${RELEASE_STRIP} ARGS ${RELEASE_STRIP_ARGS})

set(CPACK_GENERATOR "IFW")

set(CPACK_PACKAGE_NAME ${PROJECT_OUTPUT_NAME})
set(CPACK_PACKAGE_VERSION ${SIMODO_VERSION})
set(CPACK_PACKAGE_RELEASE ${SIMODO_VERSION_BUILD})
set(CPACK_PACKAGE_CONTACT "Michael Fetisov")
set(CPACK_PACKAGE_VENDOR "BMSTU")
set(CPACK_PACKAGE_FILE_NAME
    "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}-${CPACK_PACKAGE_RELEASE}.${CMAKE_SYSTEM_PROCESSOR}"
)
set(CPACK_IFW_COMPONENT_INSTALL ON)

set(CPACK_IFW_PACKAGE_NAME "SIMODO edit")
set(CPACK_IFW_PACKAGE_START_MENU_DIRECTORY "Simodo")
set(CPACK_IFW_PACKAGE_TITLE "SIMODO edit Installer")
set(CPACK_IFW_PACKAGE_START_MENU_DIRECTORY "SIMODO")
set(CPACK_IFW_TARGET_DIRECTORY "@HomeDir@/Simodo/SimodoEdit")
set(CPACK_IFW_PACKAGE_ICON "${CMAKE_SOURCE_DIR}/installer/logo-01.ico")
set(CPACK_IFW_PACKAGE_WINDOW_ICON "${CMAKE_SOURCE_DIR}/installer/logo-01.png")
set(CPACK_IFW_PACKAGE_LOGO "${CMAKE_SOURCE_DIR}/installer/logo-01-128.png")
set(CPACK_IFW_PACKAGE_WIZARD_STYLE "Modern")
set(CPACK_IFW_PACKAGE_ALLOW_NON_ASCII_CHARACTERS ON)
set(CPACK_IFW_PRODUCT_URL "https://bmstu.codes/lsx/simodo/stars")
set(CPACK_IFW_ROOT "${CMAKE_SOURCE_DIR}/Qt/QtIFW")

include(CPackIFW)

cpack_ifw_configure_component(
  simodo-edit FORCED_INSTALLATION
  NAME simodo.edit
  DISPLAY_NAME "SIMODO edit"
  DESCRIPTION "Install SIMODO edit."
  SCRIPT ${CMAKE_SOURCE_DIR}/installer/installscript.qs
  LICENSES "MIT License" ${CMAKE_SOURCE_DIR}/installer/LICENSE.MIT
  DEFAULT true
  DEPENDS support)

cpack_ifw_configure_component(
  support FORCED_INSTALLATION VIRTUAL
  NAME support
  DEFAULT true)

if(${CROSS_WIN})
  install(
    PROGRAMS ${CMAKE_SOURCE_DIR}/bin/${PROJECT_OUTPUT_NAME}.exe
    DESTINATION bin
    COMPONENT simodo-edit)

  install(
    DIRECTORY ${CMAKE_SOURCE_DIR}/bin/
    DESTINATION bin
    COMPONENT simodo-edit
    PATTERN ${PROJECT_OUTPUT_NAME}.exe EXCLUDE)

else()
  install(
    CODE "
        execute_process(
            COMMAND rm -rf tmp/ifw-installer tmp/AppRun
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            COMMAND_ERROR_IS_FATAL ANY
        )
        execute_process(
            COMMAND mkdir -p tmp/ifw-installer/bin
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            COMMAND_ERROR_IS_FATAL ANY
        )
        execute_process(
            COMMAND cp bin/${PROJECT_OUTPUT_NAME} tmp/ifw-installer/bin/${PROJECT_OUTPUT_NAME}
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            COMMAND_ERROR_IS_FATAL ANY
        )
        # -extra-plugins=
        # renderers,geometryloaders // 3d rendering
        # platformthemes,platforminputcontexts // plasma integration
        # iconengines // svg
        # platforms // platforms?
        # xcbglintegrations // graphics?
        # styles // breeze
        # kf5,kf5/kio // open file dialog
        execute_process(
            COMMAND linuxdeployqt ${PROJECT_OUTPUT_NAME} -unsupported-allow-new-glibc -bundle-non-qt-libs -extra-plugins=renderers,geometryloaders,platformthemes,platforminputcontexts,iconengines,platforms,xcbglintegrations,styles,kf5,kf5/kio
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/tmp/ifw-installer/bin
            COMMAND_ERROR_IS_FATAL ANY
        )
        "
    COMPONENT simodo-edit)

  install(
    PROGRAMS ${CMAKE_SOURCE_DIR}/tmp/ifw-installer/bin/${PROJECT_OUTPUT_NAME}
    DESTINATION bin
    COMPONENT simodo-edit)

  install(
    DIRECTORY ${CMAKE_SOURCE_DIR}/tmp/ifw-installer/
    DESTINATION .
    COMPONENT simodo-edit
    PATTERN bin/${PROJECT_OUTPUT_NAME} EXCLUDE)
endif()

if(${CROSS_WIN})
  install(
    CODE "
        execute_process(
            COMMAND rm -rf tmp/data
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            COMMAND_ERROR_IS_FATAL ANY
        )
        execute_process(
            COMMAND mkdir -p tmp
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            COMMAND_ERROR_IS_FATAL ANY
        )
        execute_process(
            COMMAND cp -RL data tmp/data
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            COMMAND_ERROR_IS_FATAL ANY
        )
        "
    COMPONENT support)

  install(
    DIRECTORY "${CMAKE_SOURCE_DIR}/tmp/data"
    DESTINATION "."
    COMPONENT support)

else()
  install(
    DIRECTORY "${CMAKE_SOURCE_DIR}/data"
    DESTINATION "."
    COMPONENT support)
endif()

if(${CROSS_WIN})
  install(
    FILES "${CMAKE_SOURCE_DIR}/installer/icons/Simodo-simodoedit.ico"
    DESTINATION "icons"
    COMPONENT support)
else()
  install(
    DIRECTORY "${CMAKE_SOURCE_DIR}/installer/icons/"
    DESTINATION "icons"
    COMPONENT support
    PATTERN *.ico EXCLUDE)

endif()

install(
  DIRECTORY "${CMAKE_SOURCE_DIR}/test/examples"
  DESTINATION "."
  COMPONENT support)

include(CPack)
