#include "WorkDirectoryGetName.h"
#include "ui_WorkDirectoryGetName.h"

WorkDirectoryGetName::WorkDirectoryGetName(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WorkDirectoryGetName)
{
    ui->setupUi(this);
    ui->leText->setFocus();
}

WorkDirectoryGetName::~WorkDirectoryGetName()
{
    delete ui;
}

void WorkDirectoryGetName::setIntro(QString intro)
{
    ui->lbIntro->setText(intro);
}

QString WorkDirectoryGetName::getText() const
{
    return ui->leText->text();
}
