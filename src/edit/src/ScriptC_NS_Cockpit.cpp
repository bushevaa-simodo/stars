#include "ScriptC_NS_Cockpit.h"

#include "simodo/convert.h"
#include "simodo/dsl/ScriptC_Interpreter.h"
#include "simodo/dsl/Exception.h"

#include <cmath>
#include <string>

using namespace std;
using namespace simodo::dsl;

namespace
{
    namespace utils
    {
        double toDouble(const SCI_Name & n)
        {
            try {
                auto & sn = get<SCI_Scalar>(n.bulk);
                return (sn.type == SemanticNameType::Int) ? static_cast<double>(get<int64_t>(sn.variant)) : get<double>(sn.variant);
            } catch(std::exception & e)
            {}

            throw Exception("SCI_tf", "Неизвестный тип чисел: qualification=" + std::to_string(static_cast<int>(n.qualification)));
        };
    }
}

ScriptC_NS_Cockpit::ScriptC_NS_Cockpit(CockpitView * cockpit_view, Cockpit3D * cockpit3d)
    : _cockpit_view(cockpit_view)
    , _cockpit3d(cockpit3d)
{
    if (_cockpit_view == nullptr)
    {
        return;
    }

    connect(this, &ScriptC_NS_Cockpit::changeHeight,        _cockpit_view, &CockpitView::handleChangedHeight);
    connect(this, &ScriptC_NS_Cockpit::changeAngles,        _cockpit_view, &CockpitView::handleChangedAngles);
    connect(this, &ScriptC_NS_Cockpit::changeSpeed,         _cockpit_view, &CockpitView::handleChangedSpeed);
    connect(this, &ScriptC_NS_Cockpit::changeVerticalSpeedSlide, _cockpit_view, &CockpitView::handleChangedVerticalSpeedSlide);
    connect(_cockpit_view, &CockpitView::pressKey,   this, &ScriptC_NS_Cockpit::handlePressedKey,  Qt::DirectConnection);
    connect(_cockpit_view, &CockpitView::releaseKey, this, &ScriptC_NS_Cockpit::handleReleasedKey, Qt::DirectConnection);

    connect(this, &ScriptC_NS_Cockpit::changePosition, _cockpit3d, &Cockpit3D::setPosition);
    connect(this, &ScriptC_NS_Cockpit::changeAngles,   _cockpit3d, &Cockpit3D::setAngles);
}

ScriptC_NS_Cockpit::~ScriptC_NS_Cockpit()
{
    if (_cockpit_view == nullptr)
    {
        return;
    }

    disconnect(this, &ScriptC_NS_Cockpit::changeHeight,        _cockpit_view, &CockpitView::handleChangedHeight);
    disconnect(this, &ScriptC_NS_Cockpit::changeAngles,        _cockpit_view, &CockpitView::handleChangedAngles);
    disconnect(this, &ScriptC_NS_Cockpit::changeSpeed,         _cockpit_view, &CockpitView::handleChangedSpeed);
    disconnect(this, &ScriptC_NS_Cockpit::changeVerticalSpeedSlide, _cockpit_view, &CockpitView::handleChangedVerticalSpeedSlide);
    disconnect(_cockpit_view, &CockpitView::pressKey,   this, &ScriptC_NS_Cockpit::handlePressedKey);
    disconnect(_cockpit_view, &CockpitView::releaseKey, this, &ScriptC_NS_Cockpit::handleReleasedKey);

    disconnect(this, &ScriptC_NS_Cockpit::changePosition, _cockpit3d, &Cockpit3D::setPosition);
    disconnect(this, &ScriptC_NS_Cockpit::changeAngles,   _cockpit3d, &Cockpit3D::setAngles);
}

void ScriptC_NS_Cockpit::setPosition(double x, double height, double z)
{
    if (_cockpit_view == nullptr || _cockpit3d == nullptr)
        throw simodo::dsl::Exception("setPosition", "Неинициированно окружение");

    emit changeHeight(height);
    emit changePosition(x, height, z);
}

void ScriptC_NS_Cockpit::setAngles(double pitch, double course, double roll)
{
    if (_cockpit_view == nullptr || _cockpit3d == nullptr)
        throw simodo::dsl::Exception("setAngles", "Неинициированно окружение");

    emit changeAngles(std::clamp(pitch, -90.0, 90.0), std::clamp(course, 0.0, 360.0), std::clamp(roll, -180.0, 180.0));
}

void ScriptC_NS_Cockpit::setSpeed(double speed)
{
    if (_cockpit_view == nullptr || _cockpit3d == nullptr)
        throw simodo::dsl::Exception("setPosition", "Неинициированно окружение");

    emit changeSpeed(speed);
}

void ScriptC_NS_Cockpit::setVerticalSpeedSlide(double vertical_speed, double slide)
{
    if (_cockpit_view == nullptr || _cockpit3d == nullptr)
        throw simodo::dsl::Exception("setPosition", "Неинициированно окружение");

    emit changeVerticalSpeedSlide(vertical_speed, slide);
}

bool ScriptC_NS_Cockpit::getKeyState(std::string key)
{
    if (_key_states.find(key) == _key_states.end())
    {
        return false;
    }

    return _key_states[key].load();
}

void ScriptC_NS_Cockpit::handlePressedKey(std::string key)
{
    _key_states[key].store(true);
}

void ScriptC_NS_Cockpit::handleReleasedKey(std::string key)
{
    _key_states[key].store(false);
}

namespace
{
    bool setPosition(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 3);

        auto x = ::utils::toDouble(stack.top(2));
        auto height = ::utils::toDouble(stack.top(1));
        auto z = ::utils::toDouble(stack.top(0));

        ScriptC_NS_Cockpit * cockpit = static_cast<ScriptC_NS_Cockpit *>(p_object);

        cockpit->setPosition(x, height, z);

        stack.pop(3);

        return true;
    }

    bool setAngles(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 3);

        auto pitch = ::utils::toDouble(stack.top(2));
        auto course = ::utils::toDouble(stack.top(1));
        auto roll = ::utils::toDouble(stack.top(0));

        ScriptC_NS_Cockpit * cockpit = static_cast<ScriptC_NS_Cockpit *>(p_object);

        cockpit->setAngles(pitch, course, roll);

        stack.pop(3);

        return true;
    }

    bool setSpeed(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 1);

        auto speed = ::utils::toDouble(stack.top(0));

        ScriptC_NS_Cockpit * cockpit = static_cast<ScriptC_NS_Cockpit *>(p_object);

        cockpit->setSpeed(speed);

        stack.pop(1);

        return true;
    }

    bool setVerticalSpeedSlide(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 2);

        auto vertical_speed = ::utils::toDouble(stack.top(1));
        auto slide = ::utils::toDouble(stack.top(0));

        ScriptC_NS_Cockpit * cockpit = static_cast<ScriptC_NS_Cockpit *>(p_object);

        cockpit->setVerticalSpeedSlide(vertical_speed, slide);

        stack.pop(2);

        return true;
    }

    bool getKeyState(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 1);


        auto * key_obj = &stack.top(0);
        
        while(key_obj->qualification == SemanticNameQualification::Reference)
            key_obj = get<SCI_Reference>(key_obj->bulk);
        
        std::u16string key = get<std::u16string>(get<SCI_Scalar>(stack.top(0).bulk).variant);

        auto cockpit = static_cast<ScriptC_NS_Cockpit *>(p_object);

        auto state = cockpit->getKeyState(simodo::convertToU8(key));

        stack.pop(1);
        stack.push({
            u"",
            SemanticNameQualification::Scalar,
            SCI_Scalar {SemanticNameType::Bool, state}
        });

        return true;
    }
}

simodo::dsl::SCI_Namespace_t ScriptC_NS_Cockpit::getNamespace()
{
    return {
        {u"setPosition", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setPosition}}},
            {u"", SemanticNameQualification::None, {}},
            {u"x", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"height", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"z", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"setAngles", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setAngles}}},
            {u"", SemanticNameQualification::None, {}},
            {u"pitch", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"course", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"roll", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"setSpeed", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setSpeed}}},
            {u"", SemanticNameQualification::None, {}},
            {u"speed", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"setVerticalSpeedAndSlide", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setVerticalSpeedSlide}}},
            {u"", SemanticNameQualification::None, {}},
            {u"vertical_speed", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"slide", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"getKeyState", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::getKeyState}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Bool, {}}},
            {u"key", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
    };
}
