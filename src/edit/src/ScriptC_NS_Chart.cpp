#include "ScriptC_NS_Chart.h"

#include "simodo/dsl/ScriptC_Interpreter.h"
#include "simodo/dsl/Exception.h"

using namespace std;
using namespace simodo::dsl;

ScriptC_NS_Chart::ScriptC_NS_Chart(QDockWidget * chart_dock, ChartView * chart_view)
    : _chart_dock(chart_dock)
    , _chart_view(chart_view)
{
    if (_chart_view == nullptr)
        return;

    connect(this, &ScriptC_NS_Chart::doInitialize, _chart_view, &ChartView::handleInitialization);
    connect(this, &ScriptC_NS_Chart::doAddSeries, _chart_view, &ChartView::handleAddSeries);
    // connect(this, &ScriptC_NS_Chart::doAddPoint, _chart_view, &ChartView::handleAddPoint);
    // connect(this, &ScriptC_NS_Chart::doShow, _chart_view, &ChartView::show);
}

ScriptC_NS_Chart::~ScriptC_NS_Chart()
{
    if (_chart_view == nullptr)
        return;

    if (!_is_showed)
        show();

    disconnect(this, &ScriptC_NS_Chart::doInitialize, _chart_view, &ChartView::handleInitialization);
    disconnect(this, &ScriptC_NS_Chart::doAddSeries, _chart_view, &ChartView::handleAddSeries);
    // disconnect(this, &ScriptC_NS_Chart::doAddPoint, _chart_view, &ChartView::handleAddPoint);
    // disconnect(this, &ScriptC_NS_Chart::doShow, _chart_view, &ChartView::show);
}

void ScriptC_NS_Chart::init(u16string title)
{
    if (_chart_dock == nullptr || _chart_view == nullptr)
        throw simodo::dsl::Exception("init", "Неинициированно окружение");

    emit doInitialize(QString::fromStdU16String(title));
}

void ScriptC_NS_Chart::addSeries(u16string serias_name, int64_t series_style)
{
    if (_chart_dock == nullptr || _chart_view == nullptr || _chart_view->chart() == nullptr)
        throw simodo::dsl::Exception("addSeries", "Неинициированно окружение");

    emit doAddSeries(QString::fromStdU16String(serias_name), static_cast<int>(series_style));
}

void ScriptC_NS_Chart::addPoint(u16string serias_name, double x, double y)
{
    if (_chart_dock == nullptr || _chart_view == nullptr || _chart_view->chart() == nullptr)
        throw simodo::dsl::Exception("addData", "Неинициированно окружение");

    _is_showed = false;

    emit doAddPoint({QString::fromStdU16String(serias_name), {x, y}});
}

void ScriptC_NS_Chart::show()
{
    _is_showed = true;

    emit doShow();
}

namespace
{
    bool init(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(!stack.empty());

        u16string title = get<u16string>(get<SCI_Scalar>(stack.top().bulk).variant);

        ScriptC_NS_Chart * chart = static_cast<ScriptC_NS_Chart *>(p_object);

        chart->init(title);

        stack.pop();

        return true;
    }

    bool addSeries(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 2);

        u16string serias_name  = get<u16string>(get<SCI_Scalar>(stack.top(1).bulk).variant);
        int64_t   serias_style = get<int64_t>(get<SCI_Scalar>(stack.top(0).bulk).variant);

        ScriptC_NS_Chart * chart = static_cast<ScriptC_NS_Chart *>(p_object);

        chart->addSeries(serias_name, serias_style);

        stack.pop(2);

        return true;
    }

    bool addPoint(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 3);

        u16string serias_name = get<u16string>(get<SCI_Scalar>(stack.top(2).bulk).variant);

        const SCI_Name &x_value = stack.top(1);
        double x = get<double>(get<SCI_Scalar>(x_value.bulk).variant);

        const SCI_Name &y_value = stack.top(0);
        double y = get<double>(get<SCI_Scalar>(y_value.bulk).variant);

        ScriptC_NS_Chart * chart = static_cast<ScriptC_NS_Chart *>(p_object);

        chart->addPoint(serias_name, x, y);

        stack.pop(3);

        return true;
    }

    bool show(IScriptC_Namespace * p_object, SCI_Stack &)
    {
        if (p_object == nullptr)
            return false;

        ScriptC_NS_Chart * chart = static_cast<ScriptC_NS_Chart *>(p_object);

        chart->show();

        return true;
    }

}

simodo::dsl::SCI_Namespace_t ScriptC_NS_Chart::getNamespace()
{
    return {
        {u"init", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::init}}},
            {u"", SemanticNameQualification::None, {}},
            {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"addSeries", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addSeries}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"series_style", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"addPoint", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addPoint}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"show", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::show}}},
            {u"", SemanticNameQualification::None, {}},
        }},
        {u"Style", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"Line", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"Spline", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(1)}},
            {u"Scatter", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(2)}},
        }},
    };
}
