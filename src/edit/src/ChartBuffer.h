#ifndef CHARTBUFFER_H
#define CHARTBUFFER_H

#include <QObject>
#include <QTimerEvent>
#include <QListWidgetItem>
#include <deque>

using ChartPoint = std::pair<QString, QPointF>;
using ChartPointVector = std::vector<ChartPoint>;

class ChartBuffer :  public QObject
{
    Q_OBJECT

    std::deque<ChartPoint> _buffer;
    int  _timer_id;
    long _timer_start_time;

    void _killTimer();

public:
    // 1 / 60Hz = 0.01(6)s
    static constexpr int TIMER_INTERVAL = 16;

    ChartBuffer(QObject * parent = nullptr);
    ~ChartBuffer();

    bool empty() { return _buffer.empty(); }
    size_t size() { return _buffer.size(); }

signals:
    void addBufferedItems(ChartPointVector items);

public slots:
    void addItem(ChartPoint item);
    void startBufferTimer();

protected:
    void timerEvent(QTimerEvent * event) override;
};

#endif // CHARTBUFFER_H
