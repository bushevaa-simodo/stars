#ifndef COCKPITVIEW_H
#define COCKPITVIEW_H

#include "CockpitAviagorizont.h"
#include "CockpitVariometer.h"
#include "Cockpit3D.h"

#include <QtWidgets/QDockWidget>
#include <QtWidgets/QWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include <QKeyEvent>
#include <QMouseEvent>
#include <unordered_map>

class ScriptC_NS_Cockpit;

class CockpitView : public QWidget
{
    Q_OBJECT

    CockpitBar<qreal> * _left_bar;
    CockpitBar<qreal> * _top_bar;
    CockpitBar<qreal> * _right_bar;
    CockpitAviagorizont<qreal> * _aviagorizont;
    CockpitVariometer<qreal> * _variometer;
    // CockpitBar * _bottom_left_bar;
    // CockpitBar * _bottom_right_bar;

    // QVBoxLayout * _vbox_layout;
    // QHBoxLayout * _hbox_layout;
    // QHBoxLayout * _bhbox_layout;

    std::unordered_map<std::string, bool> _key_states;

protected:
    void mousePressEvent(QMouseEvent * event) override;

    void keyPressEvent(QKeyEvent * event) override;
    void keyReleaseEvent(QKeyEvent * event) override;

public:
    CockpitView(QWidget * parent = nullptr);

signals:
    void pressKey(std::string key);
    void releaseKey(std::string key);

public slots:
    void handleChangedHeight(qreal height);

    // Вид из кабины вперёд
    // Тангаж -90..+90, > 0 подъём носа вверх
    // Курс 0..360, > 0 поворот вправо
    // Крен -180..+180, > 0 крен на правое крыло
    void handleChangedAngles(qreal pitch, qreal course, qreal roll);

    void handleChangedSpeed(qreal speed);
    void handleChangedVerticalSpeedSlide(qreal vertical_speed, qreal slide);
};

#endif // COCKPITVIEW_H
