#ifndef SCRIPTC_NS_CHART_H
#define SCRIPTC_NS_CHART_H

#include "simodo/dsl/SemanticBase.h"
#include "ChartView.h"
#include <QObject>
#include <vector>

using ChartPoint = std::pair<QString, QPointF>;

class ScriptC_NS_Chart : public QObject, public simodo::dsl::IScriptC_Namespace
{
    Q_OBJECT

    QDockWidget *   _chart_dock;
    ChartView *     _chart_view;

    bool            _is_showed = false;

public:
    ScriptC_NS_Chart(QDockWidget * chart_dock=nullptr, ChartView * chart_view=nullptr);
    virtual ~ScriptC_NS_Chart();

signals:
    void doInitialize(QString title);
    void doAddSeries(QString series_name, int series_style);
    void doAddPoint(ChartPoint chart_point);
    void doShow();

public:
    void init(std::u16string title);
    void addSeries(std::u16string serias_name, int64_t series_style);
    void addPoint(std::u16string serias_name, double x, double y);
    void show();

public:
    virtual simodo::dsl::SCI_Namespace_t getNamespace() override;

};

#endif // SCRIPTC_NS_CHART_H
