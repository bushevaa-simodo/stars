#include "ListReporter.h"
#include "MdiChild.h"

#include "codeeditor/ErrorInfo.h"

#include "simodo/convert.h"

#include <QJsonObject>

ListReporter::ListReporter(QObject * parent, MdiChild * editor, bool need_icons)
    : QObject(parent)
    , editor(editor)
    , need_icons(need_icons)
{}

ListReporter::ListReporter(MdiChild * editor, bool need_icons)
    : ListReporter(nullptr, editor, need_icons)
{}

static QVariant convertToVariant(simodo::dsl::TokenLocation loc)
{
    QJsonObject obj
    {
        { "file",   simodo::convertToU8(loc.file_name).c_str() },
        { "line",   static_cast<int>(loc.line) },
        { "column", static_cast<int>(loc.column) },
        { "column_tabulated", static_cast<int>(loc.column_tabulated) },
        { "begin",  static_cast<int>(loc.begin) },
        { "end",    static_cast<int>(loc.end) },
        { "context",static_cast<int>(loc.context) },
    };

    return obj;
}

void ListReporter::report(const simodo::dsl::SeverityLevel level,
                          simodo::dsl::TokenLocation loc,
                          const std::u16string &briefly,
                          const std::u16string &atlarge)
{
    QListWidgetItem * item = nullptr;
    QString           text (simodo::convertToU8(getSeverityLevelName(level) + briefly).c_str());

    if (!atlarge.empty())
    {
        text += "\n" + QString(simodo::convertToU8(atlarge).c_str());
    }

    if (need_icons)
    {
        QString level_str;

        switch(level)
        {
        case simodo::dsl::SeverityLevel::Information:
            level_str = "information";
            break;

        case simodo::dsl::SeverityLevel::Warning:
            level_str = "warning";
            break;

        default:
            level_str = "error";
            break;
        }

        const QIcon icon = QIcon::fromTheme("dialog-" + level_str, QIcon(":/images/dialog-" + level_str));
        item = new QListWidgetItem(icon, text);
        item->setData(Qt::UserRole, convertToVariant(loc));

    } else {
        item = new QListWidgetItem(text);
    }

    if (editor != nullptr)
    {
        auto & error_info = editor->error_info();
        error_info.insert(int(loc.line) - 1,
                          {
                              level,
                              loc,
                              simodo::convertToU8(briefly).c_str(),
                              simodo::convertToU8(atlarge).c_str()
                          });
    }

    emit addItem(item);
}
