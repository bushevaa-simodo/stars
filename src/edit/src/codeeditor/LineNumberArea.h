#ifndef LINENUMBERAREA_H
#define LINENUMBERAREA_H

#include <QWidget>

class CodeEditor;

class LineNumberArea : public QWidget
{
    Q_OBJECT

public:
    LineNumberArea(CodeEditor *editor);

    int getAreaWidth() const;
    int getMessageAreaWidth() const;
    int getNumberAreaWidth() const;
    int getHidingAreaWidth() const;

    QSize sizeHint() const override;

protected:
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void mouseReleaseEvent(QMouseEvent *e) override;
    virtual void mouseMoveEvent(QMouseEvent *e) override;
    virtual bool event(QEvent *event) override;

private:
    CodeEditor * _editor;
};

#endif // LINENUMBERAREA_H
