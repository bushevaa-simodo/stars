#ifndef HIGHLIGHTER_H
#define HIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QTextCharFormat>

#include "simodo/dsl/Tokenizer.h"


QT_BEGIN_NAMESPACE
class QTextDocument;
QT_END_NAMESPACE

class CodeEditor;

class Highlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    Highlighter(CodeEditor *editor, simodo::dsl::LexicalParameters lexis);

protected:
    void highlightBlock(const QString &text) override;

    QTextCharFormat setIdFormat(const simodo::dsl::Token & t
                    , int position
                    , const QTextCharFormat & default_format
                    );

    void setTokenFormat(const simodo::dsl::Token & token
                        , const QTextCharFormat & format
                        );

    std::vector<simodo::dsl::Token> checkSpell(const simodo::dsl::Token &t
                                                , const QTextCharFormat & default_format
                                                );
                                                
    std::vector<simodo::dsl::Token> produceSpellChecking(const simodo::dsl::Token & token
                                                        , simodo::dsl::LexemeType type
                                                        );

private:
    CodeEditor *                    _editor;
    simodo::dsl::LexicalParameters  _lexical_parameters;
};

#endif // HIGHLIGHTER_H
