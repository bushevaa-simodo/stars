#ifndef CODEEDITOR_H
#define CODEEDITOR_H

#include "CodeEditorColorTheme.h"
#include "LineNumberArea.h"
#include "ErrorInfo.h"
#include "RuleData.h"
#include "SpellChecker.h"

#include "simodo/dsl/Tokenizer.h"
#include "simodo/dsl/ScriptC_Semantics.h"

#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QCompleter>

QT_BEGIN_NAMESPACE
class QPaintEvent;
class QResizeEvent;
class QSize;
class QWidget;
class QStringListModel;
QT_END_NAMESPACE

struct CodeEditorParameters
{
    int     tab_size;
    bool    scope_viewing;
    bool    completion;
    bool    is_tab_replacement_necessary;
    bool    is_autoindenting_necessary;
    bool    spell_checking_english_required;
    bool    spell_checking_national_required;
    bool    id_spell_checking_requared;
    bool    highlight_semantics;
};

class LineNumberArea;
class Highlighter;

class CodeEditor : public QPlainTextEdit
{
    friend class LineNumberArea;
    friend class Highlighter;

    Q_OBJECT

public:
    CodeEditor(CodeEditorColorTheme ct,
               CodeEditorParameters params,
               SpellChecker &checker,
               const QMultiMap<int,ErrorInfo> & error_info);
    virtual ~CodeEditor() override;

    int lineNumberAreaWidth() const { return _line_number_area->getAreaWidth(); }

    const RuleData * rule_data() const { return _rule_data; }

    void setRuleData(RuleData * rule_data);

    void updateTextStructure();

    void setColorTheme(CodeEditorColorTheme ct);

    void updateSemanticInfo();

    void rehighlight();

    CodeEditorParameters parameters() const { return _parameters; }
    void setParameters(CodeEditorParameters params);

    QString getErrorText(int lineNumber, int position=-1);

//    QString getTokenInfo(const QTextBlock block, int pos);

    /*!
     * \brief Метод проверяет нахождение внутри перерисовки подсветки синтаксиса
     * \return Признак нахождения внутри перерисовки подсветки синтаксиса
     *
     * Сигнал QTextDocument::contentsChanged вызывается не только при изменении текста документа,
     * но при работе highlighter. Поэтому, чтобы не возникало зацикливаний, необходимо проверять,
     * не инициирован ли сигнал из метода rehighlight.
     *
     * Метод используется в колбек-вызовах.
     */
    bool isRehighlightActive() const { return _is_rehighlight_active; }

    size_t findValiableIndexByName(const QString & name, int position, size_t token_index, const std::vector<simodo::dsl::Token> & tokens);

public slots:
    void performBlockShiftRigth();
    void performBlockShiftLeft();
    void handleCommenting();
    void performTabReplacement();

protected:
    virtual void contextMenuEvent(QContextMenuEvent *e) override;
    virtual void resizeEvent(QResizeEvent *e) override;
    virtual void keyPressEvent(QKeyEvent *e) override;
    virtual void focusInEvent(QFocusEvent *e) override;
    virtual bool event(QEvent *event) override;

protected:
    virtual const QString currentFileName() const = 0;
    virtual std::vector<simodo::dsl::SemanticName> getNameSet() = 0;
    virtual std::vector<simodo::dsl::SemanticScope> getScopeSet() = 0;

protected:
    enum class ShiftDirection
    {
        Left,
        Right
    };

    int  shiftRight(QTextCursor & cursor, int end_selection);
    int  shiftLeft(QTextCursor & cursor, int end_selection);
    void performBlockShift(ShiftDirection direction);
    void performAutoindent();
    void performTab();
    int performTabReplacement(QTextCursor & cursor);
    bool isTabReplacementNecessary();

private slots:
    void updateLineNumberAreaWidth(int newBlockCount);
    void highlightCurrentLine();
    void updateLineNumberArea(const QRect &, int);
    void insertCompletion(const QString &completion);

private:
    enum class CompletionContext
    {
        Tuple,
        NoContext
    };

    void fillCompletionModel();
    QString textUnderCursor() const;

    void showCompliterPopup();

    void setRehighlightActive(bool is_rehighlight_active) { _is_rehighlight_active = is_rehighlight_active; }

private:
    LineNumberArea *                    _line_number_area;
    CodeEditorColorTheme                _color_theme;
    CodeEditorParameters                _parameters;
    RuleData *                          _rule_data;
    const QMultiMap<int, ErrorInfo> &   _error_info;
    Highlighter *                       _highlighter;
    std::vector<simodo::dsl::SemanticName> _name_set;
    std::vector<simodo::dsl::SemanticScope> _scope_set;
//    QMultiMap<std::u16string, size_t>    _name_map;
    QStringListModel *                  _c_model;
    QCompleter *                        _c;
    CompletionContext                   _context;
    SpellChecker &                      _checker;
    bool                                _is_rehighlight_active;
};

#endif // CODEEDITOR_H
