#include <algorithm>

#include <QStringList>

#include "SpellChecker.h"

#include "simodo/convert.h"

#include "hunspell.hxx"

const std::u16string  ALPHABET    = u"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZабвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
const std::u16string  LATIN_ALPHABET    = u"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
const std::u16string  NATIONAL_ALPHABET = u"абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

SpellChecker::SpellChecker()
    : _latin_hunspell(nullptr)
    , _national_hunspell(nullptr)
{
}

SpellChecker::~SpellChecker()
{
    if (_latin_hunspell != nullptr)
        delete _latin_hunspell;

    if (_national_hunspell != nullptr)
        delete _national_hunspell;
}

bool SpellChecker::checkLatin(const std::u16string &word)
{
    if (_latin_hunspell == nullptr)
        return true;

//    bool ok;
//    if (_encoding == "KOI8-R")
//        ok = _latin_hunspell->spell(simodo::convertToU8(word));
//    else
//        ok = true;
//    return ok;

    return _latin_hunspell->spell(simodo::convertToU8(word));
}

bool SpellChecker::checkNational(const std::u16string &word)
{
    if (_national_hunspell == nullptr)
        return true;

    return _national_hunspell->spell(convertToKoi8R(word));
}

QStringList SpellChecker::suggest(const std::u16string &word)
{
    QStringList res;

    if (word.empty())
        return res;

    if (std::string::npos != LATIN_ALPHABET.find(word[0]))
    {
        if (_latin_hunspell == nullptr)
            return res;

        std::vector<std::string> spell_suggest = _latin_hunspell->suggest(simodo::convertToU8(word));

        for(auto w : spell_suggest)
            res.push_back(QString::fromStdString(w));

        return res;
    }

    if (_national_hunspell == nullptr)
        return res;

    std::vector<std::string> spell_suggest = _national_hunspell->suggest(convertToKoi8R(word));

    for(auto w : spell_suggest)
        res.push_back(QString::fromStdU16String(convertFromKoi8R(w)));

    return res;
}

void SpellChecker::add(const std::u16string &word)
{
    if (word.empty())
        return;

    if (std::string::npos != LATIN_ALPHABET.find(word[0]))
    {
        if (_latin_hunspell != nullptr)
            _latin_hunspell->add(simodo::convertToU8(word));

        return;
    }

    if (_national_hunspell != nullptr)
        _national_hunspell->add(convertToKoi8R(word));
}

void SpellChecker::remove(const std::u16string &word)
{
    if (word.empty())
        return;

    if (std::string::npos != LATIN_ALPHABET.find(word[0]))
    {
        if (_latin_hunspell != nullptr)
            _latin_hunspell->remove(simodo::convertToU8(word));

        return;
    }

    if (_national_hunspell != nullptr)
        _national_hunspell->remove(convertToKoi8R(word));
}

const std::u16string &SpellChecker::alphabet() const
{
    return ALPHABET;
}

const std::u16string &SpellChecker::latin_alphabet() const
{
    return LATIN_ALPHABET;
}

const std::u16string &SpellChecker::national_alphabet() const
{
    return NATIONAL_ALPHABET;
}

std::string SpellChecker::convertToKoi8R(const std::u16string &word) const
{
    std::string ret;
    ret.reserve(word.size());

    for(auto ch16 : word)
    {
        auto it = std::find_if(_u8_koi8R.begin(), _u8_koi8R.end(),
                               [ch16](const std::pair<char16_t,char> &p){return (p.first == ch16);});

        char ch = (it == _u8_koi8R.end()) ? '?' : it->second;
        ret += ch;
    }

    return ret;
}

std::u16string SpellChecker::convertFromKoi8R(const std::string &word) const
{
    std::u16string ret;
    ret.reserve(word.size());

    for(auto ch : word)
    {
        auto it = std::find_if(_u8_koi8R.begin(), _u8_koi8R.end(),
                               [ch](const std::pair<char16_t,char> &p){return (p.second == ch);});

        char16_t ch16 = (it == _u8_koi8R.end()) ? ch : it->first;
        ret += ch16;
    }

    return ret;
}

bool SpellChecker::reopenDictionaries(QString gict_path, QString english_dict, QString national_dict, QString encoding)
{
    _encoding = encoding;

    if (_latin_hunspell != nullptr)
        delete _latin_hunspell;

    if (!gict_path.isEmpty() && !english_dict.isEmpty())
    {
        QString aff_path = gict_path+"/" + english_dict + ".aff";
        QString dic_path = gict_path+"/" + english_dict + ".dic";

        _latin_hunspell = new Hunspell(aff_path.toStdString().c_str(), dic_path.toStdString().c_str());
    }

    if (_national_hunspell != nullptr)
        delete _national_hunspell;

    if (!gict_path.isEmpty() && !national_dict.isEmpty())
    {
        QString aff_path = gict_path+"/" + national_dict + ".aff";
        QString dic_path = gict_path+"/" + national_dict + ".dic";

        _national_hunspell = new Hunspell(aff_path.toStdString().c_str(), dic_path.toStdString().c_str());
    }

    _u8_koi8R =
    {
        { u'а', 0xC1 },
        { u'б', 0xC2 },
        { u'в', 0xD7 },
        { u'г', 0xC7 },
        { u'д', 0xC4 },
        { u'е', 0xC5 },
        { u'ё', 0xA3 },
        { u'ж', 0xD6 },
        { u'з', 0xDA },
        { u'и', 0xC9 },
        { u'й', 0xCA },
        { u'к', 0xCB },
        { u'л', 0xCC },
        { u'м', 0xCD },
        { u'н', 0xCE },
        { u'о', 0xCF },
        { u'п', 0xD0 },
        { u'р', 0xD2 },
        { u'с', 0xD3 },
        { u'т', 0xD4 },
        { u'у', 0xD5 },
        { u'ф', 0xC6 },
        { u'х', 0xC8 },
        { u'ц', 0xC3 },
        { u'ч', 0xDE },
        { u'ш', 0xDB },
        { u'щ', 0xDD },
        { u'ъ', 0xDF },
        { u'ы', 0xD9 },
        { u'ь', 0xD8 },
        { u'э', 0xDC },
        { u'ю', 0xC0 },
        { u'я', 0xD1 },
        { u'А', 0xE1 },
        { u'Б', 0xE2 },
        { u'В', 0xF7 },
        { u'Г', 0xE7 },
        { u'Д', 0xE4 },
        { u'Е', 0xE5 },
        { u'Ё', 0xB3 },
        { u'Ж', 0xF6 },
        { u'З', 0xFA },
        { u'И', 0xE9 },
        { u'Й', 0xEA },
        { u'К', 0xEB },
        { u'Л', 0xEC },
        { u'М', 0xED },
        { u'Н', 0xEE },
        { u'О', 0xEF },
        { u'П', 0xF0 },
        { u'Р', 0xF2 },
        { u'С', 0xF3 },
        { u'Т', 0xF4 },
        { u'У', 0xF5 },
        { u'Ф', 0xE6 },
        { u'Х', 0xE8 },
        { u'Ц', 0xE3 },
        { u'Ч', 0xFE },
        { u'Ш', 0xFB },
        { u'Щ', 0xFD },
        { u'Ъ', 0xFF },
        { u'Ы', 0xF9 },
        { u'Ь', 0xF8 },
        { u'Э', 0xFC },
        { u'Ю', 0xE0 },
        { u'Я', 0xF1 }
    };

    return true;
}

