#ifndef SPELLCHECKER_H
#define SPELLCHECKER_H

#include <string>
#include <vector>

#include <QString>

class Hunspell;

class SpellChecker
{
    Hunspell *                              _latin_hunspell;
    Hunspell *                              _national_hunspell;
    QString                                 _encoding;
    std::vector<std::pair<char16_t,char>>   _u8_koi8R;

public:
    SpellChecker();
    ~SpellChecker();

    bool reopenDictionaries(QString gict_path, QString english_dict, QString national_dict, QString encoding);

    bool checkLatin(const std::u16string & word);
    bool checkNational(const std::u16string & word);
    QStringList suggest(const std::u16string& word);
    void add(const std::u16string& word);
    void remove(const std::u16string& word);

    const std::u16string & alphabet() const;
    const std::u16string & latin_alphabet() const;
    const std::u16string & national_alphabet() const;

private:
    std::string convertToKoi8R(const std::u16string & word) const;
    std::u16string convertFromKoi8R(const std::string & word) const;
};

#endif // SPELLCHECKER_H
