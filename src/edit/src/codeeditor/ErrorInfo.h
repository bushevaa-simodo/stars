#ifndef ERRORINFO_H
#define ERRORINFO_H

#include <QString>

#include "simodo/dsl/AReporter.h"

struct ErrorInfo
{
    simodo::dsl::SeverityLevel level;
    simodo::dsl::TokenLocation loc;
    QString                    briefly;
    QString                    atlarge;
};

#endif // ERRORINFO_H
