#ifndef CHARTVIEW_H
#define CHARTVIEW_H

#include "MainWindow.h"
#include <QChartView>

using ChartPoint = std::pair<QString, QPointF>;
using ChartPointVector = std::vector<ChartPoint>;

class ChartView : public QtCharts::QChartView
{
    Q_OBJECT

    MainWindow *    _main_window;
    QDockWidget *   _chart_dock;

//    bool            _is_ready;

public:
    enum SeriesStype
    {
        Line = 0,
        Spline = 1,
        Scatter = 2
    };

    ChartView(MainWindow * main_window, QDockWidget * parent = nullptr);

    virtual void contextMenuEvent(QContextMenuEvent *event) override;

public slots:
    void handleInitialization(QString title);
    void handleAddSeries(QString series_name, int series_style);
    void handleAddPoint(ChartPointVector points);
    void show();

private slots:
    void setColorTheme();
    void setLegent();

private:
    std::map<QString,QPair<SeriesStype,QList<QPointF>>> _serieses;
};

#endif // CHARTVIEW_H
