#ifndef TERMWIDGET_H
#define TERMWIDGET_H

#include <qtermwidget5/qtermwidget.h>

class MainWindow;
class QDockWidget;

class TermWidget : public QTermWidget
{
    Q_OBJECT

    int             _term_no;
    MainWindow *    _main_window;
    QDockWidget *   _parent_doc_widget;

#ifndef QT_NO_CLIPBOARD
    QAction *       _act_copy;
    QAction *       _act_paste;
#endif
    QAction *       _act_find;

    bool            _is_shell_started;
    bool            _is_terminate;

public:
    explicit TermWidget(int term_no, MainWindow * main_window, QDockWidget *parent);
    virtual ~TermWidget();

    bool isShellStarted() const { return _is_shell_started; }

    QDockWidget *   parent_doc_widget() const { return _parent_doc_widget; }

    bool runCommand(QString command, bool auto_enter=true);

public slots:
    void visibilityChanged(bool visible);

protected:
    virtual void contextMenuEvent(QContextMenuEvent *event) override;

    bool startShell();

private slots:
    void setColorTheme();
    void restart();
};

#endif // TERMWIDGET_H
