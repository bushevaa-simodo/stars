#include <QApplication>
#include <QMenu>
#include <QClipboard>

#include "TermWidget.h"
#include "MainWindow.h"

TermWidget::TermWidget(int term_no, MainWindow *main_window, QDockWidget *parent)
    : QTermWidget(0,parent)
    , _term_no(term_no)
    , _main_window(main_window)
    , _parent_doc_widget(parent)
    , _is_shell_started(false)
    , _is_terminate(false)
{
#ifndef QT_NO_CLIPBOARD
    const QIcon copyIcon = QIcon::fromTheme("edit-copy", QIcon(":/images/edit-copy"));
    _act_copy = new QAction(copyIcon, tr("Copy"), this);
    _act_copy->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_C));
    connect(_act_copy, &QAction::triggered, this, &QTermWidget::copyClipboard);
    addAction(_act_copy);

    const QIcon pasteIcon = QIcon::fromTheme("edit-paste", QIcon(":/images/edit-paste"));
    _act_paste = new QAction(pasteIcon, tr("Paste"), this);
    _act_paste->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_V));
    connect(_act_paste, &QAction::triggered, this, &QTermWidget::pasteClipboard);
    addAction(_act_paste);
#endif

    const QIcon findIcon = QIcon::fromTheme("edit-find", QIcon(":/images/edit-find"));
    _act_find = new QAction(findIcon, tr("Find..."), this);
    _act_find->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_F));
    connect(_act_find, &QAction::triggered, this, &QTermWidget::toggleShowSearchBar);
    addAction(_act_find);


    QFont font = QApplication::font();
#ifdef Q_OS_MACOS
    font.setFamily(QStringLiteral("Monaco"));
#elif defined(Q_WS_QWS)
    font.setFamily(QStringLiteral("fixed"));
#else
    font.setFamily(QStringLiteral("Monospace"));
#endif
    font.setPointSize(10);

    setTerminalFont(font);

#ifdef Q_OS_MACOS
    console->setKeyBindings("macbook");
#elif defined(Q_OS_LINUX)
    setKeyBindings("linux");
#else
    setKeyBindings("default");
#endif

    connect(this, &QTermWidget::finished, this, &TermWidget::restart);
}

TermWidget::~TermWidget()
{
    _is_terminate = true;

//    if (_is_shell_started)
//        sendText("exit");
}

bool TermWidget::runCommand(QString command, bool auto_enter)
{
    setVisible(true);

    sendText(command);

    if (auto_enter)
        sendText("\r");

    return true;
}

void TermWidget::visibilityChanged(bool visible)
{
    if (visible)
    {
        if (!_is_shell_started)
            startShell();

        _parent_doc_widget->raise();
    }
}

void TermWidget::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);

#ifndef QT_NO_CLIPBOARD
    _act_copy->setEnabled(!selectedText().isEmpty());
    _act_paste->setEnabled(!QApplication::clipboard()->text().isEmpty());

    menu.addAction(_act_copy);
    menu.addAction(_act_paste);
    menu.addSeparator();
#endif

    QStringList color_schemes = availableColorSchemes();
    if (!color_schemes.empty())
    {
        color_schemes.sort();

        QString current_theme;
        auto it = _main_window->_config.terminal_color_schemes.find(_term_no);
        if (it == _main_window->_config.terminal_color_schemes.end())
            current_theme = "Linux";
        else
            current_theme = it.value();

        QActionGroup *  color_group       = new QActionGroup(this);
        QMenu *         color_scheme_menu = menu.addMenu("Color schemes");

        for(auto color_scheme_name : color_schemes)
        {
            QAction *a = new QAction();

            a->setText(color_scheme_name);
            a->setVisible(true);
            a->setCheckable(true);
            a->setChecked(current_theme == color_scheme_name);
            connect(a, &QAction::triggered, this, &TermWidget::setColorTheme);
            color_group->addAction(a);
            color_scheme_menu->addAction(a);
        }

        menu.addSeparator();
    }

    menu.addAction(_act_find);

    menu.exec(event->globalPos());
}

bool TermWidget::startShell()
{
    QString color_scheme;
    auto it = _main_window->_config.terminal_color_schemes.find(_term_no);
    if (it == _main_window->_config.terminal_color_schemes.end())
        color_scheme = "Linux";
    else
        color_scheme = it.value();

    setColorScheme(color_scheme);
    setScrollBarPosition(QTermWidget::ScrollBarRight);

    setWorkingDirectory(_main_window->_config.working_dir);
    changeDir(_main_window->_config.working_dir);
    clear();

    setAutoClose(true);

    startShellProgram();
    _is_shell_started = true;

    return _is_shell_started;
}

void TermWidget::setColorTheme()
{
    const QAction *action = qobject_cast<const QAction *>(sender());
    if (action == nullptr)
        return;

    QString theme_name = action->text();

    if (theme_name.isEmpty())
        return;

    _main_window->_config.terminal_color_schemes[_term_no] = theme_name;
    setColorScheme(theme_name);
}

void TermWidget::restart()
{
    _is_shell_started = false;
    _parent_doc_widget->setVisible(false);

    disconnect(_parent_doc_widget, &QDockWidget::visibilityChanged, this, &TermWidget::visibilityChanged);

    if (_is_terminate)
        return;

    TermWidget * term_widget = new TermWidget(_term_no, _main_window, _parent_doc_widget);
    _parent_doc_widget->setWidget(term_widget);

    connect(_parent_doc_widget, &QDockWidget::visibilityChanged, term_widget, &TermWidget::visibilityChanged);
}
