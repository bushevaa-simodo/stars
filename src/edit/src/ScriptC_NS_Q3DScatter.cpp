#include "ScriptC_NS_Q3DScatter.h"

#include "simodo/dsl/ScriptC_Interpreter.h"
#include "simodo/dsl/Exception.h"

using namespace std;
using namespace simodo::dsl;

ScriptC_NS_Q3DScatter::ScriptC_NS_Q3DScatter(QDockWidget *graph_dock,
                                             Graph3D *scatter_graph)
    : _graph_dock(graph_dock)
    , _scatter_graph(scatter_graph)
{
    if (_scatter_graph == nullptr)
        return;

    connect(this, &ScriptC_NS_Q3DScatter::doInitialize, _scatter_graph, &Graph3D::handleInitialization);
    connect(this, &ScriptC_NS_Q3DScatter::doAddSeries, _scatter_graph, &Graph3D::handleAddSeries);
    // connect(this, &ScriptC_NS_Q3DScatter::doAddPoint, _scatter_graph, &Graph3D::handleAddPoint);
    connect(this, &ScriptC_NS_Q3DScatter::doSetPointsCounts, _scatter_graph, &Graph3D::handleSetPointsCount);
}

ScriptC_NS_Q3DScatter::~ScriptC_NS_Q3DScatter()
{
    if (_scatter_graph == nullptr)
        return;

    disconnect(this, &ScriptC_NS_Q3DScatter::doInitialize, _scatter_graph, &Graph3D::handleInitialization);
    disconnect(this, &ScriptC_NS_Q3DScatter::doAddSeries, _scatter_graph, &Graph3D::handleAddSeries);
    // disconnect(this, &ScriptC_NS_Q3DScatter::doAddPoint, _scatter_graph, &Graph3D::handleAddPoint);
    disconnect(this, &ScriptC_NS_Q3DScatter::doSetPointsCounts, _scatter_graph, &Graph3D::handleSetPointsCount);
}

void ScriptC_NS_Q3DScatter::init(std::u16string title)
{
    if (_graph_dock == nullptr || _scatter_graph == nullptr)
        throw simodo::dsl::Exception("init", "Не инициированно окружение");

    emit doInitialize(QString::fromStdU16String(title));
}

void ScriptC_NS_Q3DScatter::addSeries(std::u16string serias_name, int mesh)
{
    if (_graph_dock == nullptr || _scatter_graph == nullptr)
        throw simodo::dsl::Exception("addSeries", "Неинициированно окружение");

    emit doAddSeries(QString::fromStdU16String(serias_name), mesh);
}

void ScriptC_NS_Q3DScatter::addPoint(std::u16string serias_name, double x, double y, double z)
{
    if (_graph_dock == nullptr || _scatter_graph == nullptr)
        throw simodo::dsl::Exception("addPoint", "Неинициированно окружение");

    emit doAddPoint({QString::fromStdU16String(serias_name), {float(x), float(y), float(z)}});
}

void ScriptC_NS_Q3DScatter::setPointsCount(std::u16string serias_name, int count)
{
    if (_graph_dock == nullptr || _scatter_graph == nullptr)
        throw simodo::dsl::Exception("setPointsCount", "Неинициированно окружение");

    emit doSetPointsCounts(QString::fromStdU16String(serias_name), count);
}

namespace
{
    bool init(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(!stack.empty());

        u16string title = get<u16string>(get<SCI_Scalar>(stack.top().bulk).variant);

        ScriptC_NS_Q3DScatter * graph3d = static_cast<ScriptC_NS_Q3DScatter *>(p_object);

        graph3d->init(title);

        stack.pop();

        return true;
    }

    bool addSeries(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 2);

        u16string series_name = get<u16string>(get<SCI_Scalar>(stack.top(1).bulk).variant);
        int64_t series_mesh = get<int64_t>(get<SCI_Scalar>(stack.top(0).bulk).variant);

        ScriptC_NS_Q3DScatter * graph3d = static_cast<ScriptC_NS_Q3DScatter *>(p_object);

        graph3d->addSeries(series_name, series_mesh);

        stack.pop(2);

        return true;
    }

    bool addPoint(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 4);

        u16string series_name = get<u16string>(get<SCI_Scalar>(stack.top(3).bulk).variant);

        const SCI_Name &x_value = stack.top(2);
        double x = get<double>(get<SCI_Scalar>(x_value.bulk).variant);

        const SCI_Name &y_value = stack.top(1);
        double y = get<double>(get<SCI_Scalar>(y_value.bulk).variant);

        const SCI_Name &z_value = stack.top(0);
        double z = get<double>(get<SCI_Scalar>(z_value.bulk).variant);

        ScriptC_NS_Q3DScatter * graph3d = static_cast<ScriptC_NS_Q3DScatter *>(p_object);

        graph3d->addPoint(series_name, x, y, z);

        stack.pop(4);

        return true;
    }

    bool setPointsCount(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 2);

        u16string series_name = get<u16string>(get<SCI_Scalar>(stack.top(1).bulk).variant);
        int64_t points_count = get<int64_t>(get<SCI_Scalar>(stack.top(0).bulk).variant);

        ScriptC_NS_Q3DScatter * graph3d = static_cast<ScriptC_NS_Q3DScatter *>(p_object);

        graph3d->setPointsCount(series_name, points_count);

        stack.pop(2);

        return true;
    }

}

simodo::dsl::SCI_Namespace_t ScriptC_NS_Q3DScatter::getNamespace()
{
    return {
        {u"init", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::init}}},
            {u"", SemanticNameQualification::None, {}},
            {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"addSeries", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addSeries}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"series_mesh", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"addPoint", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addPoint}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"x", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"y", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"z", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"setPointsCount", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setPointsCount}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"points_count", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"Mesh", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"Bar", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(1)}},
            {u"Cube", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(2)}},
            {u"Pyramid", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(3)}},
            {u"Cone", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(4)}},
            {u"Cylinder", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(5)}},
            {u"BevelBar", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(6)}},
            {u"BevelCube", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(7)}},
            {u"Sphere", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(8)}},
            {u"Minimal", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(9)}},
            {u"Arrow", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(10)}},
            {u"Point", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(11)}},
        }},
    };
}
