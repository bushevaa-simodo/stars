#include "GrammarTable.h"
#include "MainWindow.h"

#include "simodo/dsl/GrammarBuilder.h"

#include <QMenu>
#include <QContextMenuEvent>
#include <QMouseEvent>

GrammarTable::GrammarTable(MainWindow *parent)
    : QTreeWidget(parent)
    , _main_window(parent)
{
    setSortingEnabled(false);
    setAnimated(true);
    setUniformRowHeights(true);
    setRootIsDecorated(false);
    setExpandsOnDoubleClick(false);
    setItemsExpandable(false);
    setHeaderHidden(false);

    QTreeWidgetItem *header = new QTreeWidgetItem();
    header->setText(0, tr("Name"));
    header->setToolTip(0,tr("Grammar name"));
    header->setTextAlignment(0, Qt::AlignCenter);
    header->setText(1, tr("Br"));
    header->setToolTip(1,tr("Builder method"));
    header->setTextAlignment(1, Qt::AlignCenter);
    header->setText(2, tr("Rs"));
    header->setToolTip(2,tr("Rules count"));
    header->setTextAlignment(2, Qt::AlignCenter);
    header->setText(3, tr("Ss"));
    header->setToolTip(3,tr("Symbols count"));
    header->setTextAlignment(3, Qt::AlignCenter);
    header->setText(4, tr("Ls"));
    header->setToolTip(4,tr("States count"));
    header->setTextAlignment(4, Qt::AlignCenter);

    setHeaderItem(header);

    const QIcon openIcon = QIcon::fromTheme("document-open", QIcon(":/images/document-open"));
    _act_open_grammar_file = new QAction(openIcon, tr("Open grammar"), this);
    _act_open_grammar_file->setShortcuts(QKeySequence::Open);
    _act_open_grammar_file->setStatusTip(tr("Open selected grammar"));
    connect(_act_open_grammar_file, &QAction::triggered, this,
            [this]()
    {
        if (!selectedIndexes().empty())
            openGrammarFile(itemFromIndex(selectedIndexes().back()));
    });
    addAction(_act_open_grammar_file);

    _act_reload_grammar = new QAction(tr("Reload grammar"), this);
    connect(_act_reload_grammar, &QAction::triggered, this,
            [this]()
    {
        if (!selectedIndexes().empty())
            reloadGrammar(itemFromIndex(selectedIndexes().back()), simodo::dsl::TableBuildMethod::none);
    });
    addAction(_act_reload_grammar);

    _act_reload_SLR = new QAction(tr("Reload with SLR method"), this);
    connect(_act_reload_SLR, &QAction::triggered, this,
            [this]()
    {
        if (!selectedIndexes().empty())
            reloadGrammar(itemFromIndex(selectedIndexes().back()), simodo::dsl::TableBuildMethod::SLR);
    });
    addAction(_act_reload_SLR);

    _act_reload_LR1 = new QAction(tr("Reload with LR1 method"), this);
    connect(_act_reload_LR1, &QAction::triggered, this,
            [this]()
    {
        if (!selectedIndexes().empty())
            reloadGrammar(itemFromIndex(selectedIndexes().back()), simodo::dsl::TableBuildMethod::LR1);
    });
    addAction(_act_reload_LR1);
}

void GrammarTable::refill()
{
    clear();

    simodo::dsl::GrammarManagement & gm = _main_window->grammar_manager();

    for(const auto & [gname,g] : gm.grammar_set())
    {
        QTreeWidgetItem *item = new QTreeWidgetItem();

        item->setText(0, QString::fromStdString(gname));
        item->setText(1, QString::fromStdU16String(simodo::dsl::getGrammarBuilderMethodName(g.build_method)));
        item->setTextAlignment(1, Qt::AlignCenter);
        item->setText(2, QString::number(g.rules.size()));
        item->setTextAlignment(2, Qt::AlignRight);
        item->setText(3, QString::number(g.columns.size()));
        item->setTextAlignment(3, Qt::AlignRight);
        item->setText(4, QString::number(g.states.size()));
        item->setTextAlignment(4, Qt::AlignRight);

        addTopLevelItem(item);
    }

    resizeColumnToContents(0);
    resizeColumnToContents(1);
    resizeColumnToContents(2);
    resizeColumnToContents(3);
    resizeColumnToContents(4);

    update();
}

void GrammarTable::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);

    menu.addAction(_act_open_grammar_file);
    menu.addSeparator();
    menu.addAction(_act_reload_grammar);
    menu.addAction(_act_reload_SLR);
    menu.addAction(_act_reload_LR1);

    menu.exec(event->globalPos());
}

void GrammarTable::mouseDoubleClickEvent(QMouseEvent *e)
{
    openGrammarFile(itemAt(e->pos()));
}

void GrammarTable::openGrammarFile(const QTreeWidgetItem *item) const
{
    if (item == nullptr)
        return;

    QString path = _main_window->config().working_dir + "/data/grammar/" + item->text(0) + ".fuze";

    if (QFileInfo::exists(path))
        _main_window->openFile(path);
    else {
        QString path = _main_window->config().data_dir + "/grammar/" + item->text(0) + ".fuze";

        _main_window->openFile(path);
    }
}

void GrammarTable::reloadGrammar(const QTreeWidgetItem *item, simodo::dsl::TableBuildMethod builder)
{
    if (item == nullptr)
        return;

    _main_window->_global_msg_list->clear();

    bool ok = _main_window->grammar_manager().loadGrammar(true, item->text(0).toStdString(), builder);

    refill();

    if (!ok)
        _main_window->_global_msg_dock->setVisible(true);
}
