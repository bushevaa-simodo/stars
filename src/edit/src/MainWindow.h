#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAction>
#include <QThread>
#include <QLabel>
#include <QDockWidget>

#include "CommonConfig.h"
#include "SimodoRuler.h"
#include "WorkDirectory.h"
#include "BufferedListWidget.h"

#include "codeeditor/ErrorInfo.h"
#include "codeeditor/SpellChecker.h"

const int MAX_TERMINALS = 4;

QT_BEGIN_NAMESPACE
class QActionGroup;
class QAction;
class QMenu;
class QMdiArea;
class QMdiSubWindow;
QT_END_NAMESPACE

struct CodeEditorColorTheme;
class MdiChild;
class WorkObject;
class FindReplace;
class GrammarTable;
class ChartView;
class Graph3D;
class CockpitView;
class Cockpit3D;
#ifndef Q_OS_WIN
class TermWidget;
#endif

class MainWindow : public QMainWindow
{
    friend class WorkObject;
    friend class WorkDirectory;
    friend class FindReplace;
    friend class GrammarTable;
#ifndef Q_OS_WIN
    friend class TermWidget;
#endif
    friend class ChartView;
    friend class CockpitView;

    Q_OBJECT

public:
    MainWindow();
    virtual ~MainWindow() override;

    bool openFile(const QString &fileName);
    bool loadFile(const QString &fileName);

    const CommonConfig &config() const { return _config; }
    simodo::dsl::GrammarManagement &grammar_manager() { return _simodo_ruler->grammar_manager(); }
    SimodoRuler &simodo_ruler();

    BufferedListWidget *getGlobalMsgList() { return _global_msg_list; };
    BufferedListWidget *getEditorMsgList() { return _editor_msg_list; }
    QDockWidget *getEditorMsgWindow() { return _editor_msg_dock; }
    BufferedListWidget *getExecuteMsgList() { return _execute_msg_list; }
    QDockWidget *getExecuteMsgWindow() { return _execute_msg_dock; }

    QDockWidget *chart_dock() { return _chart_dock; }
    ChartView *chart_view() { return _chart_view; }

    QDockWidget *graph3d_dock() { return _graph3d_dock; }
    Graph3D *scatter_graph() { return _graph3d; }

    QDockWidget *cockpit_dock() { return _cockpit_dock; }
    CockpitView *cockpit_view() { return _cockpit_view; }

    QDockWidget *cockpit3d_dock() { return _cockpit3d_dock; }
    Cockpit3D *cockpit3d() { return _cockpit3d; }

    QAction *edit_undo_action() { return _edit_undo_action; }
    QAction *edit_redo_action() { return _edit_redo_action; }

    void setRunActionEnabled(bool enable = true) { _run_action->setEnabled(enable); }

    void showRulename(QString rulename);

    void clearLineNumber();
    void showLineNumber(MdiChild *mdichild);

    void setTextZooming(int curr_font_size);

    bool reload() const { return _reload; }
    QStringList files() const { return _files; }

    QStringList getOpenedFiles() const;

    QStringList hidden_names() const { return config().hidden_names; }

    bool hasTextSelection() const;

    bool setDirectoryPosition(QString path) { return _fs_tree->setSelectorPosition(path); }

    bool isSemanticHighlight() const { return _semantic_highlight_action->isChecked(); }

    MdiChild *running_editor()
    {
        return _running_editor;
    }

signals:
    void startExecution(MdiChild *editor, bool needExecute);
    void doHandleResults(MdiChild *editor, bool success);

protected:
    virtual void closeEvent(QCloseEvent *event) override;

public slots:
    void handleResults(MdiChild *editor, bool success);
    void run(bool needExecute = true);

private slots:
    void updateRecentDirActions();
    void openRecentDir();
    void setDataDirectory();
    void setWorkDirectory();
    void updateRecentFileActions();
    void openRecentFile();
    void newFile();
    void open();
    void save();
    void saveAs();
    void saveAll();
#ifndef QT_NO_CLIPBOARD
    void cut();
    void copy();
    void paste();
#endif
    void tabulation();
    void textWrapping();
    void statusBarViewing();
    void scopeViewing();
    void complViewing();
    void onRunCommand();
    void stop();
    void setColorTheme();
    void activateMessage(QListWidgetItem *item);
    void about();
    void readMe();
    void updateMenus();
    void updateWindowMenu();
    MdiChild *createMdiChild(QIcon icon);

    void openEditWindow(const QModelIndex &index);

    void showLineNumber();

private:
    QThread _simodo_thread;
    WorkObject *_simodo_thread_object;
    MdiChild *_running_editor = nullptr;

    enum
    {
        MaxRecentDirs = 10
    };
    enum
    {
        MaxRecentFiles = 10
    };

    void createActions();
    void createThemesActions(QMenu *theme_menu);
    void createStatusBar();
    void readCommonSettings();
    void readRestSettings();
    void writeSettings();
    static bool hasRecentDirs();
    void prependToRecentDirs(const QString &dirName);
    void setRecentDirsVisible(bool visible);
    static bool hasRecentFiles();
    void prependToRecentFiles(const QString &fileName);
    void setRecentFilesVisible(bool visible);
    MdiChild *activeMdiChild() const;
    MdiChild *findMdiChild(const QString &fileName) const;
    MdiChild *findMdiChildCanonicalPath(const QString &fileNameCanonical) const;
    CodeEditorColorTheme getCurrentTheme() const { return _config.editor_color_theme; }
    void updateColorTheme();
    QString makeCommand(QString pattern);

private:
    QMdiArea *_mdi_area = nullptr;

    QToolBar *_file_toolbar = nullptr;
    QToolBar *_edit_toolbar = nullptr;
    QToolBar *_run_toolbar = nullptr;

    QDockWidget *_find_dock = nullptr;
    FindReplace *_find_form = nullptr;

    QDockWidget *_replace_dock = nullptr;
    FindReplace *_replace_form = nullptr;

    QDockWidget *_fs_dock = nullptr;
    WorkDirectory *_fs_tree = nullptr;

    QDockWidget *_global_msg_dock = nullptr;
    BufferedListWidget *_global_msg_list = nullptr;

    QDockWidget *_editor_msg_dock = nullptr;
    BufferedListWidget *_editor_msg_list = nullptr;

    QDockWidget *_execute_msg_dock = nullptr;
    BufferedListWidget *_execute_msg_list = nullptr;

    QDockWidget *_grammar_dock = nullptr;
    GrammarTable *_grammar_table = nullptr;

    QDockWidget *_chart_dock = nullptr;
    ChartView *_chart_view = nullptr;

    QDockWidget *_graph3d_dock = nullptr;
    Graph3D *_graph3d = nullptr;

    QDockWidget *_cockpit_dock = nullptr;
    CockpitView *_cockpit_view = nullptr;

    QDockWidget *_cockpit3d_dock = nullptr;
    Cockpit3D *_cockpit3d = nullptr;

#ifndef Q_OS_WIN
    QVector<TermWidget *> _terminals;
#endif

    QMenu *_window_menu;
    QAction *recentDirActs[MaxRecentDirs];
    QAction *recentDirSubMenuAct;
    QAction *recentFileActs[MaxRecentFiles];
    QAction *recentFileSubMenuAct;
    QAction *_set_data_dir_action;
    QAction *_set_work_dir_action;
    QAction *_new_action;
    QAction *_save_action;
    QAction *_save_as_action;
    QAction *_save_all_action;

    QAction *_edit_undo_action;
    QAction *_edit_redo_action;
#ifndef QT_NO_CLIPBOARD
    QAction *_cut_action;
    QAction *_copy_action;
    QAction *_paste_action;
#endif
    QAction *_color_action;
    QActionGroup *_color_action_group;
    QActionGroup *_tabulation_action_group;
    QAction *_wrap_action;
    QAction *_tab_replacement_action;
    QAction *_text_auto_indent_action;
    QAction *_text_block_shift_right_action;
    QAction *_text_block_shift_left_action;
    QAction *_text_handle_comment_action;
    QAction *_editor_zoom_in_action;
    QAction *_editor_zoom_out_action;
    QAction *_editor_unzoom_action;

    QAction *_spell_english_action;
    QAction *_spell_national_action;
    QAction *_spell_id_action;

    QAction *_statusbar_action;
    QAction *_scope_action;
    QAction *_compl_action;
    QAction *_semantic_highlight_action;

    //    QVector<QMenu *>    _run_menues;
    QVector<QAction *> _run_actions;
    QAction *_run_separator_action;
    QAction *_run_action;
    QAction *_stop_action;

    QAction *_close_action;
    QAction *_close_all_action;
    QAction *_tile_action;
    QAction *_cascade_action;
    QAction *_next_action;
    QAction *_previous_action;
    QAction *_window_menu_separator_action;

    QLabel *_rulename_label;
    QLabel *_linenumber_label;
    QLabel *_text_zooming_percentage_label;

    CommonConfig _config;
    SimodoRuler *_simodo_ruler = nullptr;

    SpellChecker _spell_checker;

    QStringList _files;
    bool _reload;
};

#endif // MAINWINDOW_H
