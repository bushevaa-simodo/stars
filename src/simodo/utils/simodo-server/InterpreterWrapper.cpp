#include "InterpreterWrapper.h"

#include "COutListener.h"
#include "ModuleManager.h"

#include "simodo/dsl/GrammarLoader.h"
#include "simodo/dsl/GrammarBuilder.h"
#include "simodo/dsl/Parser.h"
#include "simodo/dsl/Exception.h"

#include "simodo/dsl/AstBuilder.h"
#include "simodo/dsl/ScriptC_NS_Scene.h"
#include "simodo/dsl/ScriptC_NS_Math.h"
#include "simodo/dsl/ScriptC_NS_TableFunction.h"
#include "simodo/dsl/ScriptC_Semantics.h"

#include "simodo/convert.h"
#include "simodo/version.h"

#include <sstream>

using namespace simodo::dsl;

server::InterpreterWrapper::InterpreterWrapper(
      server::InterpreterWrapper::ModuleRequestCallback request_module_callback
    , AReporter & listener
    , const std::string &path_to_grammar
) : _request_module_callback(request_module_callback)
  , _listener(listener)
  , _path_to_grammar(path_to_grammar)
{}

server::InterpreterWrapper::~InterpreterWrapper()
{
    _cv.notify_all();
}

int server::InterpreterWrapper::interpret(const Module &module)
{
    std::unique_lock ulock(_mutex);

    if (module.file_path.empty())
    {
        return 1;
    }

    const std::string grammar_path = "data/grammar";
    const std::string grammar_name = "scriptc0";

    dsl::AReporter & m = _listener;
    Grammar g;
    std::vector<std::string> paths {grammar_path, _path_to_grammar};

    GrammarLoader loader(m, paths, grammar_name);

    bool ok = loader.load(false, g, TableBuildMethod::none);

    if (!ok)
        return 2;

    Parser p(module.file_path, m, g);
    AstNode             ast(simodo::convertToU16(module.file_path));
    AstBuilder          ast_builder(m,ast,g.handlers);

    std::stringstream ss;
    ss.str(module.script);
    FileStream in(ss);
    ok = p.parse(in, ast_builder);

    if (!ok)
        return 2;

    ModuleManager::ModuleSupplier module_supplier
        = [this](std::string module_path) -> std::optional<std::string>
    {
        std::unique_lock lock(_mutex);
        _stored_module = nullptr;
        _stored_error = nullptr;
        _request_module_callback(module_path);

        _cv.wait(lock, [this, module_path]
        {
            if (_machine->stop_signal())
            {
                _stored_module = std::make_unique<Module>(Module{module_path, ""});
                _stored_error = std::make_unique<std::string>("");
                return true;
            }

            if (_stored_module == nullptr)
            {
                return false;
            }

            if (_stored_module->file_path != module_path)
            {
                _request_module_callback(module_path);
                return false;
            }

            return true;
        });

        if (!_stored_error->empty())
        {
            return std::optional<std::string>();
        }

        return _stored_module->script;
    };
    ModuleManager mm( module_supplier
                    , {m, m, nullptr, paths.back()}
                    );

    _sys = std::make_unique<ScriptC_NS_Sys>(m);
    _chart = std::make_unique<Remote_NS_Chart>(m);
    _cockpit = std::make_unique<Remote_NS_Cockpit>(m);
    _graph = std::make_unique<Remote_NS_Graph3D>(m);
    _mech = std::make_unique<Remote_NS_Mechanoid>(m);
    ScriptC_NS_Math          math;
    ScriptC_NS_TableFunction tf;

    _machine = std::make_unique<ScriptC_Interpreter>(m,mm);
    ScriptC_NS_Scene    scene(_machine.get());

    _machine->importNamespace(u"sys", _sys->getNamespace());
    _machine->importNamespace(u"chart", _chart->getNamespace());
    _machine->importNamespace(u"cockpit", _cockpit->getNamespace());
    _machine->importNamespace(u"graph3d", _graph->getNamespace());
    _machine->importNamespace(u"mechanoid", _mech->getNamespace());
    _machine->importNamespace(u"math", math.getNamespace());
    _machine->importNamespace(u"tf", tf.getNamespace());
    _machine->importNamespace(u"scene", scene.getNamespace());

    ulock.unlock();

    SCI_RunnerCondition res = _machine->catchAst(ast);

    {
        std::lock_guard guard(_mutex);
        _machine = nullptr;
        _sys = nullptr;
    }

    if (res != SCI_RunnerCondition::Regular)
        return 2;

    return 0;
}

bool server::InterpreterWrapper::isRunning()
{
    std::lock_guard guard(_mutex);
    return _machine != nullptr;
}

void server::InterpreterWrapper::storeModule(const Module &module, const std::string &error)
{
    {
        std::lock_guard guard(_mutex);
        _stored_module = std::make_unique<Module>(module);
        _stored_error = std::make_unique<std::string>(error);
    }
    _cv.notify_all();
}

void server::InterpreterWrapper::recieveMsg(const Msg &msg)
{
    std::lock_guard guard(_mutex);

    if (!_sys)
    {
        return;
    }

    _sys->setInput(msg.key, msg.value);
}

void server::InterpreterWrapper::pause()
{
    std::lock_guard guard(_mutex);

    if (!_machine)
    {
        return;
    }

    _machine->pause();
}

void server::InterpreterWrapper::stop()
{
    std::lock_guard guard(_mutex);

    if (!_machine)
    {
        return;
    }

    _machine->stop();
    _cv.notify_all();
}
