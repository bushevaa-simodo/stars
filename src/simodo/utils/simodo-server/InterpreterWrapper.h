#ifndef SIMODO_DSL_SERVER_InterpreterWrapper
#define SIMODO_DSL_SERVER_InterpreterWrapper

#include "simodo/dsl/ScriptC_Interpreter.h"
#include "simodo/dsl/ScriptC_NS_Sys.h"
#include "simodo/dsl/Remote_NS_Chart.h"
#include "simodo/dsl/Remote_NS_Cockpit.h"
#include "simodo/dsl/Remote_NS_Graph3D.h"
#include "simodo/dsl/Remote_NS_Mechanoid.h"

#include <condition_variable>
#include <string>
#include <memory>
#include <mutex>
#include <functional>

namespace simodo::dsl::server
{
    struct Module
    {
        std::string file_path;
        std::string script;
    };

    struct Msg
    {
        std::string key;
        std::optional<double> value;
    };

    class InterpreterWrapper
    {
    public:
        using ModuleRequestCallback
            = std::function<void(std::string)>;
        InterpreterWrapper( ModuleRequestCallback request_module_callback
                          , simodo::dsl::AReporter & listener
                          , const std::string &path_to_grammar
                          );
        ~InterpreterWrapper();

        int interpret(const Module &module);
        bool isRunning();
        void storeModule(const Module &module, const std::string &error);
        void recieveMsg(const Msg &msg);
        void pause();
        void stop();

    private:
        std::unique_ptr<ScriptC_NS_Sys> _sys;
        std::unique_ptr<Remote_NS_Chart> _chart;
        std::unique_ptr<Remote_NS_Cockpit> _cockpit;
        std::unique_ptr<Remote_NS_Graph3D> _graph;
        std::unique_ptr<Remote_NS_Mechanoid> _mech;
        
        std::unique_ptr<ScriptC_Interpreter> _machine;

        std::unique_ptr<Module> _stored_module;
        std::unique_ptr<std::string> _stored_error;
        ModuleRequestCallback _request_module_callback;
        simodo::dsl::AReporter & _listener;
        std::string _path_to_grammar;

        std::mutex _mutex;
        std::condition_variable _cv;
    };
}

#endif // SIMODO_DSL_SERVER_InterpreterWrapper
