#ifndef SIMODO_DSL_SERVER_CallbackTypedMsgHandler
#define SIMODO_DSL_SERVER_CallbackTypedMsgHandler

#include "TypedMsgHandler.h"

namespace simodo::dsl::server
{
    class CallbackTypedMsgHandler : public TypedMsgHandler
    {
    public:
        using Callback = std::function<std::optional<int>(MsgRef)>;

        CallbackTypedMsgHandler(TypeRef type, Callback callback);
        virtual ~CallbackTypedMsgHandler() = default;

        std::optional<int> handle(MsgRef msg) override;

    private:
        Callback _callback;
    };
} // simodo::dsl::server

#endif // SIMODO_DSL_SERVER_TypedMsgHandler
