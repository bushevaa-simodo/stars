#include "TypedMsgHandler.h"

#include "JsonUtility.h"

using namespace simodo::dsl::server;

TypedMsgHandler::TypedMsgHandler(TypeRef type)
    : _type(type)
{}

std::optional<int> TypedMsgHandler::handle(MsgRef msg)
{
    if (JsonUtility::findString(msg, "type") != _type)
    {
        return {};
    }

    return 0;
}
