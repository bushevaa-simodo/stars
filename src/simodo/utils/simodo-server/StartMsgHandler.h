#ifndef SIMODO_DSL_SERVER_StartMsgHandler
#define SIMODO_DSL_SERVER_StartMsgHandler

#include "TypedMsgHandler.h"

namespace simodo::dsl::server
{
    class StartMsgHandler : public TypedMsgHandler
    {
    public:
        using InterpreterIsRunningCallback = std::function<bool()>;
        using StartInterpreterCallback = std::function<void(const std::string &, const std::string &)>;

        StartMsgHandler( InterpreterIsRunningCallback is_running_callback
                       , StartInterpreterCallback start_callback
                       );

        virtual ~StartMsgHandler() = default;

        std::optional<int> handle(MsgRef msg) override;

    private:
        InterpreterIsRunningCallback _is_running_callback;
        StartInterpreterCallback _start_callback;

    };
} // simodo::dsl::server

#endif // SIMODO_DSL_SERVER_StartMsgHandler
