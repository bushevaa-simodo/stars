#ifndef SIMODO_DSL_SERVER_IoService
#define SIMODO_DSL_SERVER_IoService

#include <optional>
#include <functional>
#include <string>
#include <memory>

namespace simodo::dsl::server
{

class IoService
{
public:
    using ReadCallback = std::function<void(std::optional<std::string>)>;
    using WriteCallback = std::function<void()>;

    static const ReadCallback DEFAULT_READ_CALLBACK;
    static const WriteCallback DEFAULT_WRITE_CALLBACK;

    virtual ~IoService() = default;

    virtual void asyncRead( ReadCallback read_callback
                              = DEFAULT_READ_CALLBACK
                          ) = 0;
    virtual void asyncWrite( std::string str
                           , WriteCallback write_callback
                               = DEFAULT_WRITE_CALLBACK
                           ) = 0;
};

} // simodo::dsl::server

#endif // SIMODO_DSL_SERVER_IoService
