#include "StdIoService.h"

#include <iostream>

namespace asio = boost::asio;

using namespace simodo::dsl::server;

namespace
{
    std::string endlString()
    {
        std::stringstream ss;
        ss << std::endl;
        return ss.str();
    }

    std::string endl_str = endlString();
}

StdIoService::StdIoService(boost::asio::io_service & io)
    : _io(io)
    , _write_strand(_io)
{
    _postAsyncRead();
}

void StdIoService::asyncRead(ReadCallback read_callback)
{
    _read_callback = read_callback; 
}

void StdIoService::asyncWrite(std::string str, WriteCallback write_callback)
{
    _write_strand.post([str, write_callback]
    {
        std::cout << (str + endl_str) << std::flush;
        write_callback();
    });
}

void StdIoService::_postAsyncRead()
{
    asio::post(_io, [this]
    {
        std::string input_line;
        bool ok = static_cast<bool>(std::getline(std::cin, input_line));
        auto callback = _read_callback;

        if (!ok && callback)
        {
            callback({});
            return;
        }

        if (callback)
        {
            callback(input_line);
        }
        if (!input_line.empty())
        {
            _postAsyncRead();
        }
    });
}
