#ifndef SIMODO_DSL_JsonUtility
#define SIMODO_DSL_JsonUtility

#include <boost/json/object.hpp>

#include <string>
#include <optional>

namespace simodo::dsl::server::JsonUtility
{
    std::optional<double> findNumber(const boost::json::object &o, const std::string &key);
    std::optional<std::string> findString(const boost::json::object &o, const std::string &key);
} // simodo::dsl::server::JsonUtility

#endif // SIMODO_DSL_JsonUtiltiy
