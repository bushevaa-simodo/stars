#ifndef SIMODO_DSL_SERVER_MsgHandler
#define SIMODO_DSL_SERVER_MsgHandler

#include <boost/json/object.hpp>

#include <optional>

namespace simodo::dsl::server
{
    class MsgHandler
    {
    public:
        using MsgRef = const boost::json::object &;

        virtual ~MsgHandler() = default;

        virtual std::optional<int> handle(MsgRef msg) = 0;
    };
} // simodo::dsl::server

#endif // SIMODO_DSL_SERVER_MsgHandler
