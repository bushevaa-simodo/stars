#include "CallbackTypedMsgHandler.h"

using namespace simodo::dsl::server;

CallbackTypedMsgHandler::CallbackTypedMsgHandler(TypeRef type, Callback callback)
            : TypedMsgHandler(type)
            , _callback(callback)
{}

std::optional<int> CallbackTypedMsgHandler::handle(MsgRef msg)
{
    if (!TypedMsgHandler::handle(msg))
    {
        return {};
    }

    return _callback(msg);
}
