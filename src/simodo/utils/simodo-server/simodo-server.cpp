/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include "COutListener.h"
#include "StdIoService.h"
#include "MsgHandler.h"
#include "StartMsgHandler.h"
#include "CallbackTypedMsgHandler.h"
#include "ModuleMsgHandler.h"
#include "InterpreterWrapper.h"
#include "JsonUtility.h"

#include <boost/json/parse.hpp>
#include <boost/system/error_code.hpp>

/*! \file Утилита тестирования средств начальной загрузки и интерпретации стартовых (FUZE) грамматик библиотеки SIMODOdsl. Проект SIMODO Stars
*/

namespace asio = boost::asio;

using namespace simodo::dsl::server;
using namespace std;

namespace
{
    struct Result
    {
        bool finished;
        int code;

        Result(int c = 0, bool f = false) : finished(f), code(c)
        {}
        Result(bool f, int c = 0) : finished(f), code(c)
        {}
    };

    class App
    {
    public:
        App(std::string path_to_grammar);
        int run();

    private:
        asio::thread_pool _io_pool;
        asio::io_service _io;
        StdIoService _std_io_service;
        COutListener _listener;
        std::vector<std::shared_ptr<MsgHandler>> _msg_handlers;

        InterpreterWrapper _interpreter_wrapper;

        Result _result;

        Result _processInput(const string &input);

        void _exit(int code);
    };
}

int main(int argc, char *argv[])
{
    App app(argc > 1 ? argv[1] : "");
    return app.run();
}

namespace
{
    App::App(std::string path_to_grammar)
        : _io_pool(4)
        , _std_io_service(_io)
        , _listener(_std_io_service)
        , _interpreter_wrapper([this](std::string module_path)
        {
            auto request_json = "{\"type\":\"request\",\"path\":\""
                              + module_path
                              + "\"}";
            _std_io_service.asyncWrite(request_json);
        }
        , _listener
        , path_to_grammar
        )
    {
        _std_io_service.asyncRead([this](optional<string> message)
        {
            asio::post(_io, [this, message]
            {
                if (!message.has_value())
                {
                    _exit(-1);
                    return;
                }

                _result = _processInput(*message);
                _std_io_service.asyncWrite("{\"type\":\"result\",\"code\":" + to_string(_result.code) + "}");
                if (_result.finished)
                {
                    _exit(_result.code);
                }
            });
        });
        _msg_handlers.push_back(
            std::make_shared<StartMsgHandler>([this]
            {
                return _interpreter_wrapper.isRunning();
            }, [this](std::string file_path, std::string script)
            {
                asio::post( _io_pool, [this, file_path, script]
                {
                    _interpreter_wrapper.interpret({file_path, script});
                    _std_io_service.asyncWrite("{\"type\":\"stop\"}");
                });
            })
        );
        _msg_handlers.push_back(
            std::make_shared<CallbackTypedMsgHandler>("pause", [this](MsgHandler::MsgRef) -> std::optional<int>
            {
                if (!_interpreter_wrapper.isRunning())
                {
                    return {6};
                }

                _interpreter_wrapper.pause();
                return {0};
            })
        );
        _msg_handlers.push_back(
            std::make_shared<CallbackTypedMsgHandler>("stop", [this](MsgHandler::MsgRef) -> std::optional<int>
            {
                if (!_interpreter_wrapper.isRunning())
                {
                    return {7};
                }

                _interpreter_wrapper.stop();
                return {0};
            })
        );
        _msg_handlers.push_back(
            std::make_shared<ModuleMsgHandler>([this]
            {
                return _interpreter_wrapper.isRunning();
            }, [this](const Module &module, const std::string &error)
            {
                _interpreter_wrapper.storeModule(module, error);
            })
        );
        _msg_handlers.push_back(
            std::make_shared<CallbackTypedMsgHandler>("message", [this](MsgHandler::MsgRef msg) -> std::optional<int>
            {
                if (!_interpreter_wrapper.isRunning())
                {
                    return {8};
                }

                auto key = JsonUtility::findString(msg, "key");
                if (!key)
                {
                    return {9};
                }

                auto value = JsonUtility::findNumber(msg, "value");

                _interpreter_wrapper.recieveMsg({*key, value});

                return {0};
            })
        );
    }

    int App::run()
    {
        asio::io_service::work w(_io);

        for (size_t i = 0; i < 3; ++i)
        {
            asio::post(_io_pool, [this] { _io.run(); });
        }
        _io_pool.join();

        return _result.code;
    }

    Result App::_processInput(const string &input)
    {
        if (input.empty())
        {
            return {true};
        }

        boost::system::error_code ec;
        auto input_object_value = boost::json::parse(input, ec);
        if (ec || !input_object_value.is_object())
        {
            _std_io_service.asyncWrite(input);
            return {1};
        }
        auto input_object = input_object_value.as_object();

        for (auto handler : _msg_handlers)
        {
            auto result = handler->handle(input_object);
            if (result)
            {
                return {*result};
            }
        }

        _std_io_service.asyncWrite(input);
        return {1};
    }

    void App::_exit(int code)
    {
        _std_io_service.asyncWrite( "{\"type\":\"exit\",\"code\":" + to_string(code) + "}"
                                    , [this]()
                                    {
                                        _io.stop();
                                        _interpreter_wrapper.stop();
                                    });
    }
}
