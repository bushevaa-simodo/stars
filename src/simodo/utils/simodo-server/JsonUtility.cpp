#include "JsonUtility.h"

#include <boost/system/error_code.hpp>

using namespace simodo::dsl::server;

std::optional<double> JsonUtility::findNumber(const boost::json::object &o, const std::string &key)
{
    auto num_it = o.find(key);
    if (num_it == o.end())
    {
        return {};
    }

    const auto &num_val = num_it->value();
    if (!num_val.is_number())
    {
        return {};
    }

    boost::system::error_code ec;
    auto number = num_val.to_number<double>(ec);

    if (ec)
    {
        return {};
    }

    return number;
}

std::optional<std::string> JsonUtility::findString(const boost::json::object &o, const std::string &key)
{
    auto str_it = o.find(key);
    if (str_it == o.end() || !str_it->value().is_string())
    {
        return {};
    }
    return str_it->value().as_string().c_str();
}
