#ifndef SIMODO_DSL_SERVER_TypedMsgHandler
#define SIMODO_DSL_SERVER_TypedMsgHandler

#include "MsgHandler.h"

#include <string>

namespace simodo::dsl::server
{
    class TypedMsgHandler : public MsgHandler
    {
    public:
        using Type = std::string;
        using TypeRef = const Type &;

        TypedMsgHandler(TypeRef type);

        virtual ~TypedMsgHandler() = default;

        std::optional<int> handle(MsgRef &msg) override;

    protected:
        Type _type;
    };
} // simodo::dsl::server

#endif // SIMODO_DSL_SERVER_TypedMsgHandler
