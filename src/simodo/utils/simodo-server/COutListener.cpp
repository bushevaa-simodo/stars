#include "COutListener.h"

#include "simodo/inout/convert/functions.h"

#include <boost/json/object.hpp>
#include <boost/json/serialize.hpp>

#include <iostream>

using namespace simodo::dsl;
using namespace simodo::dsl::server;

COutListener::COutListener(IoService & io)
    : _io(io)
{}

void COutListener::report(const SeverityLevel level,
                            TokenLocation ,
                            const std::u16string & briefly,
                            const std::u16string & atlarge)
{
    using namespace simodo::inout::convert;

    if (!briefly.empty() && briefly.at(0) == '#')
    {
        boost::json::object jo {
            {"type", "record"}
            , {"value", toU8(briefly)}
        };

        _io.asyncWrite(boost::json::serialize(jo));
        return;
    }

    auto severity_level =
        level == SeverityLevel::Information
            ? "info"
            : level == SeverityLevel::Warning
            ? "warning"
            : level == SeverityLevel::Error
            ? "error"
            : level == SeverityLevel::Fatal
            ? "fatal"
            : "unknown";
    
    boost::json::object jo {
        {"type", "message"}
        , {"level", severity_level}
        , {"briefly", toU8(briefly)}
        , {"atlarge", toU8(atlarge)}
    };

    _io.asyncWrite(boost::json::serialize(jo));
}
