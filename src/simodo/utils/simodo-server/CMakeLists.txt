cmake_minimum_required(VERSION 3.8 FATAL_ERROR)

project(simodo-server)

set(THREADS_PREFER_PTHREAD_FLAG ON)

find_package(Threads REQUIRED)

add_executable(${PROJECT_NAME}
    simodo-server.cpp
    COutListener.h
    COutListener.cpp
    StdIoService.h
    StdIoService.cpp
    InterpreterWrapper.h
    InterpreterWrapper.cpp
    ModuleManager.h
    ModuleManager.cpp
    MsgHandler.h
    TypedMsgHandler.h
    TypedMsgHandler.cpp
    CallbackTypedMsgHandler.h
    CallbackTypedMsgHandler.cpp
    StartMsgHandler.h
    StartMsgHandler.cpp
    ModuleMsgHandler.h
    ModuleMsgHandler.cpp
    JsonUtility.h
    JsonUtility.cpp
    IoService.h
    IoService.cpp
    json.cpp
)

set_target_properties(${PROJECT_NAME} PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS YES
)

target_link_libraries(${PROJECT_NAME} SIMODOdsl)
target_link_libraries(${PROJECT_NAME} Threads::Threads)

if (${CROSS_WIN})
    target_link_libraries(${PROJECT_NAME} ws2_32)
endif()

set_target_properties(${PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
