#include "IoService.h"

using namespace simodo::dsl::server;

const IoService::ReadCallback IoService::DEFAULT_READ_CALLBACK
    = [](std::optional<std::string>) {};

const IoService::WriteCallback IoService::DEFAULT_WRITE_CALLBACK
    = []() {};
