/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <fstream>
#include <iostream>
#include <locale>
#include <codecvt>
#include <memory>
#include <algorithm>

#include "simodo/dsl/AReporter.h"
#include "simodo/dsl/Stream.h"
#include "simodo/dsl/Tokenizer.h"

#include "simodo/convert.h"
#include "simodo/inout/convert/functions.h"

/*! \file Утилита тестирования средств лексического анализа библиотеки SIMODOdsl. Проект SIMODO Stars
*/

using namespace std;
using namespace simodo;
using namespace simodo::dsl;

namespace
{
    class COutListener: public AReporter
    {
    public:
        COutListener() : _max_severity(SeverityLevel::Information) {}
        virtual ~COutListener() override;

        virtual void report(const SeverityLevel level,
                            TokenLocation ,
                            const std::u16string & briefly,
                            const std::u16string & atlarge) override;

    private:
        SeverityLevel _max_severity;
    };

    COutListener::~COutListener() {}

    void COutListener::report(const SeverityLevel level,
                              TokenLocation ,
                              const std::u16string & briefly,
                              const std::u16string & atlarge)
    {
        cout << convertToU8(getSeverityLevelName(level) + briefly) << endl;
        if (!atlarge.empty())
            cout << convertToU8(atlarge) << endl;
        _max_severity = max(_max_severity,level);
    }

    int produceLexicalAnalisys (const string & file_name, bool use_string_buffer, const LexicalParameters & lex_param)
    {
        SIMODO_INOUT_CONVERT_STD_IFSTREAM(in, file_name);
        u16string string_buffer;
        unique_ptr<IStream> stream;

        if (!in)
        {
            cout << "Ошибка при открытии файла '" << file_name << "'" << endl;
            return 2;
        }

        if (use_string_buffer)
        {
            FileStream in_stream(in);

            while(true)
            {
                char16_t ch = in_stream.get();
                if (ch == char_traits<char16_t>::eof())
                    break;
                string_buffer += ch;
            }

            stream = make_unique<BufferStream>(string_buffer.data());
        }
        else
            stream = make_unique<FileStream>(in);

        Tokenizer   tzer(convertToU16(file_name), *stream, lex_param, 4);

        uint32_t comment_n = 0;
        uint32_t annotation_n = 0;
        uint32_t punctuation_n = 0;
        uint32_t keyword_n = 0;
        uint32_t word_n = 0;
        uint32_t word_national_mix_error_n = 0;
        uint32_t word_national_use_error_n = 0;
        uint32_t number_n = 0;
        uint32_t number_wrong_n = 0;
        uint32_t error_n = 0;
        uint32_t unknown = 0;
        uint32_t token_count = 0;

        Token t = tzer.getAnyToken();

        while (t.getType() != LexemeType::Empty)
        {
            token_count ++;

            switch(t.getType())
            {
            case LexemeType::Punctuation:
                punctuation_n ++;
                if (t.getQualification() == TokenQualification::Keyword)
                    keyword_n ++;
                break;
            case LexemeType::Id:
                word_n ++;
                if (t.getQualification() == TokenQualification::NationalCharacterMix)
                    error_n ++, word_national_mix_error_n ++;
                if (t.getQualification() == TokenQualification::NationalCharacterUse)
                    error_n ++, word_national_use_error_n ++;
                break;
            case LexemeType::Annotation:
                annotation_n ++;
                break;
            case LexemeType::Number:
                number_n ++;
                if (t.getQualification() == TokenQualification::NotANumber)
                    number_wrong_n ++;
                break;
            case LexemeType::Comment:
                comment_n ++;
                break;
            case LexemeType::Error:
                error_n ++;
                if (t.getQualification() == TokenQualification::NationalCharacterMix)
                    word_national_mix_error_n ++;
                if (t.getQualification() == TokenQualification::NationalCharacterUse)
                    word_national_use_error_n ++;
                break;
            default:
                unknown ++;
                break;
            }

            TokenLocation loc = t.getLocation();

            cout << convertToU8(loc.file_name) << ":" << loc.line << ":" << loc.column_tabulated << "/" << loc.column
                 << " [" << loc.begin << "-" << loc.end << "], token: \""
                 << convertToU8(t.getToken()) << "\"";

            if (t.getToken() != t.getLexeme())
                cout << ", lexeme: \"" << convertToU8(t.getLexeme()) << "\"";

            cout << ", type: " << convertToU8(getLexemeTypeName(t.getType()));

            if (t.getQualification() != TokenQualification::None)
                cout << ", qualification: " << convertToU8(getQualificationName(t.getQualification()));

            cout << endl;

            t = tzer.getAnyToken();
        }

        cout << "ИТОГО: токенов ..... " << token_count << endl
             << "       комментариев. " << comment_n << endl
             << "       аннотаций ... " << annotation_n << endl
             << "       пунктуаций .. " << punctuation_n << " (из них ключевых слов: " << keyword_n << ")" << endl
             << "       слов ........ " << word_n  << endl
             << "       чисел ....... " << number_n  << " (из них с ошибками: " << number_wrong_n << ")" << endl
             << "       ошибок ...... " << error_n << endl
             << "         с недопустимым алфавитом.. " << word_national_use_error_n << endl
             << "         c перемешанными алфавитами " << word_national_mix_error_n << endl
             << "         сбоев..................... " << unknown << endl
                ;

        return 0;
    }
}

int main(int argc, char *argv[])
{
    LexicalParameters lex_param
    {
        // std::vector<MarkupSymbol> markups
        {
            {u"/*", u"*/", u"", LexemeType::Comment},
            {u"//", u"", u"", LexemeType::Comment},
            {u"\"", u"\"", u"\\", LexemeType::Annotation},
            {u"'", u"'", u"\\", LexemeType::Annotation}
        },
        // std::vector<NumberMask> masks
        {
//            {u"#nnnnnn", LexemeType::Number, 16},
//            {u"0bN", LexemeType::Number, 2},
//            {u"0xN", LexemeType::Number, 16},
//            {BUILDING_NUMBER, LexemeType::Number, 10},
        },
        // std::u16string punctuation_chars
        u"+-,;",
        // std::vector<std::u16string>punctuation_words
        {
            u"_", u"_Nebuchadnezzar_II", u"ФУ_"
        }
    };

    vector<std::string> arguments(argv + 1, argv + argc);

    string	file_name           = "";
    bool    use_char16_buffer   = false;
    
    bool	error               = false;
    bool	help                = false;

    for(size_t i=0; i < arguments.size(); ++i)
    {
        const string & arg = arguments[i];

        if (arg[0] == '-')
        {
            if (arg == "--help" || arg == "-h")
                help = true;
            else if (arg == "--use-char16-buffer")
                use_char16_buffer = true;
            else if (arg == "--number_mask" || arg == "-n")
            {
                if (i+2 >= arguments.size())
                    error = true;
                else
                    lex_param.masks.push_back({convertToU16(arguments[++i]),
                                               LexemeType::Number,
                                               static_cast<number_system_t>(stoi(arguments[++i]))});
            }
            else
                error = true;
        }
        else if (file_name.empty())
            file_name = arg;
        else
            error = true;
    }

    if (file_name.empty())
        error = true;

    if (error)
    {
        cout << "Ошибка в параметрах запуска" << endl;
        help = true;
    }

    if (help)
        cout	<< "Утилита тестирования средств лексического анализа библиотеки SIMODOdsl. Проект SIMODO Stars" << endl
                << "Формат запуска:" << endl
                << "    simlex [<параметры>] <файл>" << endl
                << "Параметры:" << endl
                << "    -h | --help                - отображение подсказки по запуску программы" << endl
                << "         --use-char16-buffer   - провести лексический анализ заданного файла с использованием строкового буфера" << endl
                << "    -n | --number_mask <маска> <система счисления> - добавить маску числа" << endl
                ;

    if (error)
        return 1;

    return produceLexicalAnalisys(file_name, use_char16_buffer, lex_param);
}
