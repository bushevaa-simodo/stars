#ifndef TaskDidClose_h
#define TaskDidClose_h

#include "simodo/tp/Task_interface.h"
#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/Variable.h"

class ServerContext;

class DocDidClose : public simodo::tp::Task_interface
{
    ServerContext &             _server;
    simodo::variable::json::Rpc _jsonrpc;

public:
    DocDidClose() = delete;
    DocDidClose(ServerContext & server, const std::string &jsonrpc)
        : _server(server), _jsonrpc(jsonrpc)
    {}

    virtual void work() override;
};

#endif // TaskDidClose_h