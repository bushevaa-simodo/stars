#include "Initialize.h"
#include "../ServerContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"
#include "simodo/inout/convert/functions.h"

using namespace simodo;
using namespace simodo::inout;

void Initialize::work()
{
    if (_server.state() != ServerState::None
     || _jsonrpc.id() <= 0
     || _jsonrpc.params().type() != variable::ValueType::Record) {
        _server.log().critical("There are wrong parameter structure of 'initialize' command", variable::json::toString(_jsonrpc.value()));
        /// @todo Скорректировать коды (и тексты) ошибок
        _server.sending().push(variable::json::Rpc(1, u"Invalid request", _jsonrpc.id()));
        return;
    }
    const std::shared_ptr<variable::Record> params_object = _jsonrpc.params().getRecord();

    _params = lsp::parseClientParams(params_object);

    variable::json::Rpc server_response = _server.initialize(_params, _jsonrpc.id());

    if (!server_response.is_valid()) {
        _server.log().critical("Unable to create initialization structure", variable::json::toString(_jsonrpc.value()));
        /// @todo Скорректировать коды (и тексты) ошибок
        _server.sending().push(variable::json::Rpc(3, u"Unable to create initialization structure", _jsonrpc.id()));
        return;
    }

    _server.sending().push(server_response);
}
