#include "SegmentationFault.h"
#include "../ServerContext.h"
#include "../DocumentContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"
#include "simodo/inout/convert/functions.h"

using namespace simodo;
using namespace simodo::inout;

void SegmentationFault::work()
{
    if (_jsonrpc.is_valid()) {
        lsp::TextDocumentPositionParams doc_position;
        DocumentContext *               doc = _server.findDocument(doc_position.textDocument.uri);
        variable::Value                 result = doc->produceHoverInfo(doc_position.position);
        
        _server.sending().push(variable::json::Rpc(result, _jsonrpc.id()));
        return;
    }
    _server.log().error("There are wrong parameter structure of 'SegmentationFault' command", 
                        variable::json::toString(_jsonrpc.value()));
    _server.sending().push(
        /// @todo Скорректировать коды (и тексты) ошибок
        variable::json::Rpc(-1,u"There are wrong parameter structure of 'SegmentationFault' command",
                            _jsonrpc.id()));
}
