#include "Initialized.h"
#include "../ServerContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/inout/convert/functions.h"

using namespace simodo;
using namespace simodo::inout;

void Initialized::work()
{
    _server.initialized();
}

