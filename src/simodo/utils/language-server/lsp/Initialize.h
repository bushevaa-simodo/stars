#ifndef TaskInitialize_h
#define TaskInitialize_h

#include "simodo/lsp/ClientParams.h"
#include "simodo/tp/Task_interface.h"
#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/Variable.h"

class ServerContext;

class Initialize : public simodo::tp::Task_interface
{
    ServerContext &             _server;
    simodo::variable::json::Rpc _jsonrpc;

    simodo::lsp::ClientParams   _params;

public:
    Initialize() = delete;
    Initialize(ServerContext & server, const std::string &jsonrpc)
        : _server(server), _jsonrpc(jsonrpc)
    {}

    virtual void work() override;
};

#endif // TaskInitialize_h