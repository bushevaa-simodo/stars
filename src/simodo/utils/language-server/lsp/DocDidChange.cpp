#include "DocDidChange.h"
#include "../ServerContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"
#include "simodo/inout/convert/functions.h"

using namespace simodo;
using namespace simodo::inout;

void DocDidChange::work()
{
    if (_jsonrpc.is_valid() && _jsonrpc.params().type() == variable::ValueType::Record) {
        if (_server.changeDocument(*_jsonrpc.params().getRecord()))
            return;
    }
    _server.log().error("There are wrong parameter structure of 'textDocument/didChange' notification", variable::json::toString(_jsonrpc.value()));
    _server.sending().push(
        /// @todo Скорректировать коды (и тексты) ошибок
        variable::json::Rpc(-1,u"There are wrong parameter structure of 'textDocument/didChange' notification", -1));
}

