#include "DocumentContext.h"
#include "ServerContext.h"
#include "ReportCombiner.h"

#include "simodo/dsl/Parser.h"
#include "simodo/dsl/AstBuilder.h"
#include "simodo/dsl/ScriptC_Interpreter.h"
#include "simodo/dsl/ScriptC_NS_Scene.h"
#include "simodo/dsl/ScriptC_NS_Sys.h"
#include "simodo/dsl/ScriptC_NS_Math.h"
#include "simodo/dsl/ScriptC_NS_TableFunction.h"
#include "simodo/dsl/ScriptC_Semantics.h"

#include "simodo/dsl/Remote_NS_Chart.h"
#include "simodo/dsl/Remote_NS_Cockpit.h"
#include "simodo/dsl/Remote_NS_Graph3D.h"
#include "simodo/dsl/Remote_NS_Mechanoid.h"

#include "simodo/inout/convert/functions.h"
#include "simodo/lsp/CompletionItemKind.h"

#include <iostream>
#include <filesystem>
#include <future>
#include <algorithm>

namespace fs = std::filesystem;

using namespace simodo;
using namespace simodo::inout;

class AstStatisticalBuilder : public dsl::AstBuilder
{
    std::vector<dsl::Token>      _tokens;
    std::multimap<size_t,size_t> _line_to_token_index;


public:
    AstStatisticalBuilder(dsl::AReporter & reporter, 
                          dsl::AstNode & ast_root, 
                          const std::map<std::u16string,dsl::AstNode> & handlers)
        : dsl::AstBuilder(reporter, ast_root, handlers)
    {
        _tokens.reserve(1024);
    }

    virtual bool onTerminal(const dsl::Token & token) override 
    { 
        _line_to_token_index.emplace(token.getLocation().line-1, _tokens.size());
        _tokens.push_back(token);

        return dsl::AstBuilder::onTerminal(token); 
    }

    std::vector<dsl::Token> &      tokens()              { return _tokens; }
    std::multimap<size_t,size_t> & line_to_token_index() { return _line_to_token_index; }
};

/**
 * @brief Поставщик контента подменяет файлы модулей, если они загружены на сервере с клиента.
 * 
 * Кроме того, он полезен для сбора зависимостей. Зависимости позволят инициировать
 * пересборку модуля, если на клиенте были изменены модули, от которых он зависит.
 * 
 */
class ContentProvider : public simodo::dsl::FileContentProvider_interface
{
    ServerContext & _server;

    /// @brief имя_файла, содержимое файла
    std::map<std::string,std::u16string> _contents;

public:
    ContentProvider() = delete;
    ContentProvider(ServerContext & server) : _server(server) {}

    std::set<std::string> getDependencies() const 
    {
        std::set<std::string> dependencies;

        for(const auto & [file_name, content] : _contents)
            dependencies.insert(file_name);

        return dependencies;
    }

    virtual std::unique_ptr<dsl::IStream> stream(const std::string & file_name) override
    {
        auto it = _contents.find(file_name);
        if (it == _contents.end()) {
            std::u16string string_buffer;
            if (!_server.findAndCopy(file_name,string_buffer)) {
                _server.log().debug("Content '" + file_name + "' not found in server docs, load...");
                SIMODO_INOUT_CONVERT_STD_IFSTREAM(in, file_name);
                if (!in) 
                    return std::unique_ptr<dsl::IStream>();

                dsl::FileStream in_stream(in);

                for(;;) {
                    char16_t ch = in_stream.get(); 
                    if (ch == std::char_traits<char16_t>::eof())
                        break; 
                    string_buffer += ch;
                }
            }
            auto [it_content, ok] = _contents.try_emplace(file_name, string_buffer);
            if (!ok)
                return std::unique_ptr<dsl::IStream>();
            it = it_content;
        }

        return std::make_unique<dsl::BufferStream>(it->second.data());
    }
};


DocumentContext::DocumentContext(ServerContext & server, const variable::Record & textDocument_object)
    : _server(server)
    , _valid(false)
{
    open(textDocument_object);
}

bool DocumentContext::open(const variable::Record & textDocument_object)
{
    _valid = false;

    const variable::Value & uri_value = textDocument_object.find(u"uri");
    if (uri_value.type() != variable::ValueType::String)
        return false;

    const variable::Value & languageId_value = textDocument_object.find(u"languageId");
    if (languageId_value.type() != variable::ValueType::String)
        return false;

    const variable::Value & version_value = textDocument_object.find(u"version");
    if (version_value.type() != variable::ValueType::Int)
        return false;

    const variable::Value & text_value = textDocument_object.find(u"text");
    if (text_value.type() != variable::ValueType::String)
        return false;

    std::u16string text = convert::decodeSpecialChars(text_value.getString());

    std::unique_lock parse_locker(_parser.mutex);

    _parser.uri = convert::toU8(uri_value.getString());
    _parser.grammar_name = convert::toU8(languageId_value.getString());
    _parser.text.swap(text);
    _parser.version = version_value.getInt();

    if (!_server.gm().findGrammar(_parser.grammar_name) && !_server.gm().loadGrammar(false,_parser.grammar_name))
        return false;

    return _valid = _is_opened = true;
}

bool DocumentContext::change(const variable::Record & doc_params)
{
    if (!_is_opened)
        return false;

    const variable::Value & textDocument_value = doc_params.find(u"textDocument");
    if (textDocument_value.type() != variable::ValueType::Record)
        return false;

    std::shared_ptr<variable::Record> textDocument_object = textDocument_value.getRecord();
    const variable::Value & version_value = textDocument_object->find(u"version");
    if (version_value.type() != variable::ValueType::Int)
        return false;

    /// @note Заготовка для распределённых вычислений + вероятность асинхронности при работе 
    /// с пулом потоков. Не обращаем внимания на запоздавшие старые версии изменений.
    if (version_value.getInt() <= _parser.version)
        return false;

    const variable::Value & contentChanges_value = doc_params.find(u"contentChanges");
    if (contentChanges_value.type() != variable::ValueType::Array)
        return false;

    std::shared_ptr<variable::ValueArray> contentChanges = contentChanges_value.getArray();
    if (contentChanges->values().size() != 1 
     || contentChanges->values()[0].type() != variable::ValueType::Record)
        return false;

    std::shared_ptr<variable::Record> contentChangesEvent = contentChanges->values()[0].getRecord();
    const variable::Value & text_value = contentChangesEvent->find(u"text");
    if (text_value.type() != variable::ValueType::String)
        return false;

    std::u16string text = convert::decodeSpecialChars(text_value.getString());

    std::lock_guard parse_locker(_parser.mutex);

    _parser.text.swap(text);
    _parser.version = version_value.getInt();

    return true;
}

bool DocumentContext::close()
{
    if (!_is_opened)
        return false;
    _is_opened = false;
    return true;
}

bool DocumentContext::analyze()
{
    /// @todo Сделать Atomic
    if (!_valid)
        return false;
        
    ReportCombiner          m;
    std::u16string          path_to_file = convert::toU16(convertUriToPath(_parser.uri));
    std::unique_lock        parse_locker(_parser.mutex);
    dsl::AstNode            ast(path_to_file);
    dsl::BufferStream       input(_parser.text.data());
    const dsl::Grammar &    g = _server.gm().getGrammar(_parser.grammar_name);
    dsl::Parser             p(convert::toU8(path_to_file), m, g);
    AstStatisticalBuilder   ast_builder(m, ast, g.handlers);

    bool                    parse_ok = p.parse(input, ast_builder);

    _parser.line_to_token_index.swap(ast_builder.line_to_token_index());
    _parser.tokens.swap(ast_builder.tokens());

    size_t      doc_names_begin_index = 0;
    std::string grammar_name          = _parser.grammar_name;

    parse_locker.unlock();

    if (parse_ok) {
        std::vector<dsl::SemanticName>  name_set;
        std::vector<dsl::SemanticScope> scope_set;
        ContentProvider                 content_provider(_server);

        dsl::ScriptC_NS_Sys             sys(m);
        dsl::ScriptC_NS_Math            math;
        dsl::ScriptC_NS_TableFunction   tf;
        dsl::ScriptC_NS_Scene           scene;
        dsl::Remote_NS_Chart            chart(m);
        dsl::Remote_NS_Cockpit          cockpit(m);
        dsl::Remote_NS_Graph3D          graph3d(m);
        dsl::Remote_NS_Mechanoid        mech(m);

        std::vector<std::pair<std::u16string,dsl::IScriptC_Namespace*>> additional_namespaces {
            {u"chart", &chart},
            {u"cockpit", &cockpit},
            {u"graph3d", &graph3d},
            {u"mechanoid", &mech},
        };
        dsl::ModulesManagement  mm( m, m, 
                                    &_server.gm(), 
                                    _server.path_to_grammar(), 
                                    &content_provider, 
                                    additional_namespaces);

        {
            dsl::ScriptC_Semantics  checker(m, mm, name_set, scope_set);

            checker.importNamespace(u"sys",     sys.getNamespace());
            checker.importNamespace(u"math",    math.getNamespace());
            checker.importNamespace(u"tf",      tf.getNamespace());
            checker.importNamespace(u"scene",   scene.getNamespace());
            checker.importNamespace(u"chart",   chart.getNamespace());
            checker.importNamespace(u"cockpit", cockpit.getNamespace());
            checker.importNamespace(u"graph3d", graph3d.getNamespace());
            checker.importNamespace(u"mechanoid", mech.getNamespace());
            doc_names_begin_index = name_set.size();
            checker.check(ast);
        }

        std::multimap<size_t,size_t> line_to_name_index;

        for(size_t i=0; i < name_set.size(); ++i) {
            const dsl::SemanticName & name = name_set[i];
            if (name.depth >= dsl::DEPTH_TOP_LEVEL
            && (grammar_name != "diff" || !name.name.getLexeme().starts_with(u"__dt_"))) {
                if (name.definition.file_name == path_to_file)
                    line_to_name_index.emplace(name.definition.line-1, i);
                if (name.definition.end != name.name.getLocation().end
                && name.name.getLocation().file_name == path_to_file)
                    line_to_name_index.emplace(name.name.getLocation().line-1, i);
            }
            for(const dsl::TokenLocation & loc : name.references)
                if (loc.file_name == path_to_file)
                    line_to_name_index.emplace(loc.line-1, i);
        }

        std::unique_lock check_locker(_checker.mutex);

        _checker.path_to_file = path_to_file;
        _checker.doc_names_begin_index = doc_names_begin_index;
        std::set<std::string> dependencies = content_provider.getDependencies();
        _checker.dependencies.swap(dependencies);
        _checker.message_set.swap(m.messages());
        _checker.name_set.swap(name_set);
        _checker.scope_set.swap(scope_set);
        _checker.line_to_name_index.swap(line_to_name_index);
    }
    else {
        std::unique_lock check_locker(_checker.mutex);

        _checker.path_to_file.swap(path_to_file);
        _checker.message_set.swap(m.messages());
    }

    _server.sending().push(
        variable::json::Rpc {u"textDocument/publishDiagnostics", makeDiagnosticParams()} );

    // _server.log().debug("Analyze of '" + convert::toU8(_path_to_file) + "' completed.", 
    //                     "Name set size:         " + std::to_string(_name_set.size()) + "\n" + 
    //                     "Name usage index size: " + std::to_string(_line_to_name_index.size()) + "\n" +
    //                     "Dependencies size:     " + std::to_string(_dependencies.size()) + "\n" +
    //                     "Message set size:      " + std::to_string(_message_set.size()) + "\n" +
    //                     "Scope set size:        " + std::to_string(_scope_set.size()));

    return true;
}

void DocumentContext::copyContent(std::u16string & content)
{
    std::lock_guard parse_locker(_parser.mutex);
    content = _parser.text;
}

bool DocumentContext::checkDependency(const std::string & uri) 
{
    std::lock_guard check_locker(_checker.mutex);

    return _checker.dependencies.find(uri) != _checker.dependencies.end();
}

variable::Value DocumentContext::produceHoverInfo(const simodo::lsp::Position pos)
{
    std::function<bool(const dsl::TokenLocation & , int64_t , int64_t )> 
        checkLocation = [this](const dsl::TokenLocation & loc, int64_t line, int64_t character)
        {
            return  loc.file_name == _checker.path_to_file &&
                    loc.line == line+1 && 
                    character+1 >= loc.column && 
                    character+1 < loc.column + (loc.end-loc.begin);
        };
 
    std::lock_guard check_locker(_checker.mutex);
    auto            range = _checker.line_to_name_index.equal_range(static_cast<size_t>(pos.line));
    
    for(auto it = range.first; it != range.second; ++it) {
        size_t index = it->second;
        if (index < _checker.name_set.size()) {
            const dsl::SemanticName & name = _checker.name_set[index];
            const dsl::TokenLocation *ploc = nullptr;

            if (checkLocation(name.definition, pos.line, pos.character))
                ploc = &name.definition;
            else if (name.definition.end != name.name.getLocation().end
             && checkLocation(name.name.getLocation(), pos.line, pos.character))
                ploc = &name.name.getLocation();
            else
                for(const dsl::TokenLocation & loc : _checker.name_set[index].references)
                    if (checkLocation(loc, pos.line, pos.character)) {
                        ploc = &loc;
                        break;
                    }

            if (ploc)
                return variable::Record {{
                    {u"contents",   variable::Record {{
                        {u"kind",       u"plaintext"},
                        {u"value",      convert::encodeSpecialChars(makeHover_plaintext(index))},
                    }}},
                    {u"range",      makeRange(*ploc)},
                }};
        }
    }

    return {};
}

variable::Value DocumentContext::produceGotoDeclarationResult(const simodo::lsp::Position pos)
{
    size_t index = findSymbol(pos);
    if (index >= _checker.name_set.size())
        return {};

    const dsl::SemanticName & name = _checker.name_set[index];

    if (name.name.getLocation().file_name.empty())
        return {};

    return variable::Record {{
        {u"uri",    name.name.getLocation().file_name},
        {u"range",  makeRange(name.name.getLocation())},
    }};
}

variable::Value DocumentContext::produceGotoDefinitionResult(const simodo::lsp::Position pos)
{
    size_t index = findSymbol(pos);
    if (index >= _checker.name_set.size())
        return {};

    const dsl::SemanticName & name = _checker.name_set[index];

    dsl::TokenLocation loc = name.definition;

    if (loc.file_name.empty())
        loc = name.name.getLocation();

    if (loc.file_name.empty())
        return {};

    return variable::Record {{
        {u"uri",    loc.file_name},
        {u"range",  makeRange(loc)},
    }};
}

variable::Value DocumentContext::produceCompletionResult(const simodo::lsp::CompletionParams & completionParams)
{
    // Сначала проверяется наличие слева от курсора цепочки имён, разделённых триггерным 
    // символом

    /// @note Особенностью данного алгоритма является возможность построения цепочки имён, 
    /// даже если они находятся на разных строках текста и разделены пропусками или комментариями.

    std::deque<dsl::Token> name_chain;

    {
        // Массив токенов формируется в результате разбора исходного текста, 
        // поэтому он контролируется _parser_mutex
        std::lock_guard parse_locker(_parser.mutex); 

        auto    range      = _parser.line_to_token_index.equal_range(
                                            static_cast<size_t>(completionParams.position.line));
        size_t  last_index = _parser.tokens.size();

        // Просматриваем токены, содержащиеся в текущей строке, пока не найдём ближайший слева от 
        // заданной колонки
        for(auto it = range.first; it != range.second; ++it) {
            if (_parser.tokens[it->second].getLocation().column-1 >= completionParams.position.character)
                break;
            else
                last_index = it->second;
        }

        // Формируем цепочку идентификаторов, разделённых триггерным символом '.'
        if (last_index < _parser.tokens.size()) {
            // Ситуация, когда после точки начали вбивать имя члена класса.
            // Его не нужно включать в цепочку, т.к. вбиваемый текст является фильтром
            // и обрабатывается редактором
            if (_parser.tokens[last_index].getType() == dsl::LexemeType::Id) 
                last_index --;

            for(size_t i = last_index; i > 0 && i < _parser.tokens.size(); i-=2)
                if (_parser.tokens[i].getLexeme() != u"."
                 || _parser.tokens[i-1].getType() != dsl::LexemeType::Id)
                    break;
                else
                    name_chain.insert(name_chain.begin(),_parser.tokens[i-1]);

            _server.log().debug("Completion: Найдена цепочка из " + std::to_string(name_chain.size()) + " идентификаторов");
        }
    }

    if (!name_chain.empty()
    //  || completionParams.context.triggerKind == lsp::CompletionTriggerKind::TriggerCharacter
        ) {
        // Completion was triggered by a trigger character specified by the `triggerCharacters` 
        // properties of the `CompletionRegistrationOptions`.

        // Если найдена цепочка имён, значит нужно найти последнюю структуру в цепочке и
        // передать её элементы в качестве возможного завершения ввода.

        _server.log().debug("Completion: Режим поиска элементов структуры '" + convert::toU8(name_chain[0].getLexeme()) + "'");

        // Далее будет активно использоваться коллекция имён, полученная в результате 
        // семантического анализа, а также контейнеры с индексами на неё. Эти структуры 
        // контролируются _checker_mutex
        std::lock_guard checker_locker(_checker.mutex);

        // "Последняя структура"
        size_t owner_index = dsl::UNDEFINED_INDEX;

        // По первому токену (имени) в цепочке находим глобальную переменную, у которой 
        // нет владельца 
        for(size_t index=0; index < _checker.name_set.size(); ++index) {
            const dsl::SemanticName & n = _checker.name_set[index];
            // Имеет владельца - до свидания
            if (n.owner < _checker.name_set.size())
                continue;

            // Для глобальных модулей не задан файл определения, они всегда должны учитываться,
            // т.к. область видимости у них глобальная
            if (!n.name.getLocation().file_name.empty()) {
                // Нас интересуют только переменные, объявленные в заданном документе
                if (n.name.getLocation().file_name != _checker.path_to_file)
                    continue;

                // Переменные, которые не входят в зону видимости, которая нас интересует 
                // (такого быть не должно, но из-за синтаксических ошибок, структура токенов может
                // не соответствовать именам, т.к. при синтаксических ошибках семантический анализ
                // не выполняется)
                if (n.name.getLocation().line-1 > completionParams.position.line
                 || n.lower_scope.line < completionParams.position.line)
                    continue;

                // Случай, когда определена в той же строке, но где-то правее
                if (n.name.getLocation().line-1 == completionParams.position.line
                 && n.name.getLocation().column-1 > completionParams.position.character)
                    continue;

                // Случай, когда определена в той же строке, но зона видимости закончилась раньше
                // (закомментировал, т.к. возможна ситуация, когда мы раздвинули зону видимости 
                // новым (только что вбитым) текстом)
                // if (n.lower_scope.line == completionParams.position.line
                //  && n.lower_scope.column-1 < completionParams.position.character) 
                //     continue;
            }

            // Нашли?
            if (n.name.getLexeme() == name_chain[0].getLexeme()) {
                // Ситуация, когда внутри зоны видимости ранее найденной переменной
                // объявлена одноимённая. Нужно взять ту, у которой зона видимости ближе
                // к заданной позиции
                if (owner_index < _checker.name_set.size()
                 && _checker.name_set[owner_index].name.getLocation().line > n.name.getLocation().line)
                    ;
                else
                    owner_index = index;
            }
        }

        // Не удалось найти глобальную переменную по цепочке токенов - до свидания
        if (owner_index == dsl::UNDEFINED_INDEX)
            return {};

        // Проходим по цепочке токенов и определяем последнюю переменную, структуру которой нужно
        // передать для завершения
        for(size_t chain_index=1; chain_index < name_chain.size(); ++chain_index) {
            size_t i = owner_index+1;
            for(; i < _checker.name_set.size(); ++i) {
                if (_checker.name_set[i].owner != owner_index)
                    continue;

                if (_checker.name_set[i].name.getLexeme() == name_chain[chain_index].getLexeme()) {
                    owner_index = i;
                    break;
                }
            }

            if (i == _checker.name_set.size()
             || _checker.name_set[i].qualification == dsl::SemanticNameQualification::Function) {
                owner_index = dsl::UNDEFINED_INDEX;
                break;
            }
        }

        // Не смогли проследить цепочку - до свидания
        if (owner_index == dsl::UNDEFINED_INDEX)
            return {};

        std::vector<variable::Value> completion_items;

        for(size_t index=0; index < _checker.name_set.size(); ++index) {
            const dsl::SemanticName & n = _checker.name_set[index];

            if (n.owner != owner_index || n.name.getLexeme().substr(0,5) == u"__dt_")
                continue;

            completion_items.emplace_back(makeCompletionItem(index));
        }
        
        return completion_items;
    }

    if (completionParams.context.triggerKind == lsp::CompletionTriggerKind::Invoked) {
        // Completion was triggered by typing an identifier (24x7 code complete), 
        // manual invocation (e.g Ctrl+Space) or via API.

        // _server.log().debug("Completion: Режим поиска глобальных элементов");

        std::lock_guard checker_locker(_checker.mutex);

        std::vector<size_t> completion_indexes;

        for(size_t index=0; index < _checker.name_set.size(); ++index) {
            const dsl::SemanticName & n = _checker.name_set[index];
            if (n.owner < _checker.name_set.size())
                continue;

            if (!n.name.getLocation().file_name.empty()) {
                if (n.name.getLocation().file_name != _checker.path_to_file)
                    continue;

                if (n.name.getLocation().line-1 > completionParams.position.line
                 || n.lower_scope.line < completionParams.position.line)
                    continue;

                if (n.name.getLocation().line-1 == completionParams.position.line
                 && n.name.getLocation().column-1 > completionParams.position.character)
                    continue;

                // if (n.lower_scope.line == completionParams.position.line
                //  && n.lower_scope.column-1 < completionParams.position.character) 
                //     continue;
            }

            completion_indexes.push_back(index);
        }

        // Удаляем дубли
        for(size_t i=0; i < completion_indexes.size(); ++i)
            if (completion_indexes[i] < _checker.name_set.size())
                for(size_t j=i+1; j < completion_indexes.size(); ++j)
                    if (completion_indexes[j] < _checker.name_set.size()) {
                        size_t left  = completion_indexes[i];
                        size_t right = completion_indexes[j];
                        if (_checker.name_set[left].name.getLexeme() == _checker.name_set[right].name.getLexeme()) {
                            if (_checker.name_set[left].name.getLocation().begin < _checker.name_set[right].name.getLocation().begin) {
                                completion_indexes[i] = dsl::UNDEFINED_INDEX;
                                break;
                            }
                            else
                                completion_indexes[j] = dsl::UNDEFINED_INDEX;
                        }
                    }

        std::vector<variable::Value> completion_items;

        for(size_t i=0; i < completion_indexes.size(); ++i)
            if (completion_indexes[i] < _checker.name_set.size())
                completion_items.emplace_back(makeCompletionItem(completion_indexes[i]));

        std::vector<std::u16string> keywords {
            u"using", u"import", u"type",
            u"break", u"continue", u"return", u"if", u"else", u"while", u"do", u"for",
        };
        for(const std::u16string & w : keywords)
            completion_items.emplace_back(makeCompletionItem(w, u"keyword", u"", 14));

        std::vector<std::u16string> builtin_types {
            u"int", u"bool", u"float", u"void", u"auto", u"string",
        };
        for(const std::u16string & w : builtin_types)
            completion_items.emplace_back(makeCompletionItem(w, u"builtin", u"", 1));

        std::vector<std::u16string> constants {
            u"true", u"false",
        };
        for(const std::u16string & w : constants)
            completion_items.emplace_back(makeCompletionItem(w, u"constant", u"", 21));

        return completion_items;
    }

    _server.log().warning("Completion mode " 
                    + std::to_string(static_cast<int>(completionParams.context.triggerKind)) 
                    + " unsupported");

    return {};
}

variable::Value DocumentContext::produceSemanticTokens()
{
    std::function<int64_t(const dsl::SemanticName & )> 
        makeTokenType = [](const dsl::SemanticName & name) 
        {
            switch(name.qualification) {
            case dsl::SemanticNameQualification::Tuple:
                return 1;
            case dsl::SemanticNameQualification::Function:
                return (name.depth == dsl::DEPTH_TOP_LEVEL) ? 5 : 4;
            case dsl::SemanticNameQualification::Type:
                return 0;
            default:
                return (name.depth == dsl::DEPTH_TOP_LEVEL) ? 3 : 2;
            }
        };
    std::function<dsl::SemanticFlags_t (const dsl::SemanticName & )> 
        makeTokenModifiers = [this](const dsl::SemanticName & name)
        {
            int64_t tokent_modifiers = 0;

            if (name.access == dsl::SemanticNameAccess::ReadOnly)
                tokent_modifiers |= 0b00000100; // +readonly
            if (name.qualification == dsl::SemanticNameQualification::Array)
                tokent_modifiers |= 0b00001000; // +array
            if (name.owner < _checker.name_set.size()
                && _checker.name_set[name.owner].qualification == dsl::SemanticNameQualification::Function)
                tokent_modifiers |= 0b00010000; // +parameter
            if (!name.name.getLocation().file_name.empty())
                tokent_modifiers |= 0b10000000; // +anchor

            dsl::SemanticFlags_t flags = bringFlags(name);
            if (flags & dsl::SemanticFlag_Input)
                tokent_modifiers |= 0b00100000; // +input
            if (flags & dsl::SemanticFlag_Output)
                tokent_modifiers |= 0b01000000; // +output

            return tokent_modifiers;
        };

    struct TokenInfo { int64_t length, type, modifiers; };
    struct TokenComp {
        bool operator() (const std::pair<int64_t,int64_t> x1, const std::pair<int64_t,int64_t> x2) const {
            return x1.first < x2.first || (x1.first == x2.first && x1.second < x2.second);
        }
    };    

    std::map<std::pair<int64_t,int64_t>, TokenInfo, TokenComp> tokens;

    std::unique_lock check_locker(_checker.mutex);

    for(auto [line,index] : _checker.line_to_name_index) {
        if (index >= _checker.name_set.size())
            continue;

        const dsl::SemanticName & name = _checker.name_set[index];

        for(const dsl::TokenLocation & loc : _checker.name_set[index].references)
            if (loc.file_name == _checker.path_to_file && loc.end != loc.begin) {
                int64_t tokent_modifiers = makeTokenModifiers(name);
                tokens.try_emplace(
                    std::make_pair(static_cast<int64_t>(loc.line-1), static_cast<int64_t>(loc.column-1))
                    , TokenInfo{static_cast<int64_t>(loc.end-loc.begin), makeTokenType(name), tokent_modifiers}
                );
            }

        if (name.definition.file_name == _checker.path_to_file) {
            const dsl::TokenLocation & loc = name.definition;
            if (loc.end != loc.begin) {
                int64_t tokent_modifiers = 2 + makeTokenModifiers(name); // +definition
                tokens.try_emplace(
                    std::make_pair(static_cast<int64_t>(loc.line-1), static_cast<int64_t>(loc.column-1))
                    , TokenInfo{static_cast<int64_t>(loc.end-loc.begin), makeTokenType(name), tokent_modifiers }
                );
            }
        }

        if (name.definition.end != name.name.getLocation().end
         && name.name.getLocation().file_name == _checker.path_to_file) {
            const dsl::TokenLocation & loc = name.name.getLocation();
            if (loc.end != loc.begin) {
                int64_t tokent_modifiers = 1 + makeTokenModifiers(name); // +declaration
                tokens.try_emplace(
                    std::make_pair(static_cast<int64_t>(loc.line-1), static_cast<int64_t>(loc.column-1))
                    , TokenInfo{static_cast<int64_t>(loc.end-loc.begin), makeTokenType(name), tokent_modifiers }
                );
            }
        }
    }

    check_locker.unlock();

    /// @todo Отсортированный массив токенов позволит сделать работу с дельтами, 
    /// если в этом будет необходимость.

    std::vector<variable::Value> sem_tokens;

    for(const auto & [p,t] : tokens) {
        sem_tokens.push_back(p.first);
        sem_tokens.push_back(p.second);
        sem_tokens.push_back(t.length);
        sem_tokens.push_back(t.type);
        sem_tokens.push_back(t.modifiers);
    }
    
    return variable::Record {{{u"data", sem_tokens}}};
}

variable::Value DocumentContext::produceDocumentSymbols()
{
    std::lock_guard check_locker(_checker.mutex);

    if (_checker.name_set.empty() || _checker.doc_names_begin_index > _checker.name_set.size())
        return {};

    std::vector<variable::Value> doc_symbols;

    for(size_t i=_checker.doc_names_begin_index; i < _checker.name_set.size(); ++i) {
        const dsl::SemanticName & name = _checker.name_set[i];
        if (name.definition.file_name == _checker.path_to_file
         && name.owner >= _checker.name_set.size()
         && name.depth == dsl::DEPTH_TOP_LEVEL
         && name.name.getLexeme() != u"__E"
         && name.name.getLexeme() != u"__ODE") {
            if (name.qualification == dsl::SemanticNameQualification::Function) {
                size_t scope_set_index_candidate = _checker.scope_set.size();
                for(size_t j=0; j < _checker.scope_set.size(); ++j) {
                    const auto & [begin_loc,end_loc] = _checker.scope_set[j];
                    if (begin_loc.file_name == _checker.path_to_file
                    && begin_loc.begin >= name.definition.begin) {
                        if (scope_set_index_candidate == _checker.scope_set.size()
                            || _checker.scope_set[scope_set_index_candidate].first.begin > begin_loc.begin)
                            scope_set_index_candidate = j;
                    }
                }
                if (scope_set_index_candidate < _checker.scope_set.size()) {
                    std::shared_ptr<variable::Record> name_scope_range = makeRange(
                                            {name.definition.line-1, 
                                             name.definition.column-1},
                                            {_checker.scope_set[scope_set_index_candidate].second.line-1,
                                             _checker.scope_set[scope_set_index_candidate].second.column-1+1});
                    std::shared_ptr<variable::Record> name_range = makeRange(name.definition);

                    doc_symbols.emplace_back(variable::Record {{
                        {u"name",           name.name.getLexeme()},
                        {u"detail",         makeDeclaration_plaintext(i)},
                        {u"kind",           makeSymbolKind(name)},
                        {u"range",          name_scope_range},
                        {u"selectionRange", name_range},
                    }});
                }
            }
            else {
                std::u16string       inout_str;
                dsl::SemanticFlags_t flags = bringFlags(name);

                if ((flags & dsl::SemanticFlag_Input) == dsl::SemanticFlag_Input)
                    inout_str = u"in";
                if ((flags & dsl::SemanticFlag_Output) == dsl::SemanticFlag_Output) {
                    if (!inout_str.empty())
                        inout_str += u"/";
                    inout_str += u"out";
                }
                if (!inout_str.empty())
                    inout_str = u" >> " + inout_str;

                doc_symbols.emplace_back(variable::Record {{
                    {u"name",           makeName(i)},
                    {u"detail",         makeDeclaration_plaintext(i) + inout_str},
                    {u"kind",           makeSymbolKind(name)},
                    {u"range",          makeRange(name.definition)},
                    {u"selectionRange", makeRange(name.definition)},
                }});
            }
        }
    }
    
    return variable::ValueArray {doc_symbols};
}

// privates:

std::u16string DocumentContext::makeName(size_t index) const
{
    if (index >= _checker.name_set.size())
        return u"";

    // if (_grammar_name == "diff" && _name_set[index].name.getLexeme().starts_with(u"__dt_"))
    //     return _name_set[index].name.getLexeme().substr(5);

    return _checker.name_set[index].name.getLexeme();
}

std::u16string DocumentContext::makeType_plaintext(size_t index, const std::u16string &on_None_type_name) const
{
    if (index >= _checker.name_set.size())
        return u"";

    const dsl::SemanticName & name = _checker.name_set[index];
    std::u16string            type_string;

    if (name.access != dsl::SemanticNameAccess::FullAccess)
        type_string = dsl::getSemanticNameAccessName(name.access) + u" ";
    
    if (name.qualification == dsl::SemanticNameQualification::Scalar)
        type_string += dsl::getSemanticNameTypeName(name.type);
    else if (name.qualification != dsl::SemanticNameQualification::Function) {
        if (name.qualification == dsl::SemanticNameQualification::None)
            type_string += on_None_type_name;
        else if (name.type_index < _checker.name_set.size()) {
            type_string += _checker.name_set[name.type_index].name.getLexeme();
        }
        else
            type_string += dsl::getSemanticNameQualificationName(name.qualification);
    }
    return type_string;
}

std::u16string DocumentContext::makeDeclaration_plaintext(size_t index, bool include_owners) const
{
    if (index >= _checker.name_set.size())
        return u"";
        
    const dsl::SemanticName & name        = _checker.name_set[index];
    std::u16string            declaration = makeType_plaintext(index,u"void");

    std::u16string name_path;
    if (include_owners
     && name.owner < _checker.name_set.size() 
     && _checker.name_set[name.owner].qualification != dsl::SemanticNameQualification::Function) {
        size_t owner_index = name.owner;

        while(owner_index < _checker.name_set.size()) {
            if (!name_path.empty())
                name_path = u"." + name_path;
            name_path = _checker.name_set[owner_index].name.getLexeme() + name_path;
            owner_index = _checker.name_set[owner_index].owner;
        }

        name_path += u"." + name.name.getLexeme(); 
    }
    else
        name_path = makeName(index); 

    if (name.qualification == dsl::SemanticNameQualification::Function
     && index+1 < _checker.name_set.size()) {
        std::u16string args;
        for(size_t i=index+2; i < _checker.name_set.size() && _checker.name_set[i].owner == index; ++i) {
            if (!args.empty())
                args += u", ";

            args += makeType_plaintext(i,u"any");
            if (!_checker.name_set[i].name.getLexeme().empty())
                args += u" " + _checker.name_set[i].name.getLexeme();
        }
        declaration += makeType_plaintext(index+1,u"void") + u" " + name_path + u"(" + args + u")";
    }
    else {
        declaration += u" " + name_path;

        // if (_grammar_name == "diff" && _name_set[index].name.getLexeme().starts_with(u"__dt_"))
        //     declaration += u"'";
    }

    return declaration;
}

std::u16string DocumentContext::makeHover_plaintext(size_t index) const
{
    if (index >= _checker.name_set.size())
        return u"";
        
    const dsl::SemanticName & name  = _checker.name_set[index];
    std::u16string            hover = makeDeclaration_plaintext(index,true);
    dsl::SemanticFlags_t      flags = bringFlags(name);
    
    if (name.owner < _checker.name_set.size())
        hover += u"\nOwner: " + makeDeclaration_plaintext(name.owner,false);

    if ((flags & dsl::SemanticFlag_Input) == dsl::SemanticFlag_Input)
        hover += u"\nModule input variable";
    
    if ((flags & dsl::SemanticFlag_Output) == dsl::SemanticFlag_Output)
        hover += u"\nModule output variable";

    return hover;
}

variable::Value DocumentContext::makeDiagnosticParams() const
{
    std::vector<variable::Value> diagnostics;

    int i = 0;
    for(const MessageFullContent & m : _checker.message_set) {
        if (m.location.file_name == _checker.path_to_file) {
            if (m.level >= dsl::SeverityLevel::Error &&  i++ == 50) {
                /// @todo _parser?
                _server.log().warning("Too many errors produced by analyze for '" + _parser.uri + "'");
                break;
            }
            diagnostics.emplace_back(variable::Record {{
                                {u"range",      makeRange(m.location)},
                                {u"severity",   makeSeverity(m.level)},
                                {u"source",     u"SIMODO/stars parser"},
                                {u"message",    makeMessage(m)},
                            }});
        }
    }

    std::shared_ptr<variable::Record> res = std::make_shared<variable::Record>();

    /// @todo _parser?
    res->variables().emplace_back(u"uri",         convert::toU16(_parser.uri));
    /// @todo _parser?
    res->variables().emplace_back(u"version",     _parser.version);
    res->variables().emplace_back(u"diagnostics", variable::ValueArray {diagnostics});

    return res;
}

int64_t DocumentContext::makeCompletionItemKind(size_t index) const
{
    if (index >= _checker.name_set.size())
        return 0;
        
    const dsl::SemanticName & name = _checker.name_set[index];

    switch(name.qualification) {
    case dsl::SemanticNameQualification::Tuple:
        return 22;
    case dsl::SemanticNameQualification::Function:
        return 3;
    case dsl::SemanticNameQualification::Type:
        return 9;
    default:
        return 6;
    }
}

simodo::variable::Value DocumentContext::makeCompletionItem(
            const std::u16string & name, 
            const std::u16string & detail, 
            const std::u16string & description, 
            int64_t kind) const
{
    return variable::Record {{
                {u"label",          name},
                {u"labelDetails",   variable::Record {{
                    {u"detail",         detail},
                    {u"description",    description},
                }}},
                {u"kind",           kind},
            }};
}

simodo::variable::Value DocumentContext::makeCompletionItem(size_t index) const
{
    if (index >= _checker.name_set.size())
        return {};

    dsl::SemanticFlags_t flags       = bringFlags(_checker.name_set[index]);
    std::u16string       description = u"";
    
    if ((flags & dsl::SemanticFlag_Input) == dsl::SemanticFlag_Input)
        description = u"In";
    
    if ((flags & dsl::SemanticFlag_Output) == dsl::SemanticFlag_Output) {
        if (!description.empty()) description += u"/";
        description += u"Out";
    }

    return makeCompletionItem(_checker.name_set[index].name.getLexeme(),
                              makeDeclaration_plaintext(index),
                              description,
                              makeCompletionItemKind(index));
    
}

size_t DocumentContext::findSymbol(const simodo::lsp::Position pos) const
{
    std::function<bool(const dsl::TokenLocation & , int64_t , int64_t )> 
        checkLocation = [this](const dsl::TokenLocation & loc, int64_t line, int64_t character)
        {
            return  loc.file_name == _checker.path_to_file &&
                    loc.line == line+1 && 
                    character+1 >= loc.column && 
                    character+1 < loc.column + (loc.end-loc.begin);
        };
 
    std::lock_guard check_locker(_checker.mutex);
    auto            range = _checker.line_to_name_index.equal_range(static_cast<size_t>(pos.line));
    
    for(auto it = range.first; it != range.second; ++it) {
        size_t index = it->second;
        if (index < _checker.name_set.size()) {
            const dsl::SemanticName & name = _checker.name_set[index];

            if (checkLocation(name.definition, pos.line, pos.character))
                return index;
            else if (name.definition.end != name.name.getLocation().end
             && checkLocation(name.name.getLocation(), pos.line, pos.character))
                return index;
            else
                for(const dsl::TokenLocation & loc : _checker.name_set[index].references)
                    if (checkLocation(loc, pos.line, pos.character)) 
                        return index;
        }
    }

    return dsl::NOT_FOUND_INDEX;
}

std::shared_ptr<variable::Record> 
    DocumentContext::makeRange(const dsl::TokenLocation & loc)
{
    return makeRange({loc.line-1,loc.column-1},{loc.line-1,loc.column-1+(loc.end-loc.begin)});
}

std::shared_ptr<variable::Record> 
    DocumentContext::makeRange(std::pair<int64_t, int64_t> start, std::pair<int64_t, int64_t> end)
{
    std::shared_ptr<variable::Record> res = std::make_shared<variable::Record>();

    res->variables().emplace_back(u"start", makePosition(start.first,start.second));
    res->variables().emplace_back(u"end", makePosition(end.first, end.second));

    return res;
}

std::shared_ptr<simodo::variable::Record> 
    DocumentContext::makePosition(int64_t line, int64_t character)
{
    std::shared_ptr<variable::Record> res = std::make_shared<variable::Record>();

    res->variables().emplace_back(u"line", line);
    res->variables().emplace_back(u"character", character);

    return res;
}                            

int64_t DocumentContext::makeSeverity(dsl::SeverityLevel level)
{
    int64_t res;
    switch(level)
    {
    case dsl::SeverityLevel::Fatal:
    case dsl::SeverityLevel::Error:
        res = 1; // Error 
        break;
    case dsl::SeverityLevel::Warning:
        res = 2; // Warning 
        break;
    default:   
        res = 3; // Information 
        break;
    }
    return res;
}

std::u16string DocumentContext::makeMessage(const MessageFullContent & message)
{
    std::u16string res {message.briefly};

    // if (!message.atlarge.empty())
    //     res += u'\n' + message.atlarge;

    return convert::encodeSpecialChars(res);
}

int64_t DocumentContext::makeSymbolKind(const dsl::SemanticName & name)
{
    switch(name.qualification)
    {
    case dsl::SemanticNameQualification::Scalar:
        return 13; // Variable
    case dsl::SemanticNameQualification::Function:
        return 12; // Function
    case dsl::SemanticNameQualification::Array:
        return 18; // Array
    case dsl::SemanticNameQualification::Tuple:
        return 19; // Object
    case dsl::SemanticNameQualification::Type:
        return 5;  // Class
    default:
        return 21; // Null
    }
}

dsl::SemanticFlags_t DocumentContext::bringFlags(const dsl::SemanticName & name) const
{
    if (name.depth != dsl::DEPTH_TOP_LEVEL
     || name.qualification != dsl::SemanticNameQualification::Scalar)
        return 0;

    // if (_grammar_name == "diff" && name.name.getLexeme().starts_with(u"__dt_")) {
    //     std::u16string decl_name_string = name.name.getLexeme().substr(5);
    //     for(size_t i=_doc_names_begin_index; i < _name_set.size(); ++i) {
    //         const dsl::SemanticName & decl = _name_set[i];
    //         if (decl.name.getLexeme() == decl_name_string) {
    //             if (decl.definition.file_name.empty()
    //              && decl.qualification == dsl::SemanticNameQualification::Scalar
    //              && decl.owner >= _name_set.size()
    //              && decl.depth == dsl::DEPTH_TOP_LEVEL) 
    //                 return decl.semantic_flags;
    //         }
    //     }
    // }

    return name.semantic_flags;
}

std::string DocumentContext::convertUriToPath(const std::string & uri)
{
    return uri;
}
