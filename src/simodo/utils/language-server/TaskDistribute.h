#ifndef TaskDistribute_h
#define TaskDistribute_h

#include "simodo/tp/Task_interface.h"

#include <string>

class ServerContext;

class TaskDistribute : public simodo::tp::Task_interface
{
    ServerContext & _server;
    std::string     _jsonrpc_content;

public:
    TaskDistribute() = delete;
    TaskDistribute(ServerContext & server, const std::string &jsonrpc_content)
        : _server(server), _jsonrpc_content(jsonrpc_content)
    {}

    virtual void work() override;
};

#endif // TaskDistribute_h