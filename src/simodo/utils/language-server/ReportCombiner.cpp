#include "ReportCombiner.h"

#include <algorithm>

void ReportCombiner::report(const simodo::dsl::SeverityLevel level,
                    simodo::dsl::TokenLocation location,
                    const std::u16string & briefly,
                    const std::u16string & atlarge)
{
    if (_messages.end() != std::find_if(_messages.begin(), _messages.end(),
                                [location,briefly](const MessageFullContent & cont){
                                    return cont.location.file_name == location.file_name
                                     && cont.briefly == briefly;
                                }))
        return;

    _messages.emplace_back(level,location, briefly,atlarge);
}
