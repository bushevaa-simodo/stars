#include "simodo/inout/log/Logger.h"

#include <mutex>

namespace simodo::inout::log
{

std::string Logger::toString(SeverityLevel level)
{
    std::string res;
    switch(level)
    {
    case SeverityLevel::Debug:
        res = "DEBUG";
        break;        
    case SeverityLevel::Info:
        res = "INFO";
        break;        
    case SeverityLevel::Warning:
        res = "WARNING";
        break;        
    case SeverityLevel::Error:
        res = "ERROR";
        break;        
    case SeverityLevel::Critical:
        res = "CRITICAL";
        break;        
    }
    return res;
}

void Logger::output(SeverityLevel level, const std::string & message, const std::string & context)
{
    if (level >= _min_level) {
        static std::mutex logger_output_mutex;
        std::lock_guard   locker(logger_output_mutex);
        _out << toString(level) << ':' << _logger_name << ": " << message << std::endl;
        if (!context.empty())
            _out << context << std::endl;
    }
}

}
