/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/convert/functions.h"

#include <locale>
#include <codecvt>

namespace simodo::inout::convert
{
#ifdef CROSS_WIN
    /*!
     * \brief Функция переводит строку UTF-8 в wchar_t
     * \param str     Строка UTF-8
     * \return        Строка wchar_t
     */
    std::wstring fromUtf8CharToWChar(const std::string & str)
    {
        std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
        return myconv.from_bytes(str);
    }

    /*!
     * \brief Функция переводит строку wchar_t в UTF-8
     * \param str     Строка wchar_t
     * \return        Строка UTF-8
     */
    std::string fromWCharToUtf8Char(const std::wstring & str)
    {
        std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
        return myconv.to_bytes(str);
    }
#endif

std::u16string toU16(const std::string &str)
{
    std::wstring_convert<std::codecvt_utf8_utf16<char16_t>,char16_t> convert("(*сбой*)",u"(*сбой*)");

    return convert.from_bytes(str);
}

std::string toU8(const std::u16string &str)
{
    std::wstring_convert<std::codecvt_utf8_utf16<char16_t>,char16_t> convert("(*сбой*)",u"(*сбой*)");

    return convert.to_bytes(str);
}

std::string clearNumberFractionalPart(std::string s)
{
    auto dot_pos = s.find_first_of('.');

    if (dot_pos == std::string::npos)
        dot_pos = s.find_first_of(',');

    if (dot_pos != std::string::npos)
    {
        auto not_0_pos = s.find_last_not_of('0');
        if (not_0_pos != std::string::npos)
        {
            s.resize(not_0_pos + (not_0_pos == dot_pos ? 2 : 1));
        }
    }

    return s;
}
}

