/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/token/Tokenizer.h"

#include <cassert>


namespace
{
    std::vector<simodo::inout::token::Tokenizer::_NumberMask> makeInnerMask(const std::vector<simodo::inout::token::NumberMask> &mask_set)
    {
        std::vector<simodo::inout::token::Tokenizer::_NumberMask> result;

        for (const simodo::inout::token::NumberMask & mask : mask_set) {
            if (mask.chars == simodo::inout::token::BUILDING_NUMBER)
            {
                int sh = result.size();

    /* 00 */    result.push_back({u"N", mask.type, simodo::inout::token::TokenQualification::Integer, mask.system, {sh+1,sh+3,sh+4}, true, true});
    /* 01 */    result.push_back({u".", mask.type, simodo::inout::token::TokenQualification::RealNumber, mask.system, {sh+2,sh+3,sh+4}, false, true});
    /* 02 */    result.push_back({u"N", mask.type, simodo::inout::token::TokenQualification::RealNumber, mask.system, {sh+3,sh+4}, false, true});
    /* 03 */    result.push_back({u"e", mask.type, simodo::inout::token::TokenQualification::RealNumber, mask.system, {sh+5,sh+6,sh+7}, false, false});
    /* 04 */    result.push_back({u"E", mask.type, simodo::inout::token::TokenQualification::RealNumber, mask.system, {sh+5,sh+6,sh+7}, false, false});
    /* 05 */    result.push_back({u"+", mask.type, simodo::inout::token::TokenQualification::RealNumber, mask.system, {sh+7}, false, false});
    /* 06 */    result.push_back({u"-", mask.type, simodo::inout::token::TokenQualification::RealNumber, mask.system, {sh+7}, false, false});
    /* 07 */    result.push_back({u"N", mask.type, simodo::inout::token::TokenQualification::RealNumber, mask.system, {}, false, true});
            }
            else
                result.push_back({mask.chars, mask.type, simodo::inout::token::TokenQualification::Integer, mask.system, {}, true, true});
        }

        return result;
    }
}

simodo::inout::token::Tokenizer::Tokenizer(const std::u16string &file_name,
                     InputStream_interface &input_stream,
                     const LexicalParameters & parameters,  // TODO разкомментировать
                    //  LexicalParameters & parameters, // TODO закомментировать
                     uint8_t tab_size,
                     context_index_t context_no)
    : scanner(file_name,input_stream,tab_size,context_no)
    , param(parameters)
{
    assert(!param.digits.empty());
    assert(!param.latin_alphabet.empty());
//    assert(!param.national_alphabet.empty());
    assert(param.latin_alphabet.size()%2 == 0);
    assert(param.national_alphabet.size()%2 == 0);

    // TODO для дебагинга
    // parameters.masks.push_back({ u"0[1[2]][{3[4]|5[6]}[{{7}[8]}]9]", LexemeType::Number, 10 });
    // parameters.masks.push_back({ u"123{4|5}", LexemeType::Number, 10 });

    numbers = makeInnerMask(parameters.masks);
}

simodo::inout::token::Token simodo::inout::token::Tokenizer::getToken()
{
    Token t = getAnyToken();

    while(t.type == LexemeType::Comment)
        t = getAnyToken();

    return t;
}

simodo::inout::token::Token simodo::inout::token::Tokenizer::getAnyToken()
{
    if (scanner.eof()) {
        return { {u"", LexemeType::Empty}, u"", scanner.getLocation() };
    }

    // Проверяем контекст
    if (scanner.getLocation().context != NO_TOKEN_CONTEXT_INDEX)
    {
        scanner.fixLocation(scanner.getLocation().context);
        return scanMarkup(scanner.getLocation().context);
    }

    // Пропускаем пробелы
    passBlanks();

    // Фиксируем координаты начала токена
    scanner.fixLocation();

    if (scanner.eof()) {
        return { {u"", LexemeType::Empty}, u"", scanner.getLocation() };
    }

    // Символ новой строки может быть заменён на спец символ
    if (!param.nl_substitution.empty() && scanner.getChar() == '\n') {
        scanner.shift(1);
        return { {param.nl_substitution, LexemeType::NewLine}, param.nl_substitution, scanner.getLocation() };
    }

    // Маркированный текст
    for(size_t i=0; i < param.markups.size(); ++i) {
        const MarkupSymbol & mus = param.markups[i];

        if (scanner.startsWith(mus.start))
            return scanMarkup(static_cast<uint32_t>(i));
    }

    // Слово (переменная, идентификатор и пр.)
    if (scanner.startsWithAnyOf(param.id_extra_symbols))
        return scanWord(NationalCharAffiliation::Extra);

    // Слово (переменная, идентификатор и пр.)
    if (scanner.startsWithAnyOf(param.latin_alphabet))
        return scanWord(NationalCharAffiliation::Latin);

    // Слово (переменная, идентификатор и пр.)
    if (scanner.startsWithAnyOf(param.national_alphabet))
        return scanWord(NationalCharAffiliation::National);

    // Число?
    {
        LexemeType          type;
        TokenQualification  qualification;
        std::u16string      lexeme_str;

        if (scanNumber(type,qualification,lexeme_str))
            return { {lexeme_str, type}, lexeme_str, scanner.getLocation(), qualification };
    }

    // Многосимвольная пунктуация (не ключевое слово)
    for(const std::u16string & s : param.punctuation_words)
        if (scanner.startsWith(s))
        {
            if (s == param.eof_symbol)
                scanner.setEOF();

            scanner.shift(s.size());

            return { {s, LexemeType::Punctuation}, s, scanner.getLocation() };
        }

    // Односимвольная пунктуация
    if (scanner.startsWithAnyOf(param.punctuation_chars))
    {
        std::u16string s;
        s.assign(1, scanner.getFirstChar());

        if (s == param.eof_symbol)
            scanner.setEOF();

        scanner.shift(1);

        return { {s, LexemeType::Punctuation}, s, scanner.getLocation() };
    }

    // Что-то неизвестное, т.е. ошибка
    std::u16string lexeme_str;

    lexeme_str += scanner.getFirstChar();
    scanner.shift(1);

    return { {lexeme_str, LexemeType::Error}, lexeme_str, scanner.getLocation(), TokenQualification::UnknownCharacterSet };
}

void simodo::inout::token::Tokenizer::passBlanks()
{
    while(!scanner.eof())
    {
        if (!param.nl_substitution.empty() && scanner.getChar() == '\n')
            break;
        if (!isBlank(scanner.getChar()))
            break;

        scanner.shift(1);
    }
}

simodo::inout::token::Token simodo::inout::token::Tokenizer::scanMarkup(context_index_t markup_index)
{
    assert(markup_index < param.markups.size());

    std::u16string lexeme_str;
    std::u16string token_str;

    const MarkupSymbol & mus     = param.markups[markup_index];
    context_index_t      context = markup_index;

    if (scanner.getLocation().context != markup_index)
    {
        scanner.shift(mus.start.size());
        token_str += mus.start;
    }

    while(!scanner.eof())
    {
        if (scanner.startsWith(mus.ignore_sign))
        {
            token_str += mus.ignore_sign;
            scanner.shift(mus.ignore_sign.size());

            if (scanner.eof())
                break;

            token_str += scanner.getFirstChar();
            lexeme_str += scanner.getFirstChar();
            scanner.shift(1);
        }
        else
        {
            if (mus.end.empty())
            {
                if (scanner.getFirstChar() == u'\n')
                    break;
            }
            else if (scanner.startsWith(mus.end))
            {
                token_str += mus.end;
                scanner.shift(mus.end.size());
                context = NO_TOKEN_CONTEXT_INDEX;
                break;
            }

            token_str += scanner.getFirstChar();
            lexeme_str += scanner.getFirstChar();
            scanner.shift(1);
        }
    }

    if (mus.end.empty())
        context = NO_TOKEN_CONTEXT_INDEX;

    TokenLocation loc = scanner.getLocation();
    loc.context = context;

    scanner.fixLocation(context);

    return { {lexeme_str, mus.type}, token_str, loc, TokenQualification::None, markup_index };
}

simodo::inout::token::Token simodo::inout::token::Tokenizer::scanWord(Tokenizer::NationalCharAffiliation first_char)
{
    bool    has_latin    = (first_char == NationalCharAffiliation::Latin);
    bool    has_national = (first_char == NationalCharAffiliation::National);

    std::u16string lexeme_str;

    lexeme_str += scanner.getFirstChar();
    scanner.shift(1);

    // Формируем лексему
    while(!scanner.eof())
    {
        if (scanner.startsWithAnyOf(param.id_extra_symbols))
            ;
        else if (scanner.startsWithAnyOf(param.latin_alphabet))
            has_latin = true;
        else if (scanner.startsWithAnyOf(param.national_alphabet))
            has_national = true;
        else if (!scanner.startsWithAnyOf(param.digits))
            break;

        lexeme_str += scanner.getFirstChar();
        scanner.shift(1);
    }

    // Многосимвольная пунктуация
    for(const std::u16string & s : param.punctuation_words)
    {
        bool is_find;

        if (param.is_case_sensitive)
            is_find = (s == lexeme_str);
        else
            is_find = (s == convertToUpper(lexeme_str));

        if (is_find)
        {
            if (s == param.eof_symbol)
                scanner.setEOF();

            return { {lexeme_str, LexemeType::Punctuation}, s, scanner.getLocation(), TokenQualification::Keyword };
        }
    }

    // Односимвольная пунктуация
    if (lexeme_str.size() == 1)
        if (param.punctuation_chars.find(*lexeme_str.c_str()) != std::u16string::npos)
            return { {lexeme_str, LexemeType::Punctuation}, lexeme_str, scanner.getLocation(), TokenQualification::Keyword};

    if (has_national)
    {
        if (!param.may_national_letters_use)
            return { {lexeme_str, LexemeType::Error}, lexeme_str, scanner.getLocation(), TokenQualification::NationalCharacterUse };

        if (has_latin)
            return { {lexeme_str, param.may_national_letters_mix ? LexemeType::Id : LexemeType::Error},
                     lexeme_str, scanner.getLocation(), TokenQualification::NationalCharacterMix };
    }

    return { {lexeme_str, LexemeType::Id}, lexeme_str, scanner.getLocation() };
}

bool simodo::inout::token::Tokenizer::scanNumber(LexemeType &type, TokenQualification &qualification, std::u16string &lexeme_str)
{
    for(size_t i_starting=0; i_starting < numbers.size(); ++i_starting)
    {
        if (numbers[i_starting].is_starting)
        {
            lexeme_str.clear();

            size_t  i_mask_index = i_starting;
            size_t  i_mask_char  = 0;
            size_t  i_input      = 0;
            int16_t N_count      = -1;

            while(true)
            {
                const _NumberMask & mask    = numbers[i_mask_index];
                char16_t            ch      = scanner.getChar(i_input);

                if (i_mask_char == mask.chars.size())
                {
                    if (lexeme_str.empty())
                        return false;

                    if (mask.chars[i_mask_char-1] == u'n')
                    {
                        char16_t ch_upper;
                        if (mask.system > 10 )
                            ch_upper = convertLatinToUpper(ch);
                        else
                            ch_upper = ch;

                        if (param.digits.find(ch_upper) < mask.system)
                            break;
                    }

                    if (!mask.refs.empty())
                    {
                        size_t i_ref=0;
                        for(; i_ref < mask.refs.size(); ++i_ref)
                        {
                            uint8_t ref_no = mask.refs[i_ref];

                            assert(static_cast<size_t>(ref_no) < numbers.size());

                            const _NumberMask & ref_mask = numbers[static_cast<size_t>(ref_no)];

                            assert(!ref_mask.chars.empty());

                            char16_t ch_upper;

                            if (mask.system > 10 )
                                ch_upper = convertLatinToUpper(ch);
                            else
                                ch_upper = ch;

                            if (ch == ref_mask.chars[0]
                             || ((ref_mask.chars[0] == u'N' || ref_mask.chars[0] == u'n') && param.digits.find(ch_upper) < mask.system))
                                break;
                        }

                        if (i_ref < mask.refs.size())
                        {
                            i_mask_index = static_cast<size_t>(mask.refs[i_ref]);
                            i_mask_char = 0;
                            continue;
                        }

                        if (!mask.may_final)
                            break;
                    }

                    type = mask.type;
                    qualification = mask.qualification;
                    scanner.shift(lexeme_str.size());
                    return true;
                }

                if (mask.chars[i_mask_char] == u'N' || mask.chars[i_mask_char] == u'n')
                {
                    N_count ++;

                    char16_t ch_upper;
                    if (mask.system > 10 )
                        ch_upper = convertLatinToUpper(ch);
                    else
                        ch_upper = ch;

                    if (param.digits.find(ch_upper) < mask.system)
                    {
                        lexeme_str += ch;
                        if (mask.chars[i_mask_char] == u'n')
                            i_mask_char ++;
                    }
                    else if (N_count == 0)
                        break;
                    else if (mask.chars[i_mask_char] == u'n')
                        break;
                    else
                    {
                        i_mask_char ++;
                        continue;
                    }
                }
                else if (mask.chars[i_mask_char] == ch)
                {
                    lexeme_str += ch;
                    i_mask_char ++;
                    N_count = -1;
                }
                else
                    break;

                i_input ++ ;
            }
        }
        // don't remove!
        else
            break;
    }

    if (!lexeme_str.empty())
    {
        scanner.shift(lexeme_str.size());
        type = LexemeType::Error;
        qualification = TokenQualification::NotANumber;
        return true;
    }

    return false;
}

std::u16string simodo::inout::token::Tokenizer::convertToUpper(const std::u16string &s) const
{
    std::u16string res;

    for(char16_t c : s)
        res += convertToUpper(c);

    return res;
}

char16_t simodo::inout::token::Tokenizer::convertToUpper(char16_t ch) const
{
    std::string::size_type  pos_latin       = param.latin_alphabet.find(ch);
    size_t                  latin_size      = param.latin_alphabet.size();
    size_t                  national_size   = param.national_alphabet.size();

    if (pos_latin != std::string::npos)
    {
        if (pos_latin < latin_size/2)
            return param.latin_alphabet.at(latin_size/2+pos_latin);

        return ch;
    }

    std::string::size_type pos_national = param.national_alphabet.find(ch);

    if (pos_national != std::string::npos)
        if (pos_national < national_size/2)
            return param.national_alphabet.at(national_size/2+pos_national);

    return ch;
}

char16_t simodo::inout::token::Tokenizer::convertLatinToUpper(char16_t ch) const
{
    std::string::size_type  pos_latin       = param.latin_alphabet.find(ch);
    size_t                  latin_size      = param.latin_alphabet.size();

    if (pos_latin != std::string::npos)
        if (pos_latin < latin_size/2)
            return param.latin_alphabet.at(latin_size/2+pos_latin);

    return ch;
}

bool simodo::inout::token::Tokenizer::isBlank(char16_t ch) const
{
    return (std::u16string::npos != std::u16string(u" \t\r\n").find(ch));
}

