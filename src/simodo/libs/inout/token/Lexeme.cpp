/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/token/Lexeme.h"


std::u16string simodo::inout::token::getLexemeTypeName(LexemeType type)
{
    switch(type)
    {
    case LexemeType::Compound:
        return u"Compound";
    case LexemeType::Empty:
        return u"Empty";
    case LexemeType::Punctuation:
        return u"Punctuation";
    case LexemeType::Id:
        return u"Word";
    case LexemeType::Annotation:
        return u"Annotation";
    case LexemeType::Number:
        return u"Number";
    case LexemeType::Comment:
        return u"Comment";
    case LexemeType::Error:
        return u"Error";
    default:
        return u"*****";
    }
}

std::u16string simodo::inout::token::getLexemeMnemonic(const Lexeme &lex)
{
    std::u16string s;

    switch(lex.type)
    {
    case LexemeType::Id:
        s = u"(идентификатор)";
        break;
    case LexemeType::Empty:
        s = u"(конец файла)";
        break;
    case LexemeType::Error:
        s = u"(ошибка лексики)";
        break;
    case LexemeType::Number:
        s = u"(число)";
        break;
    case LexemeType::Comment:
        s = u"(комментарий)";
        break;
    case LexemeType::Compound:
        s = lex.lexeme;
        break;
    case LexemeType::Annotation:
        s = u"(строковая константа)";
        break;
    case LexemeType::Punctuation:
        s = u"'" + lex.lexeme + u"'";
        break;
    default: // default: нужен на случай расширения перечисления, чтобы видеть ошибку
        s = u"(***)";
        break;
    }

    return s;
}

