/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/token/Scanner.h"

#include <cassert>


bool simodo::inout::token::Scanner::putCharToBuffer()
{
    if (_is_end_of_file_reached)
        return false;

    // ВАЖНО! Предполагается, что входной поток содержит коды текста длиной не более двух байт
    /// \todo При чтении символов из входного потока потенциально возможно искажение (если размер кода текста > 16 бит).
    /// Кроме того, это приводит к слабости кода перед изменениями, например, при переходе на строки по 32 бита в символе.
    /// Решение: сделать класс с параметрическим типом на размер символа в потоке и строке.
    char16_t ch = _input.get();

    if (ch == std::char_traits<char16_t>::eof())
    {
        _is_end_of_file_reached = true;
        return false;
    }

    _buffer.push_back(ch);
    return true;
}

simodo::inout::token::Scanner::Scanner(const std::u16string &file_name, InputStream_interface &input_stream, uint8_t tab_size, context_index_t context_index)
    : _loc({{file_name}})
    , _input(input_stream)
    , _buffer()
    , _tab_size(tab_size)
    , _line(1)
    , _column(1)
    , _column_tabulated(1)
    , _is_end_of_file_reached(false)
    , _pos(0)
{
    assert(tab_size > 0 && tab_size <= 32);

    _buffer.reserve(20);
    _loc.context = context_index;
}

bool simodo::inout::token::Scanner::shift(size_t length)
{
    assert(_buffer.size() >= length);  // Нельзя сдвигать за пределы буфера (откуда токенайзеру знать то, чего он не видел?)

    // Вычисляем локацию
    for(size_t i=0; i < length; ++i)
    {
        char16_t ch = _buffer[i];

        /// \bug Scanner::shift: Возможно переполнение при работе с файлами > 4 мегабайт
        _pos++;

        if (ch == u'\n')
        {
            _line ++;
            _column = _column_tabulated = 1;
        }
        else if (ch == u'\t')
        {
            _column ++;
            _column_tabulated += _tab_size - _column_tabulated % _tab_size + 1;
        }
        else
        {
            _column ++;
            _column_tabulated ++;
        }
    }

    _loc.end = _pos;

    // Выпиливаем неактуальное
    _buffer = _buffer.substr(length);

    if (_buffer.empty())
        return putCharToBuffer();

    return true;
}

simodo::inout::token::TokenLocation simodo::inout::token::Scanner::getLocation() const 
{ 
    TokenLocation loc = _loc;
    loc.line_end = _line;
    return loc; 
}

void simodo::inout::token::Scanner::fixLocation(context_index_t context_index)
{
    _loc.line_begin = _line;
    _loc.column = _column;
    _loc.column_tabulated = _column_tabulated;
    _loc.begin = _pos;
    _loc.context = context_index;
}

bool simodo::inout::token::Scanner::startsWith(char16_t ch)
{
    if (_buffer.empty())
    {
        if (_is_end_of_file_reached || _input.eof())
            return false;

        bool ok = putCharToBuffer();
        if (!ok)
            return false; // Достигли конца потока
    }

    return (_buffer[0] == ch);
}

bool simodo::inout::token::Scanner::startsWith(const std::u16string &str)
{
    if (str.empty())
        return false;

    while(_buffer.size() < str.size())
    {
        if (_is_end_of_file_reached || _input.eof())
            return false;

        bool ok = putCharToBuffer();
        if (!ok)
            return false; // Достигли конца потока
    }

    return (_buffer.compare(0,str.size(),str) == 0);
}

bool simodo::inout::token::Scanner::startsWithAnyOf(const std::u16string &str)
{
    if (str.empty())
        return false;

    if (_buffer.empty())
    {
        if (_is_end_of_file_reached || _input.eof())
            return false;

        bool ok = putCharToBuffer();
        if (!ok)
            return false; // Достигли конца потока
    }

    return (str.find(_buffer[0]) != std::u16string::npos);
}

char16_t simodo::inout::token::Scanner::getFirstChar()
{
    return getChar();
}

char16_t simodo::inout::token::Scanner::getChar(size_t pos)
{
    while(_buffer.size() < pos+1)
    {
        if (_is_end_of_file_reached || _input.eof())
            return std::char_traits<char16_t>::eof();

        bool ok = putCharToBuffer();
        if (!ok)
            return std::char_traits<char16_t>::eof(); // Достигли конца потока
    }

    return _buffer[pos];
}
