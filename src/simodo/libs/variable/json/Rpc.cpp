#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"

namespace simodo::variable::json
{

Rpc::Rpc(const std::string & json)
    : _value(fromString(json))
{
    if (_value.type() != ValueType::Record)
        return;

    setupMembers();
}

Rpc::Rpc(const std::u16string & json)
    : _value(fromString(json))
{
    if (_value.type() != ValueType::Record)
        return;

    setupMembers();
}

Rpc::Rpc(variable::Value value)
    : _value(value)
{
    if (_value.type() != ValueType::Record)
        return;

    setupMembers();
}

Rpc::Rpc(std::u16string method, variable::Value params, int64_t id)
    : _value(Record {{
        {u"jsonrpc", u"2.0"},
        {u"method", method},
        {u"params", params},
        {u"id", id},
    }})
{
    setupMembers();
}

Rpc::Rpc(std::u16string method, int64_t id)
    : _value(Record {{
        {u"jsonrpc", u"2.0"},
        {u"method", method},
        {u"id", id},
    }})
{
    setupMembers();
}

Rpc::Rpc(std::u16string method, variable::Value params)
    : _value(Record {{
        {u"jsonrpc", u"2.0"},
        {u"method", method},
        {u"params", params},
    }})
{
    setupMembers();
}

// Rpc::Rpc(std::u16string method)
//     : _value(Record {{
//         {u"jsonrpc", u"2.0"},
//         {u"method", method},
//     }})
// {
//     setupMembers();
// }

Rpc::Rpc(variable::Value result, int64_t id)
    : _value(Record {{
        {u"jsonrpc", u"2.0"},
        {u"result", result},
        {u"id", id},
    }})
{
    setupMembers();
}

Rpc::Rpc(int64_t code, std::u16string message, variable::Value data, int64_t id)
    : _value(Record {{
        {u"jsonrpc", u"2.0"},
        {u"error", Record {{
            {u"code",    code},
            {u"message", message},
            {u"data",    data},
        }}},
        {u"id", id},
    }})
{
    setupMembers();
}

Rpc::Rpc(int64_t code, std::u16string message, int64_t id)
    : _value(Record {{
        {u"jsonrpc", u"2.0"},
        {u"error", Record {{
            {u"code",    code},
            {u"message", message},
        }}},
        {u"id", id},
    }})
{
    setupMembers();
}

const variable::Value & Rpc::params() const
{
    return _params ? *_params : _fail;
}

const variable::Value & Rpc::result() const
{
    return _result ? *_result : _fail;
}

const variable::Value & Rpc::error() const
{
    return _error ? *_error : _fail;
}

void Rpc::setupMembers()
{
    if (_is_valid)
        return;

    const auto object = _value.getRecord();
    const Value & jsonrpc_value = object->find(u"jsonrpc");
    const Value & method_value  = object->find(u"method");
    const Value & id_value      = object->find(u"id");
    const Value & params_value  = object->find(u"params");
    const Value & result_value  = object->find(u"result");
    const Value & error_value   = object->find(u"error");

    if (jsonrpc_value.type() == ValueType::String)
        _jsonrpc = jsonrpc_value.getString();
    else
        _jsonrpc = u"1.0";
    if (method_value.type() == ValueType::String)
        _method = method_value.getString();
    if (id_value.type() == ValueType::Int)
        _id = id_value.getInt();
    if (params_value.type() == ValueType::Array || params_value.type() == ValueType::Record)
        _params = &params_value;
    if (object->exists(u"result"))
        _result = &result_value;
    if (error_value.type() == ValueType::Record)
        _error = &error_value;

    if (!_method.empty() && (_result || _error))
        return;

    if (_method.empty() && !_result && !_error && _id < 0)
        return;

    if (_result && _error)
        return;

    if ((_result || _error) && _params)
        return;

    _is_valid = true;
}

}