/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/variable/json/parser/JsonRdp.h"
#include "simodo/inout/convert/functions.h"
#include "simodo/inout/token/FileStream.h"

#include <fstream>
#include <algorithm>

using namespace simodo::inout;
using namespace simodo::variable::json::parser;


bool JsonRdp::parse(const std::string &json_file, Value &json, int tab_size) const
{
    SIMODO_INOUT_CONVERT_STD_IFSTREAM(in, json_file);

    if (!in)
    {
        reporter().reportFatal(u"Ошибка при открытии файла '" + convert::toU16(json_file) + u"'");
        return false;
    }

    token::FileStream stream(in);

    return parse(json_file, stream, json, tab_size);
}

bool JsonRdp::parse(const std::string &json_file, token::InputStream_interface &stream, Value &json, int tab_size) const
{
#if __cplusplus >= 202002L
    LexicalParameters lex {
        .markups            = { {u"\"", u"\"", u"\\", LexemeType::Annotation} },
        .masks              = { {{ BUILDING_NUMBER, LexemeType::Number, 10 }} },
        .punctuation_chars  = u"{}[],:",
        .punctuation_words  = {u"true", u"false", u"null"},
        .digits             = DIGITS,
        .latin_alphabet     = LATIN_ALPHABET,
        .national_alphabet  = u"",
        .id_extra_symbols   = u"_",
        .may_national_letters_use = false,
        .may_national_letters_mix = false,
        .is_case_sensitive  = true,
    };
#else
    token::LexicalParameters lex;

    lex.markups = {
        {u"\"", u"\"", u"\\", token::LexemeType::Annotation}
    };
    lex.masks = {
        {token::BUILDING_NUMBER, token::LexemeType::Number, 10}
    };
    lex.punctuation_chars = u"{}[],:";
    lex.punctuation_words = {u"true", u"false", u"null"};
    lex.national_alphabet.clear();
    lex.id_extra_symbols   = u"_";
    lex.may_national_letters_use = false;
    lex.may_national_letters_mix = false;
    lex.is_case_sensitive  = true;
#endif
    token::Tokenizer tokenizer(convert::toU16(json_file), stream, lex, tab_size);

    token::Token t = tokenizer.getToken();

    if (t.type == token::LexemeType::Empty)
    {
        json = Value();
        return true;
    }

    return parseValue(tokenizer, t, json);
}

bool JsonRdp::parseValue(token::Tokenizer &tokenizer, const token::Token &t, Value &json) const
{
    if (t.type == token::LexemeType::Punctuation)
    {
        if (t.lexeme == u"{")
            return parseObject(tokenizer, json);

        if (t.lexeme == u"[")
            return parseArray(tokenizer, json);

        if (t.lexeme == u"true" || t.lexeme == u"false")
        {
            json = Value(t.lexeme == u"true");
            return true;
        }

        if (t.lexeme == u"null")
        {
            json = Value();
            return true;
        }
    }
    else if (t.type == token::LexemeType::Annotation)
    {
        json = Value(t.lexeme);
        return true;
    }
    else if (t.type == token::LexemeType::Number)
    {
        if (t.qualification == token::TokenQualification::RealNumber)
            json = Value(stod(convert::toU8(t.lexeme)));
        else
            json = Value(int64_t(stol(convert::toU8(t.lexeme))));
        return true;
    }

    return reportUnexpected(t, u"number, string constant, '{', '[', 'true', 'false' or 'null'");
}

bool JsonRdp::parseObject(token::Tokenizer &tokenizer, Value &json) const
{
    VariableSet_t member_list;

    token::Token t = tokenizer.getToken();

    if (t.type == token::LexemeType::Punctuation && t.lexeme == u"}")
    {
        json = Value(member_list);
        return true;
    }

    while(t.type != token::LexemeType::Empty)
    {
        if (t.type != token::LexemeType::Annotation)
            break;

        auto it = find_if(member_list.begin(), member_list.end(),
                          [t](const Variable & var)
        {
            return t.lexeme == var.name();
        });

        if (it != member_list.end())
        {
            reporter().reportError(t.location, u"Member '" + t.lexeme + u"' is duplicated");
            return false;
        }

        std::u16string value_name = t.lexeme;
        t = tokenizer.getToken();

        if (t.type != token::LexemeType::Punctuation || t.lexeme != u":")
            break;

        t = tokenizer.getToken();

        Value json_value;

        if (!parseValue(tokenizer,t,json_value))
            return false;

        member_list.emplace_back(Variable {value_name, json_value});

        t = tokenizer.getToken();

        if (t.type == token::LexemeType::Punctuation && t.lexeme == u"}")
        {
            json = Value(member_list);
            return true;
        }

        if (t.type != token::LexemeType::Punctuation || t.lexeme != u",")
            break;

        t = tokenizer.getToken();
    }

    return reportUnexpected(t);
}

bool JsonRdp::parseArray(token::Tokenizer &tokenizer, Value &json) const
{
    std::vector<Value> value_list;

    token::Token t = tokenizer.getToken();

    if (t.type == token::LexemeType::Punctuation && t.lexeme == u"]")
    {
        json = Value(value_list);
        return true;
    }

    while(t.type != token::LexemeType::Empty)
    {
        Value json_value;

        if (!parseValue(tokenizer,t,json_value))
            return false;

        value_list.push_back(json_value);

        t = tokenizer.getToken();

        if (t.type == token::LexemeType::Punctuation && t.lexeme == u"]")
        {
            json = Value(value_list);
            return true;
        }

        if (t.type != token::LexemeType::Punctuation || t.lexeme != u",")
            break;

        t = tokenizer.getToken();
    }

    return reportUnexpected(t);
}
