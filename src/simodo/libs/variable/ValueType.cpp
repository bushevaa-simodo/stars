/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/variable/Variable.h"

std::u16string simodo::variable::getValueTypeName(ValueType type) noexcept
{
    std::u16string s;

    switch(type)
    {
    case ValueType::Null:
        s = u"null";
        break;
    case ValueType::Undefined:
        s = u"undef";
        break;
    case ValueType::Bool:
        s = u"bool";
        break;
    case ValueType::Int:
        s = u"int";
        break;
    case ValueType::Real:
        s = u"real";
        break;
    case ValueType::String:
        s = u"string";
        break;
    case ValueType::Function:
        s = u"function";
        break;
    case ValueType::Record:
        s = u"record";
        break;
    case ValueType::Array:
        s = u"array";
        break;
    case ValueType::Ref:
        s = u"ref";
        break;
    case ValueType::ExtFunction:
        s = u"function (external)";
        break;
    case ValueType::IntFunction:
        s = u"function (internal)";
        break;
    default:
        s = UNDEF_STRING;
        break;
    }

    return s;
}
