#include "simodo/lsp/CompletionParams.h"

#include "simodo/inout/convert/functions.h"

namespace simodo::lsp
{

bool parseCompletionContext(const simodo::variable::Value & context_value, CompletionContext & context)
{
    if (context_value.type() != variable::ValueType::Record)
        return false;

    const std::shared_ptr<variable::Record> context_object = context_value.getRecord();

    const variable::Value & triggerKind_value         = context_object->find(u"triggerKind");
    const variable::Value & triggerCharacter_value    = context_object->find(u"triggerCharacter");

    if (triggerKind_value.type() != variable::ValueType::Int
     || triggerCharacter_value.type() != variable::ValueType::String)
        return false;

    context.triggerKind      = static_cast<CompletionTriggerKind>(triggerKind_value.getInt());
    context.triggerCharacter = inout::convert::toU8(triggerCharacter_value.getString());

    return true;
}

bool parseCompletionParams(const simodo::variable::Value & params, CompletionParams & completionParams)
{
    if (params.type() != variable::ValueType::Record
     || !parseTextDocumentPositionParams(params, completionParams))
        return false;

    const std::shared_ptr<variable::Record> params_object = params.getRecord();

    return parseCompletionContext(params_object->find(u"context"), completionParams.context);
}

}
