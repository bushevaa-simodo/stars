#include "simodo/stage/NestedException.h"

#include <stdexcept>

namespace simodo::stage
{
    NestedException::NestedException(
        const std::string &what
        , const std::string &nested
    )
        : _except(std::make_shared<std::logic_error>(what))
    {
        if (nested.empty()) return;
        _nested = std::make_shared<NestedException>(nested);
    }

    NestedException::NestedException(
        const NestedException &other
    )
        : _except(other._except)
        , _nested(other._nested)
    {}

    const char* NestedException::what() const noexcept
    {
        return except()->what();
    }

    typename NestedException::ExceptionReference
        NestedException::except() const
    {
        return _except;
    }

    typename NestedException::NestedExceptionReference
        NestedException::nested() const
    {
        return _nested;
    }
}
