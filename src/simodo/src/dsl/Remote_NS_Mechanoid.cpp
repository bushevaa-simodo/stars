/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/Remote_NS_Mechanoid.h"

#include "simodo/dsl/Exception.h"
#include "simodo/convert.h"
#include "simodo/version.h"

#include <chrono>

using namespace std;
using namespace simodo;
using namespace simodo::dsl;

namespace
{
    bool init(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(!stack.empty());

        u16string             panel_name  = get<u16string>(get<SCI_Scalar>(stack.top().bulk).variant);
        Remote_NS_Mechanoid * mech        = static_cast<Remote_NS_Mechanoid *>(p_object);
        size_t                panel_index = 0;

        mech->listener().reportInformation(u"#Values:Mechanoid." + convertToU16(std::to_string(panel_index))
                                            + u".S.init:" + panel_name);

        stack.pop();

        return true;
    }

    bool add(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(!stack.empty());

        u16string             mech_name   = get<u16string>(get<SCI_Scalar>(stack.top().bulk).variant);
        Remote_NS_Mechanoid * mech        = static_cast<Remote_NS_Mechanoid *>(p_object);
        size_t                panel_index = 0;

        mech->listener().reportInformation(u"#Values:Mechanoid." + convertToU16(std::to_string(panel_index))
                                            + u".S.add:" + mech_name);
        stack.pop();

        return true;
    }

    bool parameter(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 3);

        int64_t               mech_no      = get<int64_t>(get<SCI_Scalar>(stack.top(2).bulk).variant);
        u16string             param_name   = get<u16string>(get<SCI_Scalar>(stack.top(1).bulk).variant);
        u16string             param_value  = get<u16string>(get<SCI_Scalar>(stack.top(0).bulk).variant);
        Remote_NS_Mechanoid * mech         = static_cast<Remote_NS_Mechanoid *>(p_object);
        size_t                panel_index  = 0;

        mech->listener().reportInformation(u"#Values:Mechanoid." + convertToU16(std::to_string(panel_index))
                                            + u".U.parameter:" + convertToU16(to_string(mech_no))
                                            + u"/" + param_name
                                            + u"/" + param_value);
        stack.pop(3);

        return true;
    }

    bool event(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 3);

        int64_t               mech_no      = get<int64_t>(get<SCI_Scalar>(stack.top(2).bulk).variant);
        u16string             event_name   = get<u16string>(get<SCI_Scalar>(stack.top(1).bulk).variant);
        u16string             event_value  = get<u16string>(get<SCI_Scalar>(stack.top(0).bulk).variant);
        Remote_NS_Mechanoid * mech         = static_cast<Remote_NS_Mechanoid *>(p_object);
        size_t                panel_index  = 0;

        mech->listener().reportInformation(u"#Values:Mechanoid." + convertToU16(std::to_string(panel_index))
                                            + u".U.event:" + convertToU16(to_string(mech_no))
                                            + u"/" + event_name
                                            + u"/" + event_value);
        stack.pop(3);

        return true;
    }

    bool location(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 4);

        int64_t               mech_no      = get<int64_t>(get<SCI_Scalar>(stack.top(3).bulk).variant);
        double                x            = get<double>(get<SCI_Scalar>(stack.top(2).bulk).variant);
        double                y            = get<double>(get<SCI_Scalar>(stack.top(1).bulk).variant);
        double                z            = get<double>(get<SCI_Scalar>(stack.top(0).bulk).variant);
        Remote_NS_Mechanoid * mech         = static_cast<Remote_NS_Mechanoid *>(p_object);
        size_t                panel_index  = 0;

        mech->listener().reportInformation(u"#Values:Mechanoid." + convertToU16(std::to_string(panel_index))
                                            + u".U.location:" + convertToU16(to_string(mech_no))
                                            + u"/" + convertToU16(to_string(x))
                                            + u"/" + convertToU16(to_string(y))
                                            + u"/" + convertToU16(to_string(z)));
        stack.pop(4);

        return true;
    }

}

Remote_NS_Mechanoid::Remote_NS_Mechanoid(AReporter &listener)
    : _listener(listener)
{
    // _panels.push_back({});
}

Remote_NS_Mechanoid::~Remote_NS_Mechanoid()
{
}

SCI_Namespace_t Remote_NS_Mechanoid::getNamespace()
{
    return {
        {u"init", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::init}}},
            {u"", SemanticNameQualification::None, {}},
            {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        // {u"panel", SemanticNameQualification::Function, SCI_Tuple {
        //     {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panel}}},
        //     {u"", SemanticNameQualification::None, {}},
        //     {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        // }},
        {u"add", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::add}}},
            {u"", SemanticNameQualification::None, {}},
            {u"mech_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"parameter", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::parameter}}},
            {u"", SemanticNameQualification::None, {}},
            {u"mech_no", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"param_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"param_value", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"event", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::event}}},
            {u"", SemanticNameQualification::None, {}},
            {u"mech_no", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"event_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"event_value", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"location", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::location}}},
            {u"", SemanticNameQualification::None, {}},
            {u"mech_no", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"x", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"y", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"z", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        // {u"Style", SemanticNameQualification::Tuple, SCI_Tuple {
        //     {u"Line", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
        //     {u"Spline", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(1)}},
        //     {u"Scatter", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(2)}},
        // }},
    };
}


