#include "simodo/dsl/tablefunction/TableFunction.h"

namespace tablefunction::utils
{
    const simodo::dsl::SCI_Name * findNameInTuple(std::u16string name, const simodo::dsl::SCI_Tuple & tuple)
    {
        auto name_it = std::find_if(tuple.begin()
                            , tuple.end()
                            , [&name](const simodo::dsl::SCI_Name & n) -> bool
                            {
                                return n.name == name;
                            }
                            );
        return name_it == tuple.end() ? nullptr : &(*name_it);
    }

    int64_t toInt(const simodo::dsl::SCI_Name & n)
    {
        try {
            return std::get<int64_t>(std::get<simodo::dsl::SCI_Scalar>(n.bulk).variant);
        } catch(const std::exception &e)
        {}

        throw simodo::dsl::Exception("SCI_tf", "Неизвестный тип целых чисел: qualification=" + std::to_string(static_cast<int>(n.qualification)));
    }

    double toDouble(const simodo::dsl::SCI_Name & n)
    {
        try {
            auto & sn = std::get<simodo::dsl::SCI_Scalar>(n.bulk);
            return (sn.type == simodo::dsl::SemanticNameType::Int) ? double(std::get<int64_t>(sn.variant)) : std::get<double>(sn.variant);
        } catch(const std::exception &e)
        {}

        throw simodo::dsl::Exception("SCI_tf", "Неизвестный тип вещественных чисел: qualification=" + std::to_string(static_cast<int>(n.qualification)));
    };

    simodo::dsl::SCI_Name * origin(simodo::dsl::SCI_Name * n)
    {
        simodo::dsl::SCI_Name * nn = n;
        while(nn->qualification == simodo::dsl::SemanticNameQualification::Reference)
        {
            nn = std::get<simodo::dsl::SCI_Reference>(nn->bulk);
        }
        return nn;
    }
}
