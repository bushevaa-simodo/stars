/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/Grammar.h"

using namespace std;
using namespace simodo::dsl;



size_t Grammar::getColumnIndex(const Lexeme &lexeme) const
{
    /// \attention Этот метод используется до заполнения индексов terminal_symbol_index и compound_symbol_index!

    assert(first_compound_index > 0 && first_compound_index < columns.size());

    size_t i   = (lexeme.getType() != LexemeType::Compound) ? 0 : first_compound_index;
    size_t end = (lexeme.getType() != LexemeType::Compound) ? first_compound_index : columns.size();

    for(; i < end; ++i)
        if (lexeme == columns[i])
            return i;

    return columns.size();
}

size_t Grammar::getTerminalColumnIndex(const Lexeme &lexeme) const
{
    assert(first_compound_index > 0 && first_compound_index < columns.size());

    for(size_t i=0; i < first_compound_index; ++i)
    {
        if (lexeme.getType() == LexemeType::Id && columns[i].getType() == LexemeType::Id)
            return i;
        if (lexeme.getType() == LexemeType::Number && columns[i].getType() == LexemeType::Number)
            return i;
        if (lexeme.getType() == LexemeType::Comment && columns[i].getType() == LexemeType::Comment)
            return i;
        if (lexeme.getType() == LexemeType::Annotation && columns[i].getType() == LexemeType::Annotation)
            return i;
        if (lexeme == columns[i])
            return i;
    }

    return columns.size();
}

size_t Grammar::getCompaundColumnIndex(const u16string &str) const
{
    assert(first_compound_index > 0 && first_compound_index < columns.size());

    for(size_t i=first_compound_index; i < columns.size(); ++i)
        if (str == columns[i].getLexeme())
            return i;

    return columns.size();
}

u16string simodo::dsl::getFsmActionChar(FsmActionType action)
{
    switch(action)
    {
    case FsmActionType::Error:
        return u"E";
    case FsmActionType::Shift:
        return u"S";
    case FsmActionType::Reduce:
        return u"R";
    case FsmActionType::Acceptance:
        return u"A";
    }
    return u"*";
}
