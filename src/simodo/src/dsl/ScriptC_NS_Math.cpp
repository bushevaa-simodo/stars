/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>
#include <algorithm>
#include <cmath>
#include <random>

#include "simodo/dsl/SCI_StackGuard.h"
#include "simodo/dsl/ScriptC_NS_Math.h"
#include "simodo/dsl/Exception.h"

#include "simodo/convert.h"

using namespace std;
using namespace simodo::dsl;

namespace
{
    template <typename T>
    inline void stackPush(SCI_Stack &, T);

    template <>
    inline void stackPush<int64_t>(SCI_Stack &s, int64_t val)
    {
        s.push({
            u""
            , SemanticNameQualification::Scalar
            , SCI_Scalar{SemanticNameType::Int, val}
        });
    }

    template <>
    inline void stackPush<double>(SCI_Stack &s, double val)
    {
        s.push({
            u""
            , SemanticNameQualification::Scalar
            , SCI_Scalar{SemanticNameType::Float, val}
        });
    }

    template <typename T, typename R>
    using F = R(T);

    template <typename T, typename R, F<T, R> f>
    bool SCI_f(IScriptC_Namespace *, SCI_Stack &sci_stack)
    {
        R result;
        {
            SCI_StackGuard<1> guard(sci_stack);
            result = f(sci_stack.topC().getScalar<T>());
        }
        stackPush<R>(sci_stack, result);
        return true;
    }

    template <typename T, F<T, T> f>
    constexpr bool (*SCI_ff)(IScriptC_Namespace *, SCI_Stack &) = &SCI_f<T, T, f>;

    bool SCI_pi(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        stackPush<double>(sci_stack, M_PI);
        return true;
    }

    bool SCI_e(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        stackPush<double>(sci_stack, M_E);
        return true;
    }

    template <typename T>
    inline T toSign(T v)
    {
        return signbit(v) ? -1 : 1;
    }

    inline double toFloat(int64_t v)
    {
        return v;
    }

    inline int64_t toInt(double v)
    {
        return v;
    }

    inline double toRad(double v)
    {
        return 0.01745329251 * v;
    }

    inline double toDeg(double v)
    {
        return 57.2957795131 * v;
    }

    inline double notZero(double v)
    {
        if (v == 0.0) return numeric_limits<double>().epsilon();
        return v;
    }

    inline double toMinusPi_Pi(double v)
    {
        while (v < -M_PI)
        {
            v += 2 * M_PI;
        }
        while (M_PI < v)
        {
            v -= 2 * M_PI;
        }
        return v;
    }

    inline double toZero_2Pi(double v)
    {
        while (v < 0.0)
        {
            v += 2 * M_PI;
        }
        while (2 * M_PI < v)
        {
            v -= 2 * M_PI;
        }
        return v;
    }

    bool SCI_atan2(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 2);

        auto y = sci_stack.top(1).getScalar<double>();
        auto x = sci_stack.top(0).getScalar<double>();

        auto val = atan2(y, x);
        sci_stack.pop(2);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_random_int(IScriptC_Namespace * , SCI_Stack & sci_stack);

    bool SCI_random_float(IScriptC_Namespace * , SCI_Stack & sci_stack);

    bool SCI_limit(IScriptC_Namespace * , SCI_Stack & sci_stack);

    bool SCI_limitInt(IScriptC_Namespace * , SCI_Stack & sci_stack);

    bool SCI_vec2Len(IScriptC_Namespace * , SCI_Stack & sci_stack);

    bool SCI_vec3Len(IScriptC_Namespace * , SCI_Stack & sci_stack);

    bool SCI_tf1(IScriptC_Namespace * , SCI_Stack & sci_stack);
}


SCI_Namespace_t ScriptC_NS_Math::getNamespace()
{
    return {
        {u"const_pi", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_pi}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"const_e", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_e}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"sin", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, sin>}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"cos", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, cos>}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"tan", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, tan>}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"asin", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, asin>}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"acos", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, acos>}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"atan", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, atan>}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"sqrt", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, sqrt>}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"exp", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, exp>}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"ln", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, log>}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"abs", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, abs>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"absInt", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<int64_t, abs>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"round", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, round>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"val", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"floor", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, floor>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"val", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"trunc", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, trunc>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"val", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"sign", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, toSign>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"signInt", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<int64_t, toSign>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"toFloat", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_f<int64_t, double, toFloat>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"i", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"toInt", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_f<double, int64_t, toInt>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"f", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"toRad", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, toRad>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"deg", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"toDeg", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, toDeg>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"rad", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"notZero", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, notZero>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"toMinusPi_Pi", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, toMinusPi_Pi>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"Zero_2Pi", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"toZero_2Pi", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ff<double, toZero_2Pi>}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"MinusPi_Pi", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"atan2", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_atan2}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"_y_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"_x_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"random_int", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_random_int}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"random_float", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_random_float}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"limit", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_limit}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"val", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"limitInt", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_limitInt}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"val", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"vec2Len", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_vec2Len}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"x", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"y", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"vec3Len", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_vec3Len}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"x", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"y", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"z", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"tf1", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_tf1}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_tuple_", SemanticNameQualification::None, {}},
            {u"_arg_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
    };
}

namespace
{
    bool SCI_random_int(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 2);

        auto min = sci_stack.top(1).getScalar<int64_t>();
        auto max = sci_stack.top(0).getScalar<int64_t>();

        thread_local std::mt19937 random_float_generator((std::random_device())());
        std::uniform_int_distribution<int64_t> distributor(min, max);

        sci_stack.pop(2);
        auto val = int64_t(distributor(random_float_generator));
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_random_float(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 2);

        auto min = sci_stack.top(1).getScalar<double>();
        auto max = sci_stack.top(0).getScalar<double>();

        thread_local std::mt19937 random_float_generator((std::random_device())());
        std::uniform_real_distribution<double> distributor(min, max);

        sci_stack.pop(2);
        auto val = double(distributor(random_float_generator));
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_limit(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 3);

        auto val = sci_stack.top(2).getScalar<double>();
        auto min = sci_stack.top(1).getScalar<double>();
        auto max = sci_stack.top(0).getScalar<double>();

        if (max < min)
        {
            throw Exception("math.limit", "max < min : " + to_string(max) + " < " + to_string(min));
        }

        val = clamp(val, min, max);
        sci_stack.pop(3);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_limitInt(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 3);

        auto val = sci_stack.top(2).getScalar<int64_t>();
        auto min = sci_stack.top(1).getScalar<int64_t>();
        auto max = sci_stack.top(0).getScalar<int64_t>();

        if (max < min)
        {
            throw Exception("math.limit", "max < min : " + to_string(max) + " < " + to_string(min));
        }

        val = clamp(val, min, max);
        sci_stack.pop(3);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_vec2Len(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 2);

        auto x = sci_stack.top(1).getScalar<double>();
        auto y = sci_stack.top(0).getScalar<double>();

        auto val = sqrt(x * x + y * y);
        sci_stack.pop(2);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_vec3Len(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 3);

        auto x = sci_stack.top(2).getScalar<double>();
        auto y = sci_stack.top(1).getScalar<double>();
        auto z = sci_stack.top(0).getScalar<double>();

        auto val = sqrt(x * x + y * y + z * z);
        sci_stack.pop(3);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_tf1(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 2);

        SCI_Name * obj = &sci_stack.top(1);

        while(obj->qualification == SemanticNameQualification::Reference)
            obj = get<SCI_Reference>(obj->bulk);

        const SCI_Tuple & tuple = get<SCI_Tuple>(obj->bulk);
        double            x     = get<double>(get<SCI_Scalar>(sci_stack.top(0).bulk).variant);

        const SCI_Name * args_name = nullptr;
        const SCI_Name * func_name = nullptr;

        for(const SCI_Name & n : tuple)
            if (n.name == u"args")
                args_name = &n;
            else if (n.name == u"func")
                func_name = &n;

        if (args_name == nullptr || args_name->qualification != SemanticNameQualification::Array)
            throw Exception("SCI_tf1",
                            "Кортеж '" + simodo::convertToU8(sci_stack.top(1).name) +
                            "' не содержит массив с именем 'args'");

        if (func_name == nullptr || func_name->qualification != SemanticNameQualification::Array)
            throw Exception("SCI_tf1",
                            "Кортеж '" + simodo::convertToU8(sci_stack.top(1).name) +
                            "' не содержит массив с именем 'func'");

        const SCI_Array & args_array = get<SCI_Array>(args_name->bulk);
        const SCI_Array & func_array = get<SCI_Array>(func_name->bulk);

        if (args_array.dimensions.size() != 1 || func_array.dimensions.size() != 1 ||
            args_array.values.size() != func_array.values.size() || args_array.values.size() == 0)
            throw Exception("SCI_tf1",
                            "Массивы 'args' и 'func' кортежа'" + simodo::convertToU8(sci_stack.top(1).name) +
                            "' должны иметь одну размерность одинакового не нулевого размера");

        double f = 0;

        if (x < get<double>(get<SCI_Scalar>(args_array.values.front().bulk).variant))
            f = get<double>(get<SCI_Scalar>(func_array.values.front().bulk).variant);
        else
        {
            size_t i = 1;
            for(; i < args_array.values.size(); ++i)
                if (x < get<double>(get<SCI_Scalar>(args_array.values[i].bulk).variant))
                {
                    double x1 = get<double>(get<SCI_Scalar>(args_array.values[i-1].bulk).variant);
                    double x2 = get<double>(get<SCI_Scalar>(args_array.values[i].bulk).variant);
                    double f1 = get<double>(get<SCI_Scalar>(func_array.values[i-1].bulk).variant);
                    double f2 = get<double>(get<SCI_Scalar>(func_array.values[i].bulk).variant);

                    f = f1 + (f2-f1) * (x-x1) / (x2-x1);

                    break;
                }

            if (i == args_array.values.size())
                f = get<double>(get<SCI_Scalar>(func_array.values.back().bulk).variant);
        }

        sci_stack.pop(2);
        sci_stack.push({u"", SemanticNameQualification::Scalar, SCI_Scalar{SemanticNameType::Float, f}});

        return true;
    }
}
