/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include "simodo/dsl/Remote_NS_Chart.h"

#include "simodo/dsl/SCI_StackGuard.h"
#include "simodo/dsl/Exception.h"
#include "simodo/convert.h"
#include "simodo/version.h"

#include <chrono>

using namespace std;
using namespace simodo;
using namespace simodo::dsl;

namespace
{
    struct Bounds
    {
        struct Bound
        {
            double min;
            double max;
        };

        Bound x, y;
    };

    struct Point
    {
        double x, y;
    };

    inline void send(Remote_NS_Chart *chart, const u16string &message)
    {
        chart->listener().reportInformation(message);
    }

    inline u16string remote(int64_t panel_index)
    {
        auto d = u"#Values:Chart."s;
        d += convertToU16(to_string(panel_index));
        d += u".";
        return d;
    }

    inline u16string remoteS(int64_t panel_index)
    {
        return remote(panel_index) += u"S.";
    }

    inline u16string remoteU(int64_t panel_index)
    {
        return remote(panel_index) += u"U.";
    }

    inline u16string remoteInit(int64_t panel_index, const u16string &title)
    {
        auto d = remoteS(panel_index);
        d += u"init:";
        d += title;
        return d;
    }

    inline u16string remoteInitFixed(
        int64_t panel_index, const u16string &title, const Bounds &bounds
    )
    {
        auto d = remoteS(panel_index);
        d += u"initFixed:";
        d += title;
        (d += u"/") += convertToU16(to_string(bounds.x.min));
        (d += u"/") += convertToU16(to_string(bounds.x.max));
        (d += u"/") += convertToU16(to_string(bounds.y.min));
        (d += u"/") += convertToU16(to_string(bounds.y.max));
        return d;
    }

    inline u16string remoteAddSeries(
        int64_t panel_index, const u16string &name, const int64_t style
    )
    {
        auto d = remoteS(panel_index);
        d += u"addSeries:";
        d += name;
        (d += u"/") += convertToU16(to_string(style));
        return d;
    }

    inline u16string remoteAddPoint(
        int64_t panel_index, const u16string &series_name, const Point point
    )
    {
        auto d = remoteS(panel_index);
        d += u"addPoint:";
        d += series_name;
        (d += u"/") += convertToU16(to_string(point.x));
        (d += u"/") += convertToU16(to_string(point.y));
        return d;
    }

    inline u16string remoteShow(int64_t panel_index)
    {
        auto d = remoteS(panel_index);
        d += u"show:";
        return d;
    }

    bool init(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto chart = static_cast<Remote_NS_Chart *>(p_object);
        SCI_StackGuard<1> guard(stack);

        auto &title = stack.topC().getScalar<u16string>();
        send(chart, remoteInit(0, title));

        return true;
    }

    bool initFixed(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        Remote_NS_Chart *chart = static_cast<Remote_NS_Chart *>(p_object);
        SCI_StackGuard<5> guard(stack);

        auto &title = stack.topC(4).getScalar<u16string>();
        auto x_min = stack.topC(3).getScalar<double>();
        auto x_max = stack.topC(2).getScalar<double>();
        auto y_min = stack.topC(1).getScalar<double>();
        auto y_max = stack.topC(0).getScalar<double>();
        send(chart, remoteInitFixed(0, title, {{x_min, x_max}, {y_min, y_max}}));


        return true;
    }

    bool addSeries(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        Remote_NS_Chart *chart = static_cast<Remote_NS_Chart *>(p_object);
        SCI_StackGuard<2> guard(stack);

        auto &series_name = stack.topC(1).getScalar<u16string>();
        auto series_style = stack.topC(0).getScalar<int64_t>();
        send(chart, remoteAddSeries(0, series_name, series_style));

        return true;
    }

    bool addPoint(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
         if (p_object == nullptr)
            return false;
        Remote_NS_Chart *chart = static_cast<Remote_NS_Chart *>(p_object);
        SCI_StackGuard<3> guard(stack);

        auto &series_name = stack.topC(2).getScalar<u16string>();
        auto x = stack.topC(1).getScalar<double>();
        auto y = stack.topC(0).getScalar<double>();
        send(chart, remoteAddPoint(0, series_name, {x, y}));

        return true;
    }

    bool show(IScriptC_Namespace * p_object, SCI_Stack &)
    {
         if (p_object == nullptr)
            return false;
        Remote_NS_Chart *chart = static_cast<Remote_NS_Chart *>(p_object);

        send(chart, remoteShow(0));

        return true;
    }

    bool panel(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto chart = static_cast<Remote_NS_Chart *>(p_object);
        SCI_StackGuard<2> guard(stack);

        auto panel_index = stack.topC(1).getScalar<int64_t>();
        auto &title = stack.topC(0).getScalar<u16string>();
        send(chart, remoteInit(panel_index, title));

        return true;
    }

    bool panelFixed(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        Remote_NS_Chart *chart = static_cast<Remote_NS_Chart *>(p_object);
        SCI_StackGuard<6> guard(stack);

        auto panel_index = stack.topC(5).getScalar<int64_t>();
        auto &title = stack.topC(4).getScalar<u16string>();
        auto x_min = stack.topC(3).getScalar<double>();
        auto x_max = stack.topC(2).getScalar<double>();
        auto y_min = stack.topC(1).getScalar<double>();
        auto y_max = stack.topC(0).getScalar<double>();
        send(
            chart
            , remoteInitFixed(panel_index, title, {{x_min, x_max}, {y_min, y_max}})
        );

        return true;
    }

    bool panelAddSeries(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        Remote_NS_Chart *chart = static_cast<Remote_NS_Chart *>(p_object);
        SCI_StackGuard<3> guard(stack);

        auto panel_index = stack.topC(2).getScalar<int64_t>();
        auto &series_name = stack.topC(1).getScalar<u16string>();
        auto series_style = stack.topC(0).getScalar<int64_t>();
        send(chart, remoteAddSeries(panel_index, series_name, series_style));

        return true;
    }

    bool panelAddPoint(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
         if (p_object == nullptr)
            return false;
        Remote_NS_Chart *chart = static_cast<Remote_NS_Chart *>(p_object);
        SCI_StackGuard<4> guard(stack);

        auto panel_index = stack.topC(3).getScalar<int64_t>();
        auto &series_name = stack.topC(2).getScalar<u16string>();
        auto x = stack.topC(1).getScalar<double>();
        auto y = stack.topC(0).getScalar<double>();
        send(chart, remoteAddPoint(panel_index, series_name, {x, y}));

        return true;
    }

    bool panelShow(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
         if (p_object == nullptr)
            return false;
        Remote_NS_Chart *chart = static_cast<Remote_NS_Chart *>(p_object);
        SCI_StackGuard<1> guard(stack);

        auto panel_index = stack.topC().getScalar<int64_t>();
        send(chart, remoteShow(panel_index));

        return true;
    }
}

Remote_NS_Chart::Remote_NS_Chart(AReporter &listener)
    : _listener(listener)
{
}

Remote_NS_Chart::~Remote_NS_Chart()
{
}

SCI_Namespace_t Remote_NS_Chart::getNamespace()
{
    return {
        {u"init", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::init}}},
            {u"", SemanticNameQualification::None, {}},
            {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"initFixed", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::initFixed}}},
            {u"", SemanticNameQualification::None, {}},
            {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"x_min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"x_max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"y_min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"y_max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"addSeries", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addSeries}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"series_style", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"addPoint", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addPoint}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"show", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::show}}},
            {u"", SemanticNameQualification::None, {}},
        }},
        {u"panel", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"init", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panel}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            }},
            {u"initFixed", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panelFixed}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
                {u"x_min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"x_max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"y_min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"y_max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            }},
            {u"addSeries", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panelAddSeries}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
                {u"series_style", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            }},
            {u"addPoint", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panelAddPoint}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            }},
            {u"show", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panelShow}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            }},
        }},
        {u"Style", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"Line", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"Spline", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(1)}},
            {u"Scatter", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(2)}},
        }},
    };
}


