/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/ScriptC_NS_Sys.h"

#include "simodo/dsl/Exception.h"
#include "simodo/convert.h"
#include "simodo/version.h"

#include <chrono>

using namespace std;
using namespace simodo::dsl;

namespace
{
//    AReporter * p_listener = nullptr;


    u16string to_string_Name(const SCI_Name &name);

    u16string to_string_Scalar(const SCI_Scalar & value)
    {
        u16string str = u"";

        switch(value.type)
        {
        case SemanticNameType::Int:
            str += simodo::convertToU16(to_string(get<int64_t>(value.variant)));
            break;
        case SemanticNameType::Bool:
            str += get<bool>(value.variant) ? u"true"s : u"false"s;
            break;
        case SemanticNameType::Float:
            str += simodo::convertToU16(simodo::clearNumberFractionalPart(to_string(get<double>(value.variant))));
            break;
        case SemanticNameType::String:
            str += get<u16string>(value.variant);
            break;
        default:
            str +=  u"?";
            break;
        }

        return str;
    }

    u16string to_string_Tuple(SemanticNameQualification qualification, const vector<SCI_Name> & tuple)
    {
        u16string str = u"{";

        bool first = true;

        for(const SCI_Name & n : tuple)
        {
            if (!first)
                str += u", ";

            str += u"'" + n.name + u"'";
            if (qualification == SemanticNameQualification::Tuple)
                str += u"=" + to_string_Name(n);

            first = false;
        }

        return str + u"}";
    }

    u16string to_string_Array(const SCI_Array & value)
    {
        u16string str = u"[";

        bool first = true;

        for(size_t i=0; i < value.values.size(); ++i)
        {
            if (!first) str += u", ";
            str += to_string_Name(value.values[i]);
            first = false;
        }

        return str + u"]";
    }

    u16string to_string_Name(const SCI_Name &name)
    {
        if (name.qualification == SemanticNameQualification::Scalar)
            return to_string_Scalar(get<SCI_Scalar>(name.bulk));
        else if (name.qualification == SemanticNameQualification::Tuple ||
                 name.qualification == SemanticNameQualification::Function ||
                 name.qualification == SemanticNameQualification::Type)
            return to_string_Tuple(name.qualification, get<vector<SCI_Name>>(name.bulk));
        else if (name.qualification == SemanticNameQualification::Array)
            return to_string_Array(get<SCI_Array>(name.bulk));
        else
            return u"?";
    }

    bool SCI_print(IScriptC_Namespace * p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr)
            return false;

        assert(!sci_stack.empty());

        SCI_Name * name = &sci_stack.top();

        while(name->qualification == SemanticNameQualification::Reference)
            name = get<SCI_Reference>(name->bulk);

        u16string str = to_string_Name(*name);

        sci_stack.pop();

        const ScriptC_NS_Sys * sys = static_cast<const ScriptC_NS_Sys *>(p_object);

        sys->listener().reportInformation(str);

        return true;
    }

    bool SCI_typeof(IScriptC_Namespace * , SCI_Stack &sci_stack)
    {
        assert(!sci_stack.empty());

        u16string str;

        SCI_Name * name = &sci_stack.top();

        while(name->qualification == SemanticNameQualification::Reference)
            name = get<SCI_Reference>(name->bulk);

        if (name->qualification == SemanticNameQualification::Scalar)
        {
            const SCI_Scalar & scalar = get<SCI_Scalar>(name->bulk);
            str = getSemanticNameTypeName(scalar.type);
        }
        else
            str = getSemanticNameQualificationName(name->qualification);

        sci_stack.back() = {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, str}};

        return true;
    }

    bool SCI_tostring(IScriptC_Namespace * , SCI_Stack &sci_stack)
    {
        assert(!sci_stack.empty());

        SCI_Name * name = &sci_stack.top();

        while(name->qualification == SemanticNameQualification::Reference)
            name = get<SCI_Reference>(name->bulk);

        sci_stack.back() = {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, to_string_Name(*name)}};

        return true;
    }

    bool SCI_sizeof(IScriptC_Namespace * , SCI_Stack &sci_stack)
    {
        assert(!sci_stack.empty());

        SCI_Name * name = &sci_stack.top();

        while(name->qualification == SemanticNameQualification::Reference)
            name = get<SCI_Reference>(name->bulk);

        int64_t size;

        switch(name->qualification)
        {
        case SemanticNameQualification::None:
            size = 0;
            break;
        case SemanticNameQualification::Scalar:
            {
                const SCI_Scalar & scalar = get<SCI_Scalar>(name->bulk);

                switch(scalar.type)
                {
                case SemanticNameType::Undefined:
                    size = 0;
                    break;
                case SemanticNameType::Bool:
                    size = sizeof(bool);
                    break;
                case SemanticNameType::Int:
                    size = sizeof(int64_t);
                    break;
                case SemanticNameType::Float:
                    size = sizeof(double);
                    break;
                case SemanticNameType::String:
                    size = get<u16string>(scalar.variant).size();
                    break;
                case SemanticNameType::ExtFunction:
                    size = sizeof(SCI_ExtFunction);
                    break;
                case SemanticNameType::IntFunction:
                    size = sizeof(void *);//const AstNode *);
                    break;
                default:
                    size = -1;
                    break;
                }
            }
            break;
        case SemanticNameQualification::Tuple:
        case SemanticNameQualification::Function:
        case SemanticNameQualification::Type:
            size = get<SCI_Tuple>(name->bulk).size();
            break;
        case SemanticNameQualification::Array:
            size = get<SCI_Array>(name->bulk).values.size();
            break;
        default: // default: нужен на случай расширения перечисления, чтобы видеть ошибку
            size = -1;
            break;
        }

        sci_stack.back() = {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, size}};

        return true;
    }

    bool SCI_dimensions(IScriptC_Namespace * , SCI_Stack &sci_stack)
    {
        assert(!sci_stack.empty());

        SCI_Name * name = &sci_stack.top();

        while(name->qualification == SemanticNameQualification::Reference)
            name = get<SCI_Reference>(name->bulk);

        int64_t size;

        switch(name->qualification)
        {
        case SemanticNameQualification::Array:
            size = get<SCI_Array>(name->bulk).dimensions.size();
            break;
        default:
            size = 0;
            break;
        }

        sci_stack.back() = {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, size}};

        return true;
    }

    bool SCI_dimension_size(IScriptC_Namespace * , SCI_Stack &sci_stack)
    {
        assert(!sci_stack.empty());

        SCI_Name * name  = &sci_stack.top(1);
        int64_t    index = get<int64_t>(get<SCI_Scalar>(sci_stack.top(0).bulk).variant);

        while(name->qualification == SemanticNameQualification::Reference)
            name = get<SCI_Reference>(name->bulk);

        int64_t size;

        switch(name->qualification)
        {
        case SemanticNameQualification::Array:
            if (index >= 0 && index < static_cast<int64_t>(get<SCI_Array>(name->bulk).dimensions.size()))
                size = get<SCI_Array>(name->bulk).dimensions[index];
            else
                size = 0;
            break;
        default:
            size = 0;
            break;
        }

        sci_stack.pop();
        sci_stack.back() = {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, size}};

        return true;
    }

    bool SCI_time(IScriptC_Namespace * , SCI_Stack &sci_stack)
    {
        sci_stack.push({ u""
                        , SemanticNameQualification::Scalar
                        , SCI_Scalar { SemanticNameType::Int
                                        , chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count()
                                        }
                        });

        return true;
    }

    bool SCI_error(IScriptC_Namespace * , SCI_Stack &sci_stack)
    {
        assert(!sci_stack.empty());

        auto message = get<u16string>(get<SCI_Scalar>(sci_stack.top(0).bulk).variant);
        throw Exception("sys", "Программное исключение. Сообщение: " + simodo::convertToU8(message));

        return true;
    }

    bool SCI_hasInput(IScriptC_Namespace * m, SCI_Stack &sci_stack)
    {
        assert(!sci_stack.empty());

        auto module = static_cast<ScriptC_NS_Sys *>(m);
        auto key = simodo::convertToU8(get<u16string>(get<SCI_Scalar>(sci_stack.top(0).bulk).variant));

        sci_stack.pop();
        sci_stack.push({ {}
                       , SemanticNameQualification::Scalar
                       , SCI_Scalar{SemanticNameType::Bool, module->hasInput(key)}
                       , {}
                       });
        return true;
    }

    bool SCI_getInput(IScriptC_Namespace * m, SCI_Stack &sci_stack)
    {
        assert(!sci_stack.empty());

        auto module = static_cast<ScriptC_NS_Sys *>(m);
        auto key = simodo::convertToU8(get<u16string>(get<SCI_Scalar>(sci_stack.top(0).bulk).variant));

        sci_stack.pop();
        sci_stack.push({ {}
                       , SemanticNameQualification::Scalar
                       , SCI_Scalar{SemanticNameType::Float, module->getInput(key)}
                       , {}
                       });
        return true;
    }
}

ScriptC_NS_Sys::ScriptC_NS_Sys(AReporter &listener)
    : _listener(listener)
{
}

ScriptC_NS_Sys::~ScriptC_NS_Sys()
{
}

SCI_Namespace_t ScriptC_NS_Sys::getNamespace()
{
    return {
        {u"print", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_print}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_value_", SemanticNameQualification::None, {}}
        }},
        {u"typeof"s, SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_typeof}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"_value_", SemanticNameQualification::None, {}}
        }},
        {u"tostring", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_tostring}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"_value_", SemanticNameQualification::None, {}}
        }},
        {u"sizeof", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_sizeof}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"_value_", SemanticNameQualification::None, {}}
        }},
        {u"dimensions", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_dimensions}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"_value_", SemanticNameQualification::Array, {}},
        }},
        {u"dimension_size", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_dimension_size}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"_value1_", SemanticNameQualification::Array, {}},
            {u"_value2_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"version", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"major", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(simodo::version_major)}},
            {u"middle", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(simodo::version_middle)}},
            {u"minor", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(simodo::version_minor)}},
        }},
        {u"time", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_time}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"error", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_error}}},
                {u"", SemanticNameQualification::None, {}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"hasInput", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_hasInput}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Bool, {}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"getInput", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_getInput}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
    };
}

void ScriptC_NS_Sys::setInput(const string &key, optional<double> value)
{
    _input[key] = value;
}

bool ScriptC_NS_Sys::hasInput(const string &key)
{
    auto it = _input.find(key);
    return it != _input.end() && it->second.has_value();
}

double ScriptC_NS_Sys::getInput(const string &key)
{
    try
    {
        return *_input.at(key);
    }
    catch(...)
    {
        throw Exception("sys", "input '" + key + "' missed");
    }
}
