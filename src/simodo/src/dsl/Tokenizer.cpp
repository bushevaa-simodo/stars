/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/Tokenizer.h"
#include "simodo/dsl/Exception.h"

#include <algorithm>

#define REGULAR_EXPRESSION_no

#ifdef REGULAR_EXPRESSION
#include <locale>   // wstring_convert
#include <codecvt>  // codecvt_utf8
#include <iostream> // cout
#endif

using namespace std;
using namespace simodo::dsl;

#ifdef REGULAR_EXPRESSION
enum ELEMENT_TYPE {
    BASIC,                          ///< обычный элемент маски
    POSSIBLE,                       ///< элемент "[]"
    VARIANTS,                        ///< элемент "{}"
    MASK,                        ///< элемент "{}"
};

struct ElementMask
{
    ELEMENT_TYPE  type;                ///< Тип элемент маски
    std::u16string  value;             ///< Значенеи хронящееся в элементе
    std::vector<ElementMask> child;    ///< Вложенные структуры
    // /****
    // Данная операция работает только соссылками на элемент
    // для этого она нам и нужна, так мы с ее помощью
    // сравниваем элементы массива на эквивалентность
    // в maskFlattenToList, ссылка на живет только в рамках
    // одной области видимости, так что правила безопасного С++
    // не нарушены
    // ****/
    // bool operator== (const ElementMask& right) const
    // {
    //     // std::cout << "ELEMENT MASK COMPARE" << std::endl;
    //     // std::cout << &child << " " << &right.child << std::endl;
    //     return type == right.type && value == right.value && &child == &right.child;
    // }
};

struct StoreElem
{
    ELEMENT_TYPE  type;                ///< Тип элемент маски
    std::vector<unsigned long int>  child;
    // bool operator== (const StoreElem& right) const
    // {
    //     return type == right.type && &child == &right.child;
    // }
};

struct TransformerResult
{
    std::vector<StoreElem> store;
    std::vector<Tokenizer::_NumberMask> result;
};

bool isMask(std::u16string mask);
bool maskValidation(std::u16string mask);
ElementMask addBasic(ElementMask mask, std::u16string val);
ElementMask addElem(ElementMask parent, ElementMask elem);
ElementMask getElem(ElementMask elem);
ElementMask maskTokenizer(std::u16string mask, ELEMENT_TYPE type);
void maskTokenizerPrint(ElementMask mask, std::u16string tabs);
// void printFlattenElement(Tokenizer::_NumberMask child);
// void printFlattenList (std::vector<Tokenizer::_NumberMask> result);
std::string printVector(std::vector<unsigned int> const vec);
std::string printVectorInt(std::vector<int> const vec);
#endif

namespace
{
    std::vector<Tokenizer::_NumberMask> makeInnerMask(const std::vector<NumberMask> &mask_set)
    {
        std::vector<Tokenizer::_NumberMask> result;
#ifdef REGULAR_EXPRESSION
        std::vector<Tokenizer::_NumberMask> tempResult;
        ElementMask tempTokenizer;
        std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> convert;
#endif
        for (const NumberMask & mask : mask_set) {
            if (mask.chars == BUILDING_NUMBER)
            {
                int sh = result.size();

    /* 00 */    result.push_back({u"N", mask.type, TokenQualification::Integer, mask.system, {sh+1,sh+3,sh+4}, true, true});
    /* 01 */    result.push_back({u".", mask.type, TokenQualification::RealNubmer, mask.system, {sh+2,sh+3,sh+4}, false, true});
    /* 02 */    result.push_back({u"N", mask.type, TokenQualification::RealNubmer, mask.system, {sh+3,sh+4}, false, true});
    /* 03 */    result.push_back({u"e", mask.type, TokenQualification::RealNubmer, mask.system, {sh+5,sh+6,sh+7}, false, false});
    /* 04 */    result.push_back({u"E", mask.type, TokenQualification::RealNubmer, mask.system, {sh+5,sh+6,sh+7}, false, false});
    /* 05 */    result.push_back({u"+", mask.type, TokenQualification::RealNubmer, mask.system, {sh+7}, false, false});
    /* 06 */    result.push_back({u"-", mask.type, TokenQualification::RealNubmer, mask.system, {sh+7}, false, false});
    /* 07 */    result.push_back({u"N", mask.type, TokenQualification::RealNubmer, mask.system, {}, false, true});
            }
#ifdef REGULAR_EXPRESSION
            else if (isMask(mask.chars))
            {
                /*
                    has_min_length используется для идентификации потенциально пустых консрукций
                    "{a|c}" эквивалентна строкам "a" и "c"
                    "{[a]|c}" эквивалентна строкам "", "a" и "c",
                    то есть предшественник может ссылаться на "а", на "c", а может не ссылаться
                */
                try
                {
                    tempTokenizer = maskTokenizer(mask.chars);
                    // maskTokenizerPrint(tempTokenizer);
                    // std::cout << "START FLATTENING " << std::endl;
                    tempResult = maskFlattenToList(tempTokenizer);
                    // std::cout << "END FLATTENING " << std::endl;
                    // printFlattenList(tempResult);
                    tempResult = referenceNumberOffset(tempResult, result.size());
                    result.insert(result.end(), tempResult.begin(), tempResult.end());
                    // std::cout << "CORRECT " << std::endl;
                }
                catch (const Exception & e)
                {
                    // std::cout << "ERROR " << std::endl;
                }
            }
#endif
            else
                result.push_back({mask.chars, mask.type, TokenQualification::Integer, mask.system, {}, true, true});
        }

        return result;
    }
}


Tokenizer::Tokenizer(const std::u16string &file_name,
                     IStream &input_stream,
                     const LexicalParameters & parameters,
                     uint8_t tab_size,
                     context_index_t context_no)
    : scanner(file_name,input_stream,tab_size,context_no)
    , param(parameters)
{
    assert(!param.digits.empty());
    assert(!param.latin_alphabet.empty());
//    assert(!param.national_alphabet.empty());
    assert(param.latin_alphabet.size()%2 == 0);
    assert(param.national_alphabet.size()%2 == 0);

    numbers = makeInnerMask(parameters.masks);
}

Token Tokenizer::getToken()
{
    Token t = getAnyToken();

    while(t.getType() == LexemeType::Comment)
        t = getAnyToken();

    return t;
}

Token Tokenizer::getAnyToken()
{
    if (scanner.eof())
    {
        return Token( LexemeType::Empty, u"", scanner.getLocation() );
    }

    // Проверяем контекст
    if (scanner.getLocation().context != NO_TOKEN_CONTEXT_INDEX)
    {
        scanner.fixLocation(scanner.getLocation().context);
        return scanMarkup(scanner.getLocation().context);
    }

    // Пропускаем пробелы
    passBlanks();

    // Фиксируем координаты начала токена
    scanner.fixLocation();

    if (scanner.eof())
    {
        return Token( LexemeType::Empty, u"", scanner.getLocation() );
    }

    // Символ новой строки может быть заменён на спец символ
    if (!param.nl_substitution.empty() && scanner.getChar() == '\n')
    {
        scanner.shift(1);
        return Token( LexemeType::NewLine, param.nl_substitution, scanner.getLocation() );
    }

    // Маркированный текст
    for(size_t i=0; i < param.markups.size(); ++i)
    {
        const MarkupSymbol & mus = param.markups[i];

        if (scanner.startsWith(mus.start))
            return scanMarkup(static_cast<uint32_t>(i));
    }

    // Слово (переменная, идентификатор и пр.)
    if (scanner.startsWithAnyOf(param.id_extra_symbols))
        return scanWord(NationalCharAffiliation::Extra);

    // Слово (переменная, идентификатор и пр.)
    if (scanner.startsWithAnyOf(param.latin_alphabet))
        return scanWord(NationalCharAffiliation::Latin);

    // Слово (переменная, идентификатор и пр.)
    if (scanner.startsWithAnyOf(param.national_alphabet))
        return scanWord(NationalCharAffiliation::National);

    // Число?
    {
        LexemeType          type;
        TokenQualification  qualification;
        u16string           lexeme_str;

        if (scanNumber(type,qualification,lexeme_str))
            return Token( type, lexeme_str, scanner.getLocation(), qualification );
    }

    // Многосимвольная пунктуация (не ключевое слово)
    auto s = std::find_if(
        param.punctuation_words.begin()
        , param.punctuation_words.end()
        , [this](const u16string &s) -> bool
        {
            return scanner.startsWith(s);
        }
    );
    if (s != param.punctuation_words.end())
    {
        if (*s == param.eof_symbol)
            scanner.setEOF();

        scanner.shift(s->size());

        return Token( LexemeType::Punctuation, *s, scanner.getLocation() );
    }

    // Односимвольная пунктуация
    if (scanner.startsWithAnyOf(param.punctuation_chars))
    {
        u16string s;
        s.assign(1, scanner.getFirstChar());

        if (s == param.eof_symbol)
            scanner.setEOF();

        scanner.shift(1);

        return Token( LexemeType::Punctuation, s, scanner.getLocation());
    }

    // Что-то неизвестное, т.е. ошибка
    u16string lexeme_str;

    lexeme_str += scanner.getFirstChar();
    scanner.shift(1);

    return Token( LexemeType::Error, lexeme_str, scanner.getLocation(), TokenQualification::UnknownCharacterSet );
}

void Tokenizer::passBlanks()
{
    while(!scanner.eof())
    {
        if (!param.nl_substitution.empty() && scanner.getChar() == '\n')
            break;
        if (!isBlank(scanner.getChar()))
            break;

        scanner.shift(1);
    }
}

Token Tokenizer::scanMarkup(uint32_t murkup_index)
{
    assert(murkup_index < param.markups.size());

    u16string lexeme_str;
    u16string token_str;

    const MarkupSymbol & mus     = param.markups[murkup_index];
    uint32_t             context = murkup_index;

    if (scanner.getLocation().context != murkup_index)
    {
        scanner.shift(mus.start.size());
        token_str += mus.start;
    }

    while(!scanner.eof())
    {
        if (scanner.startsWith(mus.ignore_sign))
        {
            token_str += mus.ignore_sign;
            scanner.shift(mus.ignore_sign.size());

            if (scanner.eof())
                break;

            token_str += scanner.getFirstChar();
            lexeme_str += scanner.getFirstChar();
            scanner.shift(1);
        }
        else
        {
            if (mus.end.empty())
            {
                if (scanner.getFirstChar() == u'\n')
                    break;
            }
            else if (scanner.startsWith(mus.end))
            {
                token_str += mus.end;
                scanner.shift(mus.end.size());
                context = NO_TOKEN_CONTEXT_INDEX;
                break;
            }

            token_str += scanner.getFirstChar();
            lexeme_str += scanner.getFirstChar();
            scanner.shift(1);
        }
    }

    if (mus.end.empty())
        context = NO_TOKEN_CONTEXT_INDEX;

    TokenLocation loc = scanner.getLocation();
    loc.context = context;

    scanner.fixLocation(context);

    return Token( token_str, mus.type, lexeme_str, loc, murkup_index );
}

Token Tokenizer::scanWord(Tokenizer::NationalCharAffiliation first_char)
{
    bool    has_latin    = (first_char == NationalCharAffiliation::Latin);
    bool    has_national = (first_char == NationalCharAffiliation::National);

    u16string lexeme_str;

    lexeme_str += scanner.getFirstChar();
    scanner.shift(1);

    // Формируем лексему
    while(!scanner.eof())
    {
        if (scanner.startsWithAnyOf(param.id_extra_symbols))
            ;
        else if (scanner.startsWithAnyOf(param.latin_alphabet))
            has_latin = true;
        else if (scanner.startsWithAnyOf(param.national_alphabet))
            has_national = true;
        else if (!scanner.startsWithAnyOf(param.digits))
            break;

        lexeme_str += scanner.getFirstChar();
        scanner.shift(1);
    }

    // Многосимвольная пунктуация
    for(const u16string & s : param.punctuation_words)
    {
        bool is_find;

        if (param.is_case_sensitive)
            is_find = (s == lexeme_str);
        else
            is_find = (s == convertToUpper(lexeme_str));

        if (is_find)
        {
            if (s == param.eof_symbol)
                scanner.setEOF();

            return Token( lexeme_str, LexemeType::Punctuation, u16string(s), scanner.getLocation(), TokenQualification::Keyword );
        }
    }

    // Односимвольная пунктуация
    if (lexeme_str.size() == 1)
        if (param.punctuation_chars.find(*lexeme_str.c_str()) != u16string::npos)
            return Token( LexemeType::Punctuation, lexeme_str, scanner.getLocation(), TokenQualification::Keyword );

    TokenQualification qualification = TokenQualification::None;

    if (has_national)
    {
        if (!param.may_national_letters_use)
            qualification = TokenQualification::NationalCharacterUse;
        else if (has_latin)
            qualification = TokenQualification::NationalCharacterMix;
    }

    return Token( LexemeType::Id, lexeme_str, scanner.getLocation(), qualification );
}

bool Tokenizer::scanNumber(LexemeType &type, TokenQualification &qualification, u16string &lexeme_str)
{
    for(size_t i_starting=0; i_starting < numbers.size(); ++i_starting)
    {
        if (numbers[i_starting].is_statring)
        {
            lexeme_str.clear();

            size_t  i_mask_index = i_starting;
            size_t  i_mask_char  = 0;
            size_t  i_input      = 0;
            int16_t N_count      = -1;

            while(true)
            {
                const _NumberMask & mask    = numbers[i_mask_index];
                char16_t            ch      = scanner.getChar(i_input);

                if (i_mask_char == mask.chars.size())
                {
                    if (lexeme_str.empty())
                        return false;

                    if (mask.chars[i_mask_char-1] == u'n')
                    {
                        char16_t ch_upper;
                        if (mask.system > 10 )
                            ch_upper = convertLatinToUpper(ch);
                        else
                            ch_upper = ch;

                        if (param.digits.find(ch_upper) < mask.system)
                            break;
                    }

                    if (!mask.refs.empty())
                    {
                        size_t i_ref=0;
                        for(; i_ref < mask.refs.size(); ++i_ref)
                        {
                            uint8_t ref_no = mask.refs[i_ref];

                            assert(static_cast<size_t>(ref_no) < numbers.size());

                            const _NumberMask & ref_mask = numbers[static_cast<size_t>(ref_no)];

                            assert(!ref_mask.chars.empty());

                            char16_t ch_upper;

                            if (mask.system > 10 )
                                ch_upper = convertLatinToUpper(ch);
                            else
                                ch_upper = ch;

                            if (ch == ref_mask.chars[0]
                             || ((ref_mask.chars[0] == u'N' || ref_mask.chars[0] == u'n') && param.digits.find(ch_upper) < mask.system))
                                break;
                        }

                        if (i_ref < mask.refs.size())
                        {
                            i_mask_index = static_cast<size_t>(mask.refs[i_ref]);
                            i_mask_char = 0;
                            continue;
                        }

                        if (!mask.may_final)
                            break;
                    }

                    type = mask.type;
                    qualification = mask.qualification;
                    scanner.shift(lexeme_str.size());
                    return true;
                }

                if (mask.chars[i_mask_char] == u'N' || mask.chars[i_mask_char] == u'n')
                {
                    N_count ++;

                    char16_t ch_upper;
                    if (mask.system > 10 )
                        ch_upper = convertLatinToUpper(ch);
                    else
                        ch_upper = ch;

                    if (param.digits.find(ch_upper) < mask.system)
                    {
                        lexeme_str += ch;
                        if (mask.chars[i_mask_char] == u'n')
                            i_mask_char ++;
                    }
                    else if (N_count == 0)
                        break;
                    else if (mask.chars[i_mask_char] == u'n')
                        break;
                    else
                    {
                        i_mask_char ++;
                        continue;
                    }
                }
                else if (mask.chars[i_mask_char] == ch)
                {
                    lexeme_str += ch;
                    i_mask_char ++;
                    N_count = -1;
                }
                else
                    break;

                i_input ++ ;
            }
        }
        // don't remove!
        else
            break;
    }

    if (!lexeme_str.empty())
    {
        scanner.shift(lexeme_str.size());
        type = LexemeType::Error;
        qualification = TokenQualification::NotANumber;
        return true;
    }

    return false;

}

u16string Tokenizer::convertToUpper(const u16string &s) const
{
    u16string res;

    for(char16_t c : s)
        res += convertToUpper(c);

    return res;
}

char16_t Tokenizer::convertToUpper(char16_t ch) const
{
    string::size_type   pos_latin       = param.latin_alphabet.find(ch);
    size_t              latin_size      = param.latin_alphabet.size();
    size_t              national_size   = param.national_alphabet.size();

    if (pos_latin != string::npos)
    {
        if (pos_latin < latin_size/2)
            return param.latin_alphabet.at(latin_size/2+pos_latin);

        return ch;
    }

    string::size_type pos_national = param.national_alphabet.find(ch);

    if (pos_national != string::npos)
        if (pos_national < national_size/2)
            return param.national_alphabet.at(national_size/2+pos_national);

    return ch;
}

char16_t Tokenizer::convertLatinToUpper(char16_t ch) const
{
    string::size_type   pos_latin       = param.latin_alphabet.find(ch);
    size_t              latin_size      = param.latin_alphabet.size();

    if (pos_latin != string::npos)
        if (pos_latin < latin_size/2)
            return param.latin_alphabet.at(latin_size/2+pos_latin);

    return ch;
}

bool Tokenizer::isBlank(char16_t ch) const
{
    return (u16string::npos != u" \t\r\n"s.find(ch));
}

#ifdef REGULAR_EXPRESSION
int symbolCount(const std::u16string &str, char16_t symbol)
{
    int res = 0;
    for (char16_t ch: str)
    {
        if (ch == symbol) res++;
    }
    return res;
}

bool isMask(const std::u16string &mask)
{
    int count_open_brace_variant = symbolCount(mask, u'{');
    int count_close_brace_variant = symbolCount(mask, u'}');
    int count_open_brace_possible = symbolCount(mask, u'[');
    int count_close_brace_possible = symbolCount(mask, u']');

    return (count_open_brace_variant
        + count_close_brace_variant
        + count_open_brace_possible
        + count_close_brace_possible != 0);
}

bool maskValidation(const std::u16string &mask)
{
    int count_open_brace_variant = symbolCount(mask, u'{');
    int count_close_brace_variant = symbolCount(mask, u'}');
    int count_open_brace_possible = symbolCount(mask, u'[');
    int count_close_brace_possible = symbolCount(mask, u']');

    bool not_zero_variant_brace = count_open_brace_variant != 0 || count_close_brace_variant != 0;
    bool not_zero_possible_brace = count_open_brace_possible != 0 || count_close_brace_possible != 0;
    bool eq_close_open_variant_brace = count_open_brace_variant == count_close_brace_variant;
    bool eq_close_open_possible_brace = count_open_brace_possible == count_close_brace_possible;

    // std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> convert;

    // Если скобок нет
    if (!isMask(mask))
        return true;

    // Если есть ВОЗМОЖНЫЕ и ВАРИАНТНЫЕ скобки
    if (not_zero_variant_brace && not_zero_possible_brace)
    {
        return eq_close_open_variant_brace && eq_close_open_possible_brace;
    }

    // Если есть только ВАРИАНТНЫЕ скобки
    if (not_zero_variant_brace)
    {
        return eq_close_open_variant_brace;
    }

    // Если есть только ВОЗМОЖНЫЕ скобки
    if (not_zero_possible_brace)
    {
        return eq_close_open_possible_brace;
    }

    return false;
}

ElementMask addBasic(ElementMask mask, std::u16string val)
{
    if (val.size() != 0) {
        mask.child.push_back({ELEMENT_TYPE::BASIC, val, {}});
    }

    return mask;
}

ElementMask addElem(ElementMask parent, ElementMask elem)
{
    if (elem.child.size() != 0 || elem.type == ELEMENT_TYPE::BASIC) {
        parent.child.push_back(elem);
    }

    return parent;
}

ElementMask getElem(ElementMask elem)
{
    return elem.type != ELEMENT_TYPE::BASIC
            && elem.type != ELEMENT_TYPE::POSSIBLE
            && elem.child.size() == 1 ?
                getElem(elem.child[0]) :
                elem;
}

/* 
    Функция генерирует массив рекурсивных структур из которых будет удобнее делать списковую структуру
*/
ElementMask maskTokenizer(const std::u16string &mask, ELEMENT_TYPE type = ELEMENT_TYPE::MASK) {

    ElementMask  result = {type, u"", {}};
    std::u16string  temp_str;
    bool startVariant = false;
    // bool startVarticalLine = false;
    bool startPossible = false;
    int levelVariant = 0;
    int levelPossible = 0;

    // std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> convert;
    // std::string stype;
    // switch (type) {
    //     case ELEMENT_TYPE::BASIC:
    //         stype = "BASIC";
    //         break;
    //     case ELEMENT_TYPE::POSSIBLE:
    //         stype = "POSSIBLE";
    //         break;
    //     case ELEMENT_TYPE::VARIANTS:
    //         stype = "VARIANTS";
    //         break;
    //     case ELEMENT_TYPE::MASK:
    //         stype = "MASK";
    //         break;
    // }
    // std::cout << "TYPE " << stype << std::endl;
    // std::cout << "SUBMASK " << convert.to_bytes(mask.c_str()) << std::endl;

    /* 
        Проверяем маску и подмаску на наличие ошибок
    */
    if (!maskValidation(mask))
        throw Exception("maskTokenizer", "некорректная маска");

    for (char16_t ch: mask)
    {
        if (ch == u'[' && type != ELEMENT_TYPE::VARIANTS)
        {
            if (startVariant == false && startPossible == false )
            {
                result = addBasic(result, temp_str);
                temp_str = u"";
                startPossible = true;
                levelPossible = 1;
            }
            else if (startVariant)
            {
                temp_str += ch;
            }
            else if (startPossible)
            {
                levelPossible++;
                temp_str += ch;
            }
            
        }
        else if (ch == u']'  && type != ELEMENT_TYPE::VARIANTS)
        {
            if (startPossible) {
                levelPossible--;
                if (levelPossible == 0)
                {
                    startPossible = false;
                    try
                    {
                        result = addElem(result, getElem(maskTokenizer(temp_str, ELEMENT_TYPE::POSSIBLE)));
                        temp_str = u"";
                    }
                    catch (const Exception & e)
                    {
                        throw;
                    }
                }
                else
                {
                    temp_str += ch;
                }
            }
            else
            {
                temp_str += ch;
            }
        }
        else if (ch == u'{' && type != ELEMENT_TYPE::VARIANTS)
        {
            if (startVariant == false && startPossible == false ) {
                result = addBasic(result, temp_str);
                temp_str = u"";
                startVariant = true;
                levelVariant = 1;
            }
            else if (startVariant)
            {
                levelVariant++;
                temp_str += ch;
            }
            else if (startPossible)
            {
                temp_str += ch;
            }
        }
        else if (ch == u'|')
        {
            if (levelVariant == 0)
            {
                switch (type) {
                    case ELEMENT_TYPE::VARIANTS:
                        try
                        {
                            // maskTokenizerPrint(maskTokenizer(temp_str), u"");
                            result = addElem(result, getElem(maskTokenizer(temp_str)));
                        }
                        catch (const Exception & e)
                        {
                            throw;
                        }
                        temp_str = u"";
                        break;
                    default:
                        temp_str += ch;
                        break;
                }
            }
            else
            {
                temp_str += ch;
            }
        }
        else if (ch == u'}' && type != ELEMENT_TYPE::VARIANTS)
        {
            if (startVariant) {
                levelVariant--;
                if (levelVariant == 0)
                {
                    startVariant = false;
                    try
                    {
                        result = addElem(result, getElem(maskTokenizer(temp_str, ELEMENT_TYPE::VARIANTS)));
                        temp_str = u"";
                    }
                    catch (const Exception & e)
                    {
                        throw;
                    }
                }
                else
                {
                    temp_str += ch;
                }
            }
            else
            {
                temp_str += ch;
            }
        }
        else
        {
            temp_str += ch;
        }
    }

    if (type == ELEMENT_TYPE::VARIANTS)
    {
        result = addElem(result, getElem(maskTokenizer(temp_str)));
    }
    else
    {
        result = addBasic(result, temp_str);
    }

    // if (temp_str.size() != 0 && type == ELEMENT_TYPE::VARIANTS)
    //     std::cout << "END SUBMASK " << convert.to_bytes(temp_str.c_str()) << std::endl;
        
    // if (type == ELEMENT_TYPE::VARIANTS)
    //     maskTokenizerPrint(result, u"\t");

    return result;

}

void maskTokenizerPrint(ElementMask mask, const std::u16string &tabs = u"") {

    std::string type = "";
    std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> convert;

    switch (mask.type) {
        case ELEMENT_TYPE::BASIC:
            type = "BASIC";
            break;
        case ELEMENT_TYPE::POSSIBLE:
            type = "POSSIBLE";
            break;
        case ELEMENT_TYPE::VARIANTS:
            type = "VARIANTS";
            break;
        case ELEMENT_TYPE::MASK:
            type = "MASK";
            break;
    }
    std::cout << convert.to_bytes(tabs.c_str()) << type << std::endl;
    std::cout << convert.to_bytes(tabs.c_str()) << "CHILD: " << mask.child.size() << std::endl;
    if (mask.type == ELEMENT_TYPE::BASIC)
        std::cout << convert.to_bytes(tabs.c_str()) << convert.to_bytes(mask.value.c_str()) << std::endl;

    if (mask.child.size() != 0) {
        for (ElementMask child: mask.child)
        {
            maskTokenizerPrint(child, tabs+u'\t');
            std::cout << std::endl;
        }
    }

}

std::vector<unsigned int> findStartElemMask ( std::vector<Tokenizer::_NumberMask> result, StoreElem storeElem) {
    std::vector<unsigned int> res;
    for (auto index: storeElem.child) {
        if (result[index].is_statring) {
            res.push_back(index);
        }
    }

    return res;
}

std::vector<unsigned int> findStopElemMask ( std::vector<Tokenizer::_NumberMask> result, StoreElem storeElem) {
    std::vector<unsigned int> res;
    for (auto index: storeElem.child) {
        if (result[index].may_final) {
            res.push_back(index);
        }
    }

    return res;
}

std::vector<Tokenizer::_NumberMask> maskResultBackPropagationTransformer ( 
    std::vector<Tokenizer::_NumberMask> result,
    std::vector<StoreElem> store)
{

    if (store.size() <= 1) return result;

    StoreElem current;
    StoreElem target = store.back();
    ELEMENT_TYPE targetType = target.type;
    std::vector<unsigned int> targetStartElems = findStartElemMask(result, target);
    // std::vector<unsigned long int> targetStopElems = findStopElemMask(result, store.back());
    std::vector<unsigned int> startElems;
    std::vector<unsigned int> stopElems;

    // std::cout << "CHECK TARGET START ELEM" << std::endl;
    // std::cout << printVector(targetStartElems) << std::endl;
    // std::cout << "END CHECK TARGET" << std::endl;
    // std::cout << "STORE LENGTH:" << store.size() << std::endl;

    // bool possibleElem = false;

    for (int index = store.size() - 2; 0 <= index; index--)
    {
        current = store[index];
        startElems = findStartElemMask(result, current);
        stopElems = findStopElemMask(result, current);

        // std::cout << "CHECK START ELEM" << std::endl;
        // std::cout << printVector(startElems) << std::endl;
        // std::cout << "CHECK STOP ELEM" << std::endl;
        // std::cout << printVector(stopElems) << std::endl;
        // std::cout << "END CHECK" << std::endl;

        for (auto n: stopElems) {
            for (auto k: targetStartElems) {
                result[n].refs.push_back(k);
                /*************
                Если элемент n - "возможный"
                То нельзя чтобы изменилась метка "последний" у n-1 элемента, так как 
                при вставке n+1 элемента нельзя будет указать ссылку у n-1 элемента на n+1
                *************/
                if (targetType != ELEMENT_TYPE::POSSIBLE) {
                    result[n].may_final = false;
                }
                
                /*************
                Если элемент n-1 - "возможный" и у него есть стартовые элементы, то
                у элемента n нужно сохранить стартовые элементы, так иначе если
                первый элемент маски "возможный", а второй нет, точек входа у нас две, это
                "возможный элемент" и второй
                *************/
                if ( !(current.type == ELEMENT_TYPE::POSSIBLE && startElems.size() != 0) ) {
                    result[k].is_statring = false;
                }
            }
        }

        if (current.type != ELEMENT_TYPE::POSSIBLE) {
            break;
        }
    }

    return result;
}

std::vector<Tokenizer::_NumberMask> maskFlattenToList (ElementMask mask)
{
    std::vector<Tokenizer::_NumberMask> result;
    std::vector<Tokenizer::_NumberMask> subResult;
    std::vector<Tokenizer::_NumberMask> variantSubResult;
    std::vector<StoreElem> store;
    // bool start = true;
    // bool stop = false;
    StoreElem tempStoreElement;
    TransformerResult tempTransormedResult;
    if  (mask.type != ELEMENT_TYPE::BASIC)
    {
        for (ElementMask& child : mask.child)
        {
            // if (child == mask.child.back()) {
            //     stop = true;
            // }

            switch (child.type) {
                case ELEMENT_TYPE::BASIC:
                    result.push_back({
                        child.value,
                        LexemeType::Number,
                        TokenQualification::Integer,
                        10,
                        {},
                        true,
                        true
                    });
                    // std::cout << "CHECK BASIK" << std::endl;
                    // printFlattenElement({
                    //     child.value,
                    //     LexemeType::Number,
                    //     TokenQualification::Integer,
                    //     10,
                    //     {},
                    //     start,
                    //     stop
                    // });
                    store.push_back({ ELEMENT_TYPE::BASIC, { result.size()-1 } });
                    // result = tempTransormedResult.result;
                    // store = tempTransormedResult.store;
                    break;
                case ELEMENT_TYPE::VARIANTS:
                    tempStoreElement = {};
                    tempStoreElement.type = child.type;
                    for (ElementMask& variant_child : child.child) {
                        variantSubResult = maskFlattenToList(variant_child);
                        for (unsigned int i = 0; i < variantSubResult.size(); i++)
                        {
                            result.push_back(variantSubResult[i]);
                            tempStoreElement.child.push_back(result.size()-1);
                        }
                    }
                    store.push_back(tempStoreElement);
                    break;
                case ELEMENT_TYPE::POSSIBLE:
                case ELEMENT_TYPE::MASK:
                    subResult = maskFlattenToList(child);

                    tempStoreElement = {};
                    tempStoreElement.type = child.type;
                    for (unsigned int i = 0; i < subResult.size(); i++)
                    {
                        result.push_back(subResult[i]);
                        tempStoreElement.child.push_back(result.size()-1);
                    }
                    store.push_back(tempStoreElement);
                    // result = tempTransormedResult.result;
                    // store = tempTransormedResult.store;
                    break;
            }

            // if (child.type != ELEMENT_TYPE::POSSIBLE) {
            //     start = false;
            // }
            // std::cout << "BEFORE BACK PROPAGATION" << std::endl;
            // printFlattenList(result);
            result = maskResultBackPropagationTransformer(result, store);
            // std::cout << "AFTER BACK PROPAGATION" << std::endl;
            // printFlattenList(result);

        }
        
    }
    else
    {
        result = {{ mask.value, LexemeType::Number, TokenQualification::Integer, 10, {}, true, true }};
    }

    return result;
}

std::string printVector(const std::vector<unsigned int> &vec)
{
    std::string refs = "{ ";
    // std::cout << "LENGTH: " << vec.size() << std::endl;
    for (auto number: vec) {
        refs += std::to_string(number) + " ";
    }
    refs += "}";

    return refs;
}

std::string printVectorInt(const std::vector<int> &vec)
{
    std::string refs = "{ ";
    // std::cout << "LENGTH: " << vec.size() << std::endl;
    for (auto number: vec) {
        refs += std::to_string(number) + " ";
    }
    refs += "}";

    return refs;
}

// void printFlattenElement(Tokenizer::_NumberMask child) {

//     std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> convert;
//     std::string lexemeType = "";
//     std::string tokenQualification = "";

//     switch (child.type) {
//         case LexemeType::Number:
//             lexemeType = "Number";
//             break;
//     }

//     switch (child.qualification) {
//         case TokenQualification::Integer:
//             tokenQualification = "Number";
//             break;
//         case TokenQualification::RealNubmer:
//             tokenQualification = "RealNubmer";
//             break;
//     }

//     std::cout << "{ " << convert.to_bytes(child.chars.c_str()) << ", " << lexemeType << ", " << tokenQualification << ", " << child.system << ", " << printVectorInt(child.refs) << ", " << child.is_statring << ", " << child.may_final << " }" << std::endl;

// }

// void printFlattenList (std::vector<Tokenizer::_NumberMask> result) {
//     for (auto child: result) {
//         printFlattenElement(child);
//     }
// }

std::vector<Tokenizer::_NumberMask> referenceNumberOffset(std::vector<Tokenizer::_NumberMask> result, int position)
{
    for (auto elem: result) {
        for (unsigned long int index = 0; index < elem.refs.size(); index++) {
            elem.refs[index] += position;
        }
    }

    return result;
}
#endif
