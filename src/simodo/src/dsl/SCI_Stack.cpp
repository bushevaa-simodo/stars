/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include "simodo/dsl/SCI_Stack.h"
#include "simodo/dsl/Exception.h"

using namespace std;
using namespace simodo::dsl;

SCI_Stack_Impl::SCI_Stack_Impl(size_t stack_max_size)
    : _stack_max_size(stack_max_size)
{
    _stack.reserve(stack_max_size);
}

void SCI_Stack_Impl::pop(int count)
{
    while(count-- > 0)
        _stack.pop_back();
}

const SCI_Name & SCI_Stack_Impl::push(const SCI_Scalar & value, u16string name, SemanticNameAccess access)
{
    push({name, SemanticNameQualification::Scalar, value, access});

    return _stack.back();
}

const SCI_Name &SCI_Stack_Impl::push(const SCI_Tuple & value, u16string name, SemanticNameAccess access)
{
    push({name, SemanticNameQualification::Tuple, value, access});

    return _stack.back();
}

const SCI_Name &SCI_Stack_Impl::push(const SCI_Array & value, u16string name, SemanticNameAccess access)
{
    push({name, SemanticNameQualification::Array, value, access});

    return _stack.back();
}

const SCI_Name &SCI_Stack_Impl::push(const SCI_Name & value)
{
    if (_stack.size() == _stack_max_size)
        throw Exception("SCI_Stack::push", "Переполнение размера стека");

    _stack.push_back(value);

    return _stack.back();
}

size_t SCI_Stack_Impl::findLocal(u16string name, size_t local_boundary) const
{
    for(size_t i=_stack.size()-1; i >= local_boundary && i < _stack.size(); --i)
        if (_stack[i].name == name)
            return i;

    return UNDEFINED_INDEX;
}

size_t SCI_Stack_Impl::findGlobal(u16string name, size_t global_boundary) const
{
    for(size_t i=global_boundary-1; i < _stack.size(); --i)
        if (_stack[i].name == name)
            return i;

    return UNDEFINED_INDEX;
}

size_t SCI_Stack_Impl::find(u16string name) const
{
    for(size_t i=_stack.size()-1; i < _stack.size(); --i)
        if (_stack[i].name == name)
            return i;

    return UNDEFINED_INDEX;
}

SCI_Name &SCI_Stack_Impl::at(size_t index)
{
    if (index >= _stack.size())
        throw Exception("SCI_Stack::at", "Выход за пределы стека");

    return _stack[index];
}

SCI_Name &SCI_Stack_Impl::top(size_t index)
{
    if (index >= _stack.size())
        throw Exception("SCI_Stack::top", "Выход за пределы стека");

    return _stack[_stack.size()-index-1];
}

SCI_Name &SCI_Stack_Impl::back()
{
    return top();
}

const SCI_Name &SCI_Stack_Impl::atC(size_t index) const
{
    if (index >= _stack.size())
        throw Exception("SCI_Stack::at", "Выход за пределы стека");

    return _stack[index];
}

const SCI_Name &SCI_Stack_Impl::topC(size_t index) const
{
    if (index >= _stack.size())
        throw Exception("SCI_Stack::top", "Выход за пределы стека");

    return _stack[_stack.size()-index-1];
}

const SCI_Name &SCI_Stack_Impl::backC() const
{
    return topC();
}

void SCI_Stack_Impl::moveTo(size_t from, size_t to)
{
    if (from >= _stack.size() || to >= _stack.size())
        throw Exception("SCI_Stack::moveTo", "Выход за пределы стека");

    _stack[to] = move(_stack[from]);
}

SCI_Stack_Wrap::SCI_Stack_Wrap(SCI_Stack &other)
    : _base_stack(other)
    , _custom_stack(other.stack_max_size() - other.size())
{}

void SCI_Stack_Wrap::pop(int count)
{
    while(!_custom_stack.empty() && 0 < count)
    {
        _custom_stack.pop();
        --count;
    }

    if (0 < count)
    {
        throw Exception(
            "SCI_Stack_Wrap::pop"
            , "Попытка удалить элемент из базового стека"
        );
    }
}

const SCI_Name & SCI_Stack_Wrap::push(const SCI_Scalar & value, u16string name, SemanticNameAccess access)
{
    push({name, SemanticNameQualification::Scalar, value, access});

    return _custom_stack.back();
}

const SCI_Name &SCI_Stack_Wrap::push(const SCI_Tuple & value, u16string name, SemanticNameAccess access)
{
    push({name, SemanticNameQualification::Tuple, value, access});

    return _custom_stack.back();
}

const SCI_Name &SCI_Stack_Wrap::push(const SCI_Array & value, u16string name, SemanticNameAccess access)
{
    push({name, SemanticNameQualification::Array, value, access});

    return _custom_stack.back();
}

const SCI_Name &SCI_Stack_Wrap::push(const SCI_Name & value)
{
    _custom_stack.push(value);

    return _custom_stack.back();
}

size_t SCI_Stack_Wrap::findLocal(u16string name, size_t local_boundary) const
{
    auto found = _custom_stack.findLocal(name, local_boundary - _base_stack.size());

    if (found != UNDEFINED_INDEX) return found + _base_stack.size();

    return _base_stack.findLocal(name, local_boundary);
}

size_t SCI_Stack_Wrap::findGlobal(u16string name, size_t global_boundary) const
{
    auto found = _custom_stack.findGlobal(name, global_boundary - _base_stack.size());

    if (found != UNDEFINED_INDEX) return found + _base_stack.size();

    return _base_stack.findGlobal(name, global_boundary);
}

size_t SCI_Stack_Wrap::find(u16string name) const
{
    auto found = _custom_stack.find(name);

    if (found != UNDEFINED_INDEX) return found + _base_stack.size();

    return _base_stack.find(name);
}

SCI_Name &SCI_Stack_Wrap::at(size_t index)
{
    if (index < _base_stack.size()) return _base_stack.at(index);

    return _custom_stack.at(index - _base_stack.size());
}

SCI_Name &SCI_Stack_Wrap::top(size_t index)
{
    if (index < _custom_stack.size()) return _custom_stack.top(index);

    return _base_stack.top(index - _custom_stack.size());
}

SCI_Name &SCI_Stack_Wrap::back()
{
    return top();
}

const SCI_Name &SCI_Stack_Wrap::atC(size_t index) const
{
    if (index < _base_stack.size()) return _base_stack.atC(index);

    return _custom_stack.atC(index - _base_stack.size());
}

const SCI_Name &SCI_Stack_Wrap::topC(size_t index) const
{
    if (index < _custom_stack.size()) return _custom_stack.topC(index);

    return _base_stack.top(index - _custom_stack.size());
}

const SCI_Name &SCI_Stack_Wrap::backC() const
{
    return topC();
}

void SCI_Stack_Wrap::moveTo(size_t from, size_t to)
{
    at(to) = move(at(from));
}
