/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include "simodo/dsl/ScriptC_NS_TableFunction.h"

#include "simodo/dsl/tablefunction/TableFunction.h"
#include "simodo/dsl/tablefunction/v2/TableFunction.h"

#include "simodo/dsl/SCI_StackGuard.h"

using namespace std;
using namespace simodo::dsl;

namespace
{
    bool SCI_tf(IScriptC_Namespace *, SCI_Stack &sci_stack);
    bool SCI_tf_v2(IScriptC_Namespace *, SCI_Stack &sci_stack);
}

SCI_Namespace_t ScriptC_NS_TableFunction::getNamespace()
{
    return {
        {u"eval", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_tf}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_table_", SemanticNameQualification::None, {}},
            {u"_args_", SemanticNameQualification::None, {}},
        }},
        {u"v2", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"eval", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_tf_v2}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"_table_", SemanticNameQualification::None, {}},
                {u"_args_", SemanticNameQualification::None, {}},
            }},
        }},
    };
}

//#define SCI_DEBUG

#ifdef SCI_DEBUG
#include <iostream>
#endif

namespace
{
    bool SCI_tf(IScriptC_Namespace *module, SCI_Stack &sci_stack)
    {
        constexpr auto eval = [](
            IScriptC_Namespace *module, SCI_Stack &sci_stack
        ) -> double
        {
            SCI_StackGuard<2> guard(sci_stack);

            auto &&table = tablefunction::createTable<int64_t, double>(module, sci_stack);
            auto &&args = tablefunction::getArgs<int64_t, double>(
                module, sci_stack, table.dimenses.size()
            );

            auto &&hc = tablefunction::findHyperCube<int64_t, double>(table, args);

            return tablefunction::linearInterpolation<int64_t, double>(hc, args);
        };

        sci_stack.push({
            u""
            , SemanticNameQualification::Scalar
            , SCI_Scalar {SemanticNameType::Float, eval(module, sci_stack)}
        });

        return true;
    }

    bool SCI_tf_v2(IScriptC_Namespace *, SCI_Stack &sci_stack)
    {
        constexpr auto eval = [](SCI_Stack &sci_stack) -> double
        {
            SCI_StackGuard<2> guard(sci_stack);

            auto &table_name = sci_stack.topC(1).deref();
            auto &args_name = sci_stack.topC(0).deref();

#ifdef SCI_DEBUG
            std::cout << "createTable" << std::endl;
#endif
            auto &&table = tablefunction::v2::createTable<int64_t, double>(table_name);
#ifdef SCI_DEBUG
            std::cout << "getArgs" << std::endl;
#endif
            auto &&args = tablefunction::v2::getArgs<int64_t, double>(args_name, table);
#ifdef SCI_DEBUG
            std::cout << "findHyperCube" << std::endl;
#endif
            auto &&hc = tablefunction::v2::findHyperCube<int64_t, double>(table, args);

#ifdef SCI_DEBUG
            std::cout << "linearInterpolation" << std::endl;
#endif
            return tablefunction::linearInterpolation<int64_t, double>(hc, args);
        };

        sci_stack.push({
            u""
            , SemanticNameQualification::Scalar
            , SCI_Scalar {SemanticNameType::Float, eval(sci_stack)}
        });

        return true;
    }
}
