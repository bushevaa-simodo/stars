/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/Remote_NS_Graph3D.h"

#include "simodo/dsl/SCI_StackGuard.h"
#include "simodo/dsl/Exception.h"
#include "simodo/convert.h"
#include "simodo/version.h"

#include <chrono>

using namespace std;
using namespace simodo;
using namespace simodo::dsl;

namespace
{
    struct Point
    {
        double x, y, z;
    };

    inline void send(Remote_NS_Graph3D *graph3d, const u16string &message)
    {
        graph3d->listener().reportInformation(message);
    }

    inline u16string remote(int64_t panel_index)
    {
        auto d = u"#Values:Graph3D."s;
        d += convertToU16(std::to_string(panel_index));
        d += u".";
        return d;
    }

    inline u16string remoteS(int64_t panel_index)
    {
        return remote(panel_index) += u"S.";
    }

    inline u16string remoteU(int64_t panel_index)
    {
        return remote(panel_index) += u"U.";
    }

    inline u16string remoteInit(int64_t panel_index, const u16string &title)
    {
        auto d = remoteS(panel_index);
        d += u"init:";
        d += title;
        return d;
    }

    inline u16string remoteAddSeries(
        int64_t panel_index, const u16string &name, const int64_t mesh
    )
    {
        auto d = remoteS(panel_index);
        d += u"addSeries:";
        d += name;
        (d += u"/") += convertToU16(to_string(mesh));
        return d;
    }

    inline u16string remoteAddPoint(
        int64_t panel_index, const u16string &series_name, const Point &point
    )
    {
        auto d = remoteS(panel_index);
        d += u"addPoint:";
        d += series_name;
        (d += u"/") += convertToU16(to_string(point.x));
        (d += u"/") += convertToU16(to_string(point.y));
        (d += u"/") += convertToU16(to_string(point.z));
        return d;
    }

    inline u16string remoteSetPointsCount(
        int64_t panel_index, const u16string &series_name, const int points_count
    )
    {
        auto d = remoteS(panel_index);
        d += u"setPointsCount:";
        d += series_name;
        (d += u"/") += convertToU16(to_string(points_count));
        return d;
    }

    bool init(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto graph3d = static_cast<Remote_NS_Graph3D *>(p_object);
        SCI_StackGuard<1> guard(stack);

        auto &title = stack.topC().getScalar<u16string>();
        send(graph3d, remoteInit(0, title));

        return true;
    }

    bool addSeries(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto graph3d = static_cast<Remote_NS_Graph3D *>(p_object);
        SCI_StackGuard<2> guard(stack);

        auto &title = stack.topC(1).getScalar<u16string>();
        auto mesh = stack.topC(0).getScalar<int64_t>();
        send(graph3d, remoteAddSeries(0, title, mesh));

        return true;
    }

    bool addPoint(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto graph3d = static_cast<Remote_NS_Graph3D *>(p_object);
        SCI_StackGuard<4> guard(stack);

        auto &title = stack.topC(3).getScalar<u16string>();
        auto x = stack.topC(2).getScalar<double>();
        auto y = stack.topC(1).getScalar<double>();
        auto z = stack.topC(0).getScalar<double>();
        send(graph3d, remoteAddPoint(0, title, {x, y, z}));

        return true;
    }

    bool setPointsCount(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto graph3d = static_cast<Remote_NS_Graph3D *>(p_object);
        SCI_StackGuard<2> guard(stack);

        auto &title = stack.topC(1).getScalar<u16string>();
        auto points_count = stack.topC(0).getScalar<int64_t>();
        send(graph3d, remoteSetPointsCount(0, title, points_count));

        return true;
    }

    bool panel(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto graph3d = static_cast<Remote_NS_Graph3D *>(p_object);
        SCI_StackGuard<1> guard(stack);

        auto panel_index = stack.topC(1).getScalar<int64_t>();
        auto &title = stack.topC(0).getScalar<u16string>();
        send(graph3d, remoteInit(panel_index, title));

        return true;
    }

    bool panelAddSeries(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto graph3d = static_cast<Remote_NS_Graph3D *>(p_object);
        SCI_StackGuard<3> guard(stack);

        auto panel_index = stack.topC(2).getScalar<int64_t>();
        auto &title = stack.topC(1).getScalar<u16string>();
        auto mesh = stack.topC(0).getScalar<int64_t>();
        send(graph3d, remoteAddSeries(panel_index, title, mesh));

        return true;
    }

    bool panelAddPoint(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto graph3d = static_cast<Remote_NS_Graph3D *>(p_object);
        SCI_StackGuard<5> guard(stack);

        auto panel_index = stack.topC(4).getScalar<int64_t>();
        auto &title = stack.topC(3).getScalar<u16string>();
        auto x = stack.topC(2).getScalar<double>();
        auto y = stack.topC(1).getScalar<double>();
        auto z = stack.topC(0).getScalar<double>();
        send(graph3d, remoteAddPoint(panel_index, title, {x, y, z}));

        return true;
    }

    bool panelSetPointsCount(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto graph3d = static_cast<Remote_NS_Graph3D *>(p_object);
        SCI_StackGuard<3> guard(stack);

        auto panel_index = stack.topC(2).getScalar<int64_t>();
        auto &title = stack.topC(1).getScalar<u16string>();
        auto points_count = stack.topC(0).getScalar<int64_t>();
        send(graph3d, remoteSetPointsCount(panel_index, title, points_count));

        return true;
    }
}

Remote_NS_Graph3D::Remote_NS_Graph3D(AReporter &listener)
    : _listener(listener)
{
}

Remote_NS_Graph3D::~Remote_NS_Graph3D()
{
}

SCI_Namespace_t Remote_NS_Graph3D::getNamespace()
{
    return {
        {u"init", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::init}}},
            {u"", SemanticNameQualification::None, {}},
            {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"addSeries", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addSeries}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"series_mesh", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"addPoint", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addPoint}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"x", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"y", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"z", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"setPointsCount", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setPointsCount}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"points_count", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"panel", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"init", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panel}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            }},
            {u"addSeries", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panelAddSeries}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
                {u"series_mesh", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            }},
            {u"addPoint", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panelAddPoint}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
                {u"x", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"y", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"z", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            }},
            {u"setPointsCount", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panelSetPointsCount}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
                {u"points_count", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            }},
        }},
        {u"Mesh", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"Bar", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(1)}},
            {u"Cube", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(2)}},
            {u"Pyramid", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(3)}},
            {u"Cone", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(4)}},
            {u"Cylinder", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(5)}},
            {u"BevelBar", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(6)}},
            {u"BevelCube", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(7)}},
            {u"Sphere", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(8)}},
            {u"Minimal", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(9)}},
            {u"Arrow", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(10)}},
            {u"Point", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(11)}},
        }},
    };
}


