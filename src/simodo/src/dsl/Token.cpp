/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include "simodo/dsl/Token.h"

using namespace std;
using namespace simodo::dsl;

u16string simodo::dsl::getQualificationName(TokenQualification qua)
{
    switch(qua)
    {
    case TokenQualification::None:
        return u"None";
    case TokenQualification::Keyword:
        return u"Keyword";
    case TokenQualification::Integer:
        return u"Integer";
    case TokenQualification::RealNubmer:
        return u"RealNubmer";
    case TokenQualification::NewLine:
        return u"NewLine";
    case TokenQualification::NationalCharacterUse:
        return u"NationalCharacterUse";
    case TokenQualification::NationalCharacterMix:
        return u"NationalCharacterMix";
    case TokenQualification::UnknownCharacterSet:
        return u"UnknownCharacterSet";
    case TokenQualification::NotANumber:
        return u"NotANumber";
    default:
        return u"*****";
    }
}
