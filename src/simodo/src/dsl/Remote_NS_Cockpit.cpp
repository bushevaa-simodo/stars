/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/Remote_NS_Cockpit.h"

#include "simodo/dsl/SCI_StackGuard.h"
#include "simodo/dsl/Exception.h"
#include "simodo/convert.h"
#include "simodo/version.h"

#include <chrono>

using namespace std;
using namespace simodo;
using namespace simodo::dsl;

namespace
{
    struct Position
    {
        double x, height, z;
    };

    struct Angles
    {
        double pitch, course, roll;
    };

    inline void send(Remote_NS_Cockpit *cockpit, const u16string &message)
    {
        cockpit->listener().reportInformation(message);
    }

    inline u16string remote(int64_t panel_index)
    {
        auto d = u"#Values:Cockpit."s;
        d += convertToU16(to_string(panel_index));
        d += u".";
        return d;
    }

    inline u16string remoteS(int64_t panel_index)
    {
        return remote(panel_index) += u"S.";
    }

    inline u16string remoteU(int64_t panel_index)
    {
        return remote(panel_index) += u"U.";
    }

    inline u16string remoteInit(int64_t panel_index, const u16string &title)
    {
        auto d = remoteS(panel_index);
        d += u"init:";
        d += title;
        return d;
    }

    inline u16string remoteSetPosition(
        int64_t panel_index, const Position &position
    )
    {
        auto d = remoteU(panel_index);
        d += u"setPosition:";
        d += convertToU16(to_string(position.x));
        (d += u"/") += convertToU16(to_string(position.height));
        (d += u"/") += convertToU16(to_string(position.z));
        return d;
    }

    inline u16string remoteSetAngles(int64_t panel_index, const Angles &angles)
    {
        auto d = remoteU(panel_index);
        d += u"setAngles:";
        d += convertToU16(to_string(angles.pitch));
        (d += u"/") += convertToU16(to_string(angles.course));
        (d += u"/") += convertToU16(to_string(angles.roll));
        return d;
    }

    inline u16string remoteSetSpeed(int64_t panel_index, double speed)
    {
        auto d = remoteU(panel_index);
        d += u"setSpeed:";
        d += convertToU16(to_string(speed));
        return d;
    }

    inline u16string remoteSetVerticalSpeedSlide(
        int64_t panel_index, double vertical_speed, double slide
    )
    {
        auto d = remoteU(panel_index);
        d += u"setVerticalSpeedSlide:";
        d += convertToU16(to_string(vertical_speed));
        (d += u"/") += convertToU16(to_string(slide));
        return d;
    }

    bool init(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto cockpit = static_cast<Remote_NS_Cockpit *>(p_object);
        SCI_StackGuard<1> guard(stack);

        auto &title = stack.topC().getScalar<u16string>();

        send(cockpit, remoteInit(0, title));

        return true;
    }

    bool setPosition(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto cockpit = static_cast<Remote_NS_Cockpit *>(p_object);
        SCI_StackGuard<3> guard(stack);

        auto x = stack.topC(2).getScalar<double>();
        auto height = stack.topC(1).getScalar<double>();
        auto z = stack.topC(0).getScalar<double>();

        send(cockpit, remoteSetPosition(0, {x, height, z}));

        return true;
    }

    bool setAngles(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto cockpit = static_cast<Remote_NS_Cockpit *>(p_object);
        SCI_StackGuard<3> guard(stack);

        auto pitch = stack.topC(2).getScalar<double>();
        auto course = stack.topC(1).getScalar<double>();
        auto roll = stack.topC(0).getScalar<double>();

        send(cockpit, remoteSetAngles(0, {pitch, course, roll}));

        return true;
    }

    bool setSpeed(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto cockpit = static_cast<Remote_NS_Cockpit *>(p_object);
        SCI_StackGuard<1> guard(stack);

        auto speed = stack.topC(0).getScalar<double>();

        send(cockpit, remoteSetSpeed(0, speed));

        return true;
    }

    bool setVerticalSpeedSlide(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto cockpit = static_cast<Remote_NS_Cockpit *>(p_object);
        SCI_StackGuard<2> guard(stack);

        auto vertical_speed = stack.topC(1).getScalar<double>();
        auto slide = stack.topC(0).getScalar<double>();

        send(cockpit, remoteSetVerticalSpeedSlide(0, vertical_speed, slide));

        return true;
    }

    bool panel(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto cockpit = static_cast<Remote_NS_Cockpit *>(p_object);
        SCI_StackGuard<2> guard(stack);

        auto panel_index = stack.topC(1).getScalar<int64_t>();
        auto &title = stack.topC(0).getScalar<u16string>();

        send(cockpit, remoteInit(panel_index, title));

        return true;
    }

    bool panelSetPosition(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto cockpit = static_cast<Remote_NS_Cockpit *>(p_object);
        SCI_StackGuard<4> guard(stack);

        auto panel_index = stack.topC(3).getScalar<int64_t>();
        auto x = stack.topC(2).getScalar<double>();
        auto height = stack.topC(1).getScalar<double>();
        auto z = stack.topC(0).getScalar<double>();

        send(cockpit, remoteSetPosition(panel_index, {x, height, z}));

        return true;
    }

    bool panelSetAngles(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto cockpit = static_cast<Remote_NS_Cockpit *>(p_object);
        SCI_StackGuard<4> guard(stack);

        auto panel_index = stack.topC(3).getScalar<int64_t>();
        auto pitch = stack.topC(2).getScalar<double>();
        auto course = stack.topC(1).getScalar<double>();
        auto roll = stack.topC(0).getScalar<double>();

        send(cockpit, remoteSetAngles(panel_index, {pitch, course, roll}));

        return true;
    }

    bool panelSetSpeed(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto cockpit = static_cast<Remote_NS_Cockpit *>(p_object);
        SCI_StackGuard<2> guard(stack);

        auto panel_index = stack.topC(1).getScalar<int64_t>();
        auto speed = stack.topC(0).getScalar<double>();

        send(cockpit, remoteSetSpeed(panel_index, speed));

        return true;
    }

    bool panelSetVerticalSpeedSlide(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        auto cockpit = static_cast<Remote_NS_Cockpit *>(p_object);
        SCI_StackGuard<3> guard(stack);

        auto panel_index = stack.topC(2).getScalar<int64_t>();
        auto vertical_speed = stack.topC(1).getScalar<double>();
        auto slide = stack.topC(0).getScalar<double>();

        send(cockpit, remoteSetVerticalSpeedSlide(panel_index, vertical_speed, slide));

        return true;
    }

    bool getKeyState(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;
        {
            SCI_StackGuard<1> guard(stack);
        }

        constexpr bool state = false;
        stack.push({
            u"",
            SemanticNameQualification::Scalar,
            SCI_Scalar {SemanticNameType::Bool, state}
        });

        return true;
    }
}

Remote_NS_Cockpit::Remote_NS_Cockpit(AReporter &listener)
    : _listener(listener)
{
}

Remote_NS_Cockpit::~Remote_NS_Cockpit()
{
}

SCI_Namespace_t Remote_NS_Cockpit::getNamespace()
{
    return {
        {u"init", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::init}}},
            {u"", SemanticNameQualification::None, {}},
            {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"setPosition", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setPosition}}},
            {u"", SemanticNameQualification::None, {}},
            {u"x", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"height", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"z", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"setAngles", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setAngles}}},
            {u"", SemanticNameQualification::None, {}},
            {u"pitch", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"course", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"roll", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"setSpeed", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setSpeed}}},
            {u"", SemanticNameQualification::None, {}},
            {u"speed", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"setVerticalSpeedAndSlide", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setVerticalSpeedSlide}}},
            {u"", SemanticNameQualification::None, {}},
            {u"vertical_speed", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"slide", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"panel", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"init", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panel}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            }},
            {u"setPosition", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panelSetPosition}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"x", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"height", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"z", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            }},
            {u"setAngles", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panelSetAngles}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"pitch", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"course", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"roll", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            }},
            {u"setSpeed", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panelSetSpeed}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"speed", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            }},
            {u"setVerticalSpeedAndSlide", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::panelSetVerticalSpeedSlide}}},
                {u"", SemanticNameQualification::None, {}},
                {u"panel_index", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"vertical_speed", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"slide", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            }},
        }},
        {u"getKeyState", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::getKeyState}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Bool, {}}},
            {u"key", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
    };
}


