/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include "simodo/dsl/ScriptC_NS_Scene.h"
#include "simodo/dsl/Exception.h"
#include "simodo/dsl/SCI_StackGuard.h"

#include "simodo/stage/NestedException.h"
#include "simodo/stage/ThreadLocalCopyFactory.h"
#include "simodo/stage/ThreadedActorGroup.h"
#include "simodo/stage/QueuedThreadPool.h"
#include "simodo/stage/TreeActorGroup.h"
#include "simodo/stage/Model.h"
#include "simodo/stage/ProcedureSolver.h"
#include "simodo/stage/OdeModelSolver.h"
#include "simodo/stage/RungeKutta4Solver.h"
#include "simodo/stage/RungeKuttaFehlbergSolver.h"

#include "simodo/convert.h"

#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <ratio>
#include <thread>
#include <iostream>
#include <set>

using namespace simodo::stage;
using namespace simodo::dsl;
using namespace simodo::tp;
using namespace std;

namespace
{
    static void executeCallback(
        const char *hint
        , ScriptC_Interpreter * interpreter
        , const AstNode * function_body
        , SCI_Name * parent = nullptr
        , bool is_construct = false
    );

    static Procedure createCallback(
        const char *hint
        , ScriptC_Interpreter * interpreter
        , const AstNode * function_body
        , SCI_Name * parent = nullptr
        , bool is_construct = false
    );

    static Procedure createLocalCallback(
        const char *hint
        , Factory<ScriptC_Interpreter> *interpreter_factory
        , const AstNode * function_body
        , SCI_Name * parent = nullptr
        , bool is_construct = false
    );

    struct ActorSettings
    {
    public:
        enum class Solver {
            Procedure
            , RungeKutta4
            , RungeKuttaFehlberg
        };

    public:
        set<Solver> solvers;
        struct {
            optional<double> epsilon;
            optional<int64_t> recalculate_limit;
        } rkf; 
        optional<variant<int64_t, double>> step;

    public:
        ActorSettings(const SCI_Name *obj = nullptr);
    };

    static typename ScriptC_NS_Scene::Stage::ActorReference
        createActor(
            Factory<ScriptC_Interpreter> *interpreter
            , SCI_Name *obj
            , const ActorSettings &settings = {}
        );

    bool setT(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<1> guard(sci_stack);

        scene->stage.setTime(sci_stack.backC().getScalar<double>());

        return true;
    }

    bool t(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);

        sci_stack.push({
            u""
            , SemanticNameQualification::Scalar
            , SCI_Scalar {SemanticNameType::Float, scene->stage.getTime()}
        });

        return true;
    }

    bool it(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);

        sci_stack.push({
            u""
            , SemanticNameQualification::Scalar
            , SCI_Scalar {SemanticNameType::Int, int64_t(scene->stage.getIteration())}
        });

        return true;
    }

    bool setTk(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<1> guard(sci_stack);

        scene->stage.setEndTime(sci_stack.backC().getScalar<double>());

        return true;
    }

    bool tk(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);

        sci_stack.push({
            u""
            , SemanticNameQualification::Scalar
            , SCI_Scalar {SemanticNameType::Float, scene->stage.getEndTime()}
        });

        return true;
    }

    bool setDt(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<1> guard(sci_stack);

        scene->stage.setDeltaTime(sci_stack.backC().getScalar<double>());

        return true;
    }

    bool dt(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);

        sci_stack.push({
            u""
            , SemanticNameQualification::Scalar
            , SCI_Scalar {SemanticNameType::Float, scene->stage.getDeltaTime()}
        });

        return true;
    }

    bool setIterationCallback(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<2> guard(sci_stack);

        auto period = sci_stack.backC().getScalar<int64_t>();

        if (period < 0)
        {
            throw Exception(
                "setIterationCallback"
                , "Отрицательный период: " + to_string(period) + "'"
            );
        }

        auto &function_tuple = sci_stack.topC(1).get<SCI_Tuple>();
        assert(!function_tuple.empty());
        auto function_body = function_tuple.front().getScalar<const AstNode *>();

        scene->stage.setCallback(
            createCallback("scene::callback", scene->p_interpreter, function_body)
            , {
                ScriptC_NS_Scene::Stage::CallbackPeriodMode::IterationCount
                , DiscreteTime<double>() + period
            }
        );

        return true;
    }

    bool setPeriodicCallback(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<2> guard(sci_stack);

        auto period = sci_stack.backC().getScalar<double>();

        if (period < 0)
        {
            throw Exception(
                "setPeriodicCallback"
                , "Отрицательный период: " + to_string(period) + "'"
            );
        }

        auto &function_tuple = sci_stack.topC(1).get<SCI_Tuple>();
        assert(!function_tuple.empty());
        auto function_body = function_tuple.front().getScalar<const AstNode *>();

        scene->stage.setCallback(
            createCallback("scene::callback", scene->p_interpreter, function_body)
            , {
                ScriptC_NS_Scene::Stage::CallbackPeriodMode::TimeRange
                , DiscreteTime<double>(0, period)
            }
        );

        return true;
    }

    bool setEachIterationCallback(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<1> guard(sci_stack);

        auto &function_tuple = sci_stack.backC().get<SCI_Tuple>();
        assert(!function_tuple.empty());
        auto function_body = function_tuple.front().getScalar<const AstNode *>();

        scene->stage.setEachIterationCallback(
            createCallback(
                "scene::each_iteration_callback", scene->p_interpreter, function_body
            )
        );

        return true;
    }

    bool setRealtimeModeEnabled(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<1> guard(sci_stack);

        scene->stage.setMode(
            sci_stack.backC().getScalar<bool>()
            ? ScriptC_NS_Scene::Stage::Mode::RealTime
            : ScriptC_NS_Scene::Stage::Mode::AsFastAsCan
        );

        return true;
    }

    bool realtimeModeEnabled(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);

        sci_stack.push({
            u""
            , SemanticNameQualification::Scalar
            , SCI_Scalar {
                SemanticNameType::Bool
                , scene->stage.getMode() == ScriptC_NS_Scene::Stage::Mode::RealTime
            }
        });

        return true;
    }

    bool add(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<1> guard(sci_stack);

        auto obj = &sci_stack.top().deref();

        const bool ok = scene->stage.addActor(
            static_cast<const void *>(obj), createActor(&scene->interpreter_factory, obj)
        );
        if (!ok)
        {
            throw Exception(
                "addObject"
                , "Повторное добавление объекта '" + simodo::convertToU8(obj->name) + "'"
            );
        }

        return true;
    }

    bool contains(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);

        constexpr auto isContains = [](
            ScriptC_NS_Scene *scene, SCI_Stack &sci_stack
        ) -> bool
        {
            SCI_StackGuard<1> guard(sci_stack);

            auto obj = &sci_stack.topC().deref();

            return scene->stage.containsActor(static_cast<const void *>(obj));
        };

        sci_stack.push({
            u""
            , SemanticNameQualification::Scalar
            , SCI_Scalar {SemanticNameType::Bool, isContains(scene, sci_stack)}
        });

        return true;
    }

    bool addActor(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<2> guard(sci_stack);

        auto obj = &sci_stack.top(1).deref();
        auto solvers = &sci_stack.topC(0).deref();

        const bool ok = scene->stage.addActor(
            static_cast<const void *>(obj)
            , createActor(&scene->interpreter_factory, obj, solvers)
        );
        if (!ok)
        {
            throw Exception(
                "addActor"
                , "Повторное добавление объекта '" + simodo::convertToU8(obj->name) + "'"
            );
        }

        return true;
    }

    bool addActorTo(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<3> guard(sci_stack);

        auto obj = &sci_stack.top(2).deref();
        auto solvers = &sci_stack.topC(1).deref();
        auto parent = &sci_stack.topC(0).deref();

        const bool ok = scene->stage.addActorTo(
            static_cast<const void *>(obj)
            , createActor(&scene->interpreter_factory, obj, solvers)
            , static_cast<const void *>(parent)
        );
        if (!ok)
        {
            throw Exception(
                "addActorTo"
                , (
                    "Повторное добавление объекта '" + simodo::convertToU8(obj->name) + "'"
                    + " или не найден родитель '" + simodo::convertToU8(parent->name) + "'"
                )
            );
        }

        return true;
    }

    bool removeObject(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<1> guard(sci_stack);

        auto obj = &sci_stack.topC().deref();

        scene->stage.removeActor(static_cast<const void *>(obj));

        return true;
    }

    bool setActorStep(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<2> guard(sci_stack);

        auto obj = &sci_stack.top(1).deref();
        auto step = sci_stack.backC().getScalar<double>();

        if (!scene->stage.setActorStep(static_cast<const void *>(obj), step))
        {
            throw Exception(
                "setActorStep"
                , "Установка шага не добавленного на сцену актора '" + simodo::convertToU8(obj->name) + "'"
            );
        }

        return true;
    }

    bool setActorStepIt(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<2> guard(sci_stack);

        auto obj = &sci_stack.top(1).deref();
        auto step = sci_stack.backC().getScalar<int64_t>();

        if (!scene->stage.setActorStepIt(static_cast<const void *>(obj), step))
        {
            throw Exception(
                "setActorStepIt"
                , "Установка шага не добавленного на сцену актора '" + simodo::convertToU8(obj->name) + "'"
            );
        }

        return true;
    }

    bool resetActorStep(IScriptC_Namespace *p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);
        SCI_StackGuard<1> guard(sci_stack);

        auto obj = &sci_stack.top().deref();

        if (!scene->stage.resetActorStep(static_cast<const void *>(obj)))
        {
            throw Exception(
                "setActorStepIt"
                , "Установка шага не добавленного на сцену актора '" + simodo::convertToU8(obj->name) + "'"
            );
        }

        return true;
    }

    bool start(IScriptC_Namespace *p_object, SCI_Stack &)
    {
        if (p_object == nullptr) return false;
        auto scene = static_cast<ScriptC_NS_Scene *>(p_object);

        if (scene->p_interpreter == nullptr) return true;
        scene->stage.start();

        return true;
    }
}

ScriptC_NS_Scene::ScriptC_NS_Scene(
    Interpreter *p_interpreter
    , bool time_output_enabled
)
    : p_interpreter(p_interpreter)
    , interpreter_factory(*p_interpreter)
    , stage(
        make_shared<ThreadedActorGroup<const void *, int64_t, double>>(
            make_shared<QueuedThreadPool<Procedure>>(
                QueuedThreadPool<Procedure>::Settings {
                    .n_threads = 4
                    , .is_hint = true
                }
                , [this] {
                    interpreter_factory.update();
                }
            )
        )
        , [p_interpreter]() -> bool
        {
            return !p_interpreter->stop_signal();
        }
    )
    , time_output_enabled(time_output_enabled)
{}

SCI_Namespace_t ScriptC_NS_Scene::getNamespace()
{
    return {
        {u"setT", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setT}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_t_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}}
        }},
        {u"t", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::t}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}}
        }},
        {u"it", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::it}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}}
        }},
        {u"setTk", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setTk}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_t_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}}
        }},
        {u"tk", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::tk}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}}
        }},
        {u"setDt", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setDt}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_dt_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}}
        }},
        {u"dt", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::dt}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}}
        }},
        {u"setIterationCallback", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setIterationCallback}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_function_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::IntFunction, {}}},
            {u"_period_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"setPeriodicCallback", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setPeriodicCallback}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_function_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::IntFunction, {}}},
            {u"_period_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"setEachIterationCallback", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setEachIterationCallback}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_function_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::IntFunction, {}}},
        }},
        {u"setRealtimeModeEnabled", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setRealtimeModeEnabled}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_enabled_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Bool, {}}}
        }},
        {u"realtimeModeEnabled", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::realtimeModeEnabled}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Bool, {}}}
        }},
        {u"add", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::add}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_object_", SemanticNameQualification::Tuple, SCI_Tuple {}},
        }},
        {u"contains", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::contains}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Bool, {}}},
            {u"_object_", SemanticNameQualification::Tuple, SCI_Tuple {}},
        }},
        {u"addActor", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addActor}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_object_", SemanticNameQualification::Tuple, SCI_Tuple {}},
            {u"_solvers_", SemanticNameQualification::Tuple, SCI_Tuple {}},
        }},
        {u"addActorTo", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addActorTo}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_object_", SemanticNameQualification::Tuple, SCI_Tuple {}},
            {u"_solvers_", SemanticNameQualification::Tuple, SCI_Tuple {}},
            {u"_parent_", SemanticNameQualification::Tuple, SCI_Tuple {}},
        }},
        {u"remove", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::removeObject}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_object_", SemanticNameQualification::Tuple, SCI_Tuple {}},
        }},
        {u"setActorStep", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setActorStep}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_object_", SemanticNameQualification::Tuple, SCI_Tuple {}},
            {u"_step_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}}
        }},
        {u"setActorStepIt", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setActorStepIt}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_object_", SemanticNameQualification::Tuple, SCI_Tuple {}},
            {u"_step_it_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}}
        }},
        {u"resetActorStep", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::resetActorStep}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_object_", SemanticNameQualification::Tuple, SCI_Tuple {}},
        }},
        {u"start", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::start}}},
            {u"", SemanticNameQualification::None, {}},
        }},
    };
}

namespace {
    static void executeCallback(
        const char *hint
        , ScriptC_Interpreter *interpreter
        , const AstNode *function_body
        , SCI_Name *parent
        , bool is_construct
    )
    {
        try
        {
            auto result = interpreter->callInnerFunction(
                *function_body, interpreter->stack_size(), parent, is_construct
            );

            if (
                result == SCI_RunnerCondition::Regular || result == SCI_RunnerCondition::Return
            ) return;
        }
        catch (exception &e)
        {
            throw NestedException(
                string("Вызов функции ") + hint + " завершился ошибкой", e.what()
            );
        }

        throw Exception(hint, string("Вызов функции ") + hint + " завершился ошибкой");
    }

    static Procedure createCallback(
        const char *hint
        , ScriptC_Interpreter *interpreter
        , const AstNode *function_body
        , SCI_Name *parent
        , bool is_construct
    )
    {
        if (interpreter == nullptr) return []{};

        return [=] {
            executeCallback(hint, interpreter, function_body, parent, is_construct);
        };
    }

    static Procedure createLocalCallback(
        const char *hint
        , Factory<ScriptC_Interpreter> *interpreter_factory
        , const AstNode * function_body
        , SCI_Name * parent
        , bool is_construct
    )
    {
        if (interpreter_factory == nullptr) return []{};

        return [=] {
            executeCallback(
                hint
                , interpreter_factory->produce().get()
                , function_body
                , parent
                , is_construct
            );
        };
    }

    static typename ScriptC_NS_Scene::Stage::ActorReference
        createActor(
            Factory<ScriptC_Interpreter> *interpreter_factory
            , SCI_Name * object
            , const ActorSettings &settings
        )
    {
        if (interpreter_factory == nullptr) return {};

        vector<SCI_Name *> p_dxdt;
        vector<SCI_Name *> p_x;
        const AstNode *    p_equation_function_body = nullptr;
        const AstNode *    p_diff_function_body     = nullptr;
        SCI_Tuple &        tuple                    = object->get<SCI_Tuple>();

        for(auto &ndxdt : tuple)
        {
            if (ndxdt.name.substr(0,5) == u"__dt_")
            {
                u16string   x_name = ndxdt.name.substr(5);
                auto &tuple  = object->get<SCI_Tuple>();
                size_t      i      = 0;

                for(; i < tuple.size(); ++i)
                {
                    if (tuple[i].name == x_name)
                        break;
                }

                if (i == tuple.size())
                    throw Exception("createActor", "Некорректная струтура объекта, переменная '" + simodo::convertToU8(x_name) + "'");

                SCI_Name & nx = tuple[i];

    //            const SCI_Scalar & sc_x = get<SCI_Scalar>(nx.bulk);
    //            double d = get<double>(sc_x.variant);

                p_dxdt.push_back(&ndxdt);
                p_x.push_back(&nx);

                continue;
            }

            if (ndxdt.name == u"__E")
            {
                const auto &function_parameters = ndxdt.get<SCI_Tuple>();
                assert(!function_parameters.empty());

                p_equation_function_body
                    = function_parameters.front().getScalar<const AstNode *>();
            }

            if (ndxdt.name == u"__ODE")
            {
                const auto &function_parameters = ndxdt.get<SCI_Tuple>();
                assert(!function_parameters.empty());

                p_diff_function_body
                    = function_parameters.front().getScalar<const AstNode *>();
            }
        }

        // if (p_dxdt.empty() || p_diff_function_body == nullptr)
        //     throw Exception("addObject", "Неполная струтура объекта");
 
        vector<shared_ptr<Solver<double>>> actor_solvers;

        if (
            settings.solvers.find(ActorSettings::Solver::Procedure) != settings.solvers.end()
            && p_equation_function_body == nullptr
        )
        {
            throw Exception(
                "createActor"
                , "В настройках указан процедурный солвер, но у объекта нет процедуры `__E`."
            );
        }

        if (p_equation_function_body != nullptr)
        {
            actor_solvers.push_back(
                make_shared<ProcedureSolver<double>>(
                    createLocalCallback(
                        "scene::actor::equ"
                        , interpreter_factory
                        , p_equation_function_body
                        , object
                    )
                )
            );
        }

        if (
            (
                settings.solvers.find(ActorSettings::Solver::RungeKutta4)
                    != settings.solvers.end()
                || settings.solvers.find(ActorSettings::Solver::RungeKuttaFehlberg)
                    != settings.solvers.end()
            )
            && p_diff_function_body == nullptr
        )
        {
            throw Exception(
                "createActor"
                , "В настройках указан солвер ОДУ, но у объекта нет процедуры `__ODE`."
            );
        }

        if (p_diff_function_body != nullptr)
        {
            auto get_state = [p_x]() -> vector<double>
            {
                vector<double> y;
                y.reserve(p_x.size());
                std::transform(
                    p_x.cbegin()
                    , p_x.cend()
                    , std::back_inserter(y)
                    , [](const SCI_Name *x) -> double
                    {
                        return  get<double>(get<SCI_Scalar>(x->bulk).variant);
                    }
                );
                return y;
            };
            auto set_state = [p_x](const vector<double> &y) -> void
            {
                for(size_t i = 0; i < p_x.size(); ++i)
                {
                    get<SCI_Scalar>(p_x[i]->bulk).variant = y[i];
                }
            };
            auto ode = createLocalCallback(
                "scene::actor::ode"
                , interpreter_factory
                , p_diff_function_body
                , object
            );
            shared_ptr<OdeSolver<double>> ode_solver;
            if (
                settings.solvers.find(ActorSettings::Solver::RungeKuttaFehlberg)
                    != settings.solvers.end()
            )
            {
                ode_solver = make_shared<RungeKuttaFehlbergSolver<double>>(
                    (
                        settings.rkf.epsilon
                        ? *settings.rkf.epsilon
                        : RungeKuttaFehlbergSolver<double>::DEFAULT_EPSILON
                    )
                    , (
                        settings.rkf.recalculate_limit
                        ? *settings.rkf.recalculate_limit
                        : RungeKuttaFehlbergSolver<double>::DEFAULT_RECALCULATE_LIMIT
                    )
                );
            }
            else
            {
                ode_solver = make_shared<RungeKutta4Solver<double>>();
            }

            actor_solvers.push_back(make_shared<OdeModelSolver<double>>(
                get_state
                , set_state
                , [set_state, ode, p_dxdt]
                (double /*t*/, const vector<double> &y) -> vector<double>
                {
                    set_state(y);

                    ode();

                    vector<double> dy;
                    dy.reserve(p_dxdt.size());
                    std::transform(
                        p_dxdt.cbegin()
                        , p_dxdt.cend()
                        , std::back_inserter(dy)
                        , [](SCI_Name *dxdt) -> double
                        {
                            return get<double>(get<SCI_Scalar>(dxdt->bulk).variant);
                        }
                    );
                    return dy;
                }
                , ode_solver
            ));
        }

        auto actor = make_shared<Model<int64_t, double>>(actor_solvers);
        actor->setStep(settings.step);
        return actor;
    }

    ActorSettings::ActorSettings(const SCI_Name *obj)
    {
        if (obj == nullptr) return;

        auto &tuple = obj->get<SCI_Tuple>();
        for(auto &field : tuple)
        {
            if (field.name == u"proc")
            {
                solvers.insert(Solver::Procedure);
            }
            else if (field.name == u"rk4")
            {
                solvers.insert(Solver::RungeKutta4);
            }
            else if (field.name == u"rkf")
            {
                solvers.insert(Solver::RungeKuttaFehlberg);
            }
            else if (field.name.substr(0, 11) == u"rkf_epsilon")
            {
                solvers.insert(Solver::RungeKuttaFehlberg);
                rkf.epsilon = get<double>(get<SCI_Scalar>(field.bulk).variant);
            }
            else if (field.name.substr(0, 14) == u"rkf_recalc_lim")
            {
                solvers.insert(Solver::RungeKuttaFehlberg);
                rkf.recalculate_limit = get<int64_t>(get<SCI_Scalar>(field.bulk).variant);
            }
            else if (field.name == u"step" && (!step || !holds_alternative<int64_t>(*step)))
            {
                step = get<double>(get<SCI_Scalar>(field.bulk).variant);
            }
            else if (field.name == u"step_it")
            {
                step = static_cast<int>(get<int64_t>(get<SCI_Scalar>(field.bulk).variant));
            }
        }
    }
}
