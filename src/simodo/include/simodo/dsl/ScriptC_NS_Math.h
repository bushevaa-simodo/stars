/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_ScriptC_NS_math
#define SIMODO_DSL_ScriptC_NS_math

/*! \file ScriptC_NS_math.h
    \brief Пространство имён 'math' для интерпретатора
*/

#include "simodo/dsl/ScriptC_Interpreter.h"

namespace simodo::dsl
{
    /*!
     * \brief Получение ссылки на глобальное пространство имён математических операций и констант
     */
    class ScriptC_NS_Math : public IScriptC_Namespace
    {
    public:
        virtual SCI_Namespace_t getNamespace() override;
    };

}

#endif // SIMODO_DSL_ScriptC_NS_math
