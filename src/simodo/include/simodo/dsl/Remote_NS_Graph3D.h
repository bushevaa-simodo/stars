/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_Remote_NS_Graph3D
#define SIMODO_DSL_Remote_NS_Graph3D

/*! \file Remote_NS_Graph3D.h
    \brief Версия трёхмерных графиков для работы на сервере и передачи информации на клиента
*/

#include "simodo/dsl/ScriptC_Interpreter.h"

namespace simodo::dsl
{
    class Remote_NS_Graph3D : public IScriptC_Namespace
    {
        AReporter & _listener;

    public:
        Remote_NS_Graph3D() = delete;
        Remote_NS_Graph3D(AReporter & listener);
        virtual ~Remote_NS_Graph3D();

        virtual SCI_Namespace_t getNamespace() override;

        AReporter & listener() const { return _listener; }
    };

}

#endif // SIMODO_DSL_Remote_NS_Graph3D
