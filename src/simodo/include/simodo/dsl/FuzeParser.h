/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_FuzeParser_h
#define SIMODO_DSL_FuzeParser_h

/*! \file FuzeParser.h
    \brief Начальный разбор грамматики языка

    Заголовочный файл, описывающий класс начального разбора синтаксических правил языка.
*/

#include <string>
#include <set>
#include <map>

#include "simodo/dsl/FuzeParserBase.h"
#include "simodo/dsl/Grammar.h"
#include "simodo/dsl/Tokenizer.h"
#include "simodo/dsl/FuzeParser_ScriptC.h"
#include "simodo/dsl/AstNode.h"


namespace simodo::dsl
{
    /*!
     * \brief Класс начального разбора синтаксических правил языка
     *
     * Класс выполняет разбор текста с упрощённым описанием грамматики и формирует набор правил грамматики для
     * последующего построения таблицы разбора.
     *
     * Порядок работы с данным классом следующий. В конструкторе класса задаётся путь к файлам грамматики
     * и наименование грамматики (которое должно совпадать с наименованием файла). Затем нужно вызвать один
     * из методов `parse`, которые производят разбор заданной грамматики и наполняют перечень правил.
     *
     * Синтаксисическая диаграмма представлена ниже.
     *
     * \dot
     * digraph sql {
     *  rankdir = LR;
     *  ranksep=.75; size = "9,9";
     *  node       [shape=Mrecord fontsize=18];
     *  start      [label="start"       style=filled fillcolor=black shape=point];
     *  import     [label="'import'"    style=filled fillcolor=yellow];
     *  imp_ia     [label="ID \| ANNOTATION" style=filled fillcolor=yellow];
     *  main       [label="'main'"      style=filled fillcolor=yellow];
     *  m_id       [label="ID"          style=filled fillcolor=yellow];
     *  r_prod     [label="ID"          style=filled fillcolor=cyan];
     *  r_assign   [label="'='"         style=filled fillcolor=cyan];
     *  r_patt_ia  [label="ID \| ANNOTATION" style=filled fillcolor=cyan];
     *  r_next     [label="'=' \| '\|'" style=filled fillcolor=cyan];
     *  r_patt_dir [label="'\<' \| '\>'" style=filled fillcolor=cyan];
     *  sc         [label="'\;'"        style=filled fillcolor=white];
     *  block_start [label="'\{'"       style=filled fillcolor=green];
     *  block      [label="ScriptC\*"   style=filled fillcolor=green];
     *  block_end  [label="'\}'"        style=filled fillcolor=green];
     *  block_start2 [label="'\{'"       style=filled fillcolor=green];
     *  block2      [label="ScriptC\*"   style=filled fillcolor=green];
     *  block_end2  [label="'\}'"        style=filled fillcolor=green];
     *  eof        [label="EOF"         style=filled fillcolor=white];
     *  end        [label="finish"      style=filled fillcolor=black shape=point];
     *  start   -> {block_start, import, main, r_prod};
     *  block_start -> {block};
     *  block   -> {block_end};
     *  block_end -> {sc};
     *  import  -> imp_ia -> sc;
     *  main    -> m_id -> sc;
     *  r_prod  -> r_assign -> r_patt_ia -> {block_start2, r_patt_ia, r_next, r_patt_dir, sc};
     *  r_patt_dir -> {block_start2, r_next, sc};
     *  r_next  -> r_patt_ia;
     *  block_start2 -> {block2};
     *  block2   -> {block_end2};
     *  block_end2 -> {r_next, sc};
     *  sc      -> {start, eof};
     *  eof     -> end;
     * }
     * \enddot
     *
     * В таблице даётся краткое описание элементов диаграммы:
     *
     * | Обозначение        | Описание
     * | :----------------- | :--------------------------
     * | ID                 | Лексема входного потока типа LexemeType::Id, обозначающая идекнтификатор
     * | ANNOTATION         | Лексема входного потока типа LexemeType::Annotation, обозначающая строковую константу
     * | EOF                | Лексема входного потока типа LexemeType::Empty, символизирующая конец файла
     * | ScriptC*           | Блок кода на ScriptC с упрощённым синтаксисом (вызов процедуры), который разбирается в отдельном классе FuzeParser_ScriptC
     * | Остальные элементы | Элементы в одинарных кавычках являются разделителями, которые могут комбинироваться со знаком \|, означающим, что должен быть записан один из элементов
     *
     * Разбор заданной грамматики выполняется методом рекурсивного спуска и разделён на фрагменты, которые реализуют
     * различные методы.
     *
     * Файлы, которые обрабатываются данным классом имеют расширение `.fuze`. Они находятся в каталоге
     * `data/grammar` проекта.
     */
    class FuzeParser : public FuzeParserBase
    {
        std::string             _path;          ///< Путь к файлам грамматики
        std::string             _grammar_name;  ///< Наименование грамматики
        std::set<std::string> & _files;         ///< Множество загруженных файлов (для исключения рекурсивного вложения)
        uint8_t                 _tab_size;      ///< Размер табуляции

    public:
        FuzeParser() = delete; ///< Пустой конструктор не поддерживается!

        /*!
         * \brief Конструктор класса начального разбора синтаксических правил языка
         * \param m             Интерфейсная ссылка на объект обеспечения вывода информации в вызываемую программу
         * \param path          Путь к файлам грамматики
         * \param grammar_name  Наименование грамматики, которую нужно загрузить
         * \param files         Перечень грамматик, которые уже загружены (предотвращает зацикливание
         *                      при использовании оператора `include` и `import`)
         * \param tab_size      Размер табуляции, который будет использоваться при выводе колонки ошибочного токена
         */
        FuzeParser(AReporter & m, const std::string & path, const std::string & grammar_name, std::set<std::string> & files, uint8_t tab_size=4)
            : FuzeParserBase(m), _path(path), _grammar_name(grammar_name), _files(files), _tab_size(tab_size)
        {}

        /*!
         * \brief Метод разбора и наполнения параметров `rules` и `main_rule` из файла описания грамматики
         *
         * Метод выполняет разбор и наполнение параметров `rules` и `main_rule` из файла описания грамматики,
         * который конструируется по заданным в конструкторе параметрам. В качестве расширения файла используется
         * `.fuze`.
         *
         * Метод выполняет создание токенайзера на описанном выше файле и вызывает одноимённый метод `parse` с
         * созданным токенайзером.
         *
         * \param rules         Контейнер с набором правил грамматики, который нужно заполнить
         * \param main_rule     Наименование стартового правила, который определяется в операторе `main`
         * \param declaration   Ссылка на АСД для заполнения декларативной части действия при формировании грамматики
         * \param lex           Ссылка на  параметры лексики
         * \return true, если разбор завершился без ошибок, иначе - false
         */
        bool    parse(std::vector<GrammarRuleTokens> & rules, Token & main_rule, std::map<std::u16string,AstNode> & handlers, LexicalParameters & lex);

        /*!
         * \brief Метод разбора и наполнения параметров `rules` и `main_rule` из заданного токенайзера
         * \param stream        Входной поток
         * \param rules         Контейнер с набором правил грамматики, который нужно заполнить
         * \param main_rule     Наименование стартового правила, который определяется в операторе `main`
         * \param declaration   Ссылка на АСД для заполнения декларативной части действия при формировании грамматики
         * \param lex           Ссылка на  параметры лексики
         * \return true, если разбор завершился без ошибок, иначе - false
         */
        bool    parse(IStream & stream, std::vector<GrammarRuleTokens> & rules, Token & main_rule, std::map<std::u16string,AstNode> & handlers, LexicalParameters & lex);

    protected:
        /*!
         * \brief Метод анализа операторов `import` и `main`
         * \param rules         Контейнер с набором правил грамматики, который нужно заполнить
         * \param main_rule     Наименование стартового правила, который определяется в операторе `main`
         * \param declaration   Ссылка на АСД для заполнения декларативной части действия при формировании грамматики
         * \param tzer          Токенайзер, предоставляющий входной поток токенов языка (лексический анализатор)
         * \param t             Текущий токен
         * \return true, если разбор завершился без ошибок, иначе - false
         */
        bool    parseKeyword(std::vector<GrammarRuleTokens> & rules, Token & main_rule, std::map<std::u16string,AstNode> & handlers, Tokenizer & tzer, Token & t) const;

        /*!
         * \brief Метод анализа и формирования продукций языка по описанию его грамматики
         * \param rules     Контейнер с набором правил грамматики, который нужно заполнить
         * \param tzer      Токенайзер, предоставляющий входной поток токенов языка (лексический анализатор)
         * \param t         Текущий токен
         * \return true, если разбор завершился без ошибок, иначе - false
         */
        bool    parseProduction(const Token & production, std::vector<GrammarRuleTokens> & rules, Tokenizer & tzer, Token & t) const;

        /*!
         * \brief Метод анализа и формирования шаблона, обозначения действия и направления ассоциативности для конкретного правила грамматики
         * \param tzer      Токенайзер, предоставляющий входной поток токенов языка (лексический анализатор)
         * \param t         Текущий токен
         * \param pattern   Контейнер для формирования последовательности символов шаблона
         * \param ast       Ссылка для заполнения АСД кода, формирующего фрагмент АСД по разобранному паттерну
         * \param direction Ссылка для заполнения ассоциативности данного правила
         * \return true, если разбор завершился без ошибок, иначе - false
         */
        bool    parsePattern(Tokenizer & tzer,
                             Token & t,
                             std::vector<Token> & pattern,
                             AstNode &ast,
                             RuleReduceDirection & direction) const;

    };
}

#endif // SIMODO_DSL_FuzeParser_h
