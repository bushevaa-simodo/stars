#ifndef SIMODO_DSL_SCI_STACK_H
#define SIMODO_DSL_SCI_STACK_H

#include <vector>

#include "simodo/dsl/SemanticBase.h"

namespace simodo::dsl
{
    class SCI_Stack
    {
    public:
        virtual ~SCI_Stack() = default;

        virtual void pop(int count = 1) = 0;

        virtual const SCI_Name & push(const SCI_Scalar & value, std::u16string name=u"", SemanticNameAccess access = SemanticNameAccess::FullAccess) = 0;
        virtual const SCI_Name & push(const SCI_Tuple & value, std::u16string name=u"", SemanticNameAccess access = SemanticNameAccess::FullAccess) = 0;
        virtual const SCI_Name & push(const SCI_Array & value, std::u16string name=u"", SemanticNameAccess access = SemanticNameAccess::FullAccess) = 0;
        virtual const SCI_Name & push(const SCI_Name & value) = 0;

        virtual size_t find(std::u16string name) const = 0;
        virtual size_t findLocal(std::u16string name, size_t local_boundary) const = 0;
        virtual size_t findGlobal(std::u16string name, size_t global_boundary) const = 0;

        virtual SCI_Name & at(size_t index) = 0;
        virtual SCI_Name & top(size_t index=0) = 0;
        virtual SCI_Name & back() = 0;

        virtual const SCI_Name & atC(size_t index) const = 0;
        virtual const SCI_Name & topC(size_t index=0) const = 0;
        virtual const SCI_Name & backC() const = 0;

        virtual void moveTo(size_t from, size_t to) = 0;

        virtual size_t size() const = 0;
        virtual bool empty() const = 0;
        virtual size_t stack_max_size() const = 0;
    };

    class SCI_Stack_Impl : public SCI_Stack
    {
        std::vector<SCI_Name>   _stack;
        size_t                  _stack_max_size;

    public:
        SCI_Stack_Impl() = delete;

        SCI_Stack_Impl(size_t stack_max_size);

        void pop(int count = 1) override;

        const SCI_Name & push(const SCI_Scalar & value, std::u16string name=u"", SemanticNameAccess access = SemanticNameAccess::FullAccess) override;
        const SCI_Name & push(const SCI_Tuple & value, std::u16string name=u"", SemanticNameAccess access = SemanticNameAccess::FullAccess) override;
        const SCI_Name & push(const SCI_Array & value, std::u16string name=u"", SemanticNameAccess access = SemanticNameAccess::FullAccess) override;
        const SCI_Name & push(const SCI_Name & value) override;

        size_t find(std::u16string name) const override;
        size_t findLocal(std::u16string name, size_t local_boundary) const override;
        size_t findGlobal(std::u16string name, size_t global_boundary) const override;

        SCI_Name & at(size_t index) override;
        SCI_Name & top(size_t index=0) override;
        SCI_Name & back() override;

        const SCI_Name & atC(size_t index) const override;
        const SCI_Name & topC(size_t index=0) const override;
        const SCI_Name & backC() const override;

        void moveTo(size_t from, size_t to) override;

        size_t size() const override { return _stack.size(); }
        bool empty() const override { return _stack.empty(); }
        size_t stack_max_size() const override { return _stack_max_size; }
    };

    class SCI_Stack_Wrap : public SCI_Stack
    {
        SCI_Stack &_base_stack;
        SCI_Stack_Impl _custom_stack;

    public:
        SCI_Stack_Wrap() = delete;

        SCI_Stack_Wrap(SCI_Stack &other);

        void pop(int count = 1) override;

        const SCI_Name & push(const SCI_Scalar & value, std::u16string name=u"", SemanticNameAccess access = SemanticNameAccess::FullAccess) override;
        const SCI_Name & push(const SCI_Tuple & value, std::u16string name=u"", SemanticNameAccess access = SemanticNameAccess::FullAccess) override;
        const SCI_Name & push(const SCI_Array & value, std::u16string name=u"", SemanticNameAccess access = SemanticNameAccess::FullAccess) override;
        const SCI_Name & push(const SCI_Name & value) override;

        size_t find(std::u16string name) const override;
        size_t findLocal(std::u16string name, size_t local_boundary) const override;
        size_t findGlobal(std::u16string name, size_t global_boundary) const override;

        SCI_Name & at(size_t index) override;
        SCI_Name & top(size_t index=0) override;
        SCI_Name & back() override;

        const SCI_Name & atC(size_t index) const override;
        const SCI_Name & topC(size_t index=0) const override;
        const SCI_Name & backC() const override;

        void moveTo(size_t from, size_t to) override;

        size_t size() const override { return _base_stack.size() + _custom_stack.size(); }
        bool empty() const override { return _base_stack.empty() && _custom_stack.empty(); }
        size_t stack_max_size() const override { return _base_stack.stack_max_size(); }
    };
}
#endif // SIMODO_DSL_SCI_STACK_H
