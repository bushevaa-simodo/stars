/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_SemanticNameTable
#define SIMODO_DSL_SemanticNameTable

/*! \file SemanticNameTable.h
    \brief Таблица имён, используемая для семантического анализа
*/

#include <vector>
#include <map>
#include <string>
#include <limits>

#include "simodo/dsl/AReporter.h"
#include "simodo/dsl/SemanticBase.h"

namespace simodo::dsl
{
    typedef std::pair<TokenLocation,TokenLocation>  SemanticScope;

    /*!
     * \brief The SemanticNameTable class
     *
     * Реализует шаблон проектирования Immutable Interface по отношению к классу семантического анализа.
     */
    class SemanticNameTable
    {
        AReporter &                         _m;                 ///< Обработчик сообщений
        std::vector<SemanticName> &         _name_set;          ///< Ссылка на имена, найденные при анализе
        std::vector<SemanticScope> &        _scope_set;         ///< Ссылка на блоки кода, определяющие границы видимости
        std::vector<std::vector<size_t>>    _work_name_stack;   ///< Стек локальных пространств имён
                                                                ///< (ссылки на _name_set)
        std::vector<TokenLocation>          _work_scope_stack;  ///< Рабочий стек начальных зон видимости переменных
        bool                                _is_module_loading; ///< Признак ненужности проверки на неиспользуемость имён верхнего уровня
                                                                /// (при анализе модулей как классов такая проверка не нужна)

    public:
        SemanticNameTable() = delete;
        SemanticNameTable(AReporter & m,
                          std::vector<SemanticName> & name_set,
                          std::vector<SemanticScope> & scope_set,
                          bool is_module_loading=false);

    public:
        std::vector<size_t> buildElementSet(size_t owner) const;
        const std::vector<SemanticName> & name_set() const { return _name_set; }

    public:
        void    importNamespace(const std::u16string &name, const SCI_Namespace_t & ns);

        size_t  addNameDeclaration(const SemanticName & name);
        void    addNameDefinition(size_t index, const TokenLocation & location);
        void    addNameReference(size_t index, const TokenLocation & location);
        void    addNameType(size_t index, SemanticNameType type);
        void    assignChecked(size_t index);
        void    forceInputFlag(size_t index);

        void    openScope(const TokenLocation & location);
        void    closeScope(const AstNode & node);
        void    closeScope(const TokenLocation & location);

        size_t  findName(const std::u16string & name, size_t owner=UNDEFINED_INDEX) const;
        size_t  findNameOnTop(const std::u16string & name) const;

        uint16_t getDepth() const;

    private:
        void    addNameFromNamespace(size_t parent_index, const SCI_Name & name_structure);
    };

}

#endif // SIMODO_DSL_ScriptC_Semantics
