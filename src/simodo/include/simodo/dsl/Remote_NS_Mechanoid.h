/*
MIT License

Copyright (c) 2019-2024 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, Антон Бушев

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_Remote_NS_Mechanoid
#define SIMODO_DSL_Remote_NS_Mechanoid

/*! \file Remote_NS_Mechanoid.h
    \brief Версия плоских графиков для работы на сервере и передачи информации на клиента
*/

#include "simodo/dsl/ScriptC_Interpreter.h"
// #include "simodo/dsl/RemotePanelInfo.h"

namespace simodo::dsl
{
    class Remote_NS_Mechanoid : public IScriptC_Namespace
    {
        AReporter & _listener;

        // std::vector<RemotePanelInfo>  _panels;

    public:
        Remote_NS_Mechanoid() = delete;
        Remote_NS_Mechanoid(AReporter & listener);
        virtual ~Remote_NS_Mechanoid();

        virtual SCI_Namespace_t getNamespace() override;

        AReporter & listener() const { return _listener; }
        // std::vector<RemotePanelInfo> & panels() { return _panels; }
    };

}

#endif // SIMODO_DSL_Remote_NS_Mechanoid
