/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_SemanticBase
#define SIMODO_DSL_SemanticBase

/*! \file SemanticBase.h
    \brief Базовые классы для реализации работы с готовым АСД ScriptC
*/

#include <vector>
#include <variant>
#include <limits>

#include "simodo/dsl/AstNode.h"

namespace simodo::dsl
{
    /*!
     * \brief Перечисление скалярных типов данных (типов значений для переменных и констант, а также функций),
     *        размещаемых в структурах SCI
     */
    enum class SemanticNameType
    {
        Undefined,      ///< Значение не определено
        Bool,           ///< Логический тип
        Int,            ///< Целочисленный тип
        Float,          ///< Вещественный тип
        String,         ///< Строковый тип
        ExtFunction,    ///< Ссылка на внешнюю функцию
        IntFunction,    ///< Ссылка на внутренюю функцию
    };

    /*!
     * \brief Перечисление спецификаторов имени
     */
    enum class SemanticNameQualification
    {
        None,           ///< Спецификатор имени не определён
        Scalar,         ///< Скалярное значение
        Tuple,          ///< Кортеж (экземпляр встроенного namespace или пользовательского типа)
        Array,          ///< Массив
        Function,       ///< Функция                \todo Нужно вынести этот квалификатор из перечисления (может быть массив функций)
        Type,           ///< Пользовательский тип   \todo Нужно вынести этот квалификатор из перечисления
        Reference,      ///< Ссылка
    };

    inline std::u16string semanticNameQualificationToString(SemanticNameQualification qualification)
    {
        switch (qualification)
        {
        case SemanticNameQualification::None:
            return u"None";
        case SemanticNameQualification::Scalar:
            return u"Scalar";
        case SemanticNameQualification::Tuple:
            return u"Tuple";
        case SemanticNameQualification::Array:
            return u"Array";
        case SemanticNameQualification::Function:
            return u"Function";
        case SemanticNameQualification::Type:
            return u"Type";
        case SemanticNameQualification::Reference:
            return u"Reference";
        default:
            return u"UnknownQualification";
        }
    }

    /*!
     * \brief Спецификатор доступа
     */
    enum class SemanticNameAccess
    {
        FullAccess,     ///< Доступ не ограничен
        ReadOnly,       ///< Только для чтения
        Closed,         ///< Доступ закрыт
    };

    inline const std::u16string SCI_UNDEF_STRING = u"(UNDEF)";         ///< Строка для неопределённых величин

    struct SCI_Name;
    typedef std::vector<SCI_Name>   SCI_Namespace_t;            ///< Пространство имён
    typedef std::vector<SCI_Name>   SCI_Tuple;                  ///< Кортеж
    typedef std::vector<SCI_Name>   SCI_Names_t;

    class IScriptC_Namespace
    {
    public:
        virtual ~IScriptC_Namespace() {}

        virtual SCI_Namespace_t getNamespace() = 0;
    };

    class SCI_Stack;
    typedef bool (*SCI_Function_t) (IScriptC_Namespace * p_object, SCI_Stack & sci_stack);    ///< Функция-обработчик вызова метода

    typedef std::pair<IScriptC_Namespace *,SCI_Function_t> SCI_ExtFunction;

    typedef std::variant<
                        bool,                           ///< SemanticNameType::Bool
                        int64_t,                        ///< SemanticNameType::Int
                        double,                         ///< SemanticNameType::Float
                        std::u16string,                 ///< SemanticNameType::String
                        SCI_ExtFunction,                ///< SemanticNameType::ExtFunction
                        const AstNode *                 ///< SemanticNameType::IntFunction
                        >
                                    SCI_Variant_t;              ///< Вариант для хранения скалярных значений

    /*!
     * \brief Структура для хранения значения переменных и констант
     */
    struct SCI_Scalar
    {
        SemanticNameType type = SemanticNameType::Undefined;    ///< Тип значения
        SCI_Variant_t    variant;                               ///< Значение
    };

//    /*!
//     * \brief Структура для хранения значений кортежа
//     */
//    struct SCI_Tuple
//    {
//        std::string           type;
//        std::vector<SCI_Name> elements;
//    };

    /*!
     * \brief Структура для хранения значений массивов
     */
    struct SCI_Array
    {
        std::vector<uint16_t> dimensions;
        std::vector<SCI_Name> values;
    };

    typedef SCI_Name * SCI_Reference;

    /*!
     * \brief Структура для хранения описаний
     *
     * \todo Добавить спецификатор доступа (const?)
     */
    struct SCI_Name
    {
        using variant_t = std::variant<SCI_Scalar, SCI_Tuple, SCI_Array, SCI_Reference>;

        std::u16string              name;           ///< Наименование в пространстве имён
        SemanticNameQualification   qualification = SemanticNameQualification::None;
                                                    ///< Спецификатор имени (характеристики, определяющие его структуру описания)
        variant_t
                                    bulk;

//        SCI_Name *                  point_to_origin = nullptr;
        SemanticNameAccess          access = SemanticNameAccess::FullAccess;
                                                    ///< Спецификатор доступа

        template <class T> inline bool is() const
        {
            return std::holds_alternative<T>(bulk);
        }

        template <class T> inline bool isScalar() const
        {
            if (!is<SCI_Scalar>()) return false;
            return std::holds_alternative<T>(get<SCI_Scalar>().variant);
        }
        inline bool isNumber() const
        {
            return isScalar<int64_t>() || isScalar<double>();
        }

        template <class T> inline const T &get() const
        {
            return std::get<T>(bulk);
        }
        template <class T> inline const T &getScalar() const
        {
            return std::get<T>(get<SCI_Scalar>().variant);
        }
        inline double getNumber() const
        {
            if (isScalar<int64_t>()) return getScalar<int64_t>();
            return getScalar<double>();
        }

        template <class T> inline T &get()
        {
            return std::get<T>(bulk);
        }
        template <class T> inline T &getScalar()
        {
            return std::get<T>(get<SCI_Scalar>().variant);
        }

        inline const SCI_Name &deref() const
        {
            auto obj = this;

            while(obj->qualification == SemanticNameQualification::Reference)
            {
                obj = get<SCI_Reference>();
            }

            return *obj;
        }
        inline SCI_Name &deref()
        {
            auto obj = this;

            while(obj->qualification == SemanticNameQualification::Reference)
            {
                obj = get<SCI_Reference>();
            }

            return *obj;
        }
    };

    inline const size_t  NOT_FOUND_INDEX = std::numeric_limits<size_t>::max();
    inline const size_t  UNDEFINED_INDEX = NOT_FOUND_INDEX;

    inline const uint16_t DEPTH_TOP_LEVEL = 2;
    inline const uint16_t DEPTH_UNDEFINED = UINT16_MAX;

    typedef int64_t SemanticFlags_t;

    inline const SemanticFlags_t SemanticFlag_Input  = 0x0001;
    inline const SemanticFlags_t SemanticFlag_Output = 0x0002;
    inline const SemanticFlags_t SemanticFlag_Method = 0x0004;
    inline const SemanticFlags_t SemanticFlag_InputForced = 0x0008;

    struct SemanticName
    {
        enum class SemanticNameContext : uint8_t
        {
            Undefined, ScriptC, Grammar
        }
                                    context = SemanticNameContext::ScriptC;

        Token                       name;           ///< Имя (+ место объявления)
        SemanticNameQualification   qualification   = SemanticNameQualification::None;
                                                    ///< Спецификатор имени (характеристики, определяющие структуру описания)
        SemanticNameType            type            = SemanticNameType::Undefined;
                                                    ///< Тип
        size_t                      type_index      = UNDEFINED_INDEX;
                                                    ///< Ссылка на тип
        SemanticNameAccess          access          = SemanticNameAccess::FullAccess;
                                                    ///< Спецификатор доступа
        TokenLocation               definition      = TokenLocation(u"");
                                                    ///< Место определения имени (только для функции)
        std::vector<TokenLocation>  references;     ///< Массив ссылок на данное имя из кода программы
        size_t                      owner           = UNDEFINED_INDEX;
                                                    ///< Имя-владелец данного имени (ссылка на кортеж, ссылка на таблицу имён)
//        std::string                 module_name;    ///< Имя модуля, в котором определено имя
        TokenLocation               lower_scope     = TokenLocation(u"");
                                                    ///< Нижняя граница видимости имени
        uint16_t                    depth           = DEPTH_UNDEFINED;
                                                    ///< Уровень вложенности имени
        uint16_t                    dimensions      = 0;
                                                    ///< Размерность массива
        SemanticFlags_t             semantic_flags  = 0;
                                                    ///< Семантические флаги

        SemanticName() = delete;

        SemanticName(const TokenLocation & location)
            : name(Token(LexemeType::Id, u"", location))
        {}

        explicit SemanticName(const TokenLocation & location, const std::u16string &name, SemanticNameContext context=SemanticNameContext::ScriptC)
            : context(context)
            , name(Token(LexemeType::Id, name, location))
        {}

        explicit SemanticName(const TokenLocation & location, const std::u16string &name, SemanticNameQualification q, SemanticNameType t,
                              SemanticNameContext context=SemanticNameContext::ScriptC)
            : context(context)
            , name(Token(LexemeType::Id, name, location))
            , qualification(q)
            , type(t)
        {}
    };

    SemanticNameAccess strongestAccess(SemanticNameAccess one, SemanticNameAccess two);

    std::u16string getSemanticNameTypeName(SemanticNameType type);
    std::u16string getSemanticNameQualificationName(SemanticNameQualification qua);
    std::u16string getSemanticNameAccessName(SemanticNameAccess qua);
}

#endif // SIMODO_DSL_SemanticBase
