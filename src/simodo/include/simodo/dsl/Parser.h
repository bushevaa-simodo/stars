﻿/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_Parser
#define SIMODO_DSL_Parser

/*! \file Parser.h
    \brief Парсер заданной грамматики

    Заголовочный файл, объявляющий класс парсера заданной грамматики.
*/

#include <string>

#include "simodo/dsl/AReporter.h"
#include "simodo/dsl/Grammar.h"
#include "simodo/dsl/Tokenizer.h"
#include "simodo/dsl/ISyntaxTreeBuilder.h"


namespace simodo::dsl
{
    /*!
     * \brief Состояние разбора
     *
     * Объекты класса используются в качестве элементов стека состояния конечного автомата разбора.
     *
     * Неизменяемый (immutable) класс.
     */
    class ParserState: public Token
    {
        size_t state_no;    ///< Номер состояния парсера (строка в таблице разбора)
        size_t symbol_no;   ///< Номер символа грамматики (колонка в таблице разбора)

    public:
        /*!
         * \brief Конструктор состояния разбора
         * \param state_no  Номер состояния парсера (строка в таблице разбора)
         * \param sybmol_no Номер символа грамматики (колонка в таблице разбора)
         * \param token     Токен
         */
        ParserState(size_t state_no, size_t sybmol_no, const Token &token)
            : Token(token)
            , state_no(state_no)
            , symbol_no(sybmol_no)
        {
        }

        /*!
         * \brief Геттер номера состояния парсера (строка в таблице разбора)
         * \return Номер состояния парсера (строка в таблице разбора)
         */
        size_t getStateNo(void) const { return state_no; }

        /*!
         * \brief Геттер номера символа грамматики (колонка в таблице разбора)
         * \return Номер символа грамматики (колонка в таблице разбора)
         */
        size_t getSymbolNo(void) const { return symbol_no; }
    };

    /*!
     * \brief Парсер разбора текста на языке заданной грамматики
     *
     * Парсер реализует табличный метод разбора снизу-вверх семейства *LR(1). В конструктор должна быть
     * передана заполненная грамматика.
     */
    class Parser
    {
        std::string     _file_name;     ///< Наименование файла, который парсим
        AReporter &     _m;             ///< Обработчик сообщений
        const Grammar & _g;             ///< Грамматика

    public:
        Parser() = delete;  ///< Пустой конструктор не поддерживается!

        /*!
         * \brief Конструктор парсера
         * \param file_name Наименование входного файла
         * \param m         Интерфейсная ссылка на объект обеспечения вывода информации в вызываемую программу
         * \param g         Грамматика языка заданного файла
         */
        Parser(const std::string & file_name, AReporter & m, const Grammar & g);

        /*!
         * \brief Метод разбора текста из файла, указанного в конструкторе
         *
         * Метод создаёт входной поток из заданного в конструкторе файла и вызывает одноимённый метод.
         *
         * \param builder   Интерфейсная ссылка на объект построения АСД
         * \return true, если разбор закончился без ошибок, иначе - false
         */
        bool    parse(ISyntaxTreeBuilder & builder);

        /*!
         * \brief Метод разбора текста из интерфейса входного потока
         *
         * Метод позволяет выполнять разбор текста с любого входного потока,
         * в том числе из буфера в памяти, что важно при работе с интегрированной средой разработки.
         *
         * \param stream    Интерфейс входного потока
         * \param builder   Интерфейсная ссылка на объект построения АСД
         * \return true, если разбор закончился без ошибок, иначе - false
         */
        bool    parse(IStream & stream, ISyntaxTreeBuilder & builder);

        /*!
         * \brief Метод выполняет построение параметров для лексического анализатора
         *
         * Метод выполняет построение параметров для лексического анализатора (токенайзера)
         * по данным из структуры грамматики, заданной в конструкторе.
         *
         * \return Параметры лексического анализатора
         */
        static LexicalParameters makeLexicalParameters(const Grammar & g);

    protected:
        /*!
         * \brief Метод проверки входного токена на наличие лексических ошибок
         *
         * Прокси-метод, проверяющий ошибки лексического анализа.
         *
         * \param t Токен
         * \return Токен
         */
        Token   checkToken(const Token &t) const;

        /*!
         * \brief Метод формирования и отображения отчёта о синтаксической ошибке
         * \param states    Текущее состояние разбора парсера
         * \param t         Текущий токен, приведший к синтаксической ошибке
         */
        void    reportSyntaxError(const std::vector<ParserState> & states, const Token & t);

        /*!
         * \brief Метод определяет допустим ли заданный терминал (лексема) для заданного состояния разбора
         * \param states    Состояние разбора
         * \param lexeme    Терминал, который нужно проверить на допустимость для заданного состояния
         * \return true, если терминал допустим, иначе - false
         */
        bool    isLexemeValid(std::vector<ParserState> states, const Lexeme & lexeme);
    };


}

#endif // SIMODO_DSL_Parser
