#ifndef SIMODO_DSL_TABLEFUNCTION_TableFunction
#define SIMODO_DSL_TABLEFUNCTION_TableFunction

#include "LinearInterpolation.h"

#include "simodo/convert.h"
#include "simodo/dsl/Exception.h"
#include "simodo/dsl/SCI_Stack.h"

#include <cassert>
#include <algorithm>

namespace tablefunction
{
    namespace utils
    {
        const simodo::dsl::SCI_Name * findNameInTuple(
            std::u16string name, const simodo::dsl::SCI_Tuple & tuple
        );
        int64_t toInt(const simodo::dsl::SCI_Name & n);
        double toDouble(const simodo::dsl::SCI_Name & n);
        simodo::dsl::SCI_Name * origin(simodo::dsl::SCI_Name * n);

        template <typename Integer>
        class Counter;
    }


    template<typename Int, typename Float>
    struct Table;

    template <typename Int, typename Float>
    Table<Int, Float> createTable(
        simodo::dsl::IScriptC_Namespace * , simodo::dsl::SCI_Stack & sci_stack
    );

    template <typename Int, typename Float>
    std::vector<Float> getArgs(
        simodo::dsl::IScriptC_Namespace *
        , simodo::dsl::SCI_Stack & sci_stack
        , Int n_dimens
    );

    template <typename Int, typename Float>
    HyperCube<Int, Float> findHyperCube(
        const Table<Int, Float> & table, const std::vector<Float> & args
    );


    template <typename Int, typename Float>
    std::pair<Int, Int> findBounds(const std::vector<Float> & vector, Float value);
}

namespace tablefunction
{
    namespace utils
    {
        template <typename Integer>
        class Counter
        {
            std::vector<Integer> _value;
            std::vector<Integer> _min;
            std::vector<Integer> _max;
            bool _end;

        public:
            Counter(std::vector<Integer> min, std::vector<Integer> max)
            {
                setBounds(std::move(min), std::move(max));
            }

            void setBounds(std::vector<Integer> min, std::vector<Integer> max)
            {
                if (min.empty())
                {
                    throw std::logic_error("Новое минимальное значение счётчика пусто");
                }

                if (min.size() != max.size())
                {
                    throw std::logic_error(std::string("Новое минимальное и максимальные значения счётчика отличаются по размерности")
                                            + std::to_string(min.size()) + ":" + std::to_string(max.size()));
                }

                for (size_t i = min.size() - 1; i < min.size(); --i)
                {
                    if (min[i] > max[i])
                    {
                        throw std::logic_error("Новое минимальное значение больше нового максимального значения счётчика");
                    } else if (min[i] < max[i])
                    {
                        break;
                    }
                }

                _min = std::move(min);
                _max = std::move(max);
                reset();
            }

            void set(std::vector<Integer> value)
            {
                if (value.size() != _min.size())
                {
                    throw std::logic_error(std::string("Счётчик и минимальное значение должно быть одной размерности: ")
                                            + value.size() + ":" + _min.size());
                }

                bool greater_max = true;
                bool less_min = true;
                for (size_t i = _min.size() - 1; i < _min.size(); --i)
                {
                    if (greater_max)
                    {
                        if (value.at(i) > _max.at(i))
                        {
                            throw std::logic_error(std::to_string(i) + "-ое устанавливаемое значение больше максимального: "
                                                    + std::to_string(value.at(i)) + ":" + std::to_string(_max.at(i)));
                        } else if (value.at(i) < _max.at(i))
                        {
                            greater_max = false;
                        }
                    }
                    if (less_min)
                    {
                        if (value[i] < _min[i])
                        {
                            throw std::logic_error(std::to_string(i) + "-ое устанавливаемое значение меньше минимального: "
                                                    + std::to_string(value[i]) + ":" + std::to_string(_min[i]));
                        } else if (value[i] > _min[i])
                        {
                            less_min = false;
                        }
                    }
                }

                _value.swap(value);
                _end = false;
            }

            void reset()
            {
                _value = _min;
                _end = false;
            }

            const Integer & operator[](size_t counter_number) const
            {
                try {
                    return _value.at(counter_number);
                } catch (const std::out_of_range &e) {
                    throw std::out_of_range(std::string("Неверный индекс счётчика: ") + std::to_string(counter_number)
                                                + ". " + e.what());                    
                }
            }

            bool end()
            {
                return _end;
            }

            Counter & operator++()
            {
                bool carry_flag = true;
                for (size_t i = 0; i < _value.size(); ++i)
                {
                    if (!carry_flag)
                    {
                        break;
                    }

                    if (++_value[i] > _max[i])
                    {
                        _value[i] = _min[i];
                    } else {
                        carry_flag = false;
                    }
                }
                _end = carry_flag;
                return *this;
            }

            std::string toString()
            {
                auto s = std::to_string(_value.back());
                for (size_t i = _value.size() - 2; i < _value.size(); --i)
                {
                    s += "_" + std::to_string(_value[i]);
                }
                return s;
            }
        };
    }

    template<typename Int, typename Float>
    struct Table
    {
        std::vector<Int> dimenses;
        std::vector<std::vector<Float>> args;
        std::vector<std::vector<Float>> funcs;
    };

    template <typename Int, typename Float>
    Table<Int, Float> createTable(
        simodo::dsl::IScriptC_Namespace *, simodo::dsl::SCI_Stack & sci_stack
    )
    {
        Table<Int, Float> table;

        simodo::dsl::SCI_Name * table_obj = utils::origin(&sci_stack.top(1));
        std::string table_obj_name = simodo::convertToU8(table_obj->name);

        if (table_obj->qualification != simodo::dsl::SemanticNameQualification::Tuple)
        {
            throw simodo::dsl::Exception("SCI_tf"
                            , "Первый аргумент не является кортежем. Qualification: '" + std::to_string(Int(table_obj->qualification))
                            );
        }
        
        const auto & table_tuple = std::get<simodo::dsl::SCI_Tuple>(table_obj->bulk);

        const simodo::dsl::SCI_Name * table_dimens_name = utils::findNameInTuple(u"dimens", table_tuple);

        if (table_dimens_name == nullptr || table_dimens_name->qualification != simodo::dsl::SemanticNameQualification::Array)
        {
            throw simodo::dsl::Exception("SCI_tf"
                            , "Кортеж '" + table_obj_name
                                + "' не содержит массив с именем 'dimens'"
                            );
        }

        const auto & table_dimens_array = std::get<simodo::dsl::SCI_Array>(table_dimens_name->bulk);

        if (table_dimens_array.dimensions.size() != 1)
        {
            throw simodo::dsl::Exception("SCI_tf", "Массив dimens не является одномерным: " + std::to_string(table_dimens_array.dimensions.size()));
        }

        if (table_dimens_array.dimensions.at(0) == 0)
        {
            throw simodo::dsl::Exception("SCI_tf", "Массив dimens пуст");
        }

        if (table_dimens_array.dimensions.size() > 3)
        {
            throw simodo::dsl::Exception("SCI_tf", "Массив dimens содержит больше 3 элементов: " + std::to_string(table_dimens_array.dimensions.front()));
        }

        for (auto & dimension_value : table_dimens_array.values)
        {
            try {
                table.dimenses.push_back(utils::toInt(dimension_value));
            } catch (simodo::dsl::Exception & e)
            {
                throw simodo::dsl::Exception("SCI_tf", std::string("Элементы массива dimens должны быть целочисленного типа. ") + e.what());
            }

            if (table.dimenses.back() < 1)
            {
                throw simodo::dsl::Exception("SCI_tf", "Элементы массива dimens должны быть положительными целыми числами");
            }
        }

        for (size_t i = 0; i < table.dimenses.size(); ++i)
        {
            std::u16string table_arg_name_name = std::u16string(u"arg") + simodo::convertToU16(std::to_string(i + 1));
            const simodo::dsl::SCI_Name * table_arg_name = utils::findNameInTuple(table_arg_name_name, table_tuple);

            std::string table_arg_name_std_name = simodo::convertToU8(table_arg_name_name);

            if (table_arg_name == nullptr || table_arg_name->qualification != simodo::dsl::SemanticNameQualification::Array)
            {
                throw simodo::dsl::Exception("SCI_tf"
                                , "Кортеж '" + table_obj_name + "'"
                                    + " не содержит массив с именем '" + table_arg_name_std_name + "'"
                                );
            }

            const auto & table_arg_array = std::get<simodo::dsl::SCI_Array>(table_arg_name->bulk);

            if (table_arg_array.dimensions.size() != 1)
            {
                throw simodo::dsl::Exception("SCI_tf"
                                , "Массив " + table_arg_name_std_name + " не является одномерным: "
                                    + std::to_string(table_arg_array.dimensions.size())
                                );
            }

            if (table_arg_array.dimensions.front() != table.dimenses[i])
            {
                throw simodo::dsl::Exception("SCI_tf"
                                , "Массив " + table_arg_name_std_name + " должен содержать " + std::to_string(table.dimenses[i])
                                    + " элементов. Предоставлено: " + std::to_string(table_arg_array.dimensions[0])
                                );
            }

            table.args.push_back({});
            for (auto & table_arg_value : table_arg_array.values)
            {
                auto &table_args_back = table.args.back();
                try {
                    table_args_back.push_back(utils::toDouble(table_arg_value));
                } catch (simodo::dsl::Exception & e)
                {
                    throw simodo::dsl::Exception("SCI_tf"
                        , "Элементы массива " + table_arg_name_std_name
                            + " должны быть вещественного типа. " + e.what()
                        );
                }

                if (1 < table_args_back.size()
                    && (
                        0 <= table_args_back.at(table_args_back.size() - 2)
                            - table_args_back.at(table_args_back.size() - 1)
                    ))
                {
                    throw simodo::dsl::Exception("SCI_tf"
                        , "Элементы массива " + table_arg_name_std_name
                            + " должны быть отсортированы по возрастанию (без дубликатов)."
                        );
                }
            }
        }
        
        std::vector<Int> counter_min(table.dimenses.size(), Int(1));
        std::vector<Int> counter_max = table.dimenses;
        if (table.dimenses.size() == 1)
        {
            counter_max.front() = 1;
        } else {
            std::copy(counter_max.begin() + 1, counter_max.end(), counter_max.begin());
            counter_min.pop_back();
            counter_max.pop_back();
        }
        for (utils::Counter<Int> c(counter_min, counter_max); !c.end(); ++c)
        {
            std::u16string table_func_name_name = std::u16string(u"func") + simodo::convertToU16(c.toString());
            const simodo::dsl::SCI_Name * table_func_name = utils::findNameInTuple(table_func_name_name, table_tuple);

            std::string table_func_name_std_name = simodo::convertToU8(table_func_name_name);

            if (table_func_name == nullptr || table_func_name->qualification != simodo::dsl::SemanticNameQualification::Array)
            {
                throw simodo::dsl::Exception("SCI_tf"
                                , "Кортеж '" + table_obj_name + "'"
                                    + " не содержит массив с именем '" + table_func_name_std_name + "'"
                                );
            }

            const auto & table_func_array = std::get<simodo::dsl::SCI_Array>(table_func_name->bulk);

            if (table_func_array.dimensions.size() != 1)
            {
                throw simodo::dsl::Exception("SCI_tf"
                                , "Массив " + table_func_name_std_name + " не является одномерным: "
                                    + std::to_string(table_func_array.dimensions.size())
                                );
            }

            if (table_func_array.dimensions.front() != table.dimenses.front())
            {
                throw simodo::dsl::Exception("SCI_tf"
                                , "Массив " + table_func_name_std_name + " должен содержать " + std::to_string(table.dimenses.front())
                                    + " элементов. Предоставлено: " + std::to_string(table_func_array.dimensions.front())
                                );
            }

            table.funcs.push_back({});
            for (auto & table_func_value : table_func_array.values)
            {
                try {
                    table.funcs.back().push_back(utils::toDouble(table_func_value));
                } catch (simodo::dsl::Exception & e)
                {
                    throw simodo::dsl::Exception("SCI_tf"
                                    , "Элементы массива " + table_func_name_std_name + " должны быть вещественного типа. "
                                        + e.what()
                                    );
                }
            }
        }

        return table;
    }

    template <typename Int, typename Float>
    std::vector<Float> getArgs(
        simodo::dsl::IScriptC_Namespace *
        , simodo::dsl::SCI_Stack & sci_stack
        , Int n_dimens
    )
    {
        simodo::dsl::SCI_Name * arg_obj = utils::origin(&sci_stack.top(0));

        if (arg_obj->qualification != simodo::dsl::SemanticNameQualification::Array)
        {
            throw simodo::dsl::Exception("SCI_tf"
                            , "Второй аргумент не является массивом. Qualification: '" + std::to_string(Int(arg_obj->qualification))
                            );
        }

        const auto & arg_array = std::get<simodo::dsl::SCI_Array>(arg_obj->bulk);

        if (arg_array.dimensions.size() != 1)
        {
            throw simodo::dsl::Exception("SCI_tf"
                            , "Второй аргумент не является одномерным массивом: "
                                + std::to_string(arg_array.dimensions.size())
                            );
        }

        if (arg_array.dimensions.front() != n_dimens)
        {
            throw simodo::dsl::Exception("SCI_tf"
                            , "Второй аргумент (массив) не cодержит " + std::to_string(n_dimens) + " элемента(ов): "
                                + std::to_string(arg_array.dimensions.front())
                            );
        }

        std::vector<Float> args;
        for (auto & arg_value : arg_array.values)
        {
            try {
                args.push_back(utils::toDouble(arg_value));
            } catch (simodo::dsl::Exception & e)
            {
                throw simodo::dsl::Exception("SCI_tf"
                                , std::string("Элементы второго аргумента (массива) должны быть вещественного типа. ")
                                    + e.what()
                                );
            }
        }

        return args;
    }

    template <typename Int, typename Float>
    HyperCube<Int, Float> findHyperCube(
        const Table<Int, Float> & table, const std::vector<Float> & args
    )
    {
        Int n_dimens = table.dimenses.size();
        std::vector<std::pair<Float, Float>> axes;
        std::vector<std::pair<Int, Int>> axes_idx;
        std::vector<std::pair<Float, Float>> values;

        for (Int i = 0; i < n_dimens; ++i)
        {
            axes_idx.emplace_back(findBounds<Int, Float>(table.args.at(i), args.at(i)));
            auto left_bound = table.args.at(i).at(axes_idx.at(i).first);
            auto right_bound = table.args.at(i).at(axes_idx.at(i).second);
            axes.push_back({left_bound, right_bound});
        }

        auto x0_axis = axes_idx.front();

        Int two_pow_n_minus_one = pow(Float(2), n_dimens - 1);
        for (Int i = 0; i < two_pow_n_minus_one; ++i)
        {
            Int index = 0;
            for (Int dimens = 1, ii = i; dimens < n_dimens; ++dimens, ii >>= 1)
            {
                Int ind = (ii % 2 == 0) ? axes_idx.at(dimens).first : axes_idx.at(dimens).second;
                for (Int k = 1; k < dimens; ++k)
                {
                    ind *= table.dimenses.at(k);
                }
                index += ind;
            }

            values.push_back({ table.funcs.at(index).at(x0_axis.first)
                                , table.funcs.at(index).at(x0_axis.second) });
        }

        return {n_dimens, axes, values};
    }

    template <typename Int, typename Float>
    std::pair<Int, Int> findBounds(const std::vector<Float> & vector, Float value)
    {
        if (vector.empty())
        {
            throw std::logic_error("Вектор для поиска окрестности пуст");
        }

        Int left  = -1;
        Int right = -1;

        for (Int i = 0; i < Int(vector.size()); ++i)
        {
            Float each_value = vector.at(i);
            if (each_value <= value)
            {
                left = i;
            }
            if (value < each_value)
            {
                right = i;
                break;
            }
        }

        Int left_bound;
        Int right_bound;
        if (left == -1 && right != -1)
        {
            left_bound = right_bound = 0;
        } else if (left != -1 && right == -1)
        {
            left_bound = right_bound = vector.size() - 1;
        } else {
            left_bound = left;
            right_bound = right;
        }

        return {left_bound, right_bound};
    }
}

#endif // SIMODO_DSL_TABLEFUNCTION_HyperCube