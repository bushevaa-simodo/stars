#ifndef SIMODO_DSL_TABLEFUNCTION_V2_TableFunction
#define SIMODO_DSL_TABLEFUNCTION_V2_TableFunction

#include "simodo/dsl/tablefunction/HyperCube.h"
#include "simodo/dsl/tablefunction/TableFunction.h"

#include "simodo/dsl/SemanticBase.h"

#include <memory>
#include <numeric>

//#define DEBUG

#ifdef SCI_DEBUG
#include <iostream>
#endif

namespace tablefunction::v2
{
    namespace util
    {
        template <typename T>
        using SharedPtr = std::shared_ptr<T>;

        template <typename T>
        using SharedRef = const SharedPtr<T>;

        template <typename T>
        using SharedRefToConst = SharedRef<const T>;
    };


    template <typename Int, typename Float>
    struct Table;

    template <typename Int, typename Float>
    struct SciTable;


    template <typename Int, typename Float>
    util::SharedPtr<Table<Int, Float>> createTable(const simodo::dsl::SCI_Name &sci_name);

    template <typename Int, typename Float>
    std::vector<Float> getArgs(const simodo::dsl::SCI_Name &sci_name, Int n_dimens);

    template <typename Int, typename Float>
    HyperCube<Int, Float> findHyperCube(
        util::SharedRefToConst<Table<Int, Float>> &table, const std::vector<Float> &args
    );

    template <typename Int, typename Float>
    std::pair<Int, Int> findBounds(
        util::SharedRefToConst<Table<Int, Float>> &table, Int dimension, Float value
    );

    template <typename Int, typename Float>
    struct Table
    {
        virtual ~Table() = default;

        virtual Int nDimensions() const = 0;
        virtual Int dimensionSize(Int dimension) const = 0;
        virtual Float dimensionAt(Int dimension, Int index) const = 0;
        virtual Float valueAt(const std::vector<Int> &indexes) const = 0;
    };

    // @todo Определиться с направлением размерностей
    template <typename Int, typename Float>
    struct SciTable : public Table<Int, Float>
    {
        SciTable(
            std::vector<const simodo::dsl::SCI_Array *> &&sci_dimensions
            , const simodo::dsl::SCI_Array *sci_values
        )
            : _sci_dimensions(sci_dimensions)
            , _sci_values(sci_values)
        {}

        Int nDimensions() const override
        {
            return _sci_dimensions.size();
        }

        Int dimensionSize(Int dimension) const override
        {
            return _sci_dimensions.at(dimension)->dimensions.front();
        }

        Float dimensionAt(Int dimension, Int index) const override
        {
            return _sci_dimensions.at(dimension)->values.at(index).getNumber();
        }

        Float valueAt(const std::vector<Int> &indexes) const override
        {
            Int linear_index = 0;
            Int linear_multiplier = 1;

            auto dimension_it = _sci_dimensions.cbegin();
            for (auto index : indexes)
            {
                linear_index += index * linear_multiplier;
                linear_multiplier *= (*dimension_it)->dimensions.front();

                ++dimension_it;
            }
            return _sci_values->values.at(linear_index).getNumber();
        }
    
    private:
        std::vector<const simodo::dsl::SCI_Array *> _sci_dimensions;
        const simodo::dsl::SCI_Array *_sci_values;
    };


    template <typename Int, typename Float>
    util::SharedPtr<Table<Int, Float>> createTable(const simodo::dsl::SCI_Name &table_sci_name)
    {
        const auto table_u8_name = simodo::convertToU8(table_sci_name.name);

        if (table_sci_name.qualification != simodo::dsl::SemanticNameQualification::Tuple)
        {
            throw simodo::dsl::Exception(
                "tablefunction::v2::createTable"
                , "Аргумент '" + table_u8_name + "' не является кортежем."
                    + " Qualification: " + std::to_string(Int(table_sci_name.qualification))
            );
        }
        auto &table_tuple = table_sci_name.get<simodo::dsl::SCI_Tuple>();


        //// sci_values


        auto values_sci_name = utils::findNameInTuple(u"values", table_tuple);
        if (values_sci_name == nullptr)
        {
            throw simodo::dsl::Exception(
                "tablefunction::v2::createTable"
                , "Кортеж '" + table_u8_name + "' не содержит массив 'values'."
            );
        }

        if (!values_sci_name->is<simodo::dsl::SCI_Array>())
        {
            throw simodo::dsl::Exception(
                "tablefunction::v2::createTable"
                , "Поле 'values' в кортеже '" + table_u8_name + "' не является массивом."
                    " Qualification: " + std::to_string(Int(values_sci_name->qualification))
            );
        }
        auto &sci_values = values_sci_name->get<simodo::dsl::SCI_Array>();

        if (sci_values.dimensions.size() == 0 || sci_values.dimensions[0] == 0)
        {
            throw simodo::dsl::Exception(
                "tablefunction::v2::createTable"
                , "Массив 'values' в кортеже '" + table_u8_name + "' пуст."
            );
        }

        if (!sci_values.values.at(0).isNumber())
        {
            throw simodo::dsl::Exception(
                "tablefunction::v2::createTable"
                , "Массив 'values' в кортеже '" + table_u8_name + "' должен содержать только числа."
                    " Qualification: " + std::to_string(Int(sci_values.values.at(0).qualification))
            );
        }


        //// sci_dimensions


        std::vector<const simodo::dsl::SCI_Array *> sci_dimensions;
        sci_dimensions.resize(sci_values.dimensions.size());
        auto sci_dimensions_rit = sci_dimensions.rbegin();
        for (size_t i = 0; i < sci_values.dimensions.size(); ++i)
        {
            auto arg_n_name = std::u16string(u"arg") + simodo::convertToU16(std::to_string(i + 1));

            auto arg_n_sci_name = utils::findNameInTuple(arg_n_name, table_tuple);
            auto arg_n_u8_name = simodo::convertToU8(arg_n_name);

            if (arg_n_sci_name == nullptr)
            {
                throw simodo::dsl::Exception(
                    "tablefunction::v2::createTable"
                    , "Кортеж '" + table_u8_name + "'"
                        + " не содержит массив с именем '" + arg_n_u8_name + "'"
                );
            }

            if (!arg_n_sci_name->is<simodo::dsl::SCI_Array>())
            {
                throw simodo::dsl::Exception(
                    "tablefunction::v2::createTable"
                    , "Поле '" + arg_n_u8_name + "' в кортеже '" + table_u8_name
                    + "' не является массивом."
                        " Qualification: " + std::to_string(Int(arg_n_sci_name->qualification))
                );
            }

            auto &sci_args = arg_n_sci_name->get<simodo::dsl::SCI_Array>();
            if (sci_args.dimensions.size() != 1)
            {
                throw simodo::dsl::Exception(
                    "tablefunction::v2::createTable"
                    , "Массив " + arg_n_u8_name + " не является одномерным: "
                        + std::to_string(sci_args.dimensions.size())
                );
            }

            if (sci_args.dimensions.front() != sci_values.dimensions[i])
            {
                throw simodo::dsl::Exception(
                    "tablefunction::v2::createTable"
                    , "Массив " + arg_n_u8_name
                        + " должен содержать " + std::to_string(sci_values.dimensions[i])
                        + " элементов."
                        " Предоставлено: " + std::to_string(sci_args.dimensions.front())
                );
            }

            if (!sci_args.values.front().isNumber())
            {
                throw simodo::dsl::Exception(
                    "tablefunction::v2::createTable"
                    , "Массив '" + arg_n_u8_name + "' в кортеже '" + table_u8_name + "'"
                        " должен содержать только числа."
                        + " Qualification: " + std::to_string(Int(sci_args.values.front().qualification))
                );
            }

            for (
                auto sci_arg_it = sci_args.values.cbegin()
                ;
                ;
            )
            {
                auto prev_sci_arg = *sci_arg_it++;
                if (sci_arg_it == sci_args.values.cend()) break;

                if (!(*sci_arg_it).isNumber())
                {
                    throw simodo::dsl::Exception(
                        "tablefunction::v2::createTable"
                        , "Массив '" + arg_n_u8_name + "'"
                            " в кортеже '" + table_u8_name + "'"
                            " должен содержать только числа."
                            + " Qualification: " + std::to_string(Int(sci_arg_it->qualification))
                    );
                }

                if (prev_sci_arg.getNumber() >= (*sci_arg_it).getNumber())
                {
                    throw simodo::dsl::Exception(
                        "tablefunction::v2::createTable"
                        , "Элементы массива '" + arg_n_u8_name + "'"
                            " должны быть отсортированы по возрастанию (без дубликатов)."
                    );
                }                
            }

            (*sci_dimensions_rit) = &sci_args;
            ++sci_dimensions_rit;
        }

        // SciTable

        return std::make_shared<SciTable<Int, Float>>(std::move(sci_dimensions), &sci_values);
    }

    template <typename Int, typename Float>
    std::vector<Float> getArgs(
        const simodo::dsl::SCI_Name &args_sci_name, util::SharedRefToConst<Table<Int, Float>> &table
    )
    {
        const auto args_u8_name = simodo::convertToU8(args_sci_name.name);

        if (!args_sci_name.is<simodo::dsl::SCI_Array>())
        {
            throw simodo::dsl::Exception(
                "tablefunction::v2::getArgs"
                , "Аргумент '" + args_u8_name + "' не является массивом."
                    " Qualification: " + std::to_string(Int(args_sci_name.qualification))
            );
        }
        auto &args_array = args_sci_name.get<simodo::dsl::SCI_Array>();

        if (args_array.dimensions.size() != 1)
        {
            throw simodo::dsl::Exception(
                "tablefunction::v2::getArgs"
                , "Массив '" + args_u8_name + "'"
                    " не является одномерным: "
                    + std::to_string(args_array.dimensions.size())
            );
        }

        if (args_array.dimensions.front() != table->nDimensions())
        {
            throw simodo::dsl::Exception(
                "tablefunction::v2::getArgs"
                , "Размер массива '" + args_u8_name + "'"
                    " (" + std::to_string(args_array.dimensions.front()) + ")"
                    " не соответствует таблице: " + std::to_string(table->nDimensions())
            );
        }

        std::vector<Float> args;
        args.resize(table->nDimensions());
        auto args_rit = args.rbegin();
        for (const auto &arg_sci_name : args_array.values)
        {
            if (!arg_sci_name.isNumber())
            {
                throw simodo::dsl::Exception(
                    "tablefunction::v2::getArgs"
                    , "Элементы массива '" + args_u8_name + "' должны быть числами."
                        " Qualification: " + std::to_string(Int(arg_sci_name.qualification))
                );
            }
            (*args_rit) = arg_sci_name.getNumber();
            ++args_rit;
        }

        return args;
    }

    template <typename Int, typename Float>
    HyperCube<Int, Float> findHyperCube(
        util::SharedRefToConst<Table<Int, Float>> &table, const std::vector<Float> &args
    )
    {
        const Int n_dimens = table->nDimensions();

        std::vector<std::pair<Int, Int>> axes_idx;
        std::vector<std::pair<Float, Float>> axes;

#ifdef SCI_DEBUG
        std::cout << "Axes " << n_dimens << std::endl;
#endif
        for (Int i = 0; i < n_dimens; ++i)
        {
#ifdef SCI_DEBUG
            std::cout << "1" << std::endl;
            std::cout << args.size() << " as" << std::endl;
#endif
            axes_idx.emplace_back(findBounds<Int, Float>(table, i, args.at(i)));
#ifdef SCI_DEBUG
            std::cout << "2" << std::endl;
#endif
            auto left_bound = table->dimensionAt(i, axes_idx.at(i).first);
#ifdef SCI_DEBUG
            std::cout << "3" << std::endl;
#endif
            auto right_bound = table->dimensionAt(i, axes_idx.at(i).second);
#ifdef SCI_DEBUG
            std::cout << "4" << std::endl;
#endif
            axes.push_back({left_bound, right_bound});
        }

        const auto &x0_axis = axes_idx.front();

#ifdef SCI_DEBUG
        std::cout << "values" << std::endl;
#endif
        std::vector<std::pair<Float, Float>> values;

        const Int two_pow_n_minus_one = pow(Float(2), n_dimens - 1);
        std::vector<Int> coords;
        coords.resize(n_dimens);
        for (Int i = 0; i < two_pow_n_minus_one; ++i)
        {
            for (Int dimens = 1, ii = i; dimens < n_dimens; ++dimens, ii >>= 1)
            {
                coords.at(dimens) = (ii % 2 == 0)
                    ? axes_idx.at(dimens).first
                    : axes_idx.at(dimens).second;
            }

            coords.front() = x0_axis.first;
            auto f = table->valueAt(coords);
            coords.front() = x0_axis.second;
            auto s = table->valueAt(coords);
            values.push_back({f, s});
        }

        return {n_dimens, axes, values};
    }

    template <typename Int, typename Float>
    std::pair<Int, Int> findBounds(
        util::SharedRefToConst<Table<Int, Float>> &table, Int dimension, Float value
    )
    {
        Int left  = -1;
        Int right = -1;

#ifdef SCI_DEBUG
        std::cout << "findBounds" << std::endl;
#endif
        for (Int i = 0; i < Int(table->dimensionSize(dimension)); ++i)
        {
            Float each_value = table->dimensionAt(dimension, i);
            if (each_value <= value)
            {
                left = i;
            }
            if (value < each_value)
            {
                right = i;
                break;
            }
        }

#ifdef SCI_DEBUG
        std::cout << "left right" << std::endl;
#endif
        Int left_bound;
        Int right_bound;
        if (left == -1 && right != -1)
        {
            left_bound = right_bound = 0;
        } else if (left != -1 && right == -1)
        {
            left_bound = right_bound = table->dimensionSize(dimension) - 1;
        } else {
            left_bound = left;
            right_bound = right;
        }

#ifdef SCI_DEBUG
        std::cout << "end" << std::endl;
#endif
        return {left_bound, right_bound};
    }
}

#endif // SIMODO_DSL_TABLEFUNCTION_V2_TableFunction