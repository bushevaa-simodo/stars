/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_ScriptC_NS_Scene
#define SIMODO_DSL_ScriptC_NS_Scene

/*! \file ScriptC_NS_Scene.h
    \brief Пространство имён сцены моделирования
*/

#include "simodo/dsl/ScriptC_Interpreter.h"
#include "simodo/stage/ThreadLocalCopyFactory.h"
#include "simodo/stage/Stage.h"

namespace simodo::dsl
{
    class ScriptC_NS_Scene : public IScriptC_Namespace
    {
    public:
        using Interpreter = ScriptC_Interpreter;
        using InterpreterFactory
            = simodo::stage::ThreadLocalCopyFactory<Interpreter>;
        using Stage = simodo::stage::Stage<const void *>;

    public:
        Interpreter *p_interpreter;
        InterpreterFactory interpreter_factory;
        Stage stage;
        bool time_output_enabled;

    public:
        ScriptC_NS_Scene(
            Interpreter *p_interpreter = nullptr
            , bool time_output_enabled = false
        );

    public:
        virtual SCI_Namespace_t getNamespace() override;
    };
}

#endif // SIMODO_DSL_ScriptC_NS_Scene
