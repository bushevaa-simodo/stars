/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_AstBuilder_h
#define SIMODO_DSL_AstBuilder_h

/*! \file AstBuilder.h
    \brief Построитель абстрактного синтаксического дерева (АСД)
*/

#include <string>

#include "simodo/dsl/ISyntaxTreeBuilder.h"
#include "simodo/dsl/AstBuilderHelper.h"
#include "simodo/dsl/ScriptC_Interpreter.h"
#include "simodo/dsl/ModulesManagement.h"

namespace simodo::dsl
{
    /*!
     * \brief Построитель абстрактного синтаксического дерева (АСД) для выражений
     *
     * \attention Данный класс имеет формируемое состояние, копирование нужно делать аккуратно.
     */
    class AstBuilder: public ISyntaxTreeBuilder, public IScriptC_Namespace
    {
        AReporter &         _m;             ///< Обработчик сообщений
        AstNode &           _ast;
        const std::map<std::u16string,AstNode> & _handlers;
        ModulesManagement   _mm;            ///< Управление модулями
        AstBuilderHelper    _helper;        ///< Вспомогательный класс для построения АСД
        ScriptC_Interpreter _machine;       ///< Интерпретирующая машина

        Token *                 _current_production;
        std::vector<Token> *    _current_pattern;
        uint16_t                _current_stream_no = DEFAULT_STREAM_NO;

    public:
        AstBuilder() = delete;              ///< Пустой конструктор не поддерживается!

        /*!
         * \brief Инициализирующий конструктор построителя АСД выражений
         * \param reporter Обработчик сообщений
         * \param ast_root Корнейвой узел АСД
         */
        AstBuilder(AReporter & reporter, AstNode & ast_root, const std::map<std::u16string,AstNode> & handlers);

        AReporter & reporter() { return _m; }

        /*!
         * \brief Геттер на вспомогательный класс для построения АСД
         * \return Ссылка на вспомогательный класс для построения АСД
         */
        AstBuilderHelper & helper() { return _helper; }

        const Token & current_production() const { return *_current_production; }

        const std::vector<Token> & current_pattern() const { return *_current_pattern; }

        uint16_t current_stream_no() const { return _current_stream_no; }

        void setCurrentStreamNo(uint16_t no) { _current_stream_no = no; }

        /*!
         * \brief Метод вызывается парсером перед началом разбора
         * \return Если возвращается false, парсер прекращает разбор с ошибкой, иначе - продолжает
         */
        virtual bool onStart() override;

        /*!
         * \brief Метод вызывается парсером при выполнении свёртки
         *
         * Метод вызывается, когда выполняется свёртка состояний.
         *
         * \param production    Токен продукции, на которую заменяется шаблон в стеке состояний парсера
         * \param pattern       Шаблон (набор токенов), который заменяется на продукцию в стеке состояний парсера
         * \param action        АСД для интерпретатора для формирования фрагмента АСД по данной продукции
         * \param is_done       Признак завершённой обработки данного правила. Нужен, что бы сократить число проверок
         * \return Если возвращается false, парсер прекращает разбор с ошибкой, иначе - продолжает
         */
        virtual bool onProduction(Token production, std::vector<Token> pattern, const AstNode & action, bool &is_done) override;

        /*!
         * \brief Метод вызывается парсером при получении очередного токена
         * \return Если возвращается false, парсер прекращает разбор с ошибкой, иначе - продолжает
         */
        virtual bool onTerminal(const Token & ) override { return true; }

        /*!
         * \brief Метод вызывается парсером после завершения разбора текста
         *
         * Предполагается, что при вызове данного метода и при наличии признака успешности
         * разбора текста, нужно завершить формирование АСД.
         *
         * \return Если возвращается false, парсер прекращает разбор с ошибкой, иначе - продолжает
         */
        virtual bool onFinish(bool) override;

        virtual SCI_Namespace_t getNamespace() override;

    protected:
        SCI_Namespace_t _getNamespace();
    };

}

#endif // SIMODO_DSL_AstBuilder_h

