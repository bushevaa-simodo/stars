#ifndef SIMODO_DSL_SCI_STACKGUARD_H
#define SIMODO_DSL_SCI_STACKGUARD_H

#include "simodo/dsl/SCI_Stack.h"

#include <cassert>

namespace simodo::dsl
{
    template <int ARG_COUNT>
    class SCI_StackGuard
    {
    public:
        SCI_StackGuard(SCI_Stack &stack)
            : _stack(stack)
        {
            static_assert(ARG_COUNT > 0);
            assert(stack.size() >= ARG_COUNT);
        }
        ~SCI_StackGuard()
        {
            _stack.pop(ARG_COUNT);
        }

    private:
        SCI_Stack &_stack;
    };
}
#endif // SIMODO_DSL_SCI_STACKGUARD_H
