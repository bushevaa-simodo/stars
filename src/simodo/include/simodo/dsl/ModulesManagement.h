/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_ModulesManagement
#define SIMODO_DSL_ModulesManagement

/*! \file ModulesManagement.h
    \brief Управление модулями.
*/

#include <map>
#include <memory>

#include "simodo/dsl/SemanticBase.h"
#include "simodo/dsl/Stream.h"

namespace simodo::dsl
{
    class AReporter;
    class GrammarManagement;

    struct ModuleImportResults
    {
        AstNode                   ast;
        std::vector<SemanticName> names;
        std::map<std::u16string,const AstNode *> functions;
        bool                      ok = false;
    };

    class FileContentProvider_interface
    {
    public:
        virtual ~FileContentProvider_interface() = default;

        virtual std::unique_ptr<IStream> stream(const std::string & file_name) = 0;
    };

    class ModulesManagement
    {
    protected:
        AReporter &                     _m;                 ///< Обработчик сообщений
        AReporter &                     _rtm;               ///< Обработчик сообщений времени выполнения
        GrammarManagement *             _gm;                ///< Управление грамматиками
        std::string                     _path_to_grammars;  ///< Путь к описанию грамматик
        FileContentProvider_interface * _p_content_provider;///< Поставщик содержимого файлов (для LSP)
        std::vector<std::pair<std::u16string,IScriptC_Namespace*>> 
                                        _additional_namespaces;///< Модули отображения для использования в модулях
        std::map<std::string,ModuleImportResults>
                                        _module_set;        ///< Набор модулей

    public:
        ModulesManagement() = delete;
        virtual ~ModulesManagement() = default;

        ModulesManagement(AReporter & m, 
                          AReporter & rtm, 
                          GrammarManagement * gm, 
                          const std::string &path_to_grammars = "",
                          FileContentProvider_interface * p_content_provider = nullptr,
                          const std::vector<std::pair<std::u16string,IScriptC_Namespace*>> &additional_namespaces={});

        virtual const ModuleImportResults & loadModule(const Token & module_file_token, 
                                                       const Token & grammar_name_token);

        const std::map<std::string,ModuleImportResults> & modules() const { return /*(_mm != nullptr) ? _mm->modules() :*/ _module_set; }

        const ModuleImportResults & getModuleNames(const std::string &module_path) const;
    };

}

#endif // SIMODO_DSL_ModulesManagement
