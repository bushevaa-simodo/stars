/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_FuzeParserBase_h
#define SIMODO_DSL_FuzeParserBase_h

/*! \file FuzeParserBase.h
    \brief Вспомогательный класс для начального разбора описаний грамматик

    Заголовочный файл, описывающий вспомогательный класс для начального разбора описаний грамматик.
*/

#include <string>

#include "simodo/dsl/AReporter.h"
#include "simodo/dsl/Tokenizer.h"


namespace simodo::dsl
{
    /*!
     * \brief Базовый класс для разборщиков грамматики fuze
     */
    class FuzeParserBase
    {
        AReporter & _m;  	///< Обработчик сообщений

    public:
        FuzeParserBase() = delete; ///< Пустой конструктор не поддерживается!

        FuzeParserBase(AReporter & m)
            : _m(m)
        {}

        AReporter & reporter() const { return _m; };

    protected:
        /*!
         * \brief Метод формирования и передачи сообщения о синтаксической ошибке
         * \param t         Текущий токен
         * \param expected  Фрагмент текста, указывающий ожидаемый в данном контексте символ входного потока
         * \return true, если формирование и передача сообщения выполнены успешно, иначе - false
         */
        bool    reportUnexpected(const Token &t, const std::u16string &expected=u"") const;
    };
}

#endif // SIMODO_DSL_FuzeParserBase_h
