/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_Stream
#define SIMODO_DSL_Stream

/*! \file Stream.h
    \brief Интерфейс входного потока и его реализации

    Заголовочный файл, описывающий интерфейсный класс входного потока, а также реализации для файла и буфера.
*/

#include <fstream>

namespace simodo::dsl
{
    /*!
     * \brief Интерфейс входного потока
     *
     * Интерфейс используется при создании лексического анализатора (см. simodo::dsl::Tokenizer)
     */
    class IStream
    {
    public:
        virtual ~IStream() = default;    ///< Виртуальный деструктор

        /*!
         * \brief Получение очередного символа из входного потока
         * \return Символ входного потока
         */
        virtual char16_t get() = 0;

        /*!
         * \brief Возврат признака конца файла
         * \return Признак конца файла
         */
        virtual bool eof() const = 0;

        /*!
         * \brief Возврат признака нормального состояния входного потока
         * \return Признак нормального состояния входного потока
         */
        virtual bool good() const = 0;
    };

    /*!
     * \brief Реализация входного потока из файла
     *
     * \todo Хорошее место, чтобы подсчитывать точное количество символов, а не байт!
     * Нужно бы это реализовать.
     */
    class FileStream: public IStream
    {
        std::istream & _in;    ///< Ссылка на входной поток
        char16_t       _surrogate_pair;
        bool           _untouched;

    public:
        FileStream() = delete;  ///< Пустой конструктор не поддерживается!

        /*!
         * \brief Конструктор входного потока из файла
         * \param in    Ссылка на входной поток
         */
        FileStream(std::istream & in)
            : _in(in)
            , _surrogate_pair(0)
            , _untouched(true)
        {}

        /*!
         * \brief Получение очередного символа из входного потока
         * \return Символ входного потока
         */
        virtual char16_t get() override;

        /*!
         * \brief Возврат признака конца файла
         * \return Признак конца файла
         */
        virtual bool eof() const override { return _in.eof(); }

        /*!
         * \brief Возврат признака нормального состояния входного потока
         * \return Признак нормального состояния входного потока
         */
        virtual bool good() const override { return _in.good(); }
    };

    /*!
     * \brief Реализация входного потока из буфера в памяти
     */
    class BufferStream: public IStream
    {
        const char16_t * _buffer;   ///< Буфер
        size_t           _pos;      ///< Позиция чтения в буфере

    public:
        BufferStream() = delete;    ///< Пустой конструктор не поддерживается!

        /*!
         * \brief Конструктор входного потока из буфера в памяти
         * \param buffer    Буфер
         */
        BufferStream(const char16_t * buffer) : _buffer(buffer), _pos(0) {}

        /*!
         * \brief Получение очередного символа из входного потока
         * \return Символ входного потока
         */
        virtual char16_t get() override
        {
            return (_buffer[_pos] == 0) ? std::char_traits<char16_t>::eof() : _buffer[_pos++];
        }

        /*!
         * \brief Возврат признака конца файла
         * \return Признак конца файла
         */
        virtual bool eof() const override { return (_buffer[_pos] == 0); }

        /*!
         * \brief Возврат признака нормального состояния входного потока
         * \return Признак нормального состояния входного потока
         */
        virtual bool good() const override { return (_buffer[_pos] != 0); }
    };

}

#endif // SIMODO_DSL_Stream
