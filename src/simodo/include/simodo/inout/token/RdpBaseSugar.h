/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_RdpBaseSugar
#define simodo_RdpBaseSugar

/*! \file RdpBaseSugar.h
    \brief Вспомогательный базовый класс для разбора текстов на языке SIMODO fuze
*/

#include "simodo/inout/token/Token.h"
#include "simodo/inout/reporter/Reporter_abstract.h"

#include <string>

namespace simodo::inout::token
{
    /*!
     * \brief Вспомогательный базовый класс для разбора текстов на языке SIMODO fuze 
     */
    class RdpBaseSugar
    {
        reporter::Reporter_abstract & _m;  	///< Обработчик сообщений

    public:
        RdpBaseSugar() = delete; ///< Пустой конструктор не поддерживается!

        RdpBaseSugar(reporter::Reporter_abstract & m)
            : _m(m)
        {}

        reporter::Reporter_abstract & reporter() const { return _m; }

    protected:
        /*!
         * \brief Метод формирования и передачи сообщения о синтаксической ошибке
         * \param t         Текущий токен
         * \param expected  Фрагмент текста, указывающий ожидаемый в данном контексте символ входного потока
         * \return true, если формирование и передача сообщения выполнены успешно, иначе - false
         */
        bool    reportUnexpected(const Token &t, const std::u16string & expected=u"") const;
    };
}

#endif // simodo_RdpBaseSugar
