/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_InputStream_interface
#define simodo_token_InputStream_interface

/*! \file InputStream_interface.h
    \brief Интерфейс входного потока.
*/

#include <fstream>


namespace simodo::inout::token
{
    /*!
     * \brief Интерфейс входного потока.
     *
     * Интерфейс используется при создании лексического анализатора (см. simodo::dsl::Tokenizer)
     */
    class InputStream_interface
    {
    public:
        virtual ~InputStream_interface() = default;    ///< Виртуальный деструктор

        /*!
         * \brief Получение очередного символа из входного потока
         * \return Символ входного потока
         */
        virtual char16_t get() = 0;

        /*!
         * \brief Возврат признака конца файла
         * \return Признак конца файла
         */
        virtual bool eof() const = 0;

        /*!
         * \brief Возврат признака нормального состояния входного потока
         * \return Признак нормального состояния входного потока
         */
        virtual bool good() const = 0;
    };
}

#endif // simodo_token_InputStream_interface
