/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_BufferStream
#define simodo_token_BufferStream

/*! \file BufferStream.h
    \brief Реализация входного потока из буфера в памяти.
*/

#include "simodo/inout/token/InputStream_interface.h"

namespace simodo::inout::token
{
    /*!
     * \brief Реализация входного потока из буфера в памяти.
     */
    class BufferStream: public InputStream_interface
    {
        const char16_t * _buffer;   ///< Буфер
        size_t           _pos;      ///< Позиция чтения в буфере

    public:
        BufferStream() = delete;    ///< Пустой конструктор не поддерживается!

        /*!
         * \brief Конструктор входного потока из буфера в памяти
         * \param buffer    Буфер
         */
        BufferStream(const char16_t * buffer) : _buffer(buffer), _pos(0) {}

        /*!
         * \brief Получение очередного символа из входного потока
         * \return Символ входного потока
         */
        virtual char16_t get() override
        {
            return (_buffer[_pos] == 0) ? std::char_traits<char16_t>::eof() : _buffer[_pos++];
        }

        /*!
         * \brief Возврат признака конца файла
         * \return Признак конца файла
         */
        virtual bool eof() const override { return (_buffer[_pos] == 0); }

        /*!
         * \brief Возврат признака нормального состояния входного потока
         * \return Признак нормального состояния входного потока
         */
        virtual bool good() const override { return (_buffer[_pos] != 0); }
    };

}

#endif // simodo_token_BufferStream
