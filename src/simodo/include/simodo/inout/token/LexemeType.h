/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_LexemeType
#define simodo_token_LexemeType

/*! \file LexemeType.h
    \brief Тип лексемы абстрактного языка.
*/

namespace simodo::inout::token
{
    //! Тип лексемы
    enum class LexemeType
    {
        Compound = 0,   ///< Нетерминальный символ ("нетерминал")
        Empty,          ///< Лексема неопределена (используется для обозначения пустого потока, конца файла)
        Punctuation,    ///< Разделитель, пунктуация или ключевое слово (возможно уточнение TokenQualification::Keyword)
        Id,             ///< Идентификатор (возможно уточнение TokenQualification::NationalCharacterMix)
        Annotation,     ///< Аннотация, строка символов
        Number,         ///< Число (возможно уточнение TokenQualification::Integer, TokenQualification::RealNumber, TokenQualification::NotANumber)
        Comment,        ///< Комментарий
        NewLine,        ///< Получена новая строка
        Error           ///< Ошибка лексики (возможно уточнение TokenQualification::UnknownCharacterSet, TokenQualification::NationalCharacterMix, TokenQualification::NationalCharacterUse)
    };
}

#endif // simodo_token_LexemeType
