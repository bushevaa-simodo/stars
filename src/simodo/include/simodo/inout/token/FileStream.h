/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_FileStream
#define simodo_token_FileStream

/*! \file FileStream.h
    \brief Интерфейс входного потока и его реализации
*/

#include "simodo/inout/token/InputStream_interface.h"

#include <fstream>

namespace simodo::inout::token
{
    /*!
     * \brief Реализация входного потока из файла
     *
     * \todo Хорошее место, чтобы подсчитывать точное количество символов, а не байт!
     * Нужно бы это реализовать.
     */
    class FileStream: public InputStream_interface
    {
        std::istream & _in;    ///< Ссылка на входной поток
        char16_t       _surrogate_pair;

    public:
        FileStream() = delete;  ///< Пустой конструктор не поддерживается!

        /*!
         * \brief Конструктор входного потока из файла
         * \param in    Ссылка на входной поток
         */
        FileStream(std::istream & in) : _in(in), _surrogate_pair(0) {}

        /*!
         * \brief Получение очередного символа из входного потока
         * \return Символ входного потока
         */
        virtual char16_t get() override;

        /*!
         * \brief Возврат признака конца файла
         * \return Признак конца файла
         */
        virtual bool eof() const override { return _in.eof(); }

        /*!
         * \brief Возврат признака нормального состояния входного потока
         * \return Признак нормального состояния входного потока
         */
        virtual bool good() const override { return _in.good(); }
    };
}

#endif // simodo_token_FileStream
