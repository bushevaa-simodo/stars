#ifndef _simodo_inout_log_Logger_interface_H
#define _simodo_inout_log_Logger_interface_H

#include <string>

namespace simodo::inout::log
{

class Logger_interface
{
public:
    virtual ~Logger_interface() = default;

    virtual const std::string & name() const = 0;
    virtual void debug(const std::string & message, const std::string & context="") = 0;
    virtual void info(const std::string & message, const std::string & context="") = 0;
    virtual void warning(const std::string & message, const std::string & context="") = 0;
    virtual void error(const std::string & message, const std::string & context="") = 0;
    virtual void critical(const std::string & message, const std::string & context="") = 0;
};

}

#endif // _simodo_inout_log_Logger_interface_H