#ifndef CompletionParams_h
#define CompletionParams_h

#include "simodo/lsp/TextDocumentPositionParams.h"

#include <string>

namespace simodo::lsp
{
    enum class CompletionTriggerKind
    {
        /**
         * Completion was triggered by typing an identifier (24x7 code
         * complete), manual invocation (e.g Ctrl+Space) or via API.
         */
        Invoked = 1,

        /**
         * Completion was triggered by a trigger character specified by
         * the `triggerCharacters` properties of the
         * `CompletionRegistrationOptions`.
         */
        TriggerCharacter = 2,

        /**
         * Completion was re-triggered as the current completion list is incomplete.
         */
        TriggerForIncompleteCompletions = 3
    };

    struct CompletionContext
    {
        /**
         * How the completion was triggered.
         */
        CompletionTriggerKind triggerKind;

        /**
         * The trigger character (a single character) that has trigger code
         * complete. Is undefined if
         * `triggerKind !== CompletionTriggerKind.TriggerCharacter`
         */
        std::string triggerCharacter;
    };

    struct CompletionParams: public TextDocumentPositionParams
    {
        /**
         * The completion context. This is only available if the client specifies
         * to send this using the client capability
         * `completion.contextSupport === true`
         */
        CompletionContext context;
    };

    bool parseCompletionContext(const simodo::variable::Value & context_value, CompletionContext & context);
    bool parseCompletionParams(const simodo::variable::Value & params, 
                               CompletionParams & completionParams);
}
#endif // CompletionParams_h