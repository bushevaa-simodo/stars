#ifndef _simodo_lsp_client_h
#define _simodo_lsp_client_h

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"
#include "simodo/inout/convert/functions.h"
#include "simodo/inout/log/Logger_interface.h"

#include <boost/process.hpp>
#include <iostream>
#include <functional>
#include <memory>
#include <thread>
#include <mutex>
#include <map>
#include <condition_variable>

namespace simodo::lsp
{

namespace bp = boost::process;

typedef std::function<void(variable::json::Rpc)> RpcHandle;

class LanguageClient
{
    bp::opstream                        _os;
    bp::ipstream                        _is;
    std::unique_ptr<bp::child>          _language_server;
    inout::log::Logger_interface &      _log;
    std::map<int64_t, RpcHandle>        _commands_waiting_to_be_executed;
    std::mutex                          _commands_waiting_to_be_executed_mutex;
    std::map<std::string, RpcHandle>    _commands_listeners;
    std::mutex                          _commands_listeners_mutex;
    std::condition_variable             _command_complete_condition;
    std::mutex                          _command_complete_condition_mutex;
    int64_t                             _last_id = 0;
    std::unique_ptr<std::thread>        _response_thread;
    bool                                _ok = true;
    bool                                _stopping = false;

public:
    LanguageClient(std::string process_path, std::vector<std::string> process_args, 
                   variable::Record initialize_params, RpcHandle initialize_response_handle,
                   inout::log::Logger_interface & logger);
    ~LanguageClient();

    int64_t exec(const std::string &method, variable::Value params, RpcHandle handle=nullptr);

    void close();

    bool ok() { return _ok; }
    bool running() { return _ok && !_stopping && _language_server && _language_server->running(); }
    bool registerListener(const std::string &method, RpcHandle listener);

private:
    int64_t exec(const variable::json::Rpc & rpc, RpcHandle handle);
    void send(const variable::json::Rpc & rpc);
    void response_listener();
    bool waitResponse(int64_t id, int timeout_mills = 5000);
    bool verifyJson(const std::string & str);
};

}

#endif //_simodo_lsp_client_h
