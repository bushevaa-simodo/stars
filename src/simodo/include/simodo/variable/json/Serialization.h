/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_variable_json_JsonSerialization
#define simodo_variable_json_JsonSerialization

/*! \file JsonSerialization.h
    \brief Загрузка и сохранение данных в формате JSON
*/

#include "simodo/variable/Variable.h"

namespace simodo::variable::json
{
    class Rpc;

    std::string load(const std::string & file_name, Value & value);
    std::string load(std::istream & in, Value & value, const std::string & file_name = "");

    bool save(const Value & value, std::ostream & out, bool compress=true, bool skip_empty_parameters=false);
    bool save(const Value & value, const std::string & file_name, bool compress=true, bool skip_empty_parameters=false);

    bool save(const Rpc & rpc, std::ostream & out, bool compress=true, bool skip_empty_parameters=false);

    Value fromString(const std::string & json_string);
    Value fromString(const std::u16string & json_string);
    std::string  toString(const Value & value, bool compress=true, bool skip_empty_parameters=false);
}

#endif // simodo_variable_json_JsonSerialization
