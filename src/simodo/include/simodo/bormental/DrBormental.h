/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_bormental_DrBormental
#define simodo_bormental_DrBormental

/*! \file DrBormental.h
    \brief Обработка базовых исключений проекта SIMODO.
*/

#include <exception>
#include <string>

namespace simodo::bormental
{
    /*!
     * \brief Класс исключения
     *
     * Класс исключения предоставляет возможность работать с исключениями в библиотеках SIMODO.
     *
     * Неизменяемый (immutable) класс.
     */
    class DrBormental : public std::exception
    {
        std::string _where; ///< Наименования класса и метода (или пространства имён и функции), где было брошено исключение
        std::string _what;  ///< Описание исключения

    public:
        /*!
         * \brief           Конструктор исключения библиотеки miniDB
         * \param where     Задаёт наименования класса и метода (или пространства имён и функции), где было брошено исключение
         * \param what      Описание исключения
         */
        DrBormental(const std::string &where, const std::string &what)
            : std::exception()
            , _where(where)
            , _what(what)
        {}

        /*!
         * \brief       Возвращает строку с наименованиями класса и метода (или пространства имён и функции), где было брошено исключение
         * \return      Строка с наименованиями класса и метода (или пространства имён и функции), где было брошено исключение
         */
        const std::string & where() const noexcept { return _where; }

        /*!
         * \brief       Возвращает описание исключения
         * \return      Строка с описанием исключения
         */
        virtual const char* what() const noexcept override { return _what.c_str(); }
    };

}

#endif // simodo_bormental_DrBormental
