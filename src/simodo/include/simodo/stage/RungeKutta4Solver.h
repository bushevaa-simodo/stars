#ifndef simodo_stage_RungeKutta4Solver
#define simodo_stage_RungeKutta4Solver

/*! \file RungeKutta4Solver.h
    \brief Реализация солвера Рунге-Кутта 4 порядка точности
*/

#include "simodo/stage/OdeSolver.h"

#include <memory>
#include <array>
#include <algorithm>
#include <cmath>

namespace simodo::stage
{
    template <typename Number>
    class RungeKutta4Solver : public OdeSolver<Number>
    {
        template <typename From>
        static constexpr Number sc(From from)
        { return static_cast<Number>(from); }

        static constexpr Number TWO = sc(2);
        static constexpr Number THREE = sc(3);
        static constexpr Number SIX = sc(6);

    public:
        using Ode = typename OdeSolver<Number>::Ode;

        std::vector<Number> solve(
            Number previous_time
            , Number next_time
            , const std::vector<Number> &previous_y
            , Ode &ode
        ) override
        {
            const Number delta_x = next_time - previous_time;
            auto multiply_by_delta_x
                = [delta_x](const Number y) -> Number { return y * delta_x; };

            std::vector<Number> ode_result = ode(previous_time, previous_y);
            std::vector<Number> k1(previous_y.size());
            std::transform(
                ode_result.cbegin()
                , ode_result.cend()
                , k1.begin()
                , multiply_by_delta_x
            );

            std::vector<Number> y(previous_y.size());
            for (
                auto each_y = y.begin()
                , each_previous_y = previous_y.begin()
                    , each_k1 = k1.begin()
                ; each_y != y.end()
                ;
            )
            {
                (*each_y++) = (*each_previous_y++) + (*each_k1++) / TWO;
            }
            ode_result = ode(previous_time + delta_x / TWO, y);
            std::vector<Number> k2(previous_y.size());
            std::transform(
                ode_result.cbegin()
                , ode_result.cend()
                , k2.begin()
                , multiply_by_delta_x
            );

            for (
                auto each_y = y.begin()
                    , each_previous_y = previous_y.begin()
                    , each_k2 = k2.begin()
                ; each_y != y.end()
                ;
            )
            {
                (*each_y++) = (*each_previous_y++) + (*each_k2++) / TWO;
            }
            ode_result = ode(previous_time + delta_x / TWO, y);
            std::vector<Number> k3(previous_y.size());
            std::transform(
                ode_result.cbegin()
                , ode_result.cend()
                , k3.begin()
                , multiply_by_delta_x
            );

            for (
                auto each_y = y.begin()
                    , each_previous_y = previous_y.begin()
                    , each_k3 = k3.begin()
                ; each_y != y.end()
                ;
            )
            {
                (*each_y++) = (*each_previous_y++) + (*each_k3++);
            }
            ode_result = ode(next_time, y);
            std::vector<Number> k4(previous_y.size());
            std::transform(
                ode_result.cbegin()
                , ode_result.cend()
                , k4.begin()
                , multiply_by_delta_x
            );

            std::vector<Number> next_y(previous_y.size());
            for (
                auto each_next_y = next_y.begin()
                    , each_previous_y = previous_y.begin()
                    , each_k1 = k1.begin()
                    , each_k2 = k2.begin()
                    , each_k3 = k3.begin()
                    , each_k4 = k4.begin()
                ; each_next_y != next_y.end()
                ;
            )
            {
                (*each_next_y++)
                    = (*each_previous_y++)
                    + (*each_k1++) / SIX
                    + (*each_k2++) / THREE
                    + (*each_k3++) / THREE
                    + (*each_k4++) / SIX;
            }

            return next_y;
        }
    };
}

#endif //simodo_stage_RungeKutta4Solver
