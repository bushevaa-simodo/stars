#ifndef simodo_stage_DiscreteTime
#define simodo_stage_DiscreteTime

#include <cstdint>

namespace simodo::stage
{
    template <typename Time, typename N = int64_t>
    class DiscreteTime
    {
    public:
        using NType = N;

        DiscreteTime()
            : _t0(0)
            , _n(0)
            , _dt(0)
        {}

        DiscreteTime(Time t, Time dt)
            : _t0(t)
            , _n(0)
            , _dt(dt)
        {}

        DiscreteTime(const DiscreteTime<Time, N> &other)
            : _t0(other._t0)
            , _n(other._n)
            , _dt(other._dt)
        {}

        DiscreteTime &operator=(const DiscreteTime<Time, N> &other)
        {
            _t0 = other._t0;
            _n = other._n;
            _dt = other._dt;

            return *this;
        }

        Time getDelta() const
        {
            return _dt;
        }
        void setDelta(Time dt)
        {
            (*this) = Time(*this);
            _dt = dt;
        }

        N getN() const
        {
            return _n;
        }

        DiscreteTime & operator=(Time t)
        {
            _t0 = t;
            _n = 0;

            return *this;
        }

        DiscreteTime operator+(N n) const
        {
            DiscreteTime o(*this);
            o._n += n;
            return o;
        }
        DiscreteTime operator-(N n) const
        {
            DiscreteTime o(*this);
            o._n -= n;
            return o;
        }

        DiscreteTime & operator++()
        {
            ++_n;
            return *this;
        }
        DiscreteTime & operator--()
        {
            --_n;
            return *this;
        }

        operator Time() const
        {
            return _t0 + _n * _dt;
        }

    private:
        Time _t0;
        N _n;
        Time _dt;
    };
}

#endif //simodo_stage_DiscreteTime
