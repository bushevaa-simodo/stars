#ifndef simodo_stage_Procedure
#define simodo_stage_Procedure

#include <functional>

namespace simodo::stage
{
    using Procedure = std::function< void() >;
}

#endif //simodo_stage_Procedure
