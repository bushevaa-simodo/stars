#ifndef simodo_stage_Composition
#define simodo_stage_Composition

/*! \file Composition.h
    \brief Реализация композиции
*/

#include "simodo/stage/Actor.h"
#include "simodo/stage/Types.h"

#include <functional>
#include <vector>
#include <memory>
#include <algorithm>

namespace simodo::stage
{
    template <typename Number>
    class Composition : public Actor<Number>
    {
    public:
        using ActorType = Actor<Number>;

    private:
        std::vector< std::shared_ptr<ActorType> > _actors;
        EquFunction<Number> _equ;

    public:
        Composition(std::vector< std::shared_ptr<ActorType> > actors,
                    EquFunction<Number> equ)
                : _actors(actors),
                _equ(equ)
        {}

        void prolog() override
        {
            std::for_each(_actors.begin(), _actors.end(),
                        [](std::shared_ptr<ActorType> &actor) -> void
                        { actor->prolog(); });
        }

        void doStep(Number previous_t, Number next_t) override
        {
            std::for_each(_actors.begin(), _actors.end(),
                        [previous_t, next_t](std::shared_ptr<ActorType> &actor) -> void
                        { actor->doStep(previous_t, next_t); });
            _equ();
        }

        void epilog() override
        {
            std::for_each(_actors.begin(), _actors.end(),
                        [](std::shared_ptr<ActorType> &actor) -> void
                        { actor->epilog(); });
        }
    };
}

#endif //simodo_stage_Composition
