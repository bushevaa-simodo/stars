#ifndef simodo_stage_ThreadPool
#define simodo_stage_ThreadPool

#include "simodo/tp/Task_interface.h"

#include <memory>
#include <functional>

namespace simodo::stage
{
    template <typename Task>
    struct ThreadPool
    {
    public:
        using TaskReference = std::shared_ptr<Task>;

    public:
        virtual ~ThreadPool() = default;

    public:
        virtual void submit(TaskReference task) = 0;
        virtual void wait() = 0;
    };
}

#endif //simodo_stage_ThreadPool
