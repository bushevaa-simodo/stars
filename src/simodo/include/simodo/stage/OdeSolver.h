#ifndef simodo_stage_OdeSolver
#define simodo_stage_OdeSolver

#include <vector>
#include <functional>

namespace simodo::stage
{
    template <typename Number>
    class OdeSolver
    {
    public:
        using Ode = std::function<
            std::vector<Number> (Number, const std::vector<Number> &)
        >;

        virtual ~OdeSolver() = default;

        virtual std::vector<Number> solve(
            Number previous_time
            , Number next_time
            , const std::vector<Number> &previous_y
            , Ode &ode
        ) = 0;
    };
}

#endif //simodo_stage_OdeSolver
