#ifndef simodo_stage_ThreadedActorGroup
#define simodo_stage_ThreadedActorGroup

#include "simodo/stage/TreeActorGroup.h"
#include "simodo/stage/ThreadPool.h"
#include "simodo/stage/Procedure.h"

#include <unordered_map>
#include <functional>
#include <cassert>

namespace simodo::stage
{
    template <typename ActorId, typename Iter, typename Time>
    class ThreadedActorGroup : public TreeActorGroup<ActorId, Iter, Time>
    {
    public:
        using TreeActorGroupType = TreeActorGroup<ActorId, Iter, Time>;

        using Actor = typename TreeActorGroupType::Actor;
        using ActorReference = typename TreeActorGroupType::ActorReference;
        using Reference = typename TreeActorGroupType::Reference;
        using Children = typename TreeActorGroupType::Children;

        using ThreadPoolReference = std::shared_ptr<ThreadPool<Procedure>>;

    public:
        ThreadedActorGroup(
            ThreadPoolReference thread_pool
            , ActorReference main_actor = {}
            , Children children = {}
        )   : TreeActorGroupType(main_actor, children)
            , _thread_pool(thread_pool)
        {
            assert(thread_pool);
        }

    public:
        Reference add(ActorId id, std::shared_ptr<Actor> actor) override
        {
            if (!actor) return {};

            if (
                TreeActorGroupType::children.find(id)
                != TreeActorGroupType::children.end()
            )
            {
                return {};
            }

            return TreeActorGroupType::children[id]
                = std::make_shared<ThreadedActorGroup>(
                    _thread_pool
                    , actor
                    , std::unordered_map<ActorId, Reference>()
                );
        }

    protected:
        void doGroupStep(Iter iter, Time previous_time, Time next_time) override
        {
            TreeActorGroupType::doGroupStep(iter, previous_time, next_time);
            if (!TreeActorGroupType::main_actor)
            {
                _thread_pool->wait();
            }
        }

        void doChildStep(
            Reference child
            , Iter iter
            , Time previous_time
            , Time next_time
        ) override
        {
            _thread_pool->submit(std::make_shared<Procedure>(
                [this, child, iter, previous_time, next_time]
                {
                    TreeActorGroupType::doChildStep(
                        child
                        , iter
                        , previous_time
                        , next_time
                    );
                }
            ));
        }
    
    private:
        ThreadPoolReference _thread_pool;
    };
}

#endif //simodo_stage_ThreadedActorGroup
