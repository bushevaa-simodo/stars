#ifndef simodo_stage_ThreadLocalCopyFactory
#define simodo_stage_ThreadLocalCopyFactory

#include "CopyFactory.h"

namespace simodo::stage
{
    template <typename Product>
    class ThreadLocalCopyFactory : public CopyFactory<Product>
    {
    public:
        using CopyFactoryType = CopyFactory<Product>;
        using ProductType = typename CopyFactoryType::Product;
        using ProductReference = typename CopyFactoryType::ProductReference;

    public:
        ThreadLocalCopyFactory(ProductType &product)
            : CopyFactoryType(product)
            , _update_flag(false)
        {}

    public:
        void update()
        {
            _update_flag = !_update_flag;
        }

        ProductReference produce() override
        {
            static thread_local bool local_update_flag;
            static thread_local ProductReference local_product;

            if (local_update_flag != _update_flag || !local_product)
            {
                local_update_flag = _update_flag;
                local_product = CopyFactoryType::produce();
            }

            return local_product;
        }
    
    private:
        bool _update_flag;
    };
}

#endif //simodo_stage_ThreadLocalCopyFactory
