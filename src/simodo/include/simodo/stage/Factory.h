#ifndef simodo_stage_Factory
#define simodo_stage_Factory

#include <memory>
#include <functional>

namespace simodo::stage
{
    template <typename Product>
    struct Factory
    {
    public:
        using ProductType = Product;
        using ProductReference = std::shared_ptr<ProductType>;

    public:
        virtual ~Factory() = default;

    public:
        virtual ProductReference produce() = 0;
    };
}

#endif //simodo_stage_Factory
