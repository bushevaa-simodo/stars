#ifndef simodo_stage_Actor
#define simodo_stage_Actor

/*! \file Actor.h
    \brief Интерфейс актора
*/

#include <optional>
#include <variant>

namespace simodo::stage
{
    template <typename Iter, typename Time>
    class Actor
    {
        static_assert(std::is_integral_v<Iter>);
        static_assert(std::is_floating_point_v<Time>);
    public:
        using Step = std::variant<Iter, Time>;

    public:
        virtual ~Actor() = default;

    public:
        virtual void doStep(Iter iter, Time previous_time, Time next_time) = 0;
        virtual void setStep(std::optional<Step> step) = 0;
    };
}

#endif //simodo_stage_Actor
