#ifndef simodo_stage_Predicate
#define simodo_stage_Predicate

#include <functional>

namespace simodo::stage
{
    using Predicate = std::function< bool() >;
}

#endif //simodo_stage_Predicate
