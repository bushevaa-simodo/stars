#ifndef simodo_stage_RungeKuttaFehlbergSolver
#define simodo_stage_RungeKuttaFehlbergSolver

/*! \file RungeKuttaFehlbergSolver.h
    \brief Реализация адаптивного солвера Рунге-Кутта-Фехльдберга 5 порядка точности
*/

#include "simodo/stage/OdeSolver.h"

#include <memory>
#include <array>
#include <algorithm>
#include <cmath>

namespace simodo::stage
{
    template <typename Number>
    class RungeKuttaFehlbergSolver : public OdeSolver<Number>
    {
        template <typename From>
        static constexpr Number sc(From from)
        { return static_cast<Number>(from); }

        static constexpr std::array<Number, 6> A = {
                sc(0),
                sc(2.0) / sc(9.0),
                sc(1.0) / sc(3.0),
                sc(3.0) / sc(4.0),
                sc(1),
                sc(5.0) / sc(6.0)
        };

        static constexpr std::array<std::array<Number, 5>, 6> B = {{
                { sc(0),                sc(0),                  sc(0),                  sc(0),                  sc(0) },
                { sc(2.0) / sc(9.0),    sc(0),                  sc(0),                  sc(0),                  sc(0) },
                { sc(1.0) / sc(12.0),   sc(1.0) / sc(4.0),      sc(0),                  sc(0),                  sc(0) },
                { sc(69.0) / sc(128.0), sc(-243.0) / sc(128.0), sc(135.0) / sc(64.0),   sc(0),                  sc(0) },
                { sc(-17.0) / sc(12.0), sc(27.0) / sc(4.0),     sc(-27.0) / sc(5.0),    sc(16.0) / sc(15.0),    sc(0) },
                { sc(65.0) / sc(432.0), sc(-5.0) / sc(16.0),    sc(13.0) / sc(16.0),    sc(4.0) / sc(27.0),     sc(5.0) / sc(144.0) }
        }};

        static constexpr std::array<Number, 6> CH = {
                sc(47.0) / sc(450.0),
                sc(0),
                sc(12.0) / sc(25.0),
                sc(32.0) / sc(225.0),
                sc(1.0) / sc(30.0),
                sc(6.0) / sc(25.0),
        };

        static constexpr std::array<Number, 6> CT = {
                sc(-1.0) / sc(150.0),
                sc(0),
                sc(3.0) / sc(100.0),
                sc(-16.0) / sc(75.0),
                sc(-1.0) / sc(20.0),
                sc(6.0) / sc(25.0)
        };

        Number _epsilon;
        int _recalculate_limit;

    public:
        using Ode = typename OdeSolver<Number>::Ode;

        static constexpr Number DEFAULT_EPSILON = sc(0.000001);
        static constexpr int DEFAULT_RECALCULATE_LIMIT = 0;

        explicit RungeKuttaFehlbergSolver(Number epsilon = DEFAULT_EPSILON,
                                        int recalculate_limit = DEFAULT_RECALCULATE_LIMIT)
            : _epsilon(epsilon),
            _recalculate_limit(recalculate_limit)
        {}

        std::vector<Number> solve(Number previous_time,
                                Number next_time,
                                const std::vector<Number> &previous_y,
                                Ode &ode) override
        {
            Number step = next_time - previous_time;
            Number _previous_t = previous_time;
            std::vector<Number> _previous_y = previous_y;

            do {
                std::array<std::vector<Number>, 6> k;
                Number old_step = step;
                for (
                    int i = 0
                    ; (
                        (i < _recalculate_limit)
                        || (_recalculate_limit <= 0)
                    )
                    ; ++i
                )
                {
                    k = calculateK(_previous_t, old_step, _previous_y, ode);
                    Number calculation_error = calculateError(k);

                    old_step = step;
                    step = calculateNewStep(old_step, calculation_error);

                    if (calculation_error <= _epsilon)
                    {
                        break;
                    }
                }

                _previous_t += old_step;
                _previous_y = calculateNextY(_previous_y, k);

                if (step > next_time - previous_time)
                {
                    step = next_time - previous_time;
                }
            } while (_previous_t < next_time);

            return _previous_y;
        }



        std::vector<Number> calculateNextY(const std::vector<Number> &previous_y, const std::array<std::vector<Number>, 6> &k)
        {
            std::vector<Number> next_y = previous_y;
            for (size_t i = 0; i < next_y.size(); ++i)
            {
                next_y[i] += CH[0] * k[0][i] + CH[1] * k[1][i] + CH[2] * k[2][i]
                                + CH[3] * k[3][i] + CH[4] * k[4][i] + CH[5] * k[5][i];
            }
            return next_y;
        }

        Number calculateError(const std::array<std::vector<Number>, 6> &k)
        {
            std::vector<Number> te(k[0].size(), sc(0));
            for (size_t j = 0; j < te.size(); ++j)
            {
                te[j] = std::abs(CT[0] * k[0][j] + CT[1] * k[1][j] + CT[2] * k[2][j]
                                + CT[3] * k[3][j] + CT[4] * k[4][j] + CT[5] * k[5][j]);
            }
            return *std::max_element(te.cbegin(), te.cend());
        }

        std::array<std::vector<Number>, 6> calculateK(const Number previous_time,
                                                    const Number step,
                                                    const std::vector<Number> &previous_y,
                                                    Ode &ode)
        {
            std::array<std::vector<Number>, 6> k;
            for (int K = 0; K < 6; ++K)
            {
                Number t = previous_time + A[K] * step;
                std::vector<Number> y = previous_y;
                for (int L = 0; L < K; ++L)
                {
                    std::transform(y.cbegin(), y.cend(),
                                k[L].cbegin(),
                                y.begin(),
                                [K, L](Number each_y, Number each_k_L) -> Number
                                { return each_y + B[K][L] * each_k_L; });
                }

                std::vector<Number> ode_result = ode(t, y);
                k[K].resize(ode_result.size());
                std::transform(ode_result.cbegin(),
                            ode_result.cend(),
                            k[K].begin(),
                            [step](Number each_ode_result) -> Number
                            { return step * each_ode_result; });
            }
            return k;
        }

        Number calculateNewStep(Number old_step, Number calculation_error)
        {
            return sc(0.9) * old_step * pow(_epsilon / calculation_error, sc(0.2));
        }
    };
}

#endif //simodo_stage_RungeKuttaFehlbergSolver
