#ifndef simodo_stage_ProcedureSolver
#define simodo_stage_ProcedureSolver

#include "simodo/stage/Procedure.h"

namespace simodo::stage
{
    template <typename Number>
    class ProcedureSolver : public Solver<Number>
    {
    public:
        ProcedureSolver(Procedure procedure)
            : _procedure(procedure)
        {}

        void solve(Number /*previous_time*/, Number /*next_time*/) override
        {
            _procedure();
        }

    private:
        Procedure _procedure;
    };
}

#endif //simodo_stage_ProcedureSolver
