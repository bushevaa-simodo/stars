#ifndef simodo_stage_OdeModelSolver
#define simodo_stage_OdeModelSolver

#include "simodo/stage/Solver.h"
#include "simodo/stage/OdeSolver.h"

namespace simodo::stage
{
    template <typename Number>
    class OdeModelSolver : public Solver<Number>
    {
    public:
        using StateProducer = std::function< std::vector<Number>() >;
        using StateConsumer = std::function< void(const std::vector<Number> &) >;
        using OdeFunction = std::function< std::vector<Number>(Number, const std::vector<Number> &) >;

        OdeModelSolver(
            StateProducer get_state
            , StateConsumer set_state
            , OdeFunction ode
            , std::shared_ptr< OdeSolver<Number> > solver
        )   : _get_state(get_state)
            , _set_state(set_state)
            , _ode(ode)
            , _solver(solver)
        {}

        void solve(Number previous_time, Number next_time) override
        {
            _set_state(_solver->solve(previous_time, next_time, _get_state(), _ode));
        }
    
    private:
        StateProducer _get_state;
        StateConsumer _set_state;
        OdeFunction _ode;
        std::shared_ptr< OdeSolver<Number> > _solver;
    };
}

#endif //simodo_stage_Solver
