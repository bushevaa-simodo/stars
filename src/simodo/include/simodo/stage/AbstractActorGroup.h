#ifndef simodo_stage_AbstractActorGroup
#define simodo_stage_AbstractActorGroup

#include "simodo/stage/ActorGroup.h"

#include <unordered_map>

namespace simodo::stage
{
    template <typename ActorId, typename Iter, typename Time>
    class AbstractActorGroup : public ActorGroup<ActorId, Iter, Time>
    {
    public:
        using ActorGroupType = ActorGroup<ActorId, Iter, Time>;

        using ActorReference = typename ActorGroupType::ActorReference;
        using Step = typename ActorGroupType::ActorType::Step;

    public:
        AbstractActorGroup<ActorId, Iter, Time>(
            ActorReference main_actor = {}
        )
            : main_actor(main_actor)
        {}

    public:
        ActorReference getMainActor() override
        {
            return main_actor;
        }

        void doStep(Iter iter, Time previous_time, Time next_time) override
        {
            doMainActorStep(iter, previous_time, next_time);
            doGroupStep(iter, previous_time, next_time);
        }

        void setStep(std::optional<Step> step) override
        {
            if (!main_actor) return;

            main_actor->setStep(step);
        }

    protected:
        ActorReference main_actor;

    protected:
        virtual void doMainActorStep(Iter iter, Time previous_time, Time next_time)
        {
            if (main_actor)
            {
                main_actor->doStep(iter, previous_time, next_time);
            }
        }

        virtual void doGroupStep(Iter iter, Time previous_time, Time next_time) = 0;
    };
}

#endif //simodo_stage_AbstractActorGroup
