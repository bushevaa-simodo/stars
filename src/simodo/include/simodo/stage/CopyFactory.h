#ifndef simodo_stage_CopyFactory
#define simodo_stage_CopyFactory

#include "Factory.h"

namespace simodo::stage
{
    template <typename Interpreter>
    class CopyFactory : public Factory<Interpreter>
    {
    public:
        using FactoryType = Factory<Interpreter>;
        using Product = typename FactoryType::ProductType;
        using ProductReference = typename FactoryType::ProductReference;

    public:
        CopyFactory(Product &sample)
            : _sample(sample)
        {}

    public:
        ProductReference produce() override
        {
            return std::make_shared<Product>(_sample);
        }

    private:
        Product &_sample;
    };
}

#endif //simodo_stage_CopyFactory
