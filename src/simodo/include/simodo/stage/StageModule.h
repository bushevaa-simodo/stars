#ifndef simodo_stage_StageModule
#define simodo_stage_StageModule

/*! \file StageModule.h
    \brief Модуль сцены моделирования
*/

#include "simodo/interpret/host/base/Variable.h"
#include "simodo/stage/Stage.h"

#include "simodo/interpret/host/base/ModuleHost_interface.h"
#include "simodo/interpret/Interpret_interface.h"
// #include "simodo/sbl/OpParser_abstact.h"

#include <vector>
#include <string>

namespace simodo::stage
{
    using VariableRef = simodo::interpret::host::base::VariableRef;
    using Variable = simodo::interpret::host::base::Variable;
    using Interpret_interface = simodo::interpret::Interpret_interface;
    using VariableSet_t = simodo::interpret::host::base::VariableSet_t;

    using StageActorId = std::u16string;
    class StageModuleHost : public simodo::interpret::host::base::ModuleHost_interface, public Stage<double, StageActorId> {
        static const std::u16string MODEL_TYPE;
        static const std::u16string COMPOSITION_TYPE;
        static const std::u16string ACTOR;

        const simodo::interpret::Interpret_interface &_interpreter;

        std::vector<VariableRef> _actors;

        std::shared_ptr<ActorType> parseModelNamedValue(const Variable &model_named_value);
        std::shared_ptr<ActorType> parseCompositionNamedValue(const Variable &composition_named_value);
        std::shared_ptr<ActorType> parseActorNamedValue(const Variable &actor_named_value);
    public:
        static std::shared_ptr<ModuleHost_interface> create(const Interpret_interface &interpret);

        explicit StageModuleHost(const Interpret_interface &interpreter);

        void setCallback(const Variable &callback);
        void addActor(StageActorId actor_id, const Variable &actor_named_value);
        void clearActors();

        VariableSet_t getNamespace() override;
    };
}

#endif //simodo_stage_StageModule
