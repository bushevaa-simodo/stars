#ifndef simodo_stage_TreeActorGroup
#define simodo_stage_TreeActorGroup

#include "simodo/stage/AbstractActorGroup.h"

#include <unordered_map>

namespace simodo::stage
{
    template <typename ActorId, typename Iter, typename Time>
    class TreeActorGroup : public AbstractActorGroup<ActorId, Iter, Time>
    {
    public:
        using AbstractActorGroupType = AbstractActorGroup<ActorId, Iter, Time>;
        using ActorReference = typename AbstractActorGroupType::ActorReference;
        using Reference = typename AbstractActorGroupType::Reference;

        using Children = std::unordered_map<ActorId, Reference>;

    public:
        TreeActorGroup(
            ActorReference main_actor = {}
            , Children children = {}
        )   : AbstractActorGroupType(main_actor)
            , children(children)
        {}

    public:
        void doGroupStep(Iter iter, Time previous_time, Time next_time) override
        {
            for (auto & [_, actor] : children)
            {
                doChildStep(actor, iter, previous_time, next_time);
            }
        }

        Reference find(ActorId id) override
        {
            auto it = children.find(id);
            if (it != children.end())
            {
                return it->second;
            }

            for (auto & [_, group] : children)
            {
                if (auto found_group = group->find(id))
                {
                    return found_group;
                }
            }

            return {};
        }

        Reference add(ActorId id, ActorReference actor) override
        {
            if (!actor) return {};

            if (children.find(id) != children.end())
            {
                return {};
            }
            return children[id] = std::make_shared<TreeActorGroup<ActorId, Iter, Time>>(
                actor, Children()
            );
        }

        Reference remove(ActorId id) override
        {
            auto it = children.find(id);
            if (it != children.end())
            {
                auto erased_group = it->second;
                children.erase(it);
                return erased_group;
            }

            for (auto & [_, group] : children)
            {
                if (auto erased_group = group->remove(id))
                {
                    return erased_group;
                }
            }

            return {};
        }

        void clear() override
        {
            children.clear();
        }

    protected:
        Children children;
    
    protected:
        virtual void doChildStep(Reference child, Iter iter, Time previous_time, Time next_time)
        {
            child->doStep(iter, previous_time, next_time);
        }
    };
}

#endif //simodo_stage_TreeActorGroup
