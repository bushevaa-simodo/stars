#ifndef simodo_stage_Model
#define simodo_stage_Model

/*! \file Model.h
    \brief Реализация модели
*/

#include "simodo/stage/Actor.h"
#include "simodo/stage/Solver.h"

#include <functional>
#include <vector>
#include <memory>
#include <string>
#include <stdexcept>
#include <cassert>

namespace simodo::stage
{
    template <typename Iter, typename Time, typename Number = Time>
    class Model : public Actor<Iter, Time>
    {
    public:
        using ActorType = Actor<Iter, Time>;
        using Step = typename ActorType::Step;
        static_assert(std::variant_size_v<Step> == 2);
        using SolverReference = std::shared_ptr< Solver<Number> >;

    public:
        Model(
            std::vector<SolverReference> solvers = {}
            , std::optional<Step> step = {}
        )
            : _solvers(std::move(solvers))
        {
            _setStep(step);
        }

        /// @note previous_time_0 <= next_time_0 <= previous_time_1 <= next_time_1 
        void doStep(Iter iter, Time previous_time, Time next_time) override
        {
            assert(previous_time <= next_time);

            // If step is not set
            // then do from `previous_time` to `next_time`
            if (!_state)
            {
                _doStep(previous_time, next_time);
                return;
            }

            // If time step is set
            if (_holds_t())
            {
                // If `last_solution_time` is not recorded
                // then do from `previous_time`
                if (!_t_last_solution_time())
                {
                    _t_last_solution_time().emplace(previous_time, _t_step());
                }

                // Do steps until the `next_time`
                while (Time(*_t_last_solution_time() + typename _DiscreteTimeType::NType(1)) <= next_time)
                {
                    auto previous_solution_time = Time(*_t_last_solution_time());
                    auto next_solution_time = Time(++(*_t_last_solution_time()));
                    _doStep(previous_solution_time, next_solution_time);

                    // `step` might be changed
                    if (_state && _holds_t()) continue;

                    // If `step` was changed
                    // do until end of iteration
                    if (next_solution_time < next_time)
                    {
                        _doStep(next_solution_time, next_time);
                    }
                    // Record last solution time
                    if (_state && _holds_i())
                    {
                        _i_last_solution_time().emplace(next_time);
                    }
                    break;
                }
                return;
            }

            // If iteration step is set
            // Do if iteration counter is multiple iteration step
            if (_holds_i() && iter % _i_step() == 0)
            {
                // If last solution time was not recorded
                if (!_i_last_solution_time())
                {
                    _doStep(previous_time, next_time);
                }
                else
                {
                    _doStep(*_i_last_solution_time(), next_time);
                }
                /// @note Step might be changed
                // Record last solution time
                if (_state && _holds_t())
                {
                    _t_last_solution_time().emplace(next_time, _t_step());
                }
                else if (_state && _holds_i())
                {
                    _i_last_solution_time().emplace(next_time);
                }
            }
        }

        void setStep(std::optional<Step> step) override
        {
            _setStep(step);
        }

    private:
        using _DiscreteTimeType = DiscreteTime<Time>;

        struct _DiscreteTimeState
        {
            Time step;
            std::optional<_DiscreteTimeType> last_solution_time;
        };

        struct _IterationState
        {
            Iter step;
            std::optional<Time> last_solution_time;
        };

        using _State = std::variant<_DiscreteTimeState, _IterationState>;
    
    private:
        std::vector<SolverReference> _solvers;
        std::optional<_State> _state;

        template <typename T>
        inline bool _holds() { return std::holds_alternative<T>(*_state); }

        inline bool _holds_t() { return _holds<_DiscreteTimeState>(); }
        inline bool _holds_i() { return _holds<_IterationState>(); }

        template <typename T>
        inline T& _get() { return std::get<T>(*_state); }

        inline auto &_get_t() { return _get<_DiscreteTimeState>(); }
        inline auto &_get_i() { return _get<_IterationState>(); }

        inline auto _t_step() { return _get_t().step; }
        inline auto &_t_last_solution_time() { return _get_t().last_solution_time; }

        inline auto _i_step() { return _get_i().step; }
        inline auto &_i_last_solution_time() { return _get_i().last_solution_time; }

        void _doStep(Time previous_time, Time next_time)
        {
            for (auto & solver : _solvers)
            {
                solver->solve(previous_time, next_time);
            }
        }

        void _setStep(std::optional<Step> step)
        {
            static_assert(std::variant_size_v<Step> == 2);

            if (!step)
            {
                _state.reset();
                return;
            }

            const auto &stp = *step;

            // Extract old `last_solution_time`
            std::optional<Time> last_solution_time;
            if (_state)
            {
                if (_holds_t() && _t_last_solution_time())
                {
                    last_solution_time.emplace(Time(*_t_last_solution_time()));
                }
                if (_holds_i())
                {
                    last_solution_time = _i_last_solution_time();
                }
            }

            // Set time step
            if (std::holds_alternative<Time>(stp))
            {
                auto time_step = std::get<Time>(stp);
                if (time_step <= 0)
                {
                    throw std::logic_error(
                        "Time step is not greater than zero: "
                            + std::to_string(time_step)
                    );
                }

                std::optional<_DiscreteTimeType> last_solution_d_time;
                // If had `last_solution_time`
                if (last_solution_time)
                {
                    last_solution_d_time = _DiscreteTimeType {
                        *last_solution_time, time_step
                    };
                }
                _state = _DiscreteTimeState { time_step, last_solution_d_time };
                return;
            }

            // Set iteration step
            if (std::holds_alternative<Iter>(stp))
            {
                auto it_step = std::get<Iter>(stp);
                if (it_step <= 0)
                {
                    throw std::logic_error(
                        "Iteration step is not greater than zero: "
                            + std::to_string(it_step)
                    );
                }

                _state = _IterationState { it_step, last_solution_time };
                return;
            }
        }
    };
}

#endif //simodo_stage_Model
