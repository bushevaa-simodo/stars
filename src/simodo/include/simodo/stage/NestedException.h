#ifndef simodo_stage_NestedException
#define simodo_stage_NestedException

#include <exception>
#include <memory>
#include <string>

namespace simodo::stage
{
    class NestedException : public std::exception
    {
    public:
        using ExceptionReference
            = std::shared_ptr<std::exception>;
        using NestedExceptionReference
            = std::shared_ptr<NestedException>;

    public:
        NestedException(
            const std::string &what
            , const std::string &nested = {}
        );

        NestedException(const NestedException &other);

    public:
        const char* what() const noexcept override;
        ExceptionReference except() const;
        NestedExceptionReference nested() const;

    private:
        ExceptionReference _except;
        NestedExceptionReference _nested;
    };
}

#endif //simodo_stage_NestedException
