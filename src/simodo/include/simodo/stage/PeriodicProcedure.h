#ifndef simodo_stage_PeriodicProcedure
#define simodo_stage_PeriodicProcedure

#include "simodo/stage/Procedure.h"

#include <cstdint>

namespace simodo::stage
{
    template <typename Period = int64_t>
    struct PeriodicProcedure
    {
        using PeriodType = Period;

        Procedure procedure;
        Period period;

        void operator()()
        {
            procedure();
        }
    };
}

#endif //simodo_stage_PeriodicProcedure
