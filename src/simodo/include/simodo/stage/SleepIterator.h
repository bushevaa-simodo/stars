#ifndef simodo_stage_SleepIterator
#define simodo_stage_SleepIterator

#include <chrono>
#include <thread>

namespace simodo::stage
{
    template <typename Duration>
    class SleepIterator
    {
    public:
        SleepIterator()
            : _sleep_debt(0)
        {}

        void begin()
        {
            _iteration_begin_time = std::chrono::steady_clock::now();
        }

        void iterate(Duration step)
        {
            const auto iteration_end_time = std::chrono::steady_clock::now();

            const auto elapsed = iteration_end_time - _iteration_begin_time;
            _sleep_debt += std::chrono::duration_cast<Duration>(step - elapsed);

            if (0 < _sleep_debt.count())
            {
                std::this_thread::sleep_for(_sleep_debt);
                const auto after_sleep_time = std::chrono::steady_clock::now();
                _sleep_debt -= std::chrono::duration_cast<Duration>(after_sleep_time - iteration_end_time);
            }
        }

    private:
        std::chrono::steady_clock::time_point _iteration_begin_time;
        Duration _sleep_debt;
    };

    template <typename Duration>
    class SleepGuard
    {
    public:
        using SleepIteratorType = SleepIterator<Duration>;

        SleepGuard(SleepIteratorType & iterator, Duration step, bool enabled)
            : _iterator(iterator)
            , _step(step)
            , _enabled(enabled)
        {
            if (enabled) iterator.begin();
        }

        ~SleepGuard()
        {
            if (_enabled) _iterator.iterate(_step);
        }

    private:
        SleepIteratorType & _iterator;
        Duration _step;
        bool _enabled;
    };
}

#endif //simodo_stage_SleepIterator
