#ifndef simodo_stage_Solver
#define simodo_stage_Solver

/*! \file Solver.h
    \brief Интерфейс солвера
*/

namespace simodo::stage
{
    template <typename Number>
    class Solver
    {
    public:
        virtual ~Solver() = default;

        virtual void solve(Number previous_time, Number next_time) = 0;
    };
}

#endif //simodo_stage_Solver
