#ifndef simodo_stage_QueuedThreadPool
#define simodo_stage_QueuedThreadPool

#include "simodo/dsl/Exception.h"
#include "simodo/stage/NestedException.h"
#include "simodo/stage/ThreadPool.h"
#include "simodo/stage/Procedure.h"

#include <optional>
#include <thread>
#include <algorithm>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <exception>
#include <stdexcept>
#include <string>
#include <queue>

namespace simodo::stage
{
    template <typename Task>
    struct QueuedThreadPool : public ThreadPool<Task>
    {
    public:
        using ThreadPoolType = ThreadPool<Task>;
        using TaskReference = typename ThreadPoolType::TaskReference;

        struct Settings
        {
            unsigned int n_threads;
            bool is_hint;
        };

    public:
        QueuedThreadPool(
            std::optional<Settings> settings = {}
            , Procedure wait_callback = [] {})
            : _n_active_threads(0)
            , _wait_callback(wait_callback)
        {
            if (settings)
            {
                assert(1U <= settings->n_threads);
            }

            unsigned int n_hardware_threads = std::thread::hardware_concurrency();
            if (!settings || settings->n_threads < 1U)
            {
                _n_threads = n_hardware_threads == 0 ? 1 : n_hardware_threads;
                return;
            }

            _n_threads = (
                !settings->is_hint || n_hardware_threads == 0
                ? settings->n_threads
                : n_hardware_threads
            );
        }

        ~QueuedThreadPool() override
        {
            _stop();
        }

    public:
        void submit(TaskReference task) override
        {
            std::lock_guard lock(_mutex);
            if (_threads.size() == 0)
            {
                _start();
            }

            _tasks.push(task);
            _new_task_condition.notify_one();
        }

        void wait() override
        {
            std::unique_lock lock(_mutex);
            _waiting_condition.wait(
                lock
                , [this]() -> bool
                { return _tasks.empty() && _n_active_threads <= 0; }
            );

            _wait_callback();

            if (_exceptions.empty()) return;

            std::string exception_message;
            for (const auto &e : _exceptions)
            {
                exception_message += _unwindException(e);
            }
            _exceptions.clear();

            throw std::logic_error(exception_message);
        }

    private:
        unsigned int _n_threads;
        std::vector<std::thread> _threads;
        int _n_active_threads;
        Procedure _wait_callback;

        std::atomic_bool _is_running;

        std::queue<TaskReference> _tasks;

        std::mutex _mutex;

        std::condition_variable  _new_task_condition;
        std::condition_variable  _waiting_condition;

        std::vector<std::shared_ptr<NestedException>> _exceptions;

    private:
        void _start()
        try
        {
            _is_running = true;

            _threads.reserve(_n_threads);

            for(unsigned int i = 0; i < _n_threads; ++i)
            {
                _threads.emplace_back(&QueuedThreadPool::_work, this);
            }
            
            _new_task_condition.notify_all();
        }
        catch(...) {
            _stop();
            throw;
        }

        void _stop()
        {
            _is_running = false;
            _new_task_condition.notify_all();

            for (auto & thread : _threads)
            {
                if (thread.joinable())
                {
                    try {
                        thread.join();
                    }
                    catch (...)
                    {}
                }
            }

            _threads.clear();
        }

        void _work()
        {
            while(_is_running) {
                auto task = _getTask();
                if (!task) continue;

                _doTask(task);

                std::lock_guard lock(_mutex);
                --_n_active_threads;
                if (_n_active_threads <= 0)
                {
                    _waiting_condition.notify_all();
                }
            }
        }

        TaskReference _getTask()
        {
            TaskReference task;

            std::unique_lock lock(_mutex);
            _new_task_condition.wait(
                lock
                , [this, &task]() -> bool
                {
                    if (!_is_running) return true;

                    if (_tasks.empty()) return false;

                    task = _tasks.front();
                    _tasks.pop();
                    ++_n_active_threads;

                    return true;
                }
            );

            return task;
        }

        void _doTask(TaskReference task)
        try
        {
            (*task)();
        }
        catch (const NestedException &nested)
        {
            std::lock_guard lock(_mutex);
            _exceptions.push_back(std::make_shared<NestedException>(
                nested
            ));
        }
        catch (std::exception &e)
        {
            std::lock_guard lock(_mutex);
            _exceptions.push_back(std::make_shared<NestedException>(
                e.what()
            ));
        }

        std::string _unwindException(
            std::shared_ptr<NestedException> e_ptr
        )
        {
            int level = 0;
            auto message = std::string(e_ptr->what()) + '\n';
            while ((e_ptr = e_ptr->nested()))
            {
                level += 2;
                message += std::string(level, ' ') + e_ptr->what() + '\n';
            }
            return message;
        }
    };
}

#endif //simodo_stage_QueuedThreadPool
