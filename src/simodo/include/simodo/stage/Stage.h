#ifndef simodo_stage_Stage
#define simodo_stage_Stage

/*! \file Stage.h
    \brief Реализация сцены моделирования
*/

#include "simodo/stage/ActorGroup.h"
#include "simodo/stage/SleepIterator.h"
#include "simodo/stage/DiscreteTime.h"
#include "simodo/stage/PeriodicProcedure.h"
#include "simodo/stage/Predicate.h"

#include <unordered_map>
#include <cmath>
#include <optional>
#include <memory>
#include <chrono>
#include <thread>
#include <iostream>

namespace simodo::stage
{
    template <typename ActorId, typename Iter = int64_t, typename Time = double>
    class Stage
    {
    public:
        using ActorGroupType = ActorGroup<ActorId, Iter, Time>;
        using ActorGroupReference = typename ActorGroupType::Reference;
        using Actor = typename ActorGroupType::ActorType;
        using ActorReference = typename ActorGroupType::ActorReference;
        using DiscreteTimeType = DiscreteTime<Time>;

        enum class Mode
        {
            AsFastAsCan,
            RealTime
        };

        enum class CallbackPeriodMode
        {
            IterationCount,
            TimeRange
        };

        struct CallbackPeriod
        {
            CallbackPeriodMode mode;
            DiscreteTimeType data;
        };

    public:
        static constexpr Mode DEFAULT_MODE = Mode::AsFastAsCan;

        Stage(
            ActorGroupReference actors
            , Predicate is_running
        )   : _actors(actors)
            , _mode(DEFAULT_MODE)
            , _is_running(is_running)
            , _time()
            , _iteration()
        {}

        [[nodiscard]]
        Time getTime() const
        { return _time.now; }
        void setTime(Time time)
        { _time.now = time; }

        [[nodiscard]]
        Iter getIteration() const
        { return _iteration; }

        [[nodiscard]]
        Time getEndTime() const
        { return _time.end; }
        void setEndTime(Time end_time)
        { _time.end = end_time; }

        [[nodiscard]]
        Time getDeltaTime() const
        { return _time.now.getDelta(); }
        void setDeltaTime(Time delta_time)
        { _time.now.setDelta(delta_time); }

        void setCallback(Procedure callback, CallbackPeriod callback_period)
        { _callback = {callback, callback_period}; }
        void setEachIterationCallback(Procedure callback)
        { _each_iteration_callback = callback; }

        [[nodiscard]]
        Mode getMode() const
        { return _mode; }
        void setMode(Mode mode)
        { _mode = mode; }

        bool containsActor(ActorId actor_id)
        {
            return _actors->find(actor_id) != nullptr;
        }

        bool addActor(ActorId actor_id, ActorReference actor)
        {
            return _actors->add(actor_id, actor) != nullptr;
        }

        bool addActorTo(ActorId actor_id, ActorReference actor, ActorId parent_id)
        {
            auto parent = _actors->find(parent_id);

            if (!parent) return false;

            return parent->add(actor_id, actor) != nullptr;
        }

        void removeActor(ActorId actor_id)
        {
            _actors->remove(actor_id);
        }

        void clearActors()
        {
            _actors->clear();
        }

        bool setActorStep(ActorId actor_id, Time step)
        {
            auto actor = _actors->find(actor_id);

            if (!actor) return false;

            actor->setStep({step});

            return true;
        }

        bool setActorStepIt(ActorId actor_id, Iter step)
        {
            auto actor = _actors->find(actor_id);

            if (!actor) return false;

            actor->setStep({step});

            return true;
        }

        bool resetActorStep(ActorId actor_id)
        {
            auto actor = _actors->find(actor_id);

            if (!actor) return false;

            actor->setStep({});

            return true;
        }

        void start()
        {
            _iteration = 0;

            _SleepIterator sleep_iterator;
            _doZeroIteration(sleep_iterator);

            for (
                _iteration = 1
                ; (
                    _is_running()
                    && (
                        _time.end <= 0
                        || Time(_time.now) < Time(_time.end)
                    )
                )
                ; ++_iteration
            )
            {
                _SleepGuard sleep_guard(
                    sleep_iterator
                    , _stepDuration()
                    , _mode == Mode::RealTime
                );

                Time previous_time = _time.now;
                Time next_time = ++_time.now;
                _actors->doStep(_iteration, previous_time, next_time);

                _doCallback();
            }
        }

    private:
        using _StepDuration = std::chrono::microseconds;
        using _SleepIterator = SleepIterator<_StepDuration>;
        using _SleepGuard = SleepGuard<_StepDuration>;
        using _CallbackType = PeriodicProcedure<CallbackPeriod>;

    private:
        ActorGroupReference _actors;

        Mode _mode;
        Predicate _is_running;

        struct
        {
            DiscreteTime<Time> now;
            Time end;
        } _time;
        Iter _iteration;

        std::optional<_CallbackType> _callback;
        std::optional<Procedure> _each_iteration_callback;

    private:
        void _doCallback()
        {
            if (_each_iteration_callback)
            {
                (*_each_iteration_callback)();
            }

            if (!_callback) return;

            if (_callback->period.mode == CallbackPeriodMode::TimeRange)
            {
                auto &period_data = _callback->period.data;
                if (period_data.getDelta() == 0)
                {
                    (*_callback)();
                    return;
                }

                auto time_now = Time(_time.now);
                if (time_now < Time(period_data))
                {
                    period_data = time_now;
                    (*_callback)();
                    return;
                }

                auto next_callback_time = [&period_data]() -> Time
                {
                    return (
                        period_data
                        + typename DiscreteTimeType::NType(1)
                    );
                };
                if (next_callback_time() <= time_now)
                {
                    while (next_callback_time() <= time_now)
                    {
                        ++period_data;
                    }
                    (*_callback)();
                    return;
                }

                return;
            }

            auto period_n = _callback->period.data.getN();
            if (
                period_n == 0
                || _time.now.getN() % period_n == 0
            )
            {
                (*_callback)();
            }
        }

        void _doZeroIteration(_SleepIterator & sleep_iterator)
        {
            _SleepGuard sleep_guard(
                sleep_iterator
                , _stepDuration()
                , _mode == Mode::RealTime
            );
            _doCallback();
        }

        _StepDuration _stepDuration()
        {
            return _StepDuration(
                static_cast<_StepDuration::rep>(
                    std::round(
                        _time.now.getDelta()
                        * _StepDuration::period::den
                        / _StepDuration::period::num
                    )
                )
            );
        }
    };
}

#endif //simodo_stage_Stage
