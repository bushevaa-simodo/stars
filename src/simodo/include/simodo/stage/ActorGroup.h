#ifndef simodo_stage_ActorGroup
#define simodo_stage_ActorGroup

#include "simodo/stage/Actor.h"

#include <memory>

namespace simodo::stage
{
    template <typename ActorId, typename Iter, typename Time>
    class ActorGroup : public Actor<Iter, Time>
    {
    public:
        using ActorType = Actor<Iter, Time>;
        using ActorReference = std::shared_ptr<ActorType>;
        using Reference = std::shared_ptr<ActorGroup>;

    public:
        virtual ActorReference getMainActor() = 0;

        virtual Reference find(ActorId id) = 0;
        virtual Reference add(ActorId id, ActorReference actor) = 0;
        virtual Reference remove(ActorId id) = 0;
        virtual void clear() = 0;
    };
}

#endif //simodo_stage_ActorGroup
