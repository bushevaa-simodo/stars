/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_CONVERT_H
#define SIMODO_CONVERT_H

/*! \file convert.h
    \brief Функции преобразования UNICODE UTF8 и UTF16

    Заголовочный файл с функциями преобразования UNICODE UTF8 и UTF16.
*/

#include <string>

namespace simodo
{
    /*!
     * \brief Функция переводит строку UTF-8 в UTF-16
     * \param str     Строка UTF-8
     * \return        Строка UTF-16
     */
    std::u16string convertToU16(const std::string & str);

    /*!
     * \brief Функция переводит строку UTF-16 в UTF-8
     * \param str     Строка UTF-16
     * \return        Строка UTF-8
     */
    std::string convertToU8(const std::u16string & str);

    /*!
     * \brief Удаление лишних нулей в дробной части
     * \param s Строковое представление числа
     * \return Строковое представление числа с удаленными нулями в конце дробной части
     */
    std::string clearNumberFractionalPart(std::string s);

}

#endif // SIMODO_CONVERT_H
