SIMODO edit поддерживает работу с наборами иконок.

Структура каталога с иконками должна соответствовать стандарту GNU/Linux. 

Содержимое (наименования) иконок должны соответствовать правилам [freedesktop.org](https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html).

Существуют коллекции свободных иконок, например: [Open Icon Library](https://sourceforge.net/projects/openiconlibrary/). 

Некоторые свободные коллекции иконок можно посмотреть и установить, например, тут: [https://www.fossmint.com/best-icon-themes-for-linux/](https://www.fossmint.com/best-icon-themes-for-linux/).

Если в `common.json` в описании темы указан пустой каталог с иконками (""), то производится поиск иконок в следующих каталогах:

* /usr/share/icons
* /var/lib/snapd/desktop/icons
* :/icons

Если по этим путям иконок нет (что нормально для Windows и Mac), то иконки будут браться из ресурсов SIMODO edit по пути "`:/images`".

В данной версии SIMODO edit ресурсы по пути "`:/icons`" не реализованы (желательно сделать в будущем).

SIMODO edit имеет две предустановленных коллекции с иконками (обе взяты из Ubuntu 18.4):

* breeze
* breeze-dark


